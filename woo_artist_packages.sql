-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table woomen.woo_artist_packages
CREATE TABLE IF NOT EXISTS `woo_artist_packages` (
  `artist_package_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) NOT NULL,
  `package_title` varchar(255) NOT NULL,
  `package_description` longtext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_package_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  CONSTRAINT `FK_packages_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_artist_packages: ~1 rows (approximately)
DELETE FROM `woo_artist_packages`;
/*!40000 ALTER TABLE `woo_artist_packages` DISABLE KEYS */;
INSERT INTO `woo_artist_packages` (`artist_package_id`, `artist_profile_id`, `package_title`, `package_description`, `created_at`, `updated_at`) VALUES
	(1, 6, '', '<p>Test Package 2</p>', '2016-12-06 07:24:00', '2016-12-16 03:42:13');
/*!40000 ALTER TABLE `woo_artist_packages` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
