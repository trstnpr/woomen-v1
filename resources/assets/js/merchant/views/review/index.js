    $('#confirm-button-review').on('click', function() {
        $.ajax({
            url: '/merchant/reviews/delete/'+$(this).attr('data-review-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });