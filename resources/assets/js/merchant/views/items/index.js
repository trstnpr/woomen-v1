




//data table
//all items

Dropzone.autoDiscover = false;

$(document).ready(function() {
	
	// var all_items = $('#all-items').DataTable();
    var formData = new FormData();

    $('div#myAwesomeDropzone').dropzone({
        autoProcessQueue: false,
        uploadMultiple  : true,
        parallelUploads : 1,
        maxFiles        : 5,
        maxFilesize     : 500,
        addRemoveLinks  : false,
        autoDiscover    : false,
        url             : '/etits',
        init            : function() {

            var uploadDropzone = this;
            // on add file
            this.on('addedfile', function(file) {
                // Create the remove button
                var removeButton = Dropzone.createElement('<i class="fa fa-times" id="remove-file"></i>');

                // Capture the Dropzone instance as closure.
                var _this = this;

                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();

                    // Remove the file preview.
                    _this.removeFile(file);
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);
            });

            var submitButton = document.querySelector("button[type=submit]");
            
            submitButton.addEventListener("click", function() {
                var files =   uploadDropzone.files;
                for (var i = files.length - 1; i >= 0; i--) {
                    formData.append('image_path[]',files[i]); 
                }
            });
        }
    });

	$('#frm_add_item').on('submit', (function(e){
        e.preventDefault();
        formData.append('item_name', $(this).find('input[name="item_name"]').val());
        formData.append('item_price', $(this).find('input[name="item_price"]').val());
        formData.append('item_quantity', $(this).find('input[name="item_quantity"]').val());
        formData.append('item_description', $(this).find('textarea[name="item_description"]').val());
        $.ajax({
            url: "/merchant/item/store",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_create').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_d').empty();
                    $('#result_d').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#btn_create').html('Create Item');

                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    formData.reset();
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_create").html('Submit');
                    $('#result_d').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_d').html(msg.message);
                    $('#result_d').removeClass('hide');
                    setTimeout(function(){location.replace('/merchant/item');}, 3000);
                    formData.reset();
                } else{
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_d').html(msg.message);
                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    formData.reset();
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

	$('#frm_update_item').on('submit', (function(e){
        e.preventDefault();
        formData.append('item_name', $(this).find('input[name="item_name"]').val());
        formData.append('item_price', $(this).find('input[name="item_price"]').val());
        formData.append('item_quantity', $(this).find('input[name="item_quantity"]').val());
        formData.append('item_description', $(this).find('textarea[name="item_description"]').val());
        $.ajax({
            url: "/merchant/item/update/"+$('#item_id').val(),
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_edit').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_d').empty();
                    $('#result_d').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#btn_edit').html('Save Changes');

                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    formData.reset();
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_edit").html('Submit');
                    $('#result_d').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_d').html(msg.message);
                    $('#result_d').removeClass('hide');
                    setTimeout(function(){location.replace('/merchant/item');}, 3000);
                    formData.reset();
                } else{
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_d').html(msg.message);
                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    formData.reset();
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

    function reloadTable(table)
    {
        table.ajax.reload();
    }

    $('#confirm-button').on('click', function() {
        $.ajax({
            url: '/merchant/item/delete/'+$(this).attr('data-item-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('.delete_item_button').on('click', function() {
        $.ajax({
            url: '/merchant/item/delete/item_image/'+$(this).attr('data-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    location.reload();
                } else{
                    location.reload();
                }
            }
        });
    });
});
