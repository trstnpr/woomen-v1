$('#frm_profile_update').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/merchant/updateProfile",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_profile').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_d').empty();
                    $('#result_d').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#btn_profile').html('Submit');

                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_profile").html('Save Changes');
                    $('#result_d').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_d').html(msg.message);
                    $('#result_d').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_d').html(msg.message);
                    setTimeout(function(){location.reload();}, 3000);
                }
            }
        });
    }));

    $('#frm_password_update').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/merchant/passwordSettings/update",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_password').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#current_pass').val('');
                    $('#password_confirmation').val('');
                    $('#btn_password').html('Save Changes');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_password").html('Save Changes');
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html(msg.message);
                    $('#result_div').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html(msg.message);
                    setTimeout(function(){location.reload();}, 3000);
                }
            }
        });
    }));




