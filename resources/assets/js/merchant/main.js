/** START Preloade **/
$(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut('slow');
    $('body').delay(350).css({'overflow':'visible'});

    /*$('.autoplay').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000
    });*/
});
/** END Preloader **/

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

$(document).ready(function(){
	
	$("#tab-nav-1").click(function(){
		$("#tab-panel-1").show();
		$("#tab-panel-2").hide();
		$("#tab-panel-3").hide();
	});

	$("#tab-nav-2").click(function(){
		$("#tab-panel-1").hide();
		$("#tab-panel-2").show();
		$("#tab-panel-3").hide();
	});

	$("#tab-nav-3").click(function(){
		$("#tab-panel-1").hide();
		$("#tab-panel-2").hide();
		$("#tab-panel-3").show();
	});

});

/* START NumOnly Field */
$(".number").on("keypress blur", function(e){
    if ( e.type === "keypress" )
        return !!String.fromCharCode(e.which).match(/^\d$/);
    this.value = this.value.replace(/[^\d].+/, "");
});
/* END NumOnly Field */

/*upload profile image
*/
/*$(document).ready(function(){
    $("#click-browse-image").click(function(){
        $('#input-profile-image').trigger('click'); 
    });
});


function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#click-browse-image').css('background-image', 'url(' + e.target.result + ')');
            };

            reader.readAsDataURL(input.files[0]);
        }
 }       

$(document).ready(function(){
    $("#input-profile-image").change(function(){
        readURL(this);
    });
});
*/


/////////////////////////////////////////////////



$(document).ready(function(){
    $("#application-image").click(function(){
        $('#input-application-image').trigger('click'); 
    });
});


function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#application-image').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
 }       

$(document).ready(function(){
    $("#input-application-image").change(function(){
        readURL(this);
    });
});





    $('#form_upload_image').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/merchant/uploadImage",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#upload_image_btn').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_divs').empty();
                    $('#result_divs').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_divs').removeClass('alert-success hide').addClass('alert-danger');
                    $('#upload_image_btn').html('Save Changes');

                    setTimeout(function(){$('#result_divs').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#upload_image_btn").html('Save Changes');
                    $('#result_divs').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_divs').html(msg.message);
                    $('#result_divs').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_divs').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_divs').html(msg.message);
                    setTimeout(function(){location.reload();}, 3000);
                }
            }
        });
    }));

