$(document).ready(function(){
    $('#request-booking-form').on('submit', (function(e){
        e.preventDefault();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#request-booking-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#request-booking-button').html('Submit <i class="fa fa-paper-plane"></i>').attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#request-booking-button').html('Submit <i class="fa fa-paper-plane"></i>').attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                } else{
                    $('#request-booking-button').html('Submit <i class="fa fa-paper-plane"></i>').attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));

    $('#confirm-booking-form').on('submit', (function(e){
        e.preventDefault();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#confirm-booking-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#confirm-booking-button').html('Confirm').attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#confirm-booking-button').html('Confirm').attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');

                    setTimeout(function(){ window.location.href = "/artist/bookings"; }, 3000);
                } else{
                    $('#confirm-booking-button').html('Confirm').attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));

    $('#decline-booking-form').on('submit', (function(e){
        e.preventDefault();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#decline-booking-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#decline_result_div').empty();
                    $('#decline_result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#decline_result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#decline-booking-button').html('Decline Booking').attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    window.location.href = "/artist/bookings";
                } else{
                    alert(msg.message);
                    window.location.href = "/artist/bookings";
                }
            }
        });
    }));

    $('#cancel-booking-form').on('submit', (function(e){
        e.preventDefault();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#cancel-booking-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#cancel_result_div').empty();
                    $('#cancel_result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#cancel_result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#cancel-booking-button').html('Cancel Booking').attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    window.location.href = "/artist/bookings";
                } else{
                    alert(msg.message);
                    window.location.href = "/artist/bookings";
                }
            }
        });
    }));
});