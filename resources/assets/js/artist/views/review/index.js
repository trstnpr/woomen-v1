$(document).ready(function(){
    $('#review-form').on('submit', (function(e){
        e.preventDefault();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#review-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#review_result_div').empty();
                    $('#review_result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#review_result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#review-button').html('Submit Review').attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#review-form')[0].reset();
                    $('#review-button').html('Submit Review').attr('disabled', false);
                    $('#review_result_div').empty();
                    $('#review_result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#review_result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');

                    setTimeout(function(){$('#review_result_div').addClass('hide').html(''); }, 3000);
                } else{
                    $('#review-button').html('Submit Review').attr('disabled', false);
                    $('#review_result_div').empty();
                    $('#review_result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#review_result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));

    $('#confirm-button-review').on('click', function() {
        $.ajax({
            url: '/artist/review/delete/'+$(this).attr('data-review-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });
});