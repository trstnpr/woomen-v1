$(document).ready(function(){
    $('#event-form').on('submit', (function(e){
        e.preventDefault();
        var btn = $('#event-button').html();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#event-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#event-button').html(btn).attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#event-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                    
                    setTimeout(function(){ window.location.href = '/artist/event'; }, 2000);
                } else{
                    $('#event-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));
});