Dropzone.autoDiscover = false;

$(document).ready(function(){
    $('#portfolio-form').on('submit', (function(e){
        e.preventDefault();
        var btn = $('#portfolio-button').html();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#portfolio-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#portfolio-button').html(btn).attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#portfolio-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else{
                    $('#portfolio-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));

    $('#delete-video').on('click', function() {
        var btn = $('#delete-video').html();
        $.ajax({
            url: '/artist/portfolio/video/delete/'+$(this).attr('data-id'),
            headers:
            {
                'X-CSRF-Token': $(this).attr('data-token')
            },
            type: 'DELETE',
            beforeSend: function(){ $('#delete-video').html('Processing...').attr('disabled', true); },
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#delete-video').html('Deleted').attr('disabled', false);
                    location.reload();
                } else{
                    $('#delete-video').html(btn).attr('disabled', false);
                    location.reload();
                }
            }
        });
    });

    $('#album-form').on('submit', (function(e){
        e.preventDefault();
        var btn = $('#album-button').html();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#album-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#album-button').html(btn).attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#album-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else{
                    $('#album-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));

    //DROPZONE FUNCTION
    var formData = new FormData();

    $('div#myAwesomeDropzone').dropzone({
        autoProcessQueue: false,
        uploadMultiple  : true,
        parallelUploads : 1,
        maxFiles        : 5,
        maxFilesize     : 500,
        addRemoveLinks  : false,
        autoDiscover    : false,
        url             : '/url',
        init            : function() {

            var uploadDropzone = this;
            // on add file
            this.on('addedfile', function(file) {
                // Create the remove button
                var removeButton = Dropzone.createElement('<i class="fa fa-times" id="remove-file"></i>');

                // Capture the Dropzone instance as closure.
                var _this = this;

                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();

                    // Remove the file preview.
                    _this.removeFile(file);
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);
            });

            var submitButton = document.querySelector("button[type=submit]");
            
            submitButton.addEventListener("click", function() {
                var files =   uploadDropzone.files;
                formData.delete('image_path[]');
                for (var i = files.length - 1; i >= 0; i--) {
                    formData.append('image_path[]',files[i]); 
                }
            });
        }
    });

    $('#image-form').on('submit', (function(e){
        e.preventDefault();
        var btn = $('#image-button').html();
        var action = $(this).attr('action');
        formData.append('album_name', $(this).find('input[name="album_name"]').val());
        formData.append('album_description', $(this).find('textarea[name="album_description"]').val());
        $.ajax({
            url: action,
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#image-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#image-button').html(btn).attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#image-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else{
                    $('#image-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));

    $('#featured-form').on('submit', (function(e){
        e.preventDefault();
        var btn = $('#featured-button').html();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#featured-button').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#featured-button').html(btn).attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#featured-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else{
                    $('#featured-button').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));

    $('#frm_feat_img').on('submit', (function(e){
        e.preventDefault();
        var btn = $('#btn_feat').html();
        var action = "/artist/portfolio/featured-image/feat";
        $.ajax({
            url: action,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_feat').html('Processing...').attr('disabled', true); },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#btn_feat').html(btn).attr('disabled', false);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#btn_feat').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                    setTimeout(function(){ location.reload(); }, 2000);
                } else{
                    $('#btn_feat').html(btn).attr('disabled', false);
                    $('#result_div').empty();
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                }
            }
        });
    }));
});