// // START Multi Item Image uploade Previewing
// window.onload = function() {
//     //Check File API support
//     if(window.File && window.FileList && window.FileReader) {
//         $('#files').on("change", function(event) {
//             var files = event.target.files; //FileList object
//             var output = document.getElementById("item-container");
//             for(var i = 0; i< files.length; i++) {
//                 var file = files[i];
//                 //Only pics
//                 // if(!file.type.match('image'))
//                 if(file.type.match('image.*')){
//                     if(this.files[0].size < 2097152){    
//                   // continue;
//                     var picReader = new FileReader();
//                     picReader.addEventListener("load",function(event){
//                         var picFile = event.target;
//                         var div = document.createElement("div");
//                         div.innerHTML = "<div class='col-md-2 item'><figure class='item-prev' style='background-image:url(" + picFile.result + ");'></figure></div>";
//                         output.insertBefore(div,null);            
//                     });
//                     //Read the image
//                     $('#btn-clear, .file-wrap').show();
//                     picReader.readAsDataURL(file);
//                     } else {
//                         alert("Image Size is too big. Minimum size is 2MB.");
//                         $(this).val("");
//                     }
//                 } else {
//                     alert("You can only upload image file.");
//                     $(this).val("");
//                 }
//             }
//         });
//     } else {
//         console.log("Your browser does not support File API");
//     }
// }
// $('#files').on("click", function() {
//     $('.item').remove();
//     $('#btn-clear').hide();
//     $('.file-wrap').hide();
//     $(this).val("");
// });
// $('#btn-clear').on("click", function() {
//     $('.item').remove();
//     $('.file-wrap').hide();
//     $('#files').val("");
//     $(this).hide();
// });
// // END Multi Item Image uploade Previewing



//data table
//all items

Dropzone.autoDiscover = false;

$(document).ready(function() {
    
    var formData = new FormData();

    $('div#myAwesomeDropzone').dropzone({
        autoProcessQueue: false,
        uploadMultiple  : true,
        parallelUploads : 1,
        maxFiles        : 5,
        maxFilesize     : 500,
        addRemoveLinks  : false,
        autoDiscover    : false,
        url             : '/etits',
        init            : function() {

            var uploadDropzone = this;
            // on add file
            this.on('addedfile', function(file) {
                // Create the remove button
                var removeButton = Dropzone.createElement('<i class="fa fa-times" id="remove-file"></i>');

                // Capture the Dropzone instance as closure.
                var _this = this;

                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();

                    // Remove the file preview.
                    _this.removeFile(file);
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);
            });

            var submitButton = document.querySelector("button[type=submit]");
            
            submitButton.addEventListener("click", function() {
                var files =   uploadDropzone.files;
                for (var i = files.length - 1; i >= 0; i--) {
                    formData.append('image_path[]',files[i]); 
                }
            });
        }
    });


	$('#frm_add_item').on('submit', (function(e){
        e.preventDefault();
        formData.append('item_name', $(this).find('input[name="item_name"]').val());
        formData.append('item_price', $(this).find('input[name="item_price"]').val());
        formData.append('item_quantity', $(this).find('input[name="item_quantity"]').val());
        formData.append('item_description', $(this).find('textarea[name="item_description"]').val());
        $.ajax({
            url: "/artist/item/store",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_create').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_d').empty();
                    $('#result_d').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#btn_create').html('Create Item');

                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    formData.reset();
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_create").html('Submit');
                    $('#result_d').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_d').html(msg.message);
                    $('#result_d').removeClass('hide');
                    setTimeout(function(){location.replace('/artist/item');}, 3000);
                    formData.reset();
                } else{
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_d').html(msg.message);
                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    formData.reset();
                }
            }
        });
    }));

	$('#frm_update_item').on('submit', (function(e){
        e.preventDefault();
        formData.append('item_name', $(this).find('input[name="item_name"]').val());
        formData.append('item_price', $(this).find('input[name="item_price"]').val());
        formData.append('item_quantity', $(this).find('input[name="item_quantity"]').val());
        formData.append('item_description', $(this).find('textarea[name="item_description"]').val());
        $.ajax({
            url: "/artist/item/update/"+$('#item_id').val(),
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_edit').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_d').empty();
                    $('#result_d').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#btn_edit').html('Save Changes');

                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    formData.reset();
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_edit").html('Submit');
                    $('#result_d').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_d').html(msg.message);
                    $('#result_d').removeClass('hide');
                    setTimeout(function(){location.replace('/artist/item');}, 3000);
                    formData.reset();
                } else{
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_d').html(msg.message);
                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    formData.reset();
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

    function reloadTable(table)
    {
        table.ajax.reload();
    }

    $('#confirm-button').on('click', function() {
        $.ajax({
            url: '/artist/item/delete/'+$(this).attr('data-item-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('.delete_item_button').on('click', function() {
        $.ajax({
            url: '/artist/item/delete/item_image/'+$(this).attr('data-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    location.reload();
                } else{
                    location.reload();
                }
            }
        });
    });
});
