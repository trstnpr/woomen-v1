/** START Preloade **/
$(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut('slow');
    $('body').delay(350).css({'overflow':'visible'});

    $('.autoplay').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000
    });
});
/** END Preloader **/

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
$('.selectpicker').selectpicker({
	style: 'btn-lg btn-block',
	size: 4
});
/* START NumOnly Field */
$(".number").on("keypress blur", function(e){
    if ( e.type === "keypress" )
        return !!String.fromCharCode(e.which).match(/^\d$/);
    this.value = this.value.replace(/[^\d].+/, "");
});
/* END NumOnly Field */

/* START date & time picker */
$('.datePicker').datetimepicker({ format: 'MM/DD/YYYY' });
$('.timePicker').datetimepicker({ format: 'LT' });
/* END date & time picker */
