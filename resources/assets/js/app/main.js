/** START Preloader **/
$(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut('slow');
    $('body').delay(350).css({'overflow':'visible'});

    $('.autoplay').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000
    });
});
/** END Preloader **/

// /** START Mobile Defaults **/
// if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
//     $('.video-bg').hide();
//     $('.head-banner').css("background-image", "url('http://woo.devhub.ph/images/video-thumb1.jpg')");
//     // alert('Mobile');
// }
// /** END Mobile Defaults **/

/*Nav Bar*/
function checkScroll(){
    var startY = $('.navbar').height(); //The point where the navbar changes in px

    if($(window).scrollTop() > startY){
        $('.navbar').addClass("scrolled");
    }else{
        $('.navbar').removeClass("scrolled");
    }
}
if($('.navbar').length > 0){
    $(window).on("scroll load resize", function(){
        checkScroll();
    });
}
/*Change Color*/

/* START Data Image to BG */
$('.bg-data').each(function() {
    var attr = $(this).attr('data-image-src');

    if (typeof attr !== typeof undefined && attr !== false) {
        $(this).css('background-image', 'url('+attr+')');
    }
});
/* END Data Image to BG */

/** START Parallax **/
$('.parallax').parallax();
/** END Parallax **/

/* START Range Input */
$('input[type="range"]').on("change mousemove", function() {
	$(this).next().html($(this).val());
});
/* END Range Input */

/* START SelectPicker */
$('.selectpicker').selectpicker({
    style: 'btn-lg btn-block form-control input-lg select-dropdown',
    size: 4
});
/* END SelectPicker*/

/* START NumOnly Field */
$(".number").on("keypress blur", function(e){
    if ( e.type === "keypress" )
        return !!String.fromCharCode(e.which).match(/^\d$/);
    this.value = this.value.replace(/[^\d].+/, "");
});
/* END NumOnly Field */

/* START date & time picker */
$('.datePicker').datetimepicker({ format: 'MM/DD/YYYY' });
$('.timePicker').datetimepicker({ format: 'LT' });
/* END date & time picker */