//SIGNUP JQUERY FUNCTION
$(document).ready(function() {
    $('#merchantreg_form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/signup",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_merchant').html('Processing...');
                                    document.getElementById("btn_merchant").disabled = true;  },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#re-password').val('');
                    $('#btn_merchant').html('Submit');
                    document.getElementById("btn_merchant").disabled = false;

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_merchant").html('Submit');
                    $('#success_modal').modal('show');
                    $("#ok_btn").click(function(){
                        window.location.reload();
                    });  
                   
                } else{
                    $('#merchant_alert').empty();
                    $('#merchant_alert').html(msg.message);
                    $('#merchant_alert').removeClass('hide');
                    $("#password").val('');
                    $("#re-password").val('');
                    $("#btn_merchant").html('Submit');
                    document.getElementById("btn_merchant").disabled = false;
                }
            }
        });
    }));

    $('#artistreg_form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/signup",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_artist').html('Processing...');
                                    document.getElementById("btn_artist").disabled = true;  },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_divs').empty();
                    $('#result_divs').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_divs').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#re-password').val('');
                    $('#btn_artist').html('Submit');
                    document.getElementById("btn_artist").disabled = false;

                    setTimeout(function(){$('#result_divs').addClass('hide').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_artist").html('Submit');
                    $('#success_modal').modal('show');
                    $("#ok_btn").click(function(){
                        window.location.reload();
                    });  

                } else{
                    $('#artist_alert').empty();
                    $('#artist_alert').html(msg.message);
                    $('#artist_alert').removeClass('hide');
                    $("#password").val('');
                    $("#re-password").val('');
                    $("#btn_artist").html('Submit');
                    document.getElementById("btn_artist").disabled = false;
                }
            }
        });
    }));

    $('#login_form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/login",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_login').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_d').empty();
                    $('#result_d').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#btn_login').html('Submit');

                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_login").html('Submit');
                    $('#result_d').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_d').html('Successfully logged in');
                    $('#result_d').removeClass('hide');
                    setTimeout(function(){location.replace(msg.message);}, 2000);
                    
                } else{
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_d').html(msg.message);
                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    $("#email_login").val('');
                    $("#login_password").val('');
                    $("#btn_login").html('Submit');
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

    $('#reset_password_form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/password/reset",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#reset_password_button').html('Processing...').attr('disabled', true);},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#reset_result_div').empty();
                    $('#reset_result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#reset_result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#reset_password_button').html('Submit').attr('disabled', false);

                    setTimeout(function(){$('#reset_result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#reset_password_button").html('Submit').attr('disabled', false);
                    $('#reset_result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#reset_result_div').html(msg.message);
                    $('#reset_result_div').removeClass('hide');
                    setTimeout(function(){location.reload();}, 2000);
                    
                } else{
                    $('#reset_result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#reset_result_div').html(msg.message);
                    setTimeout(function(){$('#reset_result_div').addClass('hide').html(''); }, 4000);
                    $("#reset_password_button").html('Submit').attr('disabled', false);
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));

    $('#contact-frm').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/contact_submit",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_contact').val('Processing...');
                                    document.getElementById("btn_contact").disabled = true;  },
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#btn_contact').val('Submit');
                    document.getElementById("btn_contact").disabled = false;

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#result_div').empty();
                    $('#result_div').removeClass('hide');
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                    
                    setTimeout(function(){ location.reload(); }, 2000);
                }
            }
        });
    }));

    $('#inquire-form').on('submit', (function(e){
        console.log("Inquire");
        e.preventDefault();
        $.ajax({
            url: $(this).attr('data-url'),
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#inquire-button').html('Processing...').attr('disabled', true);},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#inquire_result_div').empty();
                    $('#inquire_result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+value+'</li>');
                    });
                    $('#inquire_result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#inquire-button').html('Submit');
                    document.getElementById("inquire-button").disabled = false;

                    setTimeout(function(){$('#inquire_result_div').addClass('hide').html(''); }, 4000);
                }
            },
            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $('#inquire_result_div').empty();
                    $('#inquire_result_div').removeClass('hide');
                    $('#inquire_result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
                    
                    setTimeout(function(){ location.reload(); }, 2000);
                }
            }
        });

        return false;
    }));
});