/** START Preloade **/
$(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut('slow');
    $('body').delay(350).css({'overflow':'visible'});

    $('.autoplay').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000
    });
});
/** END Preloader **/

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
$('.selectpicker').selectpicker({
    style: 'btn-lg btn-block',
    size: 4
});
$('.datePicker').datetimepicker({ format: 'MM/DD/YYYY' });
$('.timePicker').datetimepicker({ format: 'LT' });
/* START NumOnly Field */
$(".number").on("keypress blur", function(e){
    if ( e.type === "keypress" )
        return !!String.fromCharCode(e.which).match(/^\d$/);
    this.value = this.value.replace(/[^\d].+/, "");
});
/* END NumOnly Field */

// START Multi Item Image uploade Previewing
window.onload = function() {
    //Check File API support
    if(window.File && window.FileList && window.FileReader) {
        $('#files').on("change", function(event) {
            var files = event.target.files; //FileList object
            var output = document.getElementById("item-container");
            for(var i = 0; i< files.length; i++) {
                var file = files[i];
                //Only pics
                // if(!file.type.match('image'))
                if(file.type.match('image.*')){
                    if(this.files[0].size < 2097152){    
                  // continue;
                    var picReader = new FileReader();
                    picReader.addEventListener("load",function(event){
                        var picFile = event.target;
                        var div = document.createElement("div");
                        div.innerHTML = "<div class='col-md-2 item'><figure class='item-prev' style='background-image:url(" + picFile.result + ");'></figure></div>";
                        output.insertBefore(div,null);            
                    });
                    //Read the image
                    $('#btn-clear, .file-wrap').show();
                    picReader.readAsDataURL(file);
                    } else {
                        alert("Image Size is too big. Minimum size is 2MB.");
                        $(this).val("");
                    }
                } else {
                    alert("You can only upload image file.");
                    $(this).val("");
                }
            }
        });
    } else {
        console.log("Your browser does not support File API");
    }
}
$('#files').on("click", function() {
    $('.item').remove();
    $('.file-wrap').hide();
    $(this).val("");
    $('#btn-clear').hide();
});
$('#btn-clear').on("click", function() {
    $('.item').remove();
    $('.file-wrap').hide();
    $('#files').val("");
    $(this).hide();
});
// END Multi Item Image uploade Previewing