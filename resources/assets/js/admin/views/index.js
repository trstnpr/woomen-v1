//SIGNUP JQUERY FUNCTION
$(document).ready(function() {
    $('#frm_admin_login').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/authenticate",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_admin').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_d').empty();
                    $('#result_d').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#password').val('');
                    $('#btn_admin').html('Submit');

                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn_admin").html('Submit');
                    $('#result_d').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_d').html('Successfully logged in');
                    $('#result_d').removeClass('hide');
                    setTimeout(function(){location.replace(msg.message);}, 3000);
                    
                } else{
                    $('#result_d').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_d').html(msg.message);
                    setTimeout(function(){$('#result_d').addClass('hide').html(''); }, 4000);
                    $("#email_login").val('');
                    $("#login_password").val('');
                    $("#btn_admin").html('Submit');
                    /*$("#result_div").removeClass('teal hide').addClass('red').html(msg.dialog);*/
                }
            }
        });
    }));
});