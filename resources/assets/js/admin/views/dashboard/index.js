//SIGNUP JQUERY FUNCTION
var id = $('#id').val();

$(document).ready(function() {
    $('#profile-form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/artist/update/"+id,
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#profile-button').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#profile-button').html('Save Changes');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#profile-button").html('Save Changes');
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html(msg.message);
                    $('#result_div').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html(msg.message);
                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                    $("#profile-button").html('Save Changes');
                }
            }
        });
    }));

    var m_id = $('#merchant_id').val();

    $('#merchant-form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/merchant/update/"+m_id,
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#merchant-button').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#merchant-button').html('Save Changes');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#merchant-button").html('Save Changes');
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html(msg.message);
                    $('#result_div').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html(msg.message);
                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                    $("#merchant-button").html('Save Changes');
                }
            }
        });
    }));

    var e_id = $('#event_id').val();

    $('#event-form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/event/update/"+e_id,
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#event-button').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#event-button').html('Save Changes');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#event-button").html('Save Changes');
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html(msg.message);
                    $('#result_div').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html(msg.message);
                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                    $("#event-button").html('Save Changes');
                }
            }
        });
    }));

    var i_id = $('#item_id').val();

    $('#item-form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/item/update/"+i_id,
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn-item').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#btn-item').html('Save Changes');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn-item").html('Save Changes');
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html(msg.message);
                    $('#result_div').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html(msg.message);
                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                    $("#btn-item").html('Save Changes');
                }
            }
        });
    }));

    $('#upgrade-artist-form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/artist/upgrade/"+$(this).attr('data-artist-id'),
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#upgrade-artist-button').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    $('#upgrade-artist-button').html('Upgrade');
                    alert(msg.message);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#upgrade-artist-button").html('Upgrade');
                    alert(msg.message);
                    setTimeout(function(){location.reload();}, 1000);
                    
                } else{
                    $("#upgrade-artist-button").html('Upgrade');
                    alert(msg.message);
                }
            }
        });
    }));

    $('.activate-artist').on('click', function() {
        $.ajax({
            url: '/admin/artist/activate/'+$(this).attr('data-artist-id'),
            data: {status: $(this).attr('data-status')},
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'POST',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#confirm-button-artist').on('click', function() {
        $.ajax({
            url: '/admin/artist/delete/'+$(this).attr('data-artist-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#confirm-button-merchant').on('click', function() {
        $.ajax({
            url: '/admin/merchant/delete/'+$(this).attr('data-merchant-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#confirm-button-event').on('click', function() {
        $.ajax({
            url: '/admin/event/delete/'+$(this).attr('data-event-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#confirm-button-item').on('click', function() {
        $.ajax({
            url: '/admin/item/delete/'+$(this).attr('data-item-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#confirm-button-review').on('click', function() {
        $.ajax({
            url: '/admin/review/delete/'+$(this).attr('data-review-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#confirm-button-album').on('click', function() {
        $.ajax({
            url: '/admin/album/delete/'+$(this).attr('data-album-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#confirm-button-video').on('click', function() {
        $.ajax({
            url: '/admin/video/delete/'+$(this).attr('data-video-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#frm_update').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/settings/updates/create",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#btn_update').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#btn_update').html('Save Changes');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#btn-item").html('Save Changes');
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html(msg.message);
                    $('#result_div').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html(msg.message);
                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                    $("#btn_update").html('Save Changes');
                }
            }
        });
    }));

    var upId = $('#update_id').val();

    $('#update-update-frm').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/settings/updates/edit/"+upId,
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#update-btn').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#update-btn').html('Save Changes');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#update-btn").html('Save Changes');
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html(msg.message);
                    $('#result_div').removeClass('hide');
                    setTimeout(function(){location.replace('/admin/settings/updates');}, 3000);
                    
                } else{
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html(msg.message);
                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
                    $("#update-btn").html('Save Changes');
                }
            }
        });
    }));

    $('#confirm-button-update').on('click', function() {
        $.ajax({
            url: '/admin/settings/updates/delete/'+$(this).attr('data-update-id'),
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            type: 'DELETE',
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    alert(msg.message);
                    location.reload();
                } else{
                    alert(msg.message);
                    location.reload();
                }
            }
        });
    });

    $('#packagelist-form').on('submit', (function(e){
        e.preventDefault();
        $.ajax({
            url: "/admin/artists/package",
            type: "POST",
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){ $('#packagelist-button').html('Processing...');},
            error: function(data){
                if(data.readyState == 4){
                    errors = JSON.parse(data.responseText);
                    $('#result_div').empty();
                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
                    $.each(errors,function(key,value){
                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
                    });
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#packagelist-button').html('Save Changes');

                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 2000);
                }
            },

            success: function(data){
                var msg = JSON.parse(data);
                if(msg.result == 'success'){
                    $("#packagelist-button").html('Save Changes');
                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#result_div').html(msg.message);
                    $('#result_div').removeClass('hide');
                    setTimeout(function(){location.reload();}, 3000);
                    
                } else{
                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#result_div').html(msg.message);
                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 2000);
                    $("#packagelist-button").html('Save Changes');
                }
            }
        });
    }));

});





