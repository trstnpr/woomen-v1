<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1, width=device-width">
		@yield('title')
		<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/favicon/apple-icon-57x57.png')}}">
		<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/favicon/apple-icon-60x60.png')}}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicon/apple-icon-72x72.png')}}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/favicon/apple-icon-76x76.png')}}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicon/apple-icon-114x114.png')}}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicon/apple-icon-120x120.png')}}">
		<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/favicon/apple-icon-144x144.png')}}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicon/apple-icon-152x152.png')}}">
		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-icon-180x180.png')}}">
		<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/favicon/android-icon-192x192.png')}}">
		<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png')}}">
		<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon/favicon-96x96.png')}}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png')}}">
		<link rel="manifest" href="{{ asset('images/favicon/manifest.json')}}">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content"{{ asset('images/favicon/ms-icon-144x144.png')}}">
		<meta name="theme-color" content="#ffffff">
		@yield('meta')
		<link href="{{ elixir('assets/artist/artist.css') }}" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>
		<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" />	
		<link href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" rel="stylesheet" />	
		<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet" />	
		@yield('stylesheet')
	</head>
	<body>

		<div id="preloader">
			<div id="status">&nbsp;</div>
		</div>

		<div class="page-wrap">
			
			<header>
				<nav class="navbar navbar-custom navbar-fixed-top woo-app-nav">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="#menu-toggle" id="menu-toggle">
								<i class="fa fa-arrow-right" id="toggle-btn" style="color: white;"> <img alt="Brand" src="{{ asset('images/brand/logo%20white.png') }}" /></i>
							</a>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="hidden-lg hidden-md">Home</span></a></li>
								{{-- <li><a href="#"><i class="fa fa-envelope"></i> <span class="hidden-lg hidden-md">Message</span></a></li> --}}
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="hidden-lg hidden-md">Others</span></a>
									<ul class="dropdown-menu">
										<li><a href="{{route('app.about')}}">About Woomen</a></li>
										<li><a href="{{route('app.faq')}}">FAQs</a></li>
										<li><a href="{{route('app.contact')}}">Contact Us</a></li>
										<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> <span class="hidden-lg hidden-md">Account</span></a>
									<ul class="dropdown-menu">
										<li><a href="{{ route('artist.dashboard.index') }}">Dashboard</a></li>
										<li><a href="{{ route('artist.profile.index') }}">Edit Profile</a></li>
										<li><a href="{{ route('artist.password.index') }}">Change Password</a></li>
										<li><a href="{{ route('app.logout') }}">Logout</a></li>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</header>

			<div id="wrapper">
		        <div id="sidebar-wrapper">
		            <ul class="sidebar-nav">
		                <li><a href="{{ route('artist.dashboard.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		                <li><a href="{{ route('artist.portfolio.index') }}"><i class="fa fa-folder"></i> My Portfolio</a></li>
		                <li><a href="{{ route('artist.event.index') }}"><i class="fa fa-calendar"></i> My Events</a></li>
		                <li><a href="{{ route('artist.packagelist.index') }}"><i class="fa fa-list"></i> My Packages</a></li>
		                <li><a href="{{ route('artist.item.index') }}"><i class="fa fa-shopping-bag"></i> My Items</a></li>
		                <li><a href="{{ route('artist.booking.index') }}"><i class="fa fa-calendar"></i> My Bookings</a></li>
		                <li><a href="{{ url('artist/review') }}"><i class="fa fa-comments"></i> My Reviews</a></li>
		                <li>
		                	<a href="#" data-target="#setting-dropdown" data-toggle="collapse"><i class="fa fa-cog"></i> Account Settings<i class="caret"></i></a>
		                </li>
		                <div id="setting-dropdown" class="collapse in">
			                <li><a href="{{ route('artist.profile.index') }}"><i class="fa fa-user"></i> Profile Settings</a></li>
			                <li><a href="{{ route('artist.password.index') }}"><i class="fa fa-key"></i> Password Settings</a></li>
			            </div>
			            <li><a href="{{ route('app.logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
		            </ul>
		        </div>

		        <div id="page-content-wrapper">

		            @yield('content')

		            {{-- @include('App::partials.section-6') --}}

		        </div>
		       
		    </div>

		</div>
		<script src="{{ config('s3.bucket_link') . elixir('assets/artist/artist.js') }}"></script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>	
		<script type="text/javascript">
			$(document).ready(function() {
				tinymce.init({
					selector:'.wysiwyg',
					plugins: "paste",
    				paste_as_text: true,
    				menubar: false,
    				toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify"
				});
			});
		</script>

		<script type="text/javascript">
			$(document).ready(function(){
				if($('#wrapper').hasClass('toggled')){
					$('#toggle-btn').removeClass('fa-arrow-right');
					$('#toggle-btn').addClass('fa-arrow-left');
				} else {
					$('#toggle-btn').removeClass('fa-arrow-left');
					$('#toggle-btn').addClass('fa-arrow-right');
				}
				
				$('#menu-toggle').on('click', function(){
					if($('#wrapper').hasClass('toggled')){
						$('#toggle-btn').removeClass('fa-arrow-right');
						$('#toggle-btn').addClass('fa-arrow-left');
					} else {
						$('#toggle-btn').removeClass('fa-arrow-left');
						$('#toggle-btn').addClass('fa-arrow-right');
					}
				});
			});
		</script>

		@yield('custom-scripts')

		@yield('footer')
	</body>
</html>
