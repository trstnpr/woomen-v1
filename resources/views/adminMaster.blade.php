<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1, width=device-width">
		@yield('title')
		<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/favicon/apple-icon-57x57.png')}}">
		<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/favicon/apple-icon-60x60.png')}}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicon/apple-icon-72x72.png')}}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/favicon/apple-icon-76x76.png')}}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicon/apple-icon-114x114.png')}}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicon/apple-icon-120x120.png')}}">
		<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/favicon/apple-icon-144x144.png')}}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicon/apple-icon-152x152.png')}}">
		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-icon-180x180.png')}}">
		<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/favicon/android-icon-192x192.png')}}">
		<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png')}}">
		<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon/favicon-96x96.png')}}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png')}}">
		<link rel="manifest" href="{{ asset('images/favicon/manifest.json')}}">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content"{{ asset('images/favicon/ms-icon-144x144.png')}}">
		<meta name="theme-color" content="#ffffff">
		@yield('meta')
		<link href="{{ elixir('assets/admin/admin.css') }}" rel="stylesheet" />
		@yield('stylesheet')
		<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" />	
		<link href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" rel="stylesheet" />	
		<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet" />		
	</head>
	<body>

		<div id="preloader">
			<div id="status">&nbsp;</div>
		</div>

		<div class="page-wrap">
			<header>
				<nav class="navbar navbar-custom navbar-fixed-top woo-admin-nav">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="#menu-toggle" id="menu-toggle">
								<img alt="Brand" src="{{ asset('images/brand/logo%20white.png') }}" />
							</a>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="hidden-lg hidden-md">Home</span></a></li>
								{{-- <li><a href="#"><i class="fa fa-envelope"></i> <span class="hidden-lg hidden-md">Message</span></a></li> --}}
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="hidden-lg hidden-md">Others</span></a>
									<ul class="dropdown-menu">
										<li><a href="{{route('app.about')}}">About Woomen</a></li>
										<li><a href="{{route('app.faq')}}">FAQs</a></li>
										<li><a href="{{route('app.contact')}}">Contact Us</a></li>
										<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> <span class="hidden-lg hidden-md">Account</span></a>
									<ul class="dropdown-menu">
										<li><a href="/admin/dashboard">Dashboard</a></li>
										<!-- <li><a href="#">Edit Profile</a></li> -->
										<!-- <li><a href="#">Change Password</a></li> -->
										<li><a href="{{ route('app.logout') }}">Logout</a></li>
										</li>
									</ul>
								</li>
							</ul>
							{{-- <form class="navbar-form navbar-right">
								<div class="form-group">
									<div class="input-group">
										<input class="form-control" name="q" id="search" placeholder="Search" value="" type="text">
										<span class="input-group-btn">
											<button type="submit" class="btn btn-round">
												<i class="fa fa-search"></i>
											</button>
										</span>
									</div>
								</div>
							</form> --}}
						</div>
					</div>
				</nav>
			</header>

			<div id="wrapper">
		        <div id="sidebar-wrapper">
		            <ul class="sidebar-nav">
		                <li class="sidebar-brand">
		                	<center><h4>Welcome</h4></center>
		                	<center>
		                    <a href="#">
		                        {{ Auth::user()->email }}
		                    </a>
		                    </center>
		                </li>
		                <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		                <li>
		                	<a href="#artist-dropdown" data-toggle="collapse"><i class="fa fa-user"></i> Artist Management <i class="caret"></i></a>
		                </li>
		                <div id="artist-dropdown" class="collapse in">
		                	<li><a href="{{ url('admin/artists') }}"><i class="fa fa-users"></i> Artists</a></li>
		                	<li><a href="{{ url('admin/artist/events') }}"><i class="fa fa-calendar"></i> Events</a></li>
		                	<li><a href="{{ url('admin/artist/reviews') }}"><i class="fa fa-comments"></i> Reviews</a></li>  
			            </div>
		                
		                <li>
		                	<a href="#merchant-dropdown" data-toggle="collapse"><i class="fa fa-user"></i> Merchant Management <i class="caret"></i></a>
		                </li>
		                <div id="merchant-dropdown" class="collapse in">
		                	<li><a href="{{ url('admin/merchants') }}"><i class="fa fa-users"></i> Merchants</a></li>
		                	<li><a href="{{ url('admin/merchant/items') }}"><i class="fa fa-shopping-bag"></i> Items</a></li>
		                	<li><a href="{{ url('admin/merchant/feedbacks') }}"><i class="fa fa-comments"></i> Feedbacks</a></li>  
			            </div>
		                <li>
		                	<a href="#setting-dropdown" data-toggle="collapse"><i class="fa fa-cog"></i> Settings <i class="caret"></i></a>
		                </li>
		                <div id="setting-dropdown" class="collapse in">
		                	<li><a href="{{ url('admin/settings/updates') }}"><i class="fa fa-user"></i> Admin Updates</a></li>
			                {{-- <li><a href="#"><i class="fa fa-user"></i> Admin User</a></li> --}}
			                {{-- <li><a href="#"><i class="fa fa-user"></i> Profile Settings</a></li> --}}
			                <li><a href="#view-review"  data-toggle="modal"><i class="fa fa-key"></i> Password Settings</a></li>
			            </div>
			            <li><a href="{{ route('app.logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
		            </ul>
		        </div>

		        <div id="page-content-wrapper">

		            @yield('content')

		            {{-- @include('App::partials.section-6') --}}

		        </div>
		    </div>

		    <!--change password modal-->

			<!--view review modal-->
			<div class="modal fade woo-modal" id="view-review" tabindex="-1" role="dialog">
				<div class="modal-dialog review-modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header review-modal-header">
				    		<h3 class="text-center">Password Settings</h3>
				    	</div>
				    	<form id="change_password">
				    		<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
				    		<div class="col-md-12">
								<div class="alert alert-success hide" id="result_div">
						  			<ul class="fa-ul">
						  				<li><i class="fa fa-li fa-check"></i> Success!</li>
						  			</ul>
								</div>
							</div>
							<div class="modal-body review-modal-body">
								<div class="form-group">
									<label>Current Password</label>
									<input type="password" class="form-control" name="current_pass" id="current_pass" placeholder="********">
								</div>
								<div class="form-group">
									<label>New Password</label>
									<input type="password" class="form-control" name="password" id="password" placeholder="********">
								</div>
								<div class="form-group">
									<label>Confirm New Password</label>
									<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="********">
								</div>
<!-- 								<div class="form-group">
									<button><i class='fa fa-lock'></i><span>Show password</span></button>
								</div> -->
							</div>
							<div class="modal-footer">
								<input type="submit" class="btn woo-btn-submit review-submit-btn" value="Save" id="btn_password">			
								<button type="button" class="btn woo-btn-submit" data-dismiss="modal">Cancel</button>				
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
		<script src="{{ config('s3.bucket_link') . elixir('assets/admin/admin.js') }}"></script>
		<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
		
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				tinymce.init({
					selector:'.wysiwyg',
					plugins: "paste",
    				paste_as_text: true,
    				menubar: false,
    				toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify"
				});
			});

			$('#change_password').on('submit', (function(e){
		        e.preventDefault();
		        $.ajax({
		            url: "/admin/changePassword",
		            type: "POST",
		            headers:
		            {
		                'X-CSRF-Token': $('input[name="_token"]').val()
		            },
		            data: new FormData(this),
		            contentType: false,
		            cache: false,
		            processData: false,
		            beforeSend: function(){ $('#btn_password').html('Processing...');},
		            error: function(data){
		                if(data.readyState == 4){
		                    errors = JSON.parse(data.responseText);
		                    $('#result_div').empty();
		                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
		                    $.each(errors,function(key,value){
		                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
		                    });
		                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
		                    $('#password').val('');
		                    $('#password_confirmation').val('');
		                    $('#current_pass').val('');
		                    $('#btn_password').html('Save Changes');

		                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
		                }
		            },

		            success: function(data){
		                var msg = JSON.parse(data);
		                if(msg.result == 'success'){
		                    $("#btn_password").html('Save Changes');
		                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
		                    $('#result_div').html(msg.message);
		                    $('#result_div').removeClass('hide');
		                    setTimeout(function(){location.reload();}, 3000);
		                    
		                } else{
		                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
		                    $('#result_div').html(msg.message);
		                    $('#password').val('');
		                    $('#password_confirmation').val('');
		                    $('#current_pass').val('');
		                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
		                    $("#btn_password").html('Save Changes');
		                }
		            }
		        });
		    }));


		</script>
		
		@yield('custom-scripts')

		@yield('footer')
	</body>
</html>
