    <div style="padding: 30px; width:100%; color:#000; font-family: Arial, Helvetica, sans-serif; width:100%;">
        <div style="background: #fbf2e9; width:600px; margin: 0 auto; border-top: 5px solid #e9a816; border-bottom: 5px solid #e9a816; padding: 20px;">
            <center>
                <div style="padding: 1em 0; ">
                    <img alt="Woomen" src="{{ env('APP_URL') }}/images/brand/logo%20gold%20earth.png" style="margin-left: 1em; width: 310px;"/>
                </div>
            </center>

            <div style="margin: 2em 0em 1em 0em; color: #555; text-align: center;">
              <p style="font-size: 1.6em; line-height: 1.5em; letter-spacing: 0.1em;">{{ $data['first_name'].' '.$data['last_name'] }} Booked you for his/her event</p>
            </div>

            <div style='line-height: 90%;'>
                <br>
                     <ul style="list-style-type: none; border-radius: 2px; overflow: hidden; position: relative; font-style: italic; color: #666;">
                            <li style="padding: 20px;">
                                <span>Date:&nbsp;</span><span>{{ date_format(date_create($data['event_date']), 'F d, Y') }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>Time:&nbsp;</span><span>{{ date_format(date_create($data['event_time']), 'h:i A') }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>Event Title:&nbsp;</span><span>{{ $data['event_title'] }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>Preparation Venue:&nbsp;</span><span>{{ $data['venue'] }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>No of Heads:&nbsp;</span><span>{{ $data['no_of_heads'] }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>Call Time:&nbsp;</span><span>{{ date_format(date_create($data['call_time']), 'h:i A') }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>Name:&nbsp;</span><span>{{ $data['first_name'].' '.$data['last_name'] }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>Mobile:&nbsp;</span><span>{{ $data['contact_no'] }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>Email&nbsp;</span><span>{{ $data['booker_email'] }}</span>
                            </li>
                            <li style="padding: 20px;">
                                <span>Message:&nbsp;</span><span>{{ $data['remarks'] }}</span>
                            </li>
                      </ul>
                <center>
                    <br/>
                    <strong style="color: #F00;">This is an automatically generated email , please do not reply</strong>
                    <br/>
                    <br>
                    <strong style="color: #F00; line-height: 100%"> You have received a request/reply from a client. Please send your reply by clicking on client’s email</strong>
                    <br><br>
                    <small style="color: #e9a816;">
                        Copyright &copy; 2016 Woomen
                    </small>
                    
                </center>
            </div>
            <br/>
        </div>
    </div>

