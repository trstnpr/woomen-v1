<!DOCTYPE html>
<body>
	<div style="padding: 30px; width: 100%;">
		<div style="color:#000; font-family: Arial, Helvetica, sans-serif;">
			<div style="width:600px; margin: 0 auto; transition: box-shadow .25s; padding: 20px; margin: auto; border-radius: 2px; border: 2px solid rgba(227, 158, 99, 0.4);">
				<center>
					<div style="padding: 1em 0; background-color: #e9a816;">
						<img style="margin-left: 1em; width: 300px;" alt="Woomen" src="{{ env('APP_URL') }}/images/brand/logo%20white.png" />
					</div>
					<h2 style="letter-spacing: 2px; font-weight: bold;"> Welcome to Woomen</h2>
					<div>
						<p style="font-size: 120%; letter-spacing: 1px;">
							In order to start using the service<br/>
							you need to activate your account. <br/>
							Please click the link below.
						</p>
						<br>
						<div style="padding-bottom: 20px;">
							<a href="{{ $data['link'] }}">
								<button style="text-decoration: none; color: #fff; text-align: center; letter-spacing: .5px; transition: .2s ease-out; cursor: pointer; border: none; border-radius: 20px; display: inline-block; height: 36px; line-height: 36px; outline: 0; padding: 0 3rem; text-transform: uppercase; vertical-align: middle; -webkit-tap-highlight-color: transparent; background-color: #e9a816;"><strong>ACTIVATE ACCOUNT</strong></button>
							</a>
						</div>
						<small>
							Click to activate account or Copy and paste the provided link to your browser <br/>
							{{ $data['link'] }}
						</small><br/><br/>
						<small>
							Copyright &copy; 2016 Woomen
						</small>
					</div>
				</center>
			</div>
		</div>
	</div>
</body>

