
<div style="color:#000; font-family: Arial, Helvetica, sans-serif; width:100%; padding: 30px;">
    <div style="width:600px; margin: 0 auto; border: 2px solid #e9a816; padding: 20px;">
        <center>
            <div style="padding: 1em 0; background: #e9a816;">
                <img alt="Woomen" src="{{ env('APP_URL') }}/images/brand/logo%20white.png" style="margin-left: 1em; width: 300px;"/>
            </div>
            <h3 style='letter-spacing: 1px; color:#000;'> Password Reset</h3>
            <div style='margin: 1em; line-height: 150%;'>
                <p style='font-size: 15px; letter-spacing: 1px; color:#000;'>
                    We received your request!<br/>
                    We generated a temporary password to your account.<br/>
                    Please log in to <span style="color: #e9a816; font-weight:bold; font-style: italic;">Woomen</span> with the password below.
                </p>
                <br>
                    <div style="padding-bottom: 20px;">
                        <span style='-webkit-border-radius: 3px;-moz-border-radius: 3px; border-radius: 3px; font-family: Arial; color: #ffffff;font-size: 20px; background: #e9a816; padding: 15px; text-decoration: none; width:80%; display:block; margin: 0 auto; text-shadow: 1px 0px 2px #000; letter-spacing: 1px;'>
                            <strong>{{ $data['temp_password'] }}</strong>
                        </span>
                    </div>
                    <p style='font-size: 15px; letter-spacing: 1px; color:#000;'>
                    Please change the temporary password once you logged in to our website.<br>
                    Click <a href="{{ env('APP_URL') }}" style="text-decoration: none; color: #e9a816;";> here </a>to login to your account.<br/>
                    </p>
                <small>
                    Copyright &copy; 2016 Woomen
                </small>
            </div>
        </center>
    </div>
</div>

