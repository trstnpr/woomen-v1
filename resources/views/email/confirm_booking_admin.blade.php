<div style="color:#000 !important; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; width:100%; background: #eee;">
  <div style="width:600px; margin: 0 auto; background: #e9a816;padding: 3em; font-size: 15px; color: #fff;">
	<div>
      <center><img src="{{ env('APP_URL') }}/images/brand/logo%20black.png" width="50%"></center>
    </div>
    <div style="margin: 2em 0em 1em 0em; color: #fff; text-align: center;">
      <p style="font-size: 1.6em; line-height: 1.5em; letter-spacing: 0.1em;">{{ $data['first_name'].' '.$data['last_name'] }} just CONFIRMED a booking deal</p>
    </div>
    
    <div style="background-color: #fff; color: #333; padding: 2em;">
      <div style="font-family: Arial, Helvetica Neue light; font-size: 15px; line-height: 1.5em; letter-spacing: 0.05em; padding: 0em 2em;">
      	<p>Here is the event details.</p>
      </div>
      <div style="padding: 0em 2em; color: #fff; line-height: 1.5em; letter-spacing: 0.05em;">
        <div style="background: #2e2e2e; border-radius: 3px; padding: 1em;">
          <p>
            <strong>Event</strong><br>
            {{ $data['event'] }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
          	<strong>Date</strong><br>
            {{ date_format(date_create($data['date']), 'F d, Y') }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
          	<strong>Time</strong><br>
            {{ date_format(date_create($data['time']), 'h:i A') }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
          	<strong>No of Heads</strong><br>
            {{ $data['no_of_heads'] }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
          	<strong>Venue</strong><br>
            {{ $data['venue'] }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
          	<strong>Price</strong><br>
            {{ 'Php '.number_format($data['price'], 2) }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
            <strong>Notes</strong><br>
            {{ $data['notes'] }}
          </p>
        </div>
      </div>
      <br>
      
      <hr>
      
      <div style="background: #fff; color: #333; font-family: Arial, Helvetica Neue light; font-size: 15px; line-height: 1.5em; letter-spacing: 0.05em;">
        
        <br>
        <p>Cheers,<br><strong>Woomen Team.</strong></p>
        
      </div>
    </div>
  </div>
</div>