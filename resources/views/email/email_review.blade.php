    <div style="padding: 30px; width: 100%; color:#000; font-family: Arial, Helvetica, sans-serif; width:100%;">
        <div style="width:600px; margin: 0 auto; border: 2px solid #e9a816;">
            <center>
                <div style="background: #e9a816; padding: 20px;">
                    <img alt="Woomen" src="{{ env('APP_URL') }}/images/brand/logo%20white.png" style="margin-left: 1em; width: 310px;" />
                </div>
            </center>

            <div style='padding: 20px;'>
                <br>
                    <center>
                        <h2> Thank you for booking this artist for your event. </h2>
                        <h3> For review or commendation of the artist, you may click the link below: </h3>
                        <a href="{{ env('APP_URL') }}/artist/review/{{ $data['artist_profile_id'] }}"><button style="padding: 14px 20px;border: 0 none;font-weight: 700;letter-spacing: 1px;text-transform: uppercase;background-color: #e9a816;color: white;">CLICK THIS LINK</button></a>
                        <h3>Or copy this link :</h3>
                        <h4 style="padding: 15px 15px;border: 0 none;letter-spacing: 1px;background-color: #e9a816;color: white;">{{ env('APP_URL') }}/artist/review/{{ $data['artist_profile_id'] }}/?code={{ base64_encode($data['artist_profile_id']) }}</h4>
                    </center>
                <br/><br/><br/>
                <center>
                    <small style="color: #e9a816;">
                        Copyright &copy; 2016 Woomen
                    </small>
                </center>
            </div>
        </div>
    </div>


