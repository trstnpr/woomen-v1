<div style="color:#000 !important; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; width:100%; background: #eee;">
  <div style="width:600px; margin: 0 auto; background: #e9a816;padding: 3em; font-size: 15px; color: #fff;">
    <div>
      <center><img src="{{ env('APP_URL') }}/images/brand/logo%20black.png" width="50%"></center>
    </div>
    <div style="margin: 2em 0em 1em 0em; color: #fff; text-align: center;">
      <p style="font-size: 1.6em; line-height: 1.5em; letter-spacing: 0.1em;">There's an inquiry in your item <strong>{{ $data['item_name'] }}</strong></p>
    </div>
    <div style="background-color: #fff; color: #333; padding: 2em;">
      <h4 align="center">Feature Image of the item</h4>
      <center>
      <img src="{{ $data['item_image'] }}" align="center" width="300px" height="300px">
      </center>
      <div style="font-family: Arial, Helvetica Neue light; font-size: 15px; line-height: 1.5em; letter-spacing: 0.05em; padding: 0em 2em;">
        <p>Here is the inquiry details.</p>
      </div>
      <div style="padding: 0em 2em; color: #fff; line-height: 1.5em; letter-spacing: 0.05em;">
        <div style="background: #2e2e2e; border-radius: 3px; padding: 1em;">
          <p>
            <strong>Name</strong><br>
            {{ $data['inquire_name'] }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
            <strong>Email</strong><br>
            {{ $data['inquire_email'] }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
            <strong>Contact Number</strong><br>
            {{ $data['inquire_contact'] }}
          </p>
          <hr style="border: 1px solid #585858;">
          <p>
            <strong>Message</strong><br>
            {{ $data['inquire_message'] }}
          </p>
        </div>
      </div>
      <br>
      
      <hr>
      
      <div style="background: #fff; color: #333; font-family: Arial, Helvetica Neue light; font-size: 15px; line-height: 1.5em; letter-spacing: 0.05em;">
        
        <br>
        <p>Cheers,<br><strong>Woomen Team.</strong></p>
        
      </div>
    </div>
  </div>
</div>