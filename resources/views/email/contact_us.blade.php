	<div style="padding: 30px; width: 100%; color:#000; font-family: Arial, Helvetica, sans-serif; width:100%;">
		<div style="width:600px; margin: 0 auto; border: 2px solid rgba(227, 158, 99, 0.4);">
			<center>
				<div style="background: #e39e63; padding: 20px;">
					<img alt="Woomen" src="{{ env('APP_URL') }}/images/brand/logo%20white.png" style="margin-left: 1em; width: 310px;" />
				</div>
			</center>

			<div style='line-height: 80%; padding: 20px;'>
				<br>
				     <ul style="list-style-type: none; border-radius: 2px; overflow: hidden; position: relative;">
					        <li style="background-color: #fff; border-bottom: 1px solid #e0e0e0; padding: 10px 20px;"><h4>Someone messaged us.</h4></li>
					        <li style="padding: 20px;">
					        	<span>Email:&nbsp;</span><span>{{ $data['user_email'] }}</span>
					        </li>
					        <li style="padding: 20px;">
					        	<span>Full Name:&nbsp;</span><span>{{ $data['name'] }}</span>
					        </li>
					        <li style="padding: 20px;">
					        	<span>Message:&nbsp;</span><span>{{ $data['message'] }}</span>
					        </li>
				      </ul>
				<br/><br/><br/>
				<center>
					<small style="color: #e39e63;">
						Copyright &copy; 2016 Woomen
					</small>
				</center>
			</div>
		</div>
	</div>


