
<div style="color:#000; font-family: Arial, Helvetica, sans-serif; width:100%; padding: 30px;">
    <div style="width:600px; margin: 0 auto; border: 2px solid #e9a816; padding: 20px;">
        <center>
            <div style="padding: 1em 0; background: #e9a816;">
                <img alt="Woomen" src="{{ env('APP_URL') }}/images/brand/logo%20white.png" style="margin-left: 1em; width: 300px;"/>
            </div>
            <h3 style='letter-spacing: 1px; color:#000;'> Booking Cancelled</h3>
            <div style='margin: 1em; line-height: 150%;'>
                <p style='font-size: 15px; letter-spacing: 1px; color:#000;'>
                    Your booking request has been cancelled by <b>{{ $data['first_name'].' '.$data['last_name'] }}</b>
                </p>
                <hr>
                <h4 style="text-align:left;">Reason/Remarks</h4>
                <p style='font-size: 15px;margin-top:0; letter-spacing: 1px; color:#000;text-align:left;'>
                    {{ $data['reason'] }}
                </p>
                <br/>
                <small>
                    Copyright &copy; 2016 Woomen
                </small>
            </div>
        </center>
    </div>
</div>

