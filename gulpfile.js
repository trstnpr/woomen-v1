gulp = require('gulp');

var elixir = require('laravel-elixir');
require('laravel-elixir-bowerbundle');

var bower_path = "./bower_components";
var paths = {
  'bootstrap'  : bower_path + "/bootstrap-sass/assets"
};
elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    // General
    mix.bower(['jquery', 'bootstrap-sass', 'lightgallery.js','slick-carousel','magnific-popup', 'bootstrap-star-rating', 'datatables.net', 'moment', 'eonasdan-bootstrap-datetimepicker', 'bootstrap-select', 'image-picker', 'masonry', 'imagesloaded', 'desandro-matches-selector', 'ev-emitter', 'fizzy-ui-utils', 'get-size', 'outlayer', 'lity', 'parallax.js', 'dropzone', 'aos'], 'public/assets/bundle.js');
    mix.copy('bower_components/bootstrap-sass/assets/fonts/bootstrap', 'public/build/assets/fonts');
    mix.copy('bower_components/font-awesome/fonts', 'public/build/assets/fonts');
    mix.copy('resources/assets/fonts', 'public/build/assets/fonts');

    // App
    mix.scripts(['public/assets/bundle.js','resources/assets/js/app/main.js'], 'public/assets/app/app.js', './');
    mix.sass('app/main.scss', 'public/assets/app/app.css');
    mix.sass('app/views/index.scss', 'public/assets/app/views/index.css');
    mix.copy('resources/assets/js/app/views', 'public/assets/app/views');

    // Merchant
    mix.scripts(['public/assets/bundle.js','resources/assets/js/merchant/main.js'], 'public/assets/merchant/merchant.js', './');
    mix.sass('merchant/main.scss', 'public/assets/merchant/merchant.css');
    mix.sass('merchant/views/index.scss', 'public/assets/merchant/views/index.css');
    mix.sass('merchant/views/profile/index.scss', 'public/assets/merchant/views/profile/index.css');
    mix.sass('merchant/views/dashboard/index.scss', 'public/assets/merchant/views/dashboard/index.css');
    mix.sass('merchant/views/reviews/index.scss', 'public/assets/merchant/views/reviews/index.css');
    mix.sass('merchant/views/items/index.scss', 'public/assets/merchant/views/items/index.css');
    mix.copy('resources/assets/js/merchant/views', 'public/assets/merchant/views');

    // Artist
    mix.scripts(['public/assets/bundle.js','resources/assets/js/artist/main.js'], 'public/assets/artist/artist.js', './');
    mix.sass('artist/main.scss', 'public/assets/artist/artist.css');
    mix.sass('artist/views/index.scss', 'public/assets/artist/views/index.css');
    mix.copy('resources/assets/js/artist/views', 'public/assets/artist/views');
    // mix.copy('bower_components/datatables.net-dt/css/jquery.dataTables.min.css', 'public/assets/artist');

    // Admin
    mix.scripts(['public/assets/bundle.js','resources/assets/js/admin/main.js'], 'public/assets/admin/admin.js', './');
    mix.sass('admin/main.scss', 'public/assets/admin/admin.css');
    mix.sass('admin/views/index.scss', 'public/assets/admin/views/index.css');
    mix.sass('admin/views/artist/index.scss', 'public/assets/admin/views/artist/index.css');
    mix.sass('admin/views/merchant/index.scss', 'public/assets/admin/views/merchant/index.css');
    mix.copy('resources/assets/js/admin/views', 'public/assets/admin/views');

    // Versioning & Cache Busting
    mix.version([
    	// APP
        'public/assets/app/app.css',
        'public/assets/app/app.js',
        'public/assets/app/views/index.css',
        'public/assets/app/views/index.js',
        'public/assets/app/views/slick-init.js',
        'public/assets/app/views/infiniscroll.js',
        'public/assets/app/views/*',
        // Merchant
        'public/assets/merchant/merchant.css',
        'public/assets/merchant/merchant.js',
        'public/assets/merchant/views/index.css',
        'public/assets/merchant/views/index.js',
        'public/assets/merchant/views/*',
        // Artist
        'public/assets/artist/artist.css',
        'public/assets/artist/artist.js',
        'public/assets/artist/views/index.css',
        'public/assets/artist/views/*',
        // 'public/assets/artist/jquery.dataTables.min.css',
        // Admin
        'public/assets/admin/admin.css',
        'public/assets/admin/admin.js',
        'public/assets/admin/views/index.css',
        'public/assets/admin/views/artist/index.css',
        'public/assets/admin/views/merchant/index.css',
        'public/assets/admin/views/index.js',
        'public/assets/admin/views/*'
    ]);

});

/**
 * Define a new gulp task upload that uploads current static assets to S3.
 * Run gulp --production first before running this task.
 */
gulp.task('uploadDev', function() {
    var AWS = require('aws-sdk');
    var config = new AWS.Config().loadFromPath('uploadDev.json');
    var s3 = require('gulp-s3-upload')(config);
    
    gulp.src('public/build/**')
        .pipe(s3({
            Bucket: '/dev-woomen/build',
            ACL:    'public-read'
        }, {
            maxRetries: 50
        }))
    ;
    gulp.src('public/images/**')
        .pipe(s3({
            Bucket: '/dev-woomen/images/',
            ACL:    'public-read'
        }, {
            maxRetries: 50
        }))
    ;
});

/**
 * Define a new gulp task upload that uploads current static assets to S3.
 * Run gulp --production first before running this task.
 */
gulp.task('upload', function() {
    var AWS = require('aws-sdk');
    var config = new AWS.Config().loadFromPath('upload.json');
    var s3 = require('gulp-s3-upload')(config);
    
    gulp.src('public/build/**')
        .pipe(s3({
            Bucket: '/woomen/build',
            ACL:    'public-read'
        }, {
            maxRetries: 50,
            signatureVersion: 'v3'
        }))
    ;
    gulp.src('public/images/**')
        .pipe(s3({
            Bucket: '/woomen/images',
            ACL:    'public-read'
        }, {
            maxRetries: 50,
            signatureVersion: 'v3'
        }))
    ;
});

/**
 * Define a new gulp task upload that uploads current static assets to S3.
 * Run gulp --production first before running this task.
 */
gulp.task('uploadProd', function() {
    var AWS = require('aws-sdk');
    var config = new AWS.Config().loadFromPath('upload.json');
    var s3 = require('gulp-s3-upload')(config);
    
    gulp.src('public/build/**')
        .pipe(s3({
            Bucket: '/woomenph/build',
            ACL:    'public-read'
        }, {
            maxRetries: 50,
            signatureVersion: 'v3'
        }))
    ;
    gulp.src('public/images/**')
        .pipe(s3({
            Bucket: '/woomenph/images',
            ACL:    'public-read'
        }, {
            maxRetries: 50,
            signatureVersion: 'v3'
        }))
    ;
});
