<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Instagram extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'instagram';
    }

}