<?php namespace App\Modules\Artist\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistImages extends Model {

	protected $table = 'artist_images';

    protected $fillable = ['artist_profile_id', 'artist_album_id', 'image_path', 'image_type', 'is_featured'];

    protected $primaryKey = 'artist_image_id';

    /**
     * Get the images of the artist.
     */
	public function artist_album()
	{
		return $this->hasOne('App\Modules\Artist\Models\ArtistAlbums', 'artist_album_id', 'artist_album_id');
	}
}
