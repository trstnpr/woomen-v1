<?php namespace App\Modules\Artist\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistAlbums extends Model {

	protected $table = 'artist_albums';

    protected $fillable = ['artist_profile_id', 'album_name', 'album_description'];

    protected $primaryKey = 'artist_album_id';

    /**
     * Get the images of the artist.
     */
	public function artist_images()
	{
		return $this->hasMany('App\Modules\Artist\Models\ArtistImages', 'artist_album_id', 'artist_album_id');
	}

	public function album_cover()
	{
		return $this->hasOne('App\Modules\Artist\Models\ArtistImages', 'artist_album_id', 'artist_album_id');
	}
	
}
