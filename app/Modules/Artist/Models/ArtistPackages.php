<?php namespace App\Modules\Artist\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistPackages extends Model {

	protected $table = 'artist_packages';

    protected $fillable = ['artist_profile_id', 'package_title', 'package_description'];

    protected $primaryKey = 'artist_package_id';
}
