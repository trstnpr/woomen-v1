<?php namespace App\Modules\Artist\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistProfile extends Model {

	protected $table = 'artist_profile';

    protected $fillable = ['user_id', 'profile_image', 'first_name', 'middle_name', 'last_name', 'price_from', 'price_to', 'address', 'about', 'achievements', 'facebook', 'is_featured', 'user_level', 'total_earnings'];

    protected $primaryKey = 'artist_profile_id';

    /**
     * Get the images of the artist.
     */
	public function artist_images()
	{
		return $this->hasMany('App\Modules\Artist\Models\ArtistImages');
	}

	/**
     * Get the packages of the artist.
     */
	public function artist_packages()
	{
		return $this->hasMany('App\Modules\Artist\Models\ArtistPackages');
	}

	/**
     * Get the affiliations of the artist.
     */
	public function artist_affiliations()
	{
		return $this->hasMany('App\Modules\Artist\Models\ArtistAffiliations');
	}

	/**
     * Get the portfolio of the artist.
     */
	public function artist_portfolio()
	{
		return $this->hasMany('App\Modules\Artist\Models\ArtistPortfolio');
	}

	/**
     * Get the specialty of the artist.
     */
	public function artist_specialty()
	{
		return $this->hasMany('App\Modules\Artist\Models\ArtistSpecialty');
	}

	/**
     * Get the events of the artist.
     */
	public function artist_events()
	{
		return $this->hasMany('App\Modules\Event\Models\ArtistEvent');
	}

	/**
     * Get the artist account.
     */
	public function account()
	{
		return $this->hasOne('App\Modules\App\Models\User', 'id', 'user_id');
	}

	/**
     * Get the specialty of the artist.
     */
	public function artist_one_specialty()
	{
		return $this->hasOne('App\Modules\Artist\Models\ArtistSpecialty');
	}

	/**
     * Get the ratings of the artist.
     */
	public function artist_ratings()
	{
		return $this->hasOne('App\Modules\App\Models\Review');
	}

	/**
     * Get the user level of the artist.
     */
	public function artist_user_level()
	{
		return $this->hasOne('App\Modules\App\Models\UserLevel', 'level_id', 'user_level');
	}
}
