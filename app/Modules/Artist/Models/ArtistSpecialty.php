<?php namespace App\Modules\Artist\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistSpecialty extends Model {

	protected $table = 'artist_specialties';

    protected $fillable = ['artist_profile_id', 'specialty_id'];

    protected $primaryKey = 'artist_specialty_id';

    public function specialty()
    {
    	return $this->belongsTo('App\Modules\App\Models\Specialty');
    }
}
