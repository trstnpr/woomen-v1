<?php namespace App\Modules\Artist\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistAffiliations extends Model {

	protected $table = 'artist_affiliations';

    protected $fillable = ['artist_profile_id', 'affiliation'];

    protected $primaryKey = 'artist_affiliation_id';
}
