<?php namespace App\Modules\Artist\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistPortfolio extends Model {

	protected $table = 'artist_portfolio';

    protected $fillable = ['artist_profile_id', 'artist_album_id', 'file_path', 'file_type', 'embed_code', 'is_featured'];

    protected $primaryKey = 'artist_portfolio_id';
}
