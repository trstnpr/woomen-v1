@extends('artistMaster')

@section('title')
	<title>Woomen - Event</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" />	
	<link href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" rel="stylesheet" />	
	<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet" />	
@stop

@section('content')

	<div class="artistEvent-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">My Events</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Events</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				{{-- <div class="card-panel upcoming-event">
					<h4 class="title">Your Upcoming Events</h4>
					<hr/>
					<div class="event-wrap">
						<div class="event-item">
							@if($upcoming != null)
							<div class="row">
								<div class="col-md-2">
									@if($upcoming->event_image != null)
										<figure class="event-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.event') . '/' . $upcoming->event_image }}');"></figure>
									@else
									@endif
								</div>
								<div class="col-md-10">
									<div class="event-info">
										<span class="event-title">{{ $upcoming->event_title }}</span>
										<ul class="list-unstyled event-venue-date fa-ul">
											<li class="date"><i class="fa fa-calendar fa-li"></i> {{ date_format(date_create($upcoming->event_date), 'F d, Y') .' '. date_format(date_create($upcoming->event_time), 'h:i A') }}</li>
											<li class="venue"><i class="fa fa-map-marker fa-li"></i> {{ $upcoming->event_venue }}</li>
										</ul>
										<p class="event-desc">{{ $upcoming->event_description }}</p>
										<a href="#" class="btn woo-btn-button" target="_target">Read more</a>
									</div>
								</div>
							</div>
							@else
							<h4 align="center">No upcoming events for now.</h4>
							@endif
						</div>
					</div>
				</div> --}}

				<div class="upcoming-event">
					<h4 class="title">Your Upcoming Events</h4>
					<div class="event-content">
						@if($upcoming != null)
						<div class="row">
							<div class="col-md-6">
								<div class="card-event" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.event') . '/' . $upcoming->event_image }}');">
									<div class="overlay">
										<div class="col-md-6">
											<a href="#" class="title">{{ $upcoming->event_title }}</a>
										</div>
										<div class="col-md-6">
											<a href="#" class="title" id="show">
											<button class="btn woo-btn-button btn-md pull-right">Read more</button>
											</a>
										</div>
									</div>	
									<div class="card-reveal">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
										<span class="reveal-title">{{ $upcoming->event_title }}</span>
										<span class="info"><i class="fa fa-map-marker"></i> {{ $upcoming->event_venue }}, {{ date_format(date_create($upcoming->event_date), 'F d, Y') .' '. date_format(date_create($upcoming->event_time), 'h:i A') }} <label class="label label-default">{{ $upcoming->event_ticket }}</label></span>
										<p>{!! $upcoming->event_description !!}</p>
									</div>
								</div>
							</div>
						</div>
						@else
						<h4 align="center">No upcoming events for now.</h4>
						@endif
					</div>
				</div>
				
				<div class="card-panel">
					<h4 class="title">Your Events</h4>
					<hr/>
					<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				            	<th>#</th>
				                <th>Event</th>
				                <th>Date</th>
				                <th>Venue</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@foreach($events as $event)
				        	<tr>
				            	<td>{{ $event->artist_event_id }}</td>
				                <td>{{ $event->event_title }}</td>
				                <td>{{ date_format(date_create($event->event_date), 'F d, Y') .' '. date_format(date_create($event->event_time), 'h:i A') }}</td>
				                <td>{{ $event->event_venue }}</td>
				                <td>
				                	<a href="{{ route('artist.event.edit', $event->artist_event_id) }}" class="btn btn-warning btn-xs">View/Edit</a>
				                	<button class="btn btn-danger btn-xs" onclick="deleteFunction({{ $event->artist_event_id }})">Delete</button>
				                </td>
				            </tr>
				        	@endforeach
				        </tbody>
				    </table>
				</div>
			</div>
		</section>

		<div class="modal fade woo-modal" id="delete-modal">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header gold">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Please Confirm</h4>
					</div>
					<div class="modal-body">
						By taking this action, you will not retrieve the data anymore.
						<strong>Are you sure?</strong>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
						<button type="button" id="confirm-button" data-url="{{ route('artist.event.delete') }}" class="btn woo-btn-button">Yes, delete</button>
					</div>
				</div>
			</div>
		</div>

		<div class="fixed-action-btn">
			<a href="{{ url('artist/event/create') }}" class="btn-floating"><i class="fa fa-plus"></i></a>
		</div>

	</div>

@stop()

@section('custom-scripts')
	<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>	
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});
		
		function deleteFunction(id){
			$('#confirm-button').attr('data-id', id);
			$('#delete-modal').modal('show');
		}

		$(function(){
		    $('#show').on('click',function(){        
		        $('.card-reveal').slideToggle('slow');
		    });
		    
		    $('.card-reveal .close').on('click',function(){
		        $('.card-reveal').slideToggle('slow');
		    });
		});

		$(document).ready(function(){
	        $('#confirm-button').click(function(){
	            var id = $(this).data('id');
	            var url = $(this).data('url');
	            var btn = $('#confirm-button').html();
	            $.ajax({
	                url: url,
	                type: "POST",
	                data: {'id':id, '_token':'{{ Session::token() }}'},
	                beforeSend: function(){ $('#confirm-button').html('Processing...').attr('disabled', true);},
	                error: function(data){
	                    if(data.readyState == 4){
	                        errors = JSON.parse(data.responseText);
	                        $('#confirm-button').html(btn).attr('disabled', false);
	                        alert(JSON.stringify(errors));
	                    }
	                },
	                success: function(data){
	                    var msg = JSON.parse(data);
	                    if(msg.result == 'success'){
	                        $("#confirm-button").html(btn).attr('disabled', false);
	                        alert(msg.message);
	                        window.location.reload();
	                    } else{
	                        $("#confirm-button").html(btn).attr('disabled', false);
	                        alert(msg.message);
	                    }
	                }
	            });
	        });
	    });
	</script>
@stop