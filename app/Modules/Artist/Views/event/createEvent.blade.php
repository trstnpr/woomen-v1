@extends('artistMaster')

@section('title')
	<title>Woomen - Create Event</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="artistEvent-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Create Event</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Events</li>
					<li class="active">Create Event</li>
				</ol>
			</div>
		</section>

		@if($events >= 2)

		<h3 style="color: red;" align="center">Sorry, only 2 events per account are available. <br><br>
			<a href="{{ route('artist.event.index') }}" class="btn btn-primary" align="center">Back to My Events</a>
		</h3>
		@else

		<section class="section-content">
			<div class="container-fluid">
				<div class="card-panel create-event">
					<form method="POST" id="event-form" action="{{ route('artist.event.store') }}">
						<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Choose Image</label>
									<div class="well">
										<input type="file" class="" name="image_upload" id="image_upload" />
									</div>
									<div class="file-wrap"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Event Title</label>
									<input type="text" class="form-control input-lg" name="event_title" id="event_title" placeholder="Event title" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="fomr-group">
									<label>Event Date</label>
									<div class="input-group datePicker">
					                    <input type="text" class="form-control input-lg" name="event_date" id="event_date" placeholder="Event date" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Event Time</label>
									<div class="input-group timePicker">
					                    <input type="text" class="form-control input-lg" name="event_time" id="event_time" placeholder="Event time" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-time"></span>
					                    </span>
					                </div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Event Venue</label>
									<input type="text" class="form-control input-lg" name="event_venue" id="event_venue" placeholder="Event venue" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Event Description</label>
									<textarea class="form-control" rows="3" name="event_description" id="event_description" ></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Ticket</label>
									<select class="form-control input-lg" name="event_ticket" id="event_ticket">
										<option value="Free">Free</option>
										<option value="Door Sale">Door Sale</option>
										<option value="Buy Online">Buy Online</option>
									</select>
								</div>
							</div>
							{{-- <div class="col-md-6">
								<div class="form-group ticket_link hide">
									<label>Ticket Link</label>
									<input type="text" class="form-control input-lg" name="ticket_link" id="ticket_link" placeholder="Ticket Link" />
								</div>
							</div> --}}
							<div class="col-md-12">
								<div class="alert alert-success hide" id="result_div">
				  					<ul class="fa-ul">
				  						<li><i class="fa fa-li fa-check"></i> Success!</li>
				  						<li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
				  						<li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
				  					</ul>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" id="event-button" class="btn btn-lg woo-btn-submit">Create Event</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>

		@endif
	</div>

@stop()

@section('custom-scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('.datePicker').datetimepicker({ format: 'MM/DD/YYYY' });
			$('.timePicker').datetimepicker({ format: 'LT' });

			// Function to preview image after validation
			$("#image_upload").change(function() {
				var file = this.files[0];
				var imagefile = file.type;
				var imagesize = file.size;
				var match= ["image/jpeg","image/png","image/jpg"];
				if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
					alert('Invalid file type');
					$("#image_upload").val('');
					$('.file-wrap').hide();
				} else {
					if(imagesize>500000 || imagesize==0) {
						alert('Image should not exceed to 500kbs');
						$("#image_upload").val('');
						$('.file-wrap').hide();
					} else {
						$('.file-wrap').show();
						var reader = new FileReader();
						reader.onload = imageIsLoaded;
						reader.readAsDataURL(this.files[0]);
					}
				}
			});
			function imageIsLoaded(e) {
				$('.file-wrap').css('background-image', 'url(' + e.target.result + ')');
			};

			// $('#event_ticket').change(function() {
			// 	if($(this).val() == 'Buy Online') {
			// 		$('.ticket_link').removeClass('hide');
			// 	} else {
			// 		$('.ticket_link').addClass('hide');
			// 	}
			// });
		});
	</script>
	<script src="{{ elixir('assets/artist/views/event/index.js') }}"></script>
@stop