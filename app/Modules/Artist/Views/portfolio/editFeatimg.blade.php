@extends('artistMaster')

@section('title')
	<title>Woomen - Feature Images</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	<style>
		.thumbnails.image_picker_selector {
			overflow:hidden!important;
		}
		.thumbnails.image_picker_selector li {
			display:inline-block;
			width:20%;
			margin:5px!important;
		}
		.thumbnails li img{
	        width: 300px;
	    }
	</style>
@stop

@section('content')

	<div class="artistPortfolio-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Featured Image</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">Portfolio</li>
					<li class="active">Featured Image</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				<section>
					<form id="frm_feat_img" action="{{ route('artist.portfolio.featureImg') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="alert alert-success hide" id="result_div">
							<ul class="fa-ul">
								<li><i class="fa fa-li fa-check"></i> Success</li>
							</ul>
						</div>
						<div class="card-panel photos">
							<div class="clearfix">
								<h4 class="pull-left">Select your photos</h4>
								<button class="btn woo-btn-submit pull-right" type="submit" id="btn_feat">Save Changes</button>
							</div>
							<hr/>

							<select multiple="multiple" class="image-picker masonry show-html" name="artist_image_id[]">
								@foreach($images as $i)
								<option data-img-src="{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $i->image_path }}" value="{{ $i->artist_image_id }}"> </option>
								@endforeach
							</select>
						</div>
					</form>
				</section>
			</div>
		</section>

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ elixir('assets/artist/views/portfolio/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.image-picker').imagepicker();

			var container = $("select.image-picker.masonry").next("ul.thumbnails");

		    container.imagesLoaded(function(){
		      container.masonry({
		        itemSelector:   "li"
		      });
		    });
		});
	</script>
@stop