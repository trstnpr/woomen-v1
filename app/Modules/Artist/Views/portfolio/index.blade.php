@extends('artistMaster')

@section('title')
	<title>Woomen - Portfolio</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')

	<div class="artistPortfolio-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">My Portfolio</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">Portfolio</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				@include('Artist::portfolio.partials.section-1')

				@include('Artist::portfolio.partials.section-2')

				@include('Artist::portfolio.partials.section-3')

			</div>
		</section>

	</div>

	<div class="modal fade woo-modal" id="delete-video-modal">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this video?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<button type="button" id="delete-video" data-token="{{ csrf_token() }}" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

@stop()

@section('custom-scripts')
	<script src="{{ elixir('assets/artist/views/portfolio/index.js') }}"></script>
	<script type="text/javascript">
		function deleteModal(id){
			$('#delete-video').attr('data-id', id);
			$('#delete-video-modal').modal('show');
		}
	</script>
@stop