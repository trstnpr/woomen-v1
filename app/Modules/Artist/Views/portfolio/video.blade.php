@extends('artistMaster')

@section('title')
	<title>Woomen - Videos</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="artistPortfolio-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Videos</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">Portfolio</li>
					<li class="active">Videos</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				<div class="alert alert-success hide" id="result_div">
					<ul class="fa-ul">
						<li><i class="fa fa-li fa-check"></i> Success</li>
						<li><i class="fa fa-li fa-exclamation-triangle"></i> Warning</li>
						<li><i class="fa fa-li fa-times"></i> Error</li>
					</ul>
				</div>
				<section>
					@if($portfolio->count() < $profile->artist_user_level->videos)
					<form method="POST" id="portfolio-form" action="{{ route('artist.portfolio.video.post') }}">
						<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<div class="card-panel video-panel">
							<div class="form-group">
								<label>Add video link</label>
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">
										<i class="fa fa-link"></i>
									</span>
									<input type="text" class="form-control input-lg" name="video" id="video" placeholder="Video URL (e.g https://www.youtube.com/watch?v=3dSAda)" aria-describedby="basic-addon1">
								</div>
							</div>
							<div class="form-group">
								<button type="submit" id="portfolio-button" class="btn btn-lg woo-btn-submit">Save Video</button>

							</div>
						</div>
					</form>
					@else
					<div class="card-panel video-panel">
						<div class="form-group">
							<label style="color: red;">You've reached the maximum number of Videos for your account.</label>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">
									<i class="fa fa-link"></i>
								</span>
								<input type="text" class="form-control input-lg" name="video-link" id="video" placeholder="Video URL (e.g https://www.youtube.com/watch?v=3dSAda)" aria-describedby="basic-addon1" disabled="">
							</div>
						</div>
						<div class="form-group">
							<button type="button" id="portfolio-button" class="btn btn-lg woo-btn-submit" disabled="">Save Video</button>

						</div>
					</div>
					@endif
					<div class="card-panel portfolio-videos">
						<h4>Videos</h4>
						<div class="video-wrap">
							<div class="row">
							@if($portfolio->count() == 0)
								<h4 align="center">No Videos Available</h4>
							@else
								@foreach($portfolio as $video)
									<div class="col-md-4">
										<div class="video-item">
											<figure class="vid" style="background-image:url('http://img.youtube.com/vi/{{ $video->embed_code }}/0.jpg');">
												<div class="btn-group">
													<a role="button" href="{{ $video->file_path }}" class="btn btn-danger" title="Play" data-lity>
														<i class="fa fa-youtube-play"></i>
													</a>
													<button type="button" class="btn btn-primary" onclick="deleteModal({{ $video->artist_portfolio_id }})" title="Delete">
														<i class="fa fa-trash"></i>
													</button>
												</div>
											</figure>
										</div>
									</div>
								@endforeach
							@endif

								{{-- @for($x=1;$x<=6;$x++)
								<div class="col-md-4">
									<div class="video-item">
										<iframe class="vid" src="https://www.youtube.com/embed/peoWB_NcAlI" frameborder="0" allowfullscreen=""></iframe>
									</div>
								</div>
								@endfor --}}
							</div>
						</div>
					</div>
				</section>
			</div>
		</section>

	</div>

	<div class="modal fade woo-modal" id="delete-video-modal">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this video?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<button type="button" id="delete-video" data-token="{{ csrf_token() }}" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

@stop()

@section('custom-scripts')
	<script type="text/javascript">
		$('.magnific-popup').magnificPopup({type:'image'});
	</script>
	<script src="{{ elixir('assets/artist/views/portfolio/index.js') }}"></script>
	<script type="text/javascript">
		function deleteModal(id){
			$('#delete-video').attr('data-id', id);
			$('#delete-video-modal').modal('show');
		}
	</script>
@stop