<div class="card-panel section-1 featA-photo ">
	<h4>Featured Image</h4>
	<div class="row">
		@if($featured->count() != 0)
			@foreach($featured as $f)
				<div class="col-md-3 col-sm-2 col-xs 12">
					<a href="{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $f->image_path }}" data-lity>
						<figure class="feat-item" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $f->image_path }}');">
							<div class="hoverlay">
								{{-- <span class="photo-title">Photo 1</span> --}}
							</div>
						</figure>
					</a>
				</div>
			@endforeach
		@else
			<h4 align="center">No featured photos yet.</h4>
		@endif
		{{-- @for($x=1;$x<=6;$x++)
		<div class="col-md-3 col-sm-2 col-xs 12">
			<a href="#">
				<figure class="feat-item" style="background-image:url('{{ url('/images/artist/artist%20') . $x }}.png');">
					<div class="hoverlay">
						<span class="photo-title">Photo {{ $x}}</span>
					</div>
				</figure>
			</a>
		</div>
		@endfor --}}
	</div>
	<div class="form-group btn-add">
		<a class="btn woo-btn-button" href="{{ url('artist/portfolio/featured-image') }}" ><i class="fa fa-image"></i> Set Featured Images</a>
	</div>
</div>