<div class="card-panel section-2 album-photo ">
	<h4>Albums</h4>
	<div class="row">
		@if($albums->count() != 0)
			@foreach($albums as $a)
				<div class="col-md-4">
					<a href="{{ route('artist.portfolio.album.show', $a->artist_album_id) }}">
						@if($a->album_cover)
						<figure class="album-item" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $a->album_cover->image_path }}');">
							<div class="overlay">
								<span class="album-title">{{ $a->album_name }}</span>
							</div>
						</figure>
						@else
						<figure class="album-item" style="background-color: #8c8989;">
							<div class="hoverlay">
								<i class="fa fa-photo album-icon"></i><br/>
								<span class="album-title">{{ $a->album_name }}</span>
							</div>
						</figure>
						@endif
					</a>
				</div>
			@endforeach
		@else
			<h4 align="center">No albums yet.</h4>
		@endif
		{{-- @for($x=1;$x<=3;$x++)
		<div class="col-md-4">
			<a href="#">
				<figure class="album-item" style="background-image:url('{{ url('/images/artist/album%20') . $x }}.png');">
					<div class="hoverlay">
						<span class="album-title">Album {{ $x}}</span>
					</div>
				</figure>
			</a>
		</div>
		@endfor --}}
	</div>
	@if($albums->count() < $profile->artist_user_level->albums)
	<div class="form-group btn-add">
		<a href="#newAlbum" class="btn woo-btn-button" data-toggle="modal" ><i class="fa fa-plus-circle"></i> Create Album</a>
	</div>
	@else
		<p style="color: red">You've reached the maximum number of Albums for your account.</p>
		{{-- <p style="color: red">To add more Albums, You need to <a href="#">Upgrade your Account</a>.</p> --}}
	@endif
</div>

<div class="modal fade" id="newAlbum" tabindex="-1">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form id="album-form" method="POST" action="{{ route('artist.portfolio.album.create') }}">
				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">New Album</h4>
				</div>
				
				<div class="modal-body">
					<div class="col-md-12 alert alert-success hide" id="result_div">
	  					<ul class="fa-ul">
	  						<li><i class="fa fa-li fa-check"></i> Success!</li>
	  						<li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
	  						<li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
	  					</ul>
					</div>
					<div class="form-group">
						<label>Album name</label>
						<input type="text" class="form-control" name="album_name" id="album_name" placeholder="Desired album name" />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" id="album-button" class="btn woo-btn-button">Create Album</button>
				</div>
			</form>
		</div>
	</div>
</div>