<div class="card-panel section-3 video">
	<h4>VIDEOS</h4>
	<div class="row">
		@if($portfolio->count() == 0)
			<h4 align="center">No Videos Available</h4>
		@else
			@foreach($portfolio as $video)
				<div class="col-md-4">
					<div class="video-item">
						{{-- <iframe class="vid" src="{{ $video->file_path }}" frameborder="0" allowfullscreen=""></iframe> --}}
						<figure class="vid" style="background-image:url('http://img.youtube.com/vi/{{ $video->embed_code }}/0.jpg');">
							<div class="btn-group">
								<a role="button" href="{{ $video->file_path }}" class="btn btn-danger" title="Play" data-lity>
									<i class="fa fa-youtube-play"></i>
								</a>
								<button type="button" class="btn btn-primary" onclick="deleteModal({{ $video->artist_portfolio_id }})" title="Delete">
									<i class="fa fa-trash"></i>
								</button>
							</div>
						</figure>
					</div>
				</div>
			@endforeach
		@endif
		{{-- @for($x=1;$x<=6;$x++)
		<div class="col-md-4">
			<div class="video-item">
				<iframe class="vid" src="https://www.youtube.com/embed/peoWB_NcAlI" frameborder="0" allowfullscreen=""></iframe>
			</div>
		</div>
		@endfor --}}
	</div>
	@if($portfolio->count() < $profile->artist_user_level->videos)
	<div class="form-group btn-add">
		<a href="{{ route('artist.portfolio.video') }}" class="btn woo-btn-button"><i class="fa fa-play"></i> Add Video</a>
	</div>
	@else
	<p style="color: red">You've reached the maximum number of Videos for your account.</p>
	@endif
</div>