@extends('artistMaster')

@section('title')
	<title>Woomen - Album</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="artistPortfolio-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">{{ $album->album_name }}</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">Portfolio</li>
					<li class="active">Album</li>
					<li class="active">{{ $album->album_name }}</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				<div class="alert alert-success hide" id="result_div">
					<ul class="fa-ul">
						<li><i class="fa fa-li fa-check"></i> Success</li>
						<li><i class="fa fa-li fa-exclamation-triangle"></i> Warning</li>
						<li><i class="fa fa-li fa-times"></i> Error</li>
					</ul>
				</div>
				<section>
					<div class="form-group">
							{{-- <form action="/file-upload" class="dropzone">
								<div class="fallback">
									<input name="file" type="file" multiple />
								</div>
							</form> --}}
						</div>
					<div class="card-panel upload-panel">
						<form method="POST" id="image-form" action="{{ route('artist.portfolio.album.uploadImage', $album->artist_album_id) }}" enctype="multipart/form-data">
							<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
							<label>Upload Photos</label>
							<p style="color: red">Note : Only 5 photos per Album are allowed.</p>
							<div class="form-group">
								<div class="dropzone" id="myAwesomeDropzone">
									<div class="fallback">
										<input type="file" name="image_path[]" id="files" multiple="multiple" required/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Album Title</label>
								<input type="text" class="form-control input-lg" name="album_name" id="album_name" placeholder="Your album name" value="{{ $album->album_name }}" required />
							</div>
							<div class="form-group">
								<label>Album Description</label>
								<textarea class="form-control input-lg" name="album_description" id="album_description" placeholder="Your album description">{{ $album->album_description }}</textarea>
							</div>
							<div class="form-group">
								<button type="submit" id="image-button" class="btn btn-lg woo-btn-submit">Save Changes</button> 
							</div>
						</form>
					</div>

					<div class="card-panel album-photos">
						<h4>Photos</h4>
						<div class="photo-wrap">
							<div class="row">
							@if($album->artist_images->count() == 0)
								<h4 align="center">No Photos in this album.</h4>
							@else
								@foreach($album->artist_images as $image)
									<div class="col-md-2">
										<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $image->image_path }}');">
											<div class="overlay">
												<div class="btn-group">
													<a href="{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $image->image_path }}" class="btn woo-btn-button btn-xs" data-lity><i class="fa fa-eye"></i></a>
													<button type="button" class="btn woo-btn-button btn-xs" onclick="showModal('{{ route('artist.portfolio.album.deleteImage' , $image->artist_image_id)}}')"><i class="fa fa-times"></i></button>
												</div>
											</div>
										</figure>
									</div>
								@endforeach
							@endif
							</div>
						</div>
					</div>
				</section>
			</div>
		</section>

	</div>

	<div class="modal fade" id="delete-image" tabindex="-1">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Delete Image</h4>
				</div>
				
				<div class="modal-body">
					<div class="col-md-12 alert alert-success hide" id="delete_result_div">
	  					<ul class="fa-ul">
	  						<li><i class="fa fa-li fa-check"></i> Success!</li>
	  						<li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
	  						<li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
	  					</ul>
					</div>
					<div class="form-group">
						<h4>Are you sure you want to delete this image?</h4>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					<a href="#" type="button" onclick="deleteImage($(this).data('url'))" id="delete-button" class="btn woo-btn-button">Yes</a>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom-scripts')
	<script src="{{ elixir('assets/artist/views/portfolio/index.js') }}"></script>
	<script type="text/javascript">
		function showModal(url){
			$('#delete-button').attr('data-url', url);
			$('#delete-image').modal('show');
		}

		function deleteImage(url){
			$.get( url, function( data ) {
				var msg = JSON.parse(data);
	            if(msg.result == 'success'){
	                $("#delete-button").html('Yes').attr('disabled', false);
	                $('#delete_result_div').empty();
                    $('#delete_result_div').removeClass('alert-danger hide').addClass('alert-success');
                    $('#delete_result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
	                setTimeout(function(){ location.reload(); }, 2000);
	            } else{
	                $("#delete-button").html('Yes').attr('disabled', false);
	                $('#delete_result_div').empty();
                    $('#delete_result_div').removeClass('alert-success hide').addClass('alert-danger');
                    $('#delete_result_div').html('<ul class="error_list fa-ul white-text"></ul>'); 
                    $('.error_list').append('<li class="white-text"><i class="fa fa-li fa-times white-text"></i> '+msg.message+'</li>');
	            }
			});
		}
	</script>
@stop
