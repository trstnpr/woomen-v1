@extends('artistMaster')

@section('title')
	<title>Woomen - Reviews</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="artistReview-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">My Reviews</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Reviews</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				<div class="card-panel">
					<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				            	<th>#</th>
				                <th>Reviewer</th>
				                <th>Email</th>
				                <th>Rating</th>
				                <th>Review</th>
				                <th>Date</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@foreach($review as $rev)
				        	<tr>
					        	<td>{{ $rev->review_id }}</td>
					        	<td>{{ $rev->review_name }}</td>
					        	<td>{{ $rev->review_email }}</td>
					        	<td>{{ $rev->review_rating }}</td>
					        	<td>{{ $rev->review_description }}</td>
					        	<td>{{ date_format(date_create($rev->created_at), "F d, Y") }}</td>
					        	<td>
					        		<a href="#read" class="btn btn-primary btn-xs" data-toggle="modal" data-review-desc="{{ $rev->review_description }}" data-review-name="{{ $rev->review_name }}" onclick="read_rev(this)">Read</a>
					        		<a href="#delete" class="btn btn-danger btn-xs" data-toggle="modal" data-review-id="{{ $rev->review_id }}" onclick="deleteReview(this)">Delete</a>
					        	</td>
					        </tr>
					        @endforeach
				        </tbody>
				    </table>
				</div>

				{{-- Modal Read More Review --}}
				<div class="modal fade woo-modal read-review" id="read">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="review-wrap">
									<div class="avatar-cover gold">
										<!-- <figure class="avatar" style="background-image:url('{{ url('images/review/review%202.png') }}');"></figure> -->
										<h4 class="caption">Pia Wurtzbach</h4>
									</div>
									<div class="review-text">
										<i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sollicitudin neque turpis, ut blandit arcu accumsan ac. Aliquam erat libero, consectetur eget cursus sed, pharetra at metus. Phasellus massa lacus, lobortis et auctor id, posuere id sapien.
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>

				{{-- Modal Confirm Delete Review --}}
				<div class="modal fade woo-modal" id="delete">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="modal-header gold">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Please Confirm</h4>
							</div>
							<div class="modal-body">
								By taking this action, you will not retrieve the data anymore.
								<strong>Are you sure?</strong>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
								<button type="button" id="confirm-button-review" data-url="#" class="btn woo-btn-button">Yes, delete</button>
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>

	</div>

@stop()

@section('custom-scripts')

	<script src="{{ elixir('assets/artist/views/review/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		function read_rev(review) {
	        $('.review-text').html($(review).attr('data-review-desc'));
	        $('.caption').html($(review).attr('data-review-name'));
	    }

	    function deleteReview(review) {
	        $('#confirm-button-review').attr('data-review-id', $(review).attr('data-review-id'));
	    }
	</script>
	<script type="text/javascript"></script>
@stop
