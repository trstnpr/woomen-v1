@extends('artistMaster')

@section('title')
	<title>Woomen - Password Settings</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')

	<div class="artistPasswordSetting-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Password Settings</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">Account Settings</li>
					<li class="active">Password Settings</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				<div class="card-panel">
					<div class="row">
						<form method="POST" id="password-form" action="{{ route('artist.password.update') }}">
							<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
							<div class="col-md-12">
								<div class="form-group">
									<label>Current Password</label>
									<input type="password" class="form-control input-lg" name="current_pass" id="current_pass" placeholder="Current Password" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>New Password</label>
									<input type="password" class="form-control input-lg" name="password" id="password" placeholder="New Password" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Confirm New Password</label>
									<input type="password" class="form-control input-lg" name="password_confirmation" id="password_confirmation" placeholder="Confirm New Password" />
								</div>
							</div>

							<div class="col-md-12 alert alert-success hide" id="result_div">
			  					<ul class="fa-ul">
			  						<li><i class="fa fa-li fa-check"></i> Success!</li>
			  						<li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
			  						<li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
			  					</ul>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" id="password-button" class="btn btn-lg woo-btn-submit">Save Changes</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				
			</div>
		</section>

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	<script src="{{ elixir('assets/artist/views/password/index.js') }}"></script>
@stop