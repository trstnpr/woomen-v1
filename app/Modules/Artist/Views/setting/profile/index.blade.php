@extends('artistMaster')

@section('title')
	<title>Woomen - Profile Settings</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')

	<div class="artistProfileSetting-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Profile Settings</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">Account Settings</li>
					<li class="active">Profile Settings</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				<div class="card-panel form-profile">
					<div class="row">
						<form method="POST" id="profile-form" action="{{ route('artist.profile.update') }}">
							<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
							<div class="col-md-12">
								<div class="form-group">
									<label>Profile Image</label>
									<div class="well">
										<input type="file" class="" name="profile_image" id="profile_image" />
									</div>
									<div class="file-wrap"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>First Name</label>
									<input type="text" class="form-control input-lg" name="first_name" id="first_name" placeholder="Your first name" value="{{ $profile->first_name }}" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Middle Name</label>
									<input type="text" class="form-control input-lg" name="middle_name" id="middle_name" placeholder="Your middle name" value="{{ $profile->middle_name }}" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Last Name</label>
									<input type="text" class="form-control input-lg" name="last_name" id="last_name" placeholder="Your last name" value="{{ $profile->last_name }}" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Address</label>
									<input type="text" class="form-control input-lg" name="address" id="address" placeholder="Your address" value="{{ $profile->address }}" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Forte</label>
									<select class="selectpicker form-control" name="specialty[]" multiple>
										@foreach($specialty as $spe)
											<option value="{{ $spe->specialty_id }}" {{ (in_array($spe->specialty_id, $mySpecialties)) ? 'selected' : '' }} >{{ $spe->specialty_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Email</label>
									<input type="email" class="form-control input-lg" name="email-address" id="email-address" placeholder="Your email address" disabled value="{{ Auth::user()->email }}" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Facebook</label>
									<input type="text" class="form-control input-lg" name="facebook" id="facebook" placeholder="Your facebook profile URL" value="{{ $profile->facebook }}" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Price From</label>
									<input type="text" class="form-control input-lg number" name="price_from" id="price_from" placeholder="Your price range from" value="{{ $profile->price_from }}" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Price To</label>
									<input type="text" class="form-control input-lg number" name="price_to" id="price_to" placeholder="Your price range to" value="{{ $profile->price_to }}" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group clearfix">
									<label>About me</label>
									<textarea class="form-control input-lg" rows="4" name="about" id="about" placeholder="Tell more about yourself">{{ $profile->about }}</textarea>
									<small class="pull-right"><strong class="about_count">500</strong> Characters Remaining </small> 
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group affil-fields">
									<label>Affiliation</label>
									@if($profile->artist_affiliations->count() != 0)
										@foreach($profile->artist_affiliations as $affiliation)
										<div class="input-group">
											<input type="text" class="form-control input-lg" placeholder="Your affiliations" name="affiliation[]" id="affiliation" value="{{ $affiliation->affiliation }}" />
											<span class="input-group-btn">
												<button class="btn btn-warning btn-lg remove-affil" type="button"><i class="fa fa-times"></i></button>
											</span>
										</div>
										@endforeach
									@else
									<div class="input-group">
										<input type="text" class="form-control input-lg" placeholder="Your affiliations" name="affiliation[]" id="affiliation" />
										<span class="input-group-btn">
											<button class="btn btn-warning btn-lg remove-affil" type="button"><i class="fa fa-times"></i></button>
										</span>
									</div>
									@endif

								</div>
								<div class="form-group">
									<button type="button" class="btn btn-default" id="add-affil"><i class="fa fa-plus"></i> ADD</button>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert alert-success hide" id="result_div">
				  					<ul class="fa-ul">
				  						<li><i class="fa fa-li fa-check"></i> Success!</li>
				  						<li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
				  						<li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
				  					</ul>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" id="profile-button" class="btn btn-lg woo-btn-submit">Save Changes</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				
			</div>
		</section>

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ elixir('assets/artist/views/account/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// Function to preview image after validation
			$("#profile_image").change(function() {
				var file = this.files[0];
				var imagefile = file.type;
				var imagesize = file.size;
				var match= ["image/jpeg","image/png","image/jpg"];
				if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
					alert('Invalid file type');
					$("#profile_image").val('');
					$('.file-wrap').hide();
				} else {
					if(imagesize>10000000 || imagesize==0) {
						alert('Image should not exceed to 10mb');
						$("#profile_image").val('');
						$('.file-wrap').hide();
					} else {
						$('.file-wrap').show();
						var reader = new FileReader();
						reader.onload = imageIsLoaded;
						reader.readAsDataURL(this.files[0]);
					}
				}
			});
			function imageIsLoaded(e) {
				$('.file-wrap').css('background-image', 'url(' + e.target.result + ')');
			};

			// ABOUT CHARATER COUNT
			var totalChars = 500; //Total characters allowed in textarea
			var countTextBox = $('#about') // Textarea input box
			var charsCountEl = $('.about_count'); // Remaining chars count will be displayed here

			charsCountEl.text(totalChars); //initial value of countchars element
			countTextBox.keyup(function() { //user releases a key on the keyboard
				var thisChars = this.value.replace(/ /g, '').length; //get chars count in textarea
				if(thisChars > totalChars) //if we have more chars than it should be
				{
				  var CharsToDel = (thisChars-totalChars); // total extra chars to delete
				  this.value = this.value.substring(0,this.value.length-CharsToDel); //remove excess chars from textarea
				} else {
				  charsCountEl.text( totalChars - thisChars); //count remaining chars
				}
			});

			// Affiliations Fields
			$('#add-affil').click(function() {
				// ADD Affiliations
				$('.affil-fields').append('<div class="input-group"><input type="text" class="form-control input-lg" placeholder="Your affiliations" name="affiliation[]" id="affiliation" /><span class="input-group-btn"><button class="btn btn-warning btn-lg remove-affil" type="button"><i class="fa fa-times"></i></button></span></div>');

				// REMOVE Affiliations
				$('.remove-affil').on('click', function() {
					// alert('Remove Affiliation');
					$(this).parent().parent().remove();
				});
			});

			// REMOVE Affiliations
			$('.remove-affil').on('click', function() {
				// alert('Remove Affiliation');
				$(this).parent().parent().remove();
			});
		});
	</script>
@stop