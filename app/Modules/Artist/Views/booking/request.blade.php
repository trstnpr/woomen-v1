@extends('artistMaster')

@section('title')
	<title>Woomen - Booking Request</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="artistBooking-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Booking Request</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Bookings</li>
					<li class="active">Booking Request</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="card-panel">
					<h4 class="title">Booking Details</h4>
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<tbody>
								<tr>
									<th width="20%">Event</th>
									<td>{{ $request_booking->event_title }}</td>
								</tr>
								<tr>
									<th>Date</th>
									<td>{{ date_format(date_create($request_booking->event_date), 'F d, Y') }}</td>
								</tr>
								<tr>
									<th>Time</th>
									<td>{{ date_format(date_create($request_booking->event_time), 'h:i A') }}</td>
								</tr>
								<tr>
									<th>Preparation Venue</th>
									<td>{{ $request_booking->venue }}</td>
								</tr>
								<tr>
									<th>No. of Heads</th>
									<td>{{ $request_booking->no_of_heads }}</td>
								</tr>
								<tr>
									<th>Call Time</th>
									<td>{{ $request_booking->call_time }}</td>
								</tr>
								<tr>
									<th>Client</th>
									<td>{{ $request_booking->first_name.' '.$request_booking->last_name }}</td>
								</tr>
								<tr>
									<th>Mobile</th>
									<td>{{ $request_booking->contact_no }}</td>
								</tr>
								<tr>
									<th>Email</th>
									<td>{{ $request_booking->email }}</td>
								</tr>
								<tr>
									<th>Message</th>
									<td>{{ $request_booking->remarks }}</td>
								</tr>
								<tr>
									<th>Action</th>
									<td>
						        		<a href="#accept-modal" class="btn btn-default" title="Confirm booking" data-toggle="modal"><i class="fa fa-check-circle"></i> Accept</a>
						        		<a onclick="deleteBooking('{{$request_booking->booking_id}}')" class="btn btn-default" title="Decline"><i class="fa fa-times"></i> Decline</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>

		<div class="modal fade woo-modal" id="accept-modal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header gold">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Booking Confirmation</h4>
					</div>
					<div class="modal-body">
						<div class="col-md-12 alert alert-success hide" id="result_div">
	                        <ul class="fa-ul">
	                            <li><i class="fa fa-li fa-check"></i> Success!</li>
	                            <li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
	                            <li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
	                        </ul>
	                    </div>
						<form class="form-horizontal" id="confirm-booking-form" action="{{ route('artist.booking.confirm', $request_booking->booking_id) }}">
							<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
							<div class="form-group">
								<label class="control-label col-sm-3">Event Date</label>
								<div class="col-md-9">
									<div class="input-group datePicker">
										<input type="text" class="form-control" name="final_date" id="final_date" placeholder="Event Date" value="{{ date_format(date_create($request_booking->event_date), 'm/d/Y') }}" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Event Time</label>
								<div class="col-md-9">
									<div class="input-group timePicker">
										<input type="text" class="form-control" name="final_time" id="final_time" placeholder="Event Time" value="{{ date_format(date_create($request_booking->event_time), 'h:i A') }}" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-time"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Event Title</label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="final_event_title" id="final_event_title" placeholder="Event Title" value="{{ $request_booking->event_title }}" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">No. of Heads</label>
								<div class="col-md-9">
									<input type="text" class="form-control number" name="final_no_of_heads" id="final_no_of_heads" placeholder="Number of Heads" value="{{ $request_booking->no_of_heads }}" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Preparation Venue</label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="final_venue" id="final_venue" placeholder="Preparation Venue" value="{{ $request_booking->venue }}" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Price</label>
								<div class="col-md-9">
									<input type="text" class="form-control number" name="final_price" id="final_price" placeholder="State your price for this event" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Notes</label>
								<div class="col-md-9">
									<textarea class="form-control" name="final_notes" id="final_notes" rows="3" placeholder="Notes"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3"></label>
								<div class="col-md-9">
									<button type="submit" id="confirm-booking-button" data-url="#" class="btn woo-btn-button">Confirm</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</form>

					</div>
					{{-- <div class="modal-footer">
						<button type="button" id="confirm-button" data-url="#" class="btn woo-btn-button">Confirm</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div> --}}
				</div>
			</div>
		</div>

		<div class="modal fade woo-modal" id="delete-modal">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header gold">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Please Confirm</h4>
					</div>
					<div class="modal-body">
						<strong>Are you sure you want to decline this booking?</strong>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
						<button type="button" id="decline-booking-button" class="btn woo-btn-button">Yes, decline</button>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/artist/views/booking/index.js') }}"></script>
	<script type="text/javascript">
		function deleteBooking(id){
			$('#decline-booking-button').attr('data-url', '/artist/booking/request/'+id+'/decline');
			$('#delete-modal').modal('show');
		}
	</script>
@stop
