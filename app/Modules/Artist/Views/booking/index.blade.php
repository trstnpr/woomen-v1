@extends('artistMaster')

@section('title')
	<title>Woomen - Bookings</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="artistBooking-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">My Bookings</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Bookings</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="card-panel">
					<h4 class="title">Booking Requests</h4>
					<hr/>
					<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				            	<th>#</th>
				                <th>Event</th>
				                <th>Date & Time</th>
				                <th>Venue</th>
				                <th>Client</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
					        @foreach($bookings as $book)
				        	<tr>
					        	<td>{{ $book->booking_id }}</td>
					        	<td>{{ $book->event_title }}</td>
					        	<td>{{ date_format(date_create($book->event_date.' '.$book->event_time), 'F d, Y h:i A') }}</td>
					        	<td>{{ $book->venue }}</td>
					        	<td>{{ $book->first_name.' '.$book->last_name }}</td>
					        	<td>
					        		<a href="{{ route('artist.booking.request', $book->booking_id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="View details"><i class="fa fa-eye"></i></a>
					        		<a onclick="declineBooking('{{$book->booking_id}}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="Decline"><i class="fa fa-times"></i></a>
					        	</td>
				        	</tr>
					        @endforeach
				        </tbody>
				    </table>
				</div>

				<div class="card-panel">
					<h4 class="title">Confirmed Bookings</h4>
					<hr/>
					<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				            	<th>#</th>
				                <th>Event</th>
				                <th>Date & Time</th>
				                <th>Venue</th>
				                <th>Client</th>
				                <th>Status</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@foreach($confirm_bookings as $conf)
				        	<tr>
					        	<td>{{ $conf->booking_id }}</td>
					        	<td>{{ $conf->event_title }}</td>
					        	<td>{{ date_format(date_create($conf->event_date.' '.$conf->event_time), 'F d, Y h:i A') }}</td>
					        	<td>{{ $conf->venue }}</td>
					        	<td>{{ $conf->first_name.' '.$conf->last_name }}</td>
					        	<td>
					        		<label class="label label-success">Confirmed</label>
					        	</td>
								<td>
					        		<a href="{{ route('artist.booking.booking', $conf->booking_id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="View details"><i class="fa fa-eye"></i></a>
					        		<a onclick="cancelBooking('{{$conf->booking_id}}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></a>
								</td>
				        	</tr>
				        	@endforeach
				        </tbody>
				    </table>
				</div>
			</div>
		</section>

		<div class="modal fade woo-modal" id="decline-modal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form method="POST" class="form-horizontal" id="decline-booking-form">
						<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<div class="modal-header gold">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Booking Decline</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<div class="col-md-12 alert alert-success hide" id="decline_result_div">
			                        <ul class="fa-ul">
			                            <li><i class="fa fa-li fa-check"></i> Success!</li>
			                            <li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
			                            <li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
			                        </ul>
			                    </div>
								<label class="col-md-12">Reason / Remarks</label>
								<div class="col-md-12">
									<textarea class="form-control" style="resize: none;" name="reason" id="reason" rows="3" placeholder="Reason for declining the book" required></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" id="decline-booking-button" class="btn woo-btn-button">Decline Booking</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal fade woo-modal" id="cancel-modal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form method="POST" class="form-horizontal" id="cancel-booking-form">
						<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<div class="modal-header gold">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Booking Cancel</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<div class="col-md-12 alert alert-success hide" id="cancel_result_div">
			                        <ul class="fa-ul">
			                            <li><i class="fa fa-li fa-check"></i> Success!</li>
			                            <li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
			                            <li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
			                        </ul>
			                    </div>
								<label class="col-sm-12">Reason / Remarks</label>
								<div class="col-md-12">
									<textarea class="form-control" style="resize: none;" name="reason" id="reason" rows="3" placeholder="Reason for cancelling the book" required></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" id="cancel-booking-button" class="btn woo-btn-button">Cancel Booking</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>

@stop()

@section('custom-scripts')

	<script src="{{ config('s3.bucket_link') . elixir('assets/artist/views/booking/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});

		function declineBooking(id){
			$('#decline-booking-form').attr('action', '/artist/booking/request/'+id+'/decline');
			$('#decline-modal').modal('show');
		}

		function cancelBooking(id){
			$('#cancel-booking-form').attr('action', '/artist/booking/request/'+id+'/cancel');
			$('#cancel-modal').modal('show');
		}
	</script>
@stop