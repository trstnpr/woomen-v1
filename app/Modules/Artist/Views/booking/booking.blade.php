@extends('artistMaster')

@section('title')
	<title>Woomen - Booking</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="artistBooking-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Booking</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Bookings</li>
					<li class="active">Booking</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="card-panel">
					<h4 class="title">Booking Details</h4>
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<tbody>
								<tr>
									<th width="20%">Event</th>
									<td>{{ $booking->event_title }}</td>
								</tr>
								<tr>
									<th>Date</th>
									<td>{{ date_format(date_create($booking->event_date), 'F d, Y') }}</td>
								</tr>
								<tr>
									<th>Time</th>
									<td>{{ date_format(date_create($booking->event_time), 'h:i A') }}</td>
								</tr>
								<tr>
									<th>Preparation Venue</th>
									<td>{{ $booking->venue }}</td>
								</tr>
								<tr>
									<th>No. of Heads</th>
									<td>{{ $booking->no_of_heads }}</td>
								</tr>
								<tr>
									<th>Call Time</th>
									<td>{{ $booking->call_time }}</td>
								</tr>
								<tr>
									<th>Client</th>
									<td>{{ $booking->first_name.' '.$booking->last_name }}</td>
								</tr>
								<tr>
									<th>Mobile</th>
									<td>{{ $booking->contact_no }}</td>
								</tr>
								<tr>
									<th>Email</th>
									<td>{{ $booking->email }}</td>
								</tr>
								<tr>
									<th>Message</th>
									<td>{{ $booking->remarks }}</td>
								</tr>
								<tr>
									<th>Status</th>
									<td>
										<label class="label label-success">Confirm</label>
									</td>
								</tr>
								{{-- <tr>
									<th>Action</th>
									<td>
						        		<a href="#delete-modal" data-toggle="modal" class="btn btn-default" title="Delete"><i class="fa fa-times"></i> Delete</a>
									</td>
								</tr> --}}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>

		<div class="modal fade woo-modal" id="delete-modal">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header gold">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Please Confirm</h4>
					</div>
					<div class="modal-body">
						By taking this action, you will not retrieve the data anymore.
						<strong>Are you sure?</strong>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
						<button type="button" id="confirm-button" data-url="{{ route('artist.event.delete') }}" class="btn woo-btn-button">Yes, delete</button>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop()

@section('custom-scripts')
	<script type="text/javascript"></script>
@stop