@extends('artistMaster')

@section('title')
	<title>Woomen - Packages</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')

	<div class="artistPackagelist-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">My Packages</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Packages</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="card-panel">
					<form method="POST" id="packagelist-form" action="{{ route('artist.packagelist.update') }}">
						<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						
						<div class="form-group packages-fields">
							<label>Write your package below</label>
							
							{{-- <div class="input-group">
								<input type="text" class="form-control input-lg" placeholder="Your packages" name="package[]" id="package" />
								<span class="input-group-btn">
									<button class="btn btn-warning btn-lg remove-package" type="button"><i class="fa fa-times"></i></button>
								</span>
							</div> --}}
							{{--@if($packages->count() != 0)
								@foreach($packages as $package)
									<div class="well">
										<div class="form-group">
											<label>Package</label>
											<input type="text" class="form-control" name="package[]" value="{{ $package->package_title }}" id="package" required />
										</div>
										<div class="form-group">
											<label>Description</label>
											<textarea class="form-control" rows="3" name="description[]" id="description" required >{{ $package->package_description }}</textarea>
										</div>
										<div class="form-group">
											<button class="btn btn-warning remove-package" type="button">REMOVE</button>
										</div>
									</div>
								@endforeach
							@else
								<div class="well">
									<div class="form-group">
										<label>Package</label>
										<input type="text" class="form-control" name="package[]" id="package" required />
									</div>
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control" rows="3" name="description[]" id="description" required ></textarea>
									</div>
									<div class="form-group">
										<button class="btn btn-warning remove-package" type="button">REMOVE</button>
									</div>
								</div>
							@endif --}}
							<div class="form-group">
								<textarea class="form-control wysiwyg" rows="3" name="package_description" id="package_description">{!! ($package) ? $package->package_description : '' !!}</textarea>
							</div>
						</div>

						{{--<div class="form-group">
							<button type="button" class="btn btn-default" id="add-package">ADD</button>
						</div> --}}

						<div class="col-md-12 alert alert-success hide" id="result_div">
		  					<ul class="fa-ul">
		  						<li><i class="fa fa-li fa-check"></i> Success!</li>
		  						<li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
		  						<li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
		  					</ul>
						</div>
						<div class="form-group">
							<button type="submit" id="packagelist-button" class="btn btn-lg woo-btn-submit">Save Changes</button>
						</div>
					</form>
				</div>

				<div class="card-panel packagelist">
					<label>Preview</label>
					@if($package)
					<p>{!! $package->package_description !!}</p>
					@endif
				</div>
				
			</div>
		</section>

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ elixir('assets/artist/views/packagelist/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// Packages Fields
			$('#add-package').click(function() {
				// ADD Packages
				$('.packages-fields').append('<div class="well"><div class="form-group"><label>Package</label><input type="text" class="form-control" name="package[]" id="package" required/></div><div class="form-group"><label>Description</label><textarea class="form-control" rows="3" name="description[]" id="description" required></textarea></div><div class="form-group"><button class="btn btn-warning remove-package" type="button">REMOVE</button></div></div>');

				// REMOVE Packages
				$('.remove-package').on('click', function() {
					$(this).parent().parent().remove();
				});
			});
		});
	</script>
@stop