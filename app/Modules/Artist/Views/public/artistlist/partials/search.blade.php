<div id="toggle-show-search">
	<i class="fa fa-search"></i>
</div>
<section class="search-header">
	<div class="col-md-3">
		<form class="search" method="GET">
			<div class="form-group">
				<div class="input-group">
					<input class="form-control" name="q" id="search" placeholder="Search for artist" value="{{ (isset($_REQUEST['q'])) ? $_REQUEST['q'] : '' }}" type="text">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-round gold">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-9 filter-search">
		<form>
			<div class="col-md-6 no-padding">
				<div class="form-group col-md-3">
					<select class="form-control selectpicker input-sm" name="forte[]" multiple>
						@foreach($specialty as $sp)
							@if(isset($myForte))
								@if($myForte == NULL)
									<option value="{{ $sp->specialty_id }}">{{ $sp->specialty_name }}</option>
								@else
								<option value="{{ $sp->specialty_id }}" {{ ($sp->specialty_id == in_array($sp->specialty_id, $myForte)) ? 'selected' : '' }}>{{ $sp->specialty_name }}</option>
								@endif
							@else
									<option value="{{ $sp->specialty_id }}">{{ $sp->specialty_name }}</option>
							@endif
						@endforeach
					</select> 
				</div>
				<div class="form-group col-md-4">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">&#8369;</span>
						<input type="number" class="form-control" placeholder="Quote" min="1" name="quote" value="{{ Request::get('quote') }}" />	
					</div>
				</div>
				<div class="form-group col-md-5">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-map-marker"></i></span>
						<input type="text" class="form-control" placeholder="Location" name="loc" value="{{ Request::get('loc') }}" />
					</div>
				</div>
			</div>
			<div class="col-md-4 no-padding">
				<div class="form-group col-md-6">
					<select class="form-control" name="order">
						<option value selected disabled>Order by:</option>
						<option value="asc">Ascending (A-Z)</option>
						<option value="desc">Descending (Z-A)</option>
					</select> 
				</div>	
				<div class="form-group col-md-6">
					<select class="form-control" name="date">
						<option value selected disabled>Order by date:</option>
						<option value="asc">New Artist</option>
						<option value="desc">Old Artist</option>
					</select> 
				</div>		
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<button class="btn btn-default btn-block gold">Filter</button>
				</div>
			</div>
		</form>	
		<div class="col-md-12">
			<button class="btn btn-default" id="toggle-hide-search">Close</button>
		</div>
	</div>
</section>









{{-- <div class="search-panel card-panel">
	<form class="search" method="GET">
		<div class="form-group">
			<label><i class="fa fa-fw fa-search"></i> Search</label>
			<div class="input-group">
				<input class="form-control" name="q" id="search" placeholder="Search for artist" value="{{ (isset($_REQUEST['q'])) ? $_REQUEST['q'] : '' }}" type="text">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-round gold">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</div>
	</form>
</div>

<div class="filter-panel card-panel">
	<form class="filter-search">
		<div class="form-group">
			<label><i class="fa fa-fw fa-navicon"></i>Filters</label>
			<ul class="nav nav-stacked filters" id="accordion">
				<li>
					<a data-toggle="collapse" data-parent="#accordion"  href="#forte">Forte <i class="fa fa-sort pull-right caret_show"></i></a>
					<ul class="list-unstyled filters_dp collapse in" id="forte">
						@foreach($specialty as $sp)
							<li class="checkbox">
								<label>
									@if(isset($myForte))
										@if($myForte == NULL)
										<input type="checkbox" name="forte[]" value="{{ $sp->specialty_id }}"/> {{ $sp->specialty_name }}
										@else
										<input type="checkbox" name="forte[]" value="{{ $sp->specialty_id }}" {{ ($sp->specialty_id == in_array($sp->specialty_id, $myForte)) ? 'checked' : '' }}/> {{ $sp->specialty_name }}
										@endif
									@else
										<input type="checkbox" name="forte[]" value="{{ $sp->specialty_id }}"/> {{ $sp->specialty_name }}
									@endif
								</label>
							</li>
						@endforeach
					</ul>
				</li>

				<li>
					<a data-toggle="collapse" data-parent="#accordion"  href="#sort">Sort by<i class="fa fa-sort pull-right caret_show"></i></a>
					<ul class="list-unstyled filters_dp collapse in" id="sort">
						<li class="checkbox">
							<label>
								<input type="radio" name="order" value="asc" {{ (Request::get('order') == 'asc') ? 'checked' : '' }}> Ascending (A-Z)
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="radio" name="order" value="desc" {{ (Request::get('order') == 'desc') ? 'checked' : '' }}> Descending (Z-A)
							</label>
						</li>
						<hr>
						<li class="checkbox">
							<label>
								<input type="radio" name="date" value="desc" {{ (Request::get('date') == 'desc') ? 'checked' : '' }}> New Artist
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="radio" name="date" value="asc" {{ (Request::get('date') == 'asc') ? 'checked' : '' }}> Old Artist
							</label>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="form-group">
			<label><i class="fa fa-money"></i> What's your Quotation?</label>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">&#8369;</span>
				<input type="number" class="form-control" placeholder="Quote" name="quote" value="{{ Request::get('quote') }}" />	
			</div>
		</div>
		<div class="form-group">
			<label><i class="fa fa-map-marker"></i> Location</label>
			<input type="text" class="form-control" placeholder="Location" name="loc" value="{{ Request::get('loc') }}" />
		</div>

				<div class="form-group">
			<button class="btn btn-default btn-block">Filter Selection</button>
		</div>
	</form>
</div>


 --}}