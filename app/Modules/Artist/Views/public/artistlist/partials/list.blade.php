<section class="artistlist-container">
	<div class="container-fluid" style="padding:0;">
		<div class="col-md-12" style="padding:0;">
			<div class="col-md-12">
				<h3 style="text-align:center;"><b><span>{{ $artistList->count() }}</span> artists found</b></h3>
			</div>
			<div class="artists-wrap">
				@foreach($artistList as $artist)
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<div class="artist-item">
							<div class="avatar-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $artist->profile_image }}');">
								<div class="overlay">
									<figure class="artist-avatar" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $artist->profile_image }}');"></figure>
								</div>
							</div>
							<div class="info">
								<input value="{{ $artist->ratings }}" class="rating" data-size="xxs" data-display-only="true"/>
								<h4 class="artist-name">{{ ucwords($artist->first_name).' '.ucwords($artist->last_name) }}</h4>
								@if($artist->artist_specialty->count() != 0)
								<h5 class="forte">{{ $artist->artist_specialty[0]->specialty->specialty_name . ((($result = $artist->artist_specialty->count() - 1) == 0) ? '' : ' and '.$result.' more' ) }}</h5>
								@else
								<h5 class="forte">No Specialty Given</h5>
								@endif
								<a href="{{ route('artist.list.show', $artist->artist_profile_id) }}" class="btn woo-btn btn-block btn-sm">VIEW PROFILE</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>		
	</div>
</section>	

{{-- {{ $artistList->appends(\Request::except('page'))->links() }} --}}
{{-- <ul class="pagination">
	<li class="active"><a href="#">1</a></li>
	<li><a href="#">2</a></li>
	<li><a href="#">3</a></li>
	<li><a href="#">4</a></li>
	<li><a href="#">5</a></li>
	<li><a href="#">Next</a></li>
</ul> --}}