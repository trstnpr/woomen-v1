<div class="events suggested-events">
	<div class="events-head">
		<h3 class="label-title">Suggested Events</h3>
	</div>
	<div class="events-wrap">
		@if($events->count() != 0)
		@foreach($events as $evt)
		<div class="event-item">
			<a href="{{ url('event/view') }}/{{$evt->artist_event_id}}" title="{{ $evt->event_title }}">
				<figure class="event-poster" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.event') .'/'. $evt->event_image }}');">
					<div class="hoverlay">
						<span class="event-name" style="color: white; background: #E9A816;padding:5px;">{{ $evt->event_title }}</span>
					</div>
				</figure>
			</a>
		</div>
		@endforeach
		@else
		<h4>No event to display</h4>
		@endif
	</div>
</div>