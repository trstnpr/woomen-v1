<div class="event single-event">
	<div class="card-panel">
		<div class="event-head">
			<h1 class="event-title">{{ $event->event_title }}</h1>
		</div>
		<div class="poster-wrap">
			<a href="{{ config('s3.bucket_link') . config('cdn.event') .'/'. $event->event_image }}" data-lity>
				<img src="{{ config('s3.bucket_link') . config('cdn.event') .'/'. $event->event_image }}" class="img-responsive poster" alt="{{ $event->event_title }}" />
			</a>
		</div>
		<div class="event-details">
			<h2 class="title">{{ $event->event_title }}</h2>
			<span class="datetime">{{ $event->event_date->format('F d, Y') .' '. date_format(date_create($event->event_time), 'h:i A') }}</span>
		</div>
	</div>

	<div class="card-panel">
		<div class="card-head">
			<h3 class="desc-label">Description</h3>
		</div>
		<div class="event-description">
			<p>{{ $event->event_description }}</p>
		</div>
	</div>

	<div class="card-panel">
		<div class="card-head">
			<h3 class="ticket-label">Ticket</h3>
		</div>
		<div class="form-group">
			<a class="btn woo-btn" href="#">{{ strtoupper($event->event_ticket) }}</a>
		</div>
	</div>
</div>