@extends('appMaster')

@section('title')
	<title>Woomen - Event</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')
	
	@include('App::universalHead')

	<div class="event-content">
		
		<section class="section section-event">
			
			<div class="container">
				
				<div class="row">
					
					<div class="col-md-9">
						
						@include('Artist::public.event.partials.single-event')

					</div>

					<div class="col-md-3">
						
						@include('Artist::public.event.partials.suggested-events')

					</div>

				</div>

			</div>

		</section>

	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
@stop