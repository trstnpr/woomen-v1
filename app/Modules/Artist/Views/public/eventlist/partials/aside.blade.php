<div class="search-panel card-panel">
	<form class="search" method="GET">
		<div class="form-group">
			<label><i class="fa fa-fw fa-search"></i> Search</label>
			<div class="input-group">
				<input class="form-control" name="q" id="search" placeholder="Search for events" value="{{ (isset($_REQUEST['q'])) ? $_REQUEST['q'] : '' }}" type="text">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-round gold">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</div>
	</form>
</div>

<div class="filter-panel card-panel">
	<form class="filter-search">
		<div class="form-group">
			<label><i class="fa fa-fw fa-navicon"></i>Filters</label>
			<ul class="nav nav-stacked filters" id="accordion">
				{{-- <li>
					<a data-toggle="collapse" data-parent="#accordion"  href="#month">Months <i class="fa fa-sort pull-right caret_show"></i></a>
					<ul class="list-unstyled filters_dp collapse in" id="month">
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="01"/> January
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="02"/> February
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="03"/> March
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="04"/> April
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="05"/> May
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="06"/> June
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="07"/> July
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="08"/> August
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="09"/> September
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="10"/> October
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="11"/> November
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="checkbox" name="month[]" value="12"/> December
							</label>
						</li>
					</ul>
				</li> --}}

				<li>
					<a data-toggle="collapse" data-parent="#accordion"  href="#ticket">Ticket <i class="fa fa-sort pull-right caret_show"></i></a>
					<ul class="list-unstyled filters_dp collapse in" id="ticket">
						<li class="checkbox">
							<label>
								<input type="radio" name="ticket" value="Free"> Free
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="radio" name="ticket" value="Door Sale"> Door Sale
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="radio" name="ticket" value="Buy Online"> Buy Online
							</label>
						</li>
					</ul>
				</li>

				<li>
					<a data-toggle="collapse" data-parent="#accordion"  href="#sort">Sort by<i class="fa fa-sort pull-right caret_show"></i></a>
					<ul class="list-unstyled filters_dp collapse in" id="sort">
						<li class="checkbox">
							<label>
								<input type="radio" name="order" value="asc"> Ascending (A-Z)
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="radio" name="order" value="desc"> Descending (Z-A)
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="radio" name="date" value="desc"> New Events
							</label>
						</li>
						<li class="checkbox">
							<label>
								<input type="radio" name="date" value="asc"> Old Events
							</label>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="form-group">
			<label><i class="fa fa-map-marker"></i> Venue</label>
			<input type="text" class="form-control" placeholder="Event Venue" name="venue"/>
		</div>
		<div class="form-group">
			<button class="btn btn-default btn-block">Filter Selection</button>
		</div>
	</form>
</div>