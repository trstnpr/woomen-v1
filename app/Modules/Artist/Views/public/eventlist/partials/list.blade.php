<div class="row">
	@if($events->count() != 0)
		@foreach($events as $evt)
	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="event-item">
			<a href="{{ url('event/view') }}/{{$evt->artist_event_id}}" title="{{ $evt->event_title }}">
				<figure class="event-poster" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.event') .'/'. $evt->event_image }}');">
					<div class="hoverlay">
						<span class="event-name">{{ $evt->event_title }}</span>
					</div>
				</figure>
			</a>
		</div>
	</div>
	@endforeach
	@else
	<h3 align="center">No Events Found</h3>
	@endif
</div>

{{ $events->appends(\Request::except('page'))->links() }}
{{-- <ul class="pagination">
	<li class="active"><a href="#">1</a></li>
	<li><a href="#">2</a></li>
	<li><a href="#">3</a></li>
	<li><a href="#">4</a></li>
	<li><a href="#">5</a></li>
	<li><a href="#">Next</a></li>
</ul> --}}