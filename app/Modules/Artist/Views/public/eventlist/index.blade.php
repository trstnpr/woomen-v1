@extends('appMaster')

@section('title')
	<title>Woomen - Browse Events</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	<style>
		* {
			font-family: Arial, Helvetica, sans-serif;
		}
    
	</style>
@stop

@section('content')

	@include('App::universalHead')

	<div class="eventlist-content">
		<section class="eventlist-container">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="aside">
							
							@include('Artist::public.eventlist.partials.aside')

						</div>
					</div>
					<div class="col-md-9">
						<div class="event-wrap">

						    @include('Artist::public.eventlist.partials.list')

						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.filter-search').submit(function(){
				if($('input[name="venue"]').val() == ''){
					$('input[name="venue"]').remove();
				}
			});
		});
	</script>
@stop