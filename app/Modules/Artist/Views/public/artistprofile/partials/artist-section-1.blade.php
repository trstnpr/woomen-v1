<section class="section-1 profile-cover parallax" data-parallax="scroll" data-image-src="{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $artist->profile_image }}">
	<div class="overlay" style="background-image:url('{{ asset('images/grid-60.png') }}');">
		<div class="container">
			<div class="dp-container">
				<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $artist->profile_image }}');"></figure>
			</div>

			<div class="social-action">
                <span class="fa-stack fa-lg">
                    <div class="fb-share-button" data-href="{{ env('APP_URL') }}/artist/view/{{ $artist->artist_profile_id }}" data-layout="button" data-size="small"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(env('APP_URL')) }}%2Fartist%2Fview%2F{{ $artist->artist_profile_id }}&amp;src=sdkpreparse">Share</a></div>
                </span>               
                <span class="fa-stack fa-lg">
                    <a target="_blank">
                        <i class="fa fa-diamond fa-lg {{ $diamond_color }}" data-toggle="tooltip" data-placement="top" title="{{ $diamond_count }} DIAMONDS"></i>
                    </a>
                </span>               
                <span class="fa-stack fa-lg">
                        <i class="fa fa-star fa-lg" data-toggle="tooltip" data-placement="top" title="{{ $ratings }} STARS"></i>
                </span>
			</div>

		</div>
	</div>
</section>

<!-- Review Modal -->
<div class="modal fade woo-modal" id="review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form method="POST" id="review-form" action="{{ route('artist.review.post', $artist->artist_profile_id) }}">
            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
            <div class="modal-content">
                <div class="modal-header gold">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Write Review/Feedback</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="review_name" placeholder="Your Name" required />
                    </div>
                    <div class="form-group">
                        <label>Email (we do not publish this in public)</label>
                        <input type="email" class="form-control" name="review_email" placeholder="Your Email Address" required />
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="review_title" placeholder="Title" required />
                    </div>
                    <div class="form-group">
                        <label>Review</label>
                        <textarea class="form-control" name="review_description" rows="4" placeholder="Your Review/Feedback" required ></textarea>
                    </div>
                    <div class="form-group">
                        <label>Rate Artist</label>
                        <input type="text" name="review_rating" value="" id="star" class="rating" data-size="xs" data-show-clear="false" data-min="0" data-max="5" data-step="0.1" />
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-success hide" id="review_result_div">
                            <ul class="fa-ul">
                                <li><i class="fa fa-li fa-check"></i> Success!</li>
                                <li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
                                <li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn woo-btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" id="review-button" class="btn woo-btn">Submit Review</button>
                </div>
            </div>
        </form>
    </div>
</div>