<section class="section-3 photos-updates">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h2 class="title" align="center">Featured Photos</h2>
				<div class="subsection main-side">
					<div class="artistPhoto-wrap">
							@if($featured->count() == 0)
								<h2 class="sub-title" align="center">No featured photos</h2>
							@else
						<div class="artistPhoto-carousel row">
								@foreach($featured as $feat)
									<div class="col-md-12">
										<a href="{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $feat->image_path }}" data-lity>
											<figure class="artistPhoto-item view-item" data-image-src="{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $feat->image_path }}" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $feat->image_path }}');">
												<div class="hoverlay">
													<span class="photo-caption">View full image</span>
												</div>
											</figure>
										</a>
									</div>
								@endforeach
						</div>
							@endif
					</div>

					<div class="portfolio">
						<h2 class="title" align="center">Portfolio</h2>
						
						<div class="album">
							<h4 class="sub-title">PHOTOS</h4>
							
							@if($albums->count() == 0)
								<h3 align="center">No albums available</h3>
							@else
								@foreach($albums as $a)
								<div class="album-gallery">
									
									<span class="album-title">{{ $a->album_name }}</span>
										
									<div class="albumPhoto-carousel row">	
										@if($a->artist_images->count() != 0)
											@foreach($a->artist_images as $img)
											<div class="col-md-12">
												<a href="{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $img->image_path }}" data-lity>
													<figure class="album-item view-item" data-image-src="{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $img->image_path }}" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $img->image_path }}');">
														<div class="hoverlay">
															<span class="album-title">{{ $img->image_name }}</span>
														</div>
													</figure>
												</a>
											</div>
											@endforeach
										@else
										<h3 align="center">No images in this album</h3>
										@endif
									</div>
								</div>
								@endforeach
							@endif
						</div>
						<div class="video">
							<h4 class="sub-title">VIDEO</h4>
							<div class="row">
								@if($portfolio->count() == 0)
									<h3 align="center">No videos available</h3>
								@else
									@foreach($portfolio as $video)
										<div class="col-md-6">
											<div class="video-item">
												{{-- <iframe class="vid" src="{{ $video->file_path }}" frameborder="0" allowfullscreen=""></iframe> --}}
												<figure class="vid" style="background-image:url('http://img.youtube.com/vi/{{ $video->embed_code }}/0.jpg');">
													<a href="{{ $video->file_path }}" title="Play" data-lity>
														<i class="fa fa-youtube-play fa-3x"></i>
													</a>
												</figure>
											</div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>

					<div class="event">
						<h2 class="title" align="center">Events</h2>
						<div class="event-wrap">
							<div class="row">
								@if($events->count() == 0)
									<h3 align="center">No events available</h3>
								@else
									@foreach($events as $evt)
										<div class="col-md-4">
											<a>
												<figure class="event-item" data-event-title="{{ $evt->event_title }}" data-event-description="{{ $evt->event_description }}" data-event-venue="{{ $evt->event_venue }}" data-event-date="{{ $evt->event_date->format('F d, Y') }}" data-event-time="{{ date_format(date_create($evt->event_time), 'h:i A') }}" data-event-ticket="{{ $evt->event_ticket }}" data-event-image="{{ config('s3.bucket_link') . config('cdn.event') .'/'. $evt->event_image }}" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.event') .'/'. $evt->event_image }}');" data-toggle="modal" data-target="#viewEvent">
													<div class="hoverlay">
														<span class="event-title">{{ $evt->event_title }}</span>
													</div>
												</figure>
											</a>
										</div>
									@endforeach
								@endif
							</div>
						</div>

					</div>

					<div class="artist-review">
						<h2 class="title">Reviews about the artist</h2>
						
						<div class="review-wrap">
						@if($reviews->count() == 0)
								<h3 align="center">No reviews available</h3>
							@else
								@foreach($reviews as $rev)
									<div class="media review-item">
										<div class="media-left">
											<a href="#">
												<img class="media-object img-circle" src="{{ url('images/review') }}/review%201.png" alt="...">
											</a>
										</div>
										<div class="media-body">
											<div class="card-panel">
												<h4>{{ $rev->review_title }} <small class="pull-right"><input value="{{ $rev->review_rating }}" class="rating" data-size="xxs" data-display-only="true"/></small></h4>
												<i class="fa fa-quote-left"></i> {{ $rev->review_description }}
											</div>
											<h3 class="media-heading">{{ $rev->review_name }}, <span class="date">{{ $rev->created_at->format('m/d/Y') }}</span></h3>
										</div>
									</div>
								@endforeach
							@endif
						</div>

					</div>

				</div>
			</div>

			<div class="col-md-3">
				<div class="subsection small-side">
					@foreach($update as $u)
					<div class="updates card-panel">
						<span class="card-title">{{ $u->update_title }}</span>
						<p>{{ $u->update_desc }}</p>
					</div>
					@endforeach
					<div class="ad-space">
						<script type='text/javascript'><!--// <![CDATA[
					    /* [id63] woomen.ph - Default */
					    OA_show(63); // ]]> --></script><noscript><a target='_blank' href='http://ads.devhub.ph/revive/www/delivery/ck.php?n=c64578e'><img border='0' alt='' src='http://ads.devhub.ph/revive/www/delivery/avw.php?zoneid=63&amp;n=c64578e' /></a></noscript>​
					    <br/>
<!--/*
  *
  * Revive Adserver Asynchronous JS Tag
  * - Generated with Revive Adserver v3.2.4
  *
  */-->

<ins data-revive-zoneid="64" data-revive-id="8ef7760561e74586c7fffc1e9d039a83"></ins>

<script async src="//ads.devhub.ph/revive/www/delivery/asyncjs.php"></script>
					</div>
				</div>
			</div>

		</div>
	</div>

</section>