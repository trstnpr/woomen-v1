<section class="section-2 basic-info">
    <div class="container">
        <div class="name-skill">
            <h2 class="name gold-text">{{ ucfirst($artist->first_name).' '.ucfirst($artist->last_name) }}</h2>
            @if($artist->artist_specialty->count() != 0)
            <h4 class="skill-city">{{ $artist->artist_specialty[0]->specialty->specialty_name . ((($result = $artist->artist_specialty->count() - 1) == 0) ? '' : ' and '.$result.' more' ) }}</h4>
            @else
            <h4 class="skill-city">No Specialty Given</h4>
            @endif
            <h4 class="skill-city">{{ ($artist->address != null) ? $artist->address : 'No Address Given' }}</h4>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="form-group">
                    <button type="button" class="btn btn-lg btn-block woo-btn" id="toggle-pricelist">View Packages</a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    @if($booking < $artist->artist_user_level->bookings)
                    <button type="button" class="btn btn-lg btn-block woo-btn" id="toggle-book">Request for Booking</button>
                    @else
                    <button type="button" class="btn btn-lg btn-block woo-btn" data-toggle="tooltip" data-placement="top" title="Booking is not available to this Artist">Request for Booking</button>
                    @endif
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        
        <hr class="line-divider" />

        
        <div class="jumbotron pricelist">
            @if($package)
            <p>{!! $package->package_description !!}</p>
            @endif
        </div>
        <div class="jumbotron booking">
            <h3 class="title">Request for Booking</h3>
            <form id="request-booking-form" method="POST" action="{{ route('artist.booking.store', $artist->artist_profile_id) }}">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Date</label>
                            {{-- <input type="date" class="form-control input-lg" name="event_date" id="event_date" /> --}}
                            <div class="input-group datePicker">
                                <input type="text" class="form-control input-lg" name="event_date" id="event_date" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Time</label>
                            {{-- <input type="time" class="form-control input-lg" name="event_time" id="event_time" /> --}}
                            <div class="input-group timePicker">
                                <input type="text" class="form-control input-lg" name="event_time" id="event_time" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Event Title</label>
                            <input type="text" class="form-control input-lg" name="event_title" id="event_title" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Preparation Venue</label>
                            <input type="text" class="form-control input-lg" name="venue" id="venue" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No of Heads</label>
                            <input type="text" class="form-control input-lg" name="no_of_heads" id="no_of_heads" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Call Time</label>
                            {{-- <input type="time" class="form-control input-lg" name="call_time" id="call_time" /> --}}
                            <div class="input-group timePicker">
                                <input type="text" class="form-control input-lg" name="call_time" id="call_time" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control input-lg" name="first_name" id="first_name" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control input-lg" name="last_name" id="last_name" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="text" class="form-control input-lg" name="contact_no" id="contact_no" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control input-lg" name="email" id="email" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control input-lg" rows="3" name="remarks" id="remarks"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 alert alert-success hide" id="result_div">
                        <ul class="fa-ul">
                            <li><i class="fa fa-li fa-check"></i> Success!</li>
                            <li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
                            <li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" id="request-booking-button" class="btn woo-btn woobook-submit btn-lg">Submit <i class="fa fa-paper-plane"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="about-artist">
                    <h2 class="title">About the Artist</h2>
                    <p class="about-artist-text">
                    @if($artist->about != null)
                        {!! $artist->about !!}
                    @else
                        <h3>Not yet updated</h3>
                    @endif
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="affiliation-artist">
                    <h2 class="title">Affiliation / Certificates / Schools</h2>
                    <ul class="list-unstyled affiliation-list">
                        @if($artist->artist_affiliations->count() != 0)
                            @foreach($artist->artist_affiliations as $aff)
                                <li class="affiliation-text">{{ $aff->affiliation }}</li>
                            @endforeach
                        @else
                            <h3>Not yet updated</h3>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>