<div class="section section-modal">
	<div class="modal fade" id="viewPhoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	          <img src="#" style="width:100%;" id="photo-src">
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="viewEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog  modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
		      	<div class="row">
		      		<div class="col-md-6">
		          		<img src="#" style="width:100%;" class="img-responsive" id="event-photo-src">
		          	</div>
			        <div class="col-md-6">
		          		<h2 id="event-title">Parokya ni edga inuman session</h2>
		          		<hr/>
		          		<p id="event-description"></p>
		          		<h5><span id="event-date"></span>, <span id="event-time"></span> <span id="event-venue"></span></h5>
		          		<h5>Ticket will be <SpaN id="event-ticket"></SpaN></h5>
			        </div>
		      	</div>
		      </div>
	    </div>
	  </div>
	</div>
</div>