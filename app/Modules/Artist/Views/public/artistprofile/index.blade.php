@extends('appMaster')

@section('title')
	<title>Woomen</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
	<meta property="fb:app_id"        content="{{ env('FACEBOOK_APP_ID') }}">
	<meta property="og:url"           content="{{ env('APP_URL') }}/artist/view/{{ $artist->artist_profile_id }}" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="{{ $artist->first_name.' '.$artist->last_name.' - Woomen' }}" />
	<meta property="og:description"   content="{{ $artist->about }}" />
	<meta property="og:image"         content="{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $artist->profile_image }}" />
	<meta property="og:image:width"   content="1200">
	<meta property="og:image:height"  content="630">
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')
	
	@include('App::app.partials.appHeader')

	<div class="artist-content">
		
		@include('Artist::public.artistprofile.partials.artist-section-1')

		@include('Artist::public.artistprofile.partials.artist-section-2')

		@include('Artist::public.artistprofile.partials.artist-section-3')

		@include('Artist::public.artistprofile.partials.artist-modal-section')
	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#toggle-pricelist').click(function() {
				$('.pricelist').toggle(500, 'linear');
				if($('.pricelist').is(":visible")){
					$("body, html").animate({ 
				      scrollTop: $( '.pricelist' ).offset().top - 100
				    }, 600);
				}
			});
			$('#toggle-book').click(function() {
				$('.booking').toggle(500, 'linear');
				if($('.booking').is(":visible")){
					$("body, html").animate({ 
				      scrollTop: $( '.booking' ).offset().top - 100
				    }, 600);
				}
			});
			$('[rel="tooltip"]').tooltip(); 
			
			$(function () {
				$('[data-toggle="tooltip"]').tooltip()
			})

		});
	</script>
	<script src="{{ elixir('assets/artist/views/booking/index.js') }}"></script>
	<script src="{{ elixir('assets/artist/views/review/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>

	<script type="text/javascript">
		
		$(document).ready(function(){

			$(".view-item").click(function(){
				var src = $(this).attr("data-image-src");
				$("#photo-src").attr('src',src);
			});

			$(".view-item").click(function(){
				var src = $(this).attr("data-image-src");
				$("#photo-src").attr('src',src);
			});


			$(".event-item").click(function(){
				var image = $(this).attr('data-event-image');
				$("#event-photo-src").attr('src',image);
				var title = $(this).attr('data-event-title');
				$("#event-title").html(title);
				var desc = $(this).attr('data-event-description');
				$("#event-description").html(desc);
				var time = $(this).attr('data-event-time');
				$("#event-time").html(time);
				var date = $(this).attr('data-event-date');
				$("#event-date").html(date);
				var ticket = $(this).attr('data-event-ticket');
				$("#event-ticket").html(ticket);
				var venue = $(this).attr('data-event-venue');
				$("#event-venue").html(venue);
			});			
					
		});


        lightGallery(document.getElementById('lightgallery'));

	</script>

@stop