@extends('appMaster')

@section('title')
	<title>Review Artist</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')
	
	{{-- @include('App::app.partials.appHeader') --}}

	<div class="artistReview-content">
		
		<div class="container">
			
			<section class="section review-artist">
				
				<div class="artist-cover bg-data" data-image-src="{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $profile->profile_image }}">
					<div class="overlay bg-data" data-image-src="{{ asset('images/grid-60.png') }}">
						<figure class="avatar bg-data" data-image-src="{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $profile->profile_image }}"></figure>
						<ul class="info list-unstyled">
							<li class="name">{{ $profile->first_name.' '.$profile->last_name }}</li>
							@if($profile->artist_specialty->count() > 0)
							<li class="forte">{{ $profile->artist_specialty[0]->specialty->specialty_name . ((($result = $profile->artist_specialty->count() - 1) == 0) ? '' : ' and '.$result.' more' ) }}</li>
							@endif
							<br>
							<li class="button"><a href="{{ route('artist.list.show', $profile->artist_profile_id) }}" class="btn woo-btn">VIEW PROFILE</a></li>
						</ul>
					</div>
				</div>

				<div class="review-head gold" style="padding:20px;text-align:center;">
					<h1 class="page-title">Write Your Review</h1>
				</div>

				<div class="card-panel review">

					<p class="page-text">We value the feedback from our customers. Your review will help the artist build reputation and help others learn about ous business.</p>

					<hr/>

					<div class="form-wrapper">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<form class="form-horizontal" method="POST" id="review-form" action="{{ route('artist.review.post', $profile->artist_profile_id) }}">
									<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
									<div class="form-group">
										<label class="col-sm-2 control-label">Name</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="review_name" id="review_name" placeholder="Your Name">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Email (we do not publish this in public)</label>
										<div class="col-sm-10">
											<input type="email" class="form-control" name="review_email" id="review_email" placeholder="Your Email Address">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Rating</label>
										<div class="col-sm-10">
											<input id="input-7-xs" name="review_rating" class="rating" value="0" data-min="0" data-max="5" data-step="1" data-size="xs" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Title</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="review_title" id="review_title" placeholder="Review Title">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Description</label>
										<div class="col-sm-10">
											<textarea class="form-control" name="review_description" id="review_description" rows="3" placeholder="Review Description"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label"></label>
										<div class="col-sm-2">
											<button type="submit" id="review-button" class="btn woo-btn">Submit</button>
										</div>
										<div class="col-md-8">
											<div class="alert alert-success hide" id="review_result_div">
					                            <ul class="fa-ul">
					                                <li><i class="fa fa-li fa-check"></i> Success!</li>
					                                <li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
					                                <li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
					                            </ul>
					                        </div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

			</section>

		</div>

	</div>

@stop()

@section('footer')
	{{-- @include('App::app.partials.footer') --}}
@stop()

@section('custom-scripts')
	<script src="{{ elixir('assets/artist/views/review/index.js') }}"></script>
@stop