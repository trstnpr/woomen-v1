		<section class="section" id="tab-panel-1">
			<div class="container-fluid">
				<div class="card-panel recent-items">
					<h4>Recent Added Items</h4>
					<hr/>
					<div class="row">
						<div class="recent-added-items">
							@foreach($items as $i)
<!-- 							<div class="col-md-12">
								<a href="/artist/item/show/{{ $i->item_id }}">
									<div class="item-wrap">
										<img src="{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->feat_image['image_path'] }}" class="img-responsive">
										<h5>{{ $i->item_name }}</h5>
										<p>{{ strip_tags(str_limit($i->item_description, 45)) }}</p>
									</div>
								</a>
							</div> -->
							<div class="col-md-12">
								<a href="{{ url('item/browse')}}/{{ $i->item_id }}">
									<div class="merchandise-item">
										<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->feat_image['image_path'] }}');"></figure>
										<div class="item-info">
											<h4 class="item-name">{{ $i->item_name }}</h4>
											<span class="item-price">{{ 'Php '.number_format($i->item_price, 2) }}</span>
										</div>
									</div>
								</a>
							</div>							
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section" id="tab-panel-1">
			<div class="container-fluid">
				<div class="alert alert-success hide" id="result_d">
					Successfully Deleted!
				</div>

				<div class="card-panel">
					<h4>All Items</h4>
					<a href="{{ route('artist.item.create') }}"><button class="woo-btn-submit btn"><i class="fa fa-plus"></i> Add Item</button></a>
					<hr/>
					<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th>Item Name</th>
				                <th>Description</th>
				                <th>Item Contact</th>
				                <th>Quantity</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@foreach($all_items as $item)
				            <tr>
				                <td>{{ $item->item_name }}</td>
				                <td>{!! str_limit($item->item_description, 50)!!}</td>
				                <td>{{ $item->item_contact }}</td>
				                <td>{{ $item->item_quantity }}</td>
				                <td>
				                	<a href="/item/browse/{{ $item->item_id }}" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>
				                	<a href="/artist/item/edit/{{ $item->item_id }}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o" data-id="{{ $item->item_id }}"></i></a>					
				                	<button id="btn-delete-item" type="button" class="btn btn-default btn-xs" title="Delete" data-toggle="modal" data-target="#delete-item"
				                        onclick="deleteItem(this)"
				                        data-item-id="{{ $item->item_id }}">
				                        <i class="fa fa-trash"></i>
				                    </button>
				                </td>
				            </tr>
				            @endforeach
				        </tbody>
				    </table>
				</div>
			</div>
		</section>

	    <div class="modal fade woo-modal" id="delete-item">
	    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header gold">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Please Confirm</h4>
					</div>
					<div class="modal-body">
						By taking this action, you will not retrieve the data anymore.
						<strong>Are you sure?</strong>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
						<button type="button" id="confirm-button" data-url="#" class="btn woo-btn-button">Yes, delete</button>
					</div>
				</div>
			</div>
		</div>
