		<section class="section-content item-form">
			<div class="container">
				
				<div class="form-group">
					<label>Upload Photos</label>
					{{-- <form action="/file-upload" class="dropzone">
						<div class="fallback">
							<input name="file" type="file" multiple />
						</div>
					</form> --}}
				</div>

				<form id="frm_update_item">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="card-panel">
						<h4>Edit Item</h4>
						<hr/>
						<div class="alert alert-success hide" id="result_d">
						    Successfully Updated!
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="dropzone" id="myAwesomeDropzone">
										<div class="fallback">
											<input type="file" name="image_path[]" id="files" multiple="multiple"/>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									{{-- <label>Choose Images</label>
									<div class="well">
										<input type="file" name="image_path[]" id="files" multiple="multiple" />
									</div>
									<label><button type="button" class="btn btn-warning btn-xs" id="btn-clear">Clear</button></label>
									<div class="file-wrap">
										<div class="row" id="item-container"></div>
									</div> --}}
									<label>Current Images</label>
									<div class="itemimg-wrap">
										<div class="row">
											@foreach($item_images as $image)
											<div class="col-md-2">
												<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $image->image_path }}');">
													<div class="overlay">
														<a href="#!" type="button" data-id="{{ $image->item_image_id }}" class="btn btn-warning btn-xs delete_item_button"><i class="fa fa-times"></i></a>
													</div>
												</figure>
											</div>
											@endforeach
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Item Name</label>
									<input type="text" class="form-control input-lg" name="item_name" id="item_name" placeholder="Item Name" value="{{ $item['item_name'] }}" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Item Price</label>
								   	<div class="input-group">
								     	 <div class="input-group-addon">₱</div>
								      	<input type="text" class="form-control input-lg" id="InputAmount" name="item_price" placeholder="Amount" value="{{ $item['item_price'] }}">
								    </div>
								</div>  
							</div>
							<div class="col-md-3">
								<div class="fomr-group">
									<label>Item Quantity</label>
									<input type="text" class="form-control input-lg" name="item_quantity" id="item_quantity" placeholder="Item Quantity" value="{{ $item['item_quantity'] }}" />
								</div>
							</div>
							{{-- <div class="col-md-6">
								<div class="form-group">
									<label>Item Contact</label>
									<input type="text" class="form-control input-lg" name="item_contact" id="item_contact" placeholder="Item Contact" value="{{ $item['item_contact'] }}" />
								</div>
							</div> --}}
							<div class="col-md-12">
								<div class="form-group">
									<label>Item Description</label>
									<textarea class="form-control wysiwyg" rows="3" name="item_description" id="item_description" placeholder="Item Description" >{!! $item['item_description'] !!}</textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" class="btn btn-lg woo-btn" id="btn_edit">Save Changes</button>
								</div>
							</div>
						</div>
					</div>
				</form>
		</section>

		<input type="hidden" id="item_id" value="{{ $item['item_id'] }}">