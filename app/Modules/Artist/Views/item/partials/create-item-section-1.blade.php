		<section class="section-content item-form">
			<div class="container-fluid">
				<form id="frm_add_item" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="card-panel">
						<h4>Item Details</h4>
						<hr/>
						<div class="alert alert-success hide" id="result_d">
					   	 	Successfully Added!
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="dropzone" id="myAwesomeDropzone">
										<div class="fallback">
											<input type="file" name="image_path[]" id="files" multiple="multiple" required/>
										</div>
									</div>
								</div>
							</div>
							{{-- <div class="col-md-12">
								<div class="form-group">
									<label>Choose Image</label>
									<div class="well">
										<input type="file" name="image_path[]" id="files" multiple="multiple" required/>
									</div>
									<button type="button" class="btn btn-warning btn-xs" id="btn-clear">Clear</button>
									<div class="file-wrap">
										<div class="row" id="item-container"></div>
									</div>
								</div>
							</div> --}}
							<div class="col-md-6">
								<div class="form-group">
									<label>Item Name</label>
									<input type="text" class="form-control input-lg" name="item_name" id="item_name" placeholder="Item Name" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Item Price</label>
								   	<div class="input-group">
								     	 <div class="input-group-addon">₱</div>
								      	<input type="text" class="form-control input-lg" id="exampleInputAmount" name="item_price" placeholder="Amount">
								    </div>
								</div>  
							</div>
							<div class="col-md-3">
								<div class="fomr-group">
									<label>Item Quantity</label>
									<input type="text" class="form-control input-lg" name="item_quantity" id="item_quantity" placeholder="Item Quantity" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Item Description</label>
									<textarea class="form-control wysiwyg" rows="3" name="item_description" id="item_description" placeholder="Item Description" ></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" class="btn btn-lg woo-btn-submit" id="btn_create">Create Item</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</section>