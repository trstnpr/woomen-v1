@extends('artistMaster')

@section('title')
	<title>Woomen - Items</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" />	
	<link href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" rel="stylesheet" />	
	<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet" />	
@stop

@section('content')

	<div class="artistDashboard-content">
		<section class="section page-head">
			<div class="container-fluid">
				<h2 class="title">My Items</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Items</li>
				</ol>
			</div>
		</section>
			@include('Artist::item.partials.items-section-1')
	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	
	<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>	

	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick.min.js') }}"></script>
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick-init.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/artist/views/item/index.js') }}"></script>
	<script type="text/javascript">
		$('.dataTable').DataTable({
		    responsive: true
		});
	</script>
	<script type="text/javascript">
		function viewItem(item) {
	        $('#name').html($(item).attr('data-item-name'));
	        $('#description').html($(item).attr('data-item-description'));
	        $('#contact').html($(item).attr('data-item-contact'));
	        $('#quantity').html($(item).attr('data-item-quantity'));
	        $('#image').html($(item).attr('data-item-image'));

	        var item_id = $(item).attr('data-item-id');

	        $.get('/artist/item/images/'+item_id, function(data){
		        if(data.result) {
		            var output = [];
	                    $.each(data.message, function(key, value){
	                        console.log(value.image_path);
	                    });
		        }
		    });
	    }

	    function deleteItem(item) {
	        $('#confirm-button').attr('data-item-id', $(item).attr('data-item-id'));
	    }
	</script>
@stop