@extends('artistMaster')

@section('title')
	<title>Woomen - Items</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')

	<div class="artistItem-content">
		<section class="section page-head">
			<div class="container-fluid">
				<h2 class="title">Create Item</h2>
				<ol class="breadcrumb">
					<li class="active">Account</li>
					<li class="active">My Items</li>
					<li class="active">Create Item</li>
				</ol>
			</div>
		</section>
		@include('Artist::item.partials.create-item-section-1')
	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/artist/views/item/index.js') }}"></script>
@stop