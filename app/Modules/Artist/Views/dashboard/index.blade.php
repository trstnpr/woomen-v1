@extends('artistMaster')

@section('title')
	<title>Woomen - Artist</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/artist/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')

	<div class="artistDashboard-content">

		@include('Artist::dashboard.partials.dashboard-section-1')

		@include('Artist::dashboard.partials.dashboard-section-2')

		@include('Artist::dashboard.partials.dashboard-section-3')

		@include('Artist::dashboard.partials.dashboard-section-4')

		@include('Artist::dashboard.partials.dashboard-section-5')

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	<script type="text/javascript"></script>
@stop