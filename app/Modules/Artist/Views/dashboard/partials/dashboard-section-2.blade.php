<section class="section-2 featured-photo">
	<div class="container-fluid">
		<div class="featPhoto-wrap">
			<div class="row">
				<h3 class="text-header">Featured Photos</h3>
			</div>
			<div class="featPhoto-carousel row">
				@if($featured->count() == 0)
						<h5 class="placeholder">No featured photos</h5>
				@else
					@foreach($featured as $feat)
						<div class="col-md-12">
							<a href="{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $feat->image_path }}" data-lity>
								<figure class="artistPhoto-item" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $feat->image_path }}');">
									{{-- <div class="hoverlay">
										<span class="photo-caption">Photo 1</span>
									</div> --}}
								</figure>
							</a>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div>
</section>