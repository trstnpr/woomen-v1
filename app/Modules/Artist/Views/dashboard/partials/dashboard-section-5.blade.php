<section class="section-5 event">
	<div class="container-fluid">
		<h2 class="title" align="center">Events</h2>
		<div class="event-wrap">
			<div class="row">
				@if($events->count() == 0)
					<h4 align="center">No Events Available</h4>
				@else
					@foreach($events as $evt)
						<div class="col-md-3">
							<a href="{{ route('event.view', $evt->artist_event_id) }}" target="_blank">
								<figure class="event-item" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.event') .'/'. $evt->event_image }}');">
									<div class="hoverlay">
										<span class="event-title">{{ $evt->event_title }}</span>
									</div>
								</figure>
							</a>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div>
</section>
