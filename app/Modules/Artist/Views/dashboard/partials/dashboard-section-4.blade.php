<section class="section-4 portfolio">
	<div class="container-fluid">
		<h2 class="title" align="center">Portfolio</h2>
		<div class="album">
			<h4 class="sub-title">ALBUMS</h4>
			<div class="row">
				@if($albums->count() != 0)
					@foreach($albums as $a)
						<div class="col-md-4">
							<a href="{{ route('artist.portfolio.album.show', $a->artist_album_id) }}">
								@if($a->album_cover)
								<figure class="album-item" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $a->album_cover->image_path }}');">
									<div class="overlay">
										<span class="album-title">{{ $a->album_name }}</span>
									</div>
								</figure>
								@else
								<figure class="album-item" style="background-color: #8c8989;">
									<div class="hoverlay">
										<i class="fa fa-photo album-icon"></i><br/>
										<span class="album-title">{{ $a->album_name }}</span>
									</div>
								</figure>
								@endif
							</a>
						</div>
					@endforeach
				@else
					<h4 align="center" class="placeholder">No albums available</h4>
				@endif
			</div>
		</div>
		<div class="video">
			<h4 class="sub-title">VIDEOS</h4>
			<div class="row">
				@if($portfolio->count() == 0)
					<h4 align="center" class="placeholder">No videos available</h4>
				@else
					@foreach($portfolio as $video)
						<div class="col-md-4">
							<div class="video-item">
								<figure class="vid" style="background-image:url('http://img.youtube.com/vi/{{ $video->embed_code }}/0.jpg');">
									<a href="{{ $video->file_path }}" title="Play" data-lity>
										<i class="fa fa-youtube-play fa-3x"></i>
									</a>
								</figure>
							</div>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div>
</section>