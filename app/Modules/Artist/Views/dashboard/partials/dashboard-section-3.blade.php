<section class="section-3 more-info">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<div class="about-artist">
					<h2 class="title">About me</h2>
					<p class="about-text">{{ $profile->about }}</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="affiliation-artist">
                    <h2 class="title">Affiliation / Certificates / Schools</h2>
                    {{-- <p class="affiliation-text">{{ $profile->achievements }}</p> --}}
                    <ul class="list-unstyled affiliation-list">
                    	@if($profile->artist_affiliations->count() != 0)
                    		@foreach($profile->artist_affiliations as $aff)
                        		<li class="affiliation-text">{{ $aff->affiliation }}</li>
                    		@endforeach
                    	@endif
                    </ul>
                </div>
			</div>
		</div>
	</div>
</section>