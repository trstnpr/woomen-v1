<section class="section-1 profile-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $profile->profile_image }}');">
	<div class="overlay" style="background-image:url('{{ asset('images/grid-60.png') }}');">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2 col-sm-4 col-xs-12">
					<div class="dp-container">
						<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $profile->profile_image }}');"></figure>
					</div>
				</div>
				<div class="col-md-10 col-sm-8 col-xs-12">
					<div class="basic-info">
						<ul class="list-unstyled">
							<li class="artist">{{ $profile->first_name.' '.$profile->middle_name.' '.$profile->last_name }}</li>
							@if($profile->artist_specialty->count() > 0)
							<li class="forte">{{ $profile->artist_specialty[0]->specialty->specialty_name . ((($result = $profile->artist_specialty->count() - 1) == 0) ? '' : ' and '.$result.' more' ) }}</li>
							@endif
							{{-- <li class="address"><i class="fa fa-map-marker"></i> {{ $profile->address }}</li> --}}
							<li class="fb-link">{{ $profile->address }}</li>
							<li class="fb-link"><i class="fa fa-facebook"></i> {{ $profile->facebook }}</li>
							<li class="heart-star">
								<i class="fa fa-star"></i>
								<span class="label label-default">{{ ($rating) ? $rating : 'No ratings yet' }}</span>
								{{-- <i class="fa fa-heart"></i>
								<span class="label label-default">10</span> --}}
							</li>
							<li class="diamond-count">
								<i class="fa fa-diamond fa-lg {{ $diamond_color }}"></i>
								<span class="label label-default">{{ $diamond_count.' Diamonds' }}</span>
							</li>
						</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>