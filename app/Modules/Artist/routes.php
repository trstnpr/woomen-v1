<?php

Route::group(array('prefix' => 'artist', 'module' => 'Artist', 'namespace' => 'App\Modules\Artist\Controllers', 'middleware' => ['web', 'auth', 'artist']), function() {

    //ARTIST
    Route::get('dashboard', ['uses' => 'ArtistController@index', 'as' => 'artist.dashboard.index']);
    
    Route::get('packagelist', ['uses' => 'ArtistController@packagelist', 'as' => 'artist.packagelist.index']);

    Route::get('profile', ['uses' => 'ArtistController@profileSetting', 'as' => 'artist.profile.index']);
    Route::post('profile/update', ['uses' => 'ArtistController@profileUpdate', 'as' => 'artist.profile.update']);

    Route::get('password', ['uses' => 'ArtistController@passwordSetting', 'as' => 'artist.password.index']);
    Route::post('password/update', ['uses' => 'ArtistController@passwordUpdate', 'as' => 'artist.password.update']);

    Route::get('packagelist', ['uses' => 'ArtistController@packagelist', 'as' => 'artist.packagelist.index']);
    Route::post('packagelist/update', ['uses' => 'ArtistController@packagelistUpdate', 'as' => 'artist.packagelist.update']);
    
    Route::get('portfolio', ['uses' => 'PortfolioController@index', 'as' => 'artist.portfolio.index']);
    
    Route::get('portfolio/featured-image', ['uses' => 'PortfolioController@editFeatimg', 'as' => 'artist.portfolio.editFeatimg']);
    Route::post('portfolio/featured-image/feat', ['uses' => 'PortfolioController@featureImg', 'as' => 'artist.portfolio.featureImg']);
    
    Route::post('portfolio/album/create', ['uses' => 'PortfolioController@createAlbum', 'as' => 'artist.portfolio.album.create']);
    Route::get('portfolio/album/{id}', ['uses' => 'PortfolioController@album', 'as' => 'artist.portfolio.album.show']);
    Route::post('portfolio/album/{id}/uploadImage', ['uses' => 'PortfolioController@uploadImage', 'as' => 'artist.portfolio.album.uploadImage']);
    Route::get('portfolio/album/{id}/deleteImage', ['uses' => 'PortfolioController@deleteImage', 'as' => 'artist.portfolio.album.deleteImage']);

    Route::get('portfolio/video', ['uses' => 'PortfolioController@video', 'as' => 'artist.portfolio.video']);
    Route::post('portfolio/video/post', ['uses' => 'PortfolioController@postVideo', 'as' => 'artist.portfolio.video.post']);
    Route::delete('portfolio/video/delete/{id}', ['uses' => 'PortfolioController@deleteVideo', 'as' => 'artist.portfolio.video.delete']);

    Route::get('review', ['uses' => 'ReviewController@index', 'as' => 'artist.review.index']);
    Route::delete('review/delete/{id}', ['uses' => 'ReviewController@deleteReview', 'as' => 'artist.review.delete']);

    Route::get('item', ['uses' => 'ItemController@index', 'as' => 'artist.item.index']);
    Route::get('item/show/{id}', ['uses' => 'ItemController@showItem', 'as' => 'artist.item.show']);
    Route::get('item/create', ['uses' => 'ItemController@createItem', 'as' => 'artist.item.create']);
    Route::post('item/store', ['uses' => 'ItemController@storeItem', 'as' => 'artist.item.store']);
    Route::get('item/edit/{id}', ['uses' => 'ItemController@editItem', 'as' => 'artist.item.edit']);
    Route::post('item/update/{id}', ['uses' => 'ItemController@updateItem', 'as' => 'artist.item.update']);
    Route::delete('item/delete/{id}', ['uses' => 'ItemController@deleteItem', 'as' => 'artist.item.delete']);
    Route::delete('item/delete/item_image/{id}', ['uses' => 'ItemController@deleteImageItem', 'as' => 'merchant.item.deleteImageItem']);

    Route::get('bookings', ['uses' => 'BookingController@index', 'as' => 'artist.booking.index']);
    Route::get('booking/{id}', ['uses' => 'BookingController@bookingID', 'as' => 'artist.booking.booking']);
    Route::get('booking/request/{id}', ['uses' => 'BookingController@requestID', 'as' => 'artist.booking.request']);
    Route::post('booking/request/{id}/confirm', ['uses' => 'BookingController@confirmBooking', 'as' => 'artist.booking.confirm']);
    Route::post('booking/request/{id}/decline', ['uses' => 'BookingController@declineBooking', 'as' => 'artist.booking.decline']);
    Route::post('booking/request/{id}/cancel', ['uses' => 'BookingController@cancelBooking', 'as' => 'artist.booking.cancel']);
});

Route::group(array('prefix' => 'artist', 'module' => 'Artist', 'namespace' => 'App\Modules\Artist\Controllers', 'middleware' => ['web']), function() {

    /** Test View **/
    Route::get('/view/{id}', ['uses' => 'ArtistController@publicArtist', 'as' => 'artist.list.show']);
    Route::get('/browse', ['uses' => 'ArtistController@artistlist', 'as' => 'artist.list.index']);
    
    //POST BOOKING
    Route::post('booking/{id}', ['uses' => 'BookingController@booking', 'as' => 'artist.booking.store']);

    /* REVIEW */
    Route::get('/review/{id}', ['uses' => 'ReviewController@reviewID', 'as' => 'artist.public.review.index']);
    
    //POST REVIEW
    Route::post('review/post/{id}', ['uses' => 'ReviewController@review', 'as' => 'artist.review.post']);

});
