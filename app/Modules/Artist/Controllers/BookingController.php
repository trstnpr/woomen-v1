<?php namespace App\Modules\Artist\Controllers;

use Auth;
use Mail;
use Hash;
use Carbon\Carbon;

use App\Services\MailSender;

use App\Modules\App\Models\User;
use App\Modules\App\Models\Booking;
use App\Modules\Artist\Models\ArtistProfile;

use App\Http\Requests\BookingRequest;
use App\Http\Requests\ConfirmBookingRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class BookingController extends Controller {

	public function index()
	{
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();

        $this->data['bookings'] = Booking::where('artist_profile_id', '=', $profile['artist_profile_id'])->where('status', '=', 0)->get();
        $this->data['confirm_bookings'] = Booking::where('artist_profile_id', '=', $profile['artist_profile_id'])->where('status', '=', 1)->get();

		return view('Artist::booking.index', $this->data);
	}

    public function bookingID($id)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $this->data['booking'] = Booking::where('artist_profile_id', '=', $profile['artist_profile_id'])->where('booking_id', '=', $id)->where('status', 1)->first();
        if($this->data['booking']){
            return view('Artist::booking.booking', $this->data);
        }
        return redirect()->route('artist.booking.index');
    }

    public function requestID($id)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $this->data['request_booking'] = Booking::where('artist_profile_id', '=', $profile['artist_profile_id'])->where('booking_id', '=', $id)->where('status', 0)->first();
        if($this->data['request_booking']){
            return view('Artist::booking.request', $this->data);
        }
        return redirect()->route('artist.booking.index');
    }

    public function confirmBooking($id, ConfirmBookingRequest $request, MailSender $mailSender)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $profile->total_earnings = $profile->total_earnings + $request->final_price;
        $profile->save();
        
        $booking = Booking::where('booking_id', $id)->first();
        $booking->final_event_title = $request->final_event_title;
        $booking->final_date = $request->final_date;
        $booking->final_time = $request->final_time;
        $booking->final_no_of_heads = $request->final_no_of_heads;
        $booking->final_venue = $request->final_venue;
        $booking->final_price = $request->final_price;
        $booking->final_notes = $request->final_notes;
        $booking->status = 1;
        $booking->save();

        $data_booking = [
            'email' => $booking->email,
            'event' => $booking->final_event_title,
            'date' => $booking->final_date,
            'time' => $booking->final_time,
            'no_of_heads' => $booking->final_no_of_heads,
            'venue' => $booking->final_venue,
            'price' => $booking->final_price,
            'notes' => $booking->final_notes,
            'first_name' => $profile->first_name,
            'last_name' => $profile->last_name
        ];

        $mailSender->send('email.confirm_booking', 'Confirm Booking', $data_booking);

        $data_booking['artist_profile_id'] = $profile['artist_profile_id'];

        $mailSender->sendLater('email.email_review', 'Booking Confirmation', $data_booking);
        
        $data_booking['email'] = 'woomen.ceo@gmail.com';

        $mailSender->send('email.confirm_booking_admin', 'Booking Confirmation', $data_booking);


        return json_encode(array('result' => 'success', 'message' => 'Confirmation of the Booking Successfully Sent!'));
    }

    public function declineBooking($id, Request $request, MailSender $mailSender)
    {
        if(trim($request->reason, ' ') != '' || trim($request->reason, ' ') != null ){

            $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
            $booking = Booking::where('booking_id', $id)->where('artist_profile_id', $profile['artist_profile_id'])->first();

            if($booking){
                $booking->status = 2;
                $booking->cancel_reason = $request->reason;
                $booking->save();

                $email_data = [
                    'email' => $booking->email,
                    'first_name' => $profile->first_name,
                    'last_name' => $profile->last_name,
                    'reason' => $request->reason
                ];

                $mailSender->send('email.booking_declined', 'Booking Declined - Woomen', $email_data);

                $email_data['email'] = 'woomen.ceo@gmail.com';

                $mailSender->send('email.booking_declined_admin', 'Booking Declined - Woomen', $email_data);

                return json_encode(array('result' => 'success', 'message' => 'Booking Declined!'));
            }
            return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered in the process. Please try again later.'));
        }
        return json_encode(array('result' => 'error', 'message' => 'Please indicate your reason.'));
    }

    public function cancelBooking($id, Request $request, MailSender $mailSender)
    {
        if(trim($request->reason, ' ') != '' || trim($request->reason, ' ') != null ){
            $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
            $booking = Booking::where('booking_id', $id)->where('artist_profile_id', $profile['artist_profile_id'])->first();

            if($booking){
                $booking->status = 3;
                $booking->cancel_reason = $request->reason;
                $booking->save();

                $profile->total_earnings = $profile->total_earnings - $booking->final_price;
                $profile->save();

                $email_data = [
                    'email' => $booking->email,
                    'first_name' => $profile->first_name,
                    'last_name' => $profile->last_name,
                    'reason' => $request->reason
                ];

                $mailSender->send('email.booking_cancelled', 'Booking Cancelled - Woomen', $email_data);

                $email_data['email'] = 'woomen.ceo@gmail.com';

                $mailSender->send('email.booking_cancelled_admin', 'Booking Cancelled - Woomen', $email_data);

                return json_encode(array('result' => 'success', 'message' => 'Booking Cancelled!'));
            }

            return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered in the process. Please try again later.'));
        }
        return json_encode(array('result' => 'error', 'message' => 'Please indicate your reason.'));
    }

	public function booking($id, BookingRequest $request, MailSender $mailSender)
	{
		$profile = ArtistProfile::where('artist_profile_id', $id)->first();
        $artist_email = User::where('id', '=', $profile['user_id'])->value('email');
    	$data_booking = [
    		'artist_profile_id' => $id,
    		'first_name' => $request->first_name,
    		'last_name' => $request->last_name,
    		'contact_no' => $request->contact_no,
    		'email' => $request->email,
    		'event_title' => $request->event_title,
    		'no_of_heads' => $request->no_of_heads,
    		'event_date' => $request->event_date,
    		'event_time' => $request->event_time,
    		'venue' => $request->venue,
    		'call_time' => $request->call_time,
    		'remarks' => $request->remarks,
    		'created_at' => Carbon::now(),
    	];

    	$booking = Booking::insertGetId($data_booking);

    	if($booking){
    		$data_booking['email'] = $artist_email;
    		$data_booking['booker_email'] = $request->email;

            $mailSender->send('email.request_booking', 'Request Booking - Woomen', $data_booking);

            $data_booking['email'] = 'woomen.ceo@gmail.com';
            $data_booking['artist_name'] = $profile['first_name'].' '.$profile['last_name'];

            $mailSender->send('email.request_booking_admin', 'Request Booking - Woomen', $data_booking);

            return json_encode(array('result' => 'success', 'message' => 'Booking Successfully Sent! You will receive a reply in your email/phone number.'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while sending your booking'));

	}

}
