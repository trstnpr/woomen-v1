<?php namespace App\Modules\Artist\Controllers;

use Auth;
use Hash;
use Carbon\Carbon;

use App\Modules\App\Models\User;
use App\Modules\Artist\Models\ArtistProfile;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;

use App\Http\Requests\ItemRequest;

use App\Services\ImageUploader;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ItemController extends Controller {

	public function index()
	{
        $data['email'] = User::where('id', Auth::user()->id)->first();
        $data['profile'] = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $data['items'] = Item::with('feat_image')
                            ->where('artist_profile_id', $data['profile']['artist_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->limit(5)
                            ->get();

        $data['all_items'] = Item::with('feat_image')
                            ->where('artist_profile_id', $data['profile']['artist_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->get();

		return view('Artist::item.index', $data);
	}

    public function showItem($id)
    {
        return view('Artist::item.show');
    }

    public function createItem()
    {
        $data['email'] = User::where('id', Auth::user()->id)->first();
        $data['artist'] = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $data['items'] = Item::with('feat_image')
                            ->where('artist_profile_id', $data['artist']['artist_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->limit(5)
                            ->get();

        $data['all_items'] = Item::with('feat_image')
                            ->where('artist_profile_id', $data['artist']['artist_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->get();

        return view('Artist::item.create', $data);
    }

    public function storeItem(ItemRequest $request)
    {

        if($request->hasFile('image_path')){

            if($request->image_path != ''){

                $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();

                $data_item = [
                    'artist_profile_id' => $profile['artist_profile_id'],
                    'item_name' => $request->item_name,
                    'item_description' => $request->item_description,
                    'item_quantity' => $request->item_quantity,
                    'item_price' => $request->item_price,
                    'created_at' => Carbon::now(),
                ];

                $item = Item::insertGetId($data_item);

                if($item){
                    if ($request->hasFile('image_path')){
                        $images = $request->image_path;

                        if($images != ''){
                            foreach($images as $imageUrl){
                                    $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                                    $imageUploader = new ImageUploader;

                                    $result = $imageUploader->upload($imageUrl, $item, 'items');

                                    $data_campaign_image = [
                                        'item_id' => $item,
                                        'image_path' => $item.'/'.$result,
                                        'created_at' => Carbon::now()
                                    ];

                                $insert_Images = ItemImages::insert($data_campaign_image);
                            }
                        }
                    }

                    return json_encode(array('result' => 'success', 'message' => 'Item Successfully Added!'));
                }
                return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
            }
        }

        return json_encode(array('result' => 'error', 'message' => 'Please Insert at least 1 image for your item.'));
    }

    public function editItem($id)
    {
        $data['email'] = User::where('id', Auth::user()->id)->first();
        $data['artist'] = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $data['items'] = Item::with('feat_image')
                            ->where('artist_profile_id', $data['artist']['artist_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->limit(5)
                            ->get();

        $data['all_items'] = Item::with('feat_image')
                            ->where('artist_profile_id', $data['artist']['artist_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->get();
                                    
        $data['item'] = Item::where('item_id', $id)->first();
        $data['item_images'] = ItemImages::where('item_id', $id)->get();

        return view('Artist::item.edit', $data);
    }

    public function updateItem($id, Request $request)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();

        $data_item = [
            'artist_profile_id' => $profile['artist_profile_id'],
            'item_name' => $request->item_name,
            'item_description' => $request->item_description,
            'item_quantity' => $request->item_quantity,
            'item_price' => $request->item_price,
            'updated_at' => Carbon::now(),
        ];

        $item = Item::where('item_id', $id)->update($data_item);

        if($item){
            if ($request->hasFile('image_path')){
                $images = $request->image_path;
                /*$del = ItemImages::where('item_id', $id)->delete();*/

                if($images != ''){
                    foreach($images as $imageUrl){
                            $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                            $imageUploader = new ImageUploader;

                            $result = $imageUploader->upload($imageUrl, $id, 'items');

                            $data_campaign_image = [
                                'item_id' => $id,
                                'image_path' => $id.'/'.$result,
                                'updated_at' => Carbon::now()
                            ];

                        $insert_Images = ItemImages::insert($data_campaign_image);
                    }
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Item Successfully Updated!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while updating'));
    }

    public function deleteItem($id)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();

        $item = Item::where('item_id', $id)->where('artist_profile_id', $profile['artist_profile_id'])->first();

        if($item){
            if (Item::where('item_id', $id)->delete()) {
                return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
            }

            return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'You are not allowed to delete this item.'));
    }

    public function deleteImageItem($id)
    {
        $item = ItemImages::where('item_image_id', $id)->first();
        if($item){
            if (ItemImages::where('item_image_id', $id)->delete()) 
            {
                $imageUploader = new ImageUploader;
                $imageUploader::deleteS3(config('cdn.item') . '/' . $item['image_path']);

                return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
            }

            return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
        }
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
