<?php namespace App\Modules\Artist\Controllers;

use Auth;
use Mail;
use Carbon\Carbon;

use App\Services\ImageUploader;

use App\Modules\App\Models\User;
use App\Modules\Artist\Models\ArtistProfile;
use App\Modules\Artist\Models\ArtistPortfolio;
use App\Modules\Artist\Models\ArtistImages;
use App\Modules\Artist\Models\ArtistAlbums;

use App\Http\Requests\BookingRequest;
use App\Http\Requests\ReviewRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PortfolioController extends Controller {

	public function index()
	{
        $this->data['profile'] = ArtistProfile::where('user_id', '=', Auth::user()->id)->first();

        $this->data['portfolio'] = ArtistPortfolio::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->get();
        $this->data['featured'] = ArtistImages::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->where('is_featured', '=', 1)->get();
        $this->data['albums'] = ArtistAlbums::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->get();

		return view('Artist::portfolio.index', $this->data);
	}

    /* FEATURED FUNCTIONS */

    public function editFeatimg()
    {   
        $profile = ArtistProfile::where('user_id', '=', Auth::user()->id)->first();

        $this->data['images'] = ArtistImages::where('artist_profile_id', '=', $profile['artist_profile_id'])->get();

        return view('Artist::portfolio.editFeatimg', $this->data);
    }

    public function featureImg(Request $request)
    {   
        $dataFeat = ['is_featured' => 1];
        $update_feat = ArtistImages::whereIn('artist_image_id', $request->artist_image_id)->update($dataFeat);

        if($update_feat){
            return json_encode(array('result' => 'success', 'message' => 'Successfully featured image'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while featuring your image.'));

    }

    /* FEATURED FUNCTIONS END */

    /* ALBUM FUNCTIONS */

    public function createAlbum(Request $request)
    {
        $profile = ArtistProfile::where('user_id', '=', Auth::user()->id)->first();
        
        if($request->album_name != null || $request->album_name != ''){
            $input = [
                'artist_profile_id' => $profile['artist_profile_id'],
                'album_name' => $request->album_name
            ];

            if(ArtistAlbums::create($input)){
                return json_encode(array('result' => 'success', 'message' => 'Album '. $request->album_name .' Created!'));
            }
            return json_encode(array('result' => 'error', 'message' => 'There\'s an error creating your album. Please try again later.'));
        }
        return json_encode(array('result' => 'error', 'message' => 'Album Name is required!'));

    }

    public function album($id)
    {
        $profile = ArtistProfile::where('user_id', '=', Auth::user()->id)->first();
        $this->data['album'] = ArtistAlbums::with('artist_images')->where('artist_album_id', '=', $id)->where('artist_profile_id', '=', $profile['artist_profile_id'])->first();
        /*if(!$this->data['album']){
            abort(403, 'Unauthorized action.');
        }*/

        return view('Artist::portfolio.album', $this->data);
    }

    public function uploadImage($id, Request $request)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $album = ArtistAlbums::where('artist_album_id', '=', $id)->where('artist_profile_id', '=', $profile['artist_profile_id'])->first();
        $album->album_description = $request->album_description;
        $album->album_name = $request->album_name;
        $album->save();

        if ($request->hasFile('image_path')){

            $files = $request->image_path;

            if($files != ''){
                //dd($files);
                $no_of_images = ArtistImages::where('artist_profile_id', '=', $profile['artist_profile_id'])->where('artist_album_id', '=', $album->artist_album_id)->count();
                //dd(count($files));
                if($no_of_images + count($files) <= $profile->artist_user_level->pictures){
                    $i = 0;
                    $len = count($files);
                    foreach($files as $fileUrl){
                        $this->_validate_image_format($fileUrl->getClientOriginalExtension());

                        $imageUploader = new ImageUploader;

                        $albumName = $profile['artist_profile_id'].'/'.$album['album_name'];

                        $result = $imageUploader->upload($fileUrl, $albumName, 'portfolio');

                        $data_portfolio = [
                            'artist_profile_id' => $profile['artist_profile_id'],
                            'artist_album_id' => $id,
                            'image_path' => $albumName . '/' . $result,
                            'image_type' => $fileUrl->getClientOriginalExtension()
                        ];

                        ArtistImages::create($data_portfolio);

                        if($i == $len - 1){
                            return json_encode(array('result' => 'success', 'message' => 'Image Successfully Saved!'));
                        }
                        $i++;
                    }

                }

                return json_encode(array('result' => 'error', 'message' => 'Only 5 photos per album are allowed!'));
            }

            return json_encode(array('result' => 'error', 'message' => 'Select Image to upload.'));
        }

        return json_encode(array('result' => 'success', 'message' => 'Album Successfully Updated.'));

    }

    public function deleteImage($id)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $image = ArtistImages::where('artist_image_id', '=', $id)->where('artist_profile_id', '=', $profile['artist_profile_id'])->first();

        if($image){
            if($image->image_path != null || $image->image_path != ''){
                $imageUploader = new ImageUploader;
                $imageUploader->deleteS3(config('cdn.portfolio').'/'.$image->image_path);
                $image->delete();

                return json_encode(array('result' => 'success', 'message' => 'Image Successfully Deleted!'));
            }
        }

        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while deleting your image.'));
    }

    /* ALBUM FUNCTIONS END */

    /* VIDEO FUNCTIONS */

    public function video()
    {
        $this->data['profile'] = ArtistProfile::where('user_id', '=', Auth::user()->id)->first();
        $this->data['portfolio'] = ArtistPortfolio::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->get();

        return view('Artist::portfolio.video', $this->data);
    }

    public function postVideo(Request $request)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
        
        if($request->has('video')){
            if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $request->video, $id)) {
              $values = $id[1];
            } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $request->video, $id)) {
              $values = $id[1];
            } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $request->video, $id)) {
              $values = $id[1];
            } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $request->video, $id)) {
              $values = $id[1];
            } else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
            } else {   
                return json_encode(array('result' => 'error', 'message' => 'This is not a Youtube video'));
            }

            $data_portfolio = [
                'artist_profile_id' => $profile['artist_profile_id'],
                'file_path' => 'https://www.youtube.com/embed/'.$values,
                'file_type' => 'video',
                'embed_code' => $values,
            ];

            $portfolio = ArtistPortfolio::create($data_portfolio);
            // $files = $request->video;

            // if($files != ''){
            //     foreach($files as $fileUrl){

            //         $data_portfolio = [
            //             'artist_profile_id' => $profile['artist_profile_id'],
            //             'file_path' => $fileUrl->video,
            //             'file_type' => 'video',
            //         ];

            //         $portfolio = ArtistPortfolio::create($data_portfolio);
            //     }
            // }
        }

        return json_encode(array('result' => 'success', 'message' => 'Portfolio Successfully Saved!'));
    }

    public function deleteVideo($id)
    {
        $profile = ArtistProfile::where('user_id', '=', Auth::user()->id)->first();

        $result = ArtistPortfolio::where('artist_portfolio_id', '=', $id)->where('artist_profile_id', '=', $profile['artist_profile_id'])->delete();

        if($result) {
            return json_encode(array('result' => 'success', 'message' => 'Video Successfully Deleted!'));
        } else {
            return json_encode(array('result' => 'error', 'message' => 'There\'s an error deleting your video'));
        }
    }

    /* VIDEO FUNCTIONS END */

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
