<?php 
namespace App\Modules\Artist\Controllers;

use Auth;
use Mail;
use Hash;

use App\Modules\App\Models\User;
use App\Modules\App\Models\Specialty;
use App\Modules\App\Models\Review;
use App\Modules\App\Models\Update;
use App\Modules\App\Models\Booking;

use App\Modules\Artist\Models\ArtistImages;
use App\Modules\Artist\Models\ArtistPackages;
use App\Modules\Artist\Models\ArtistAffiliations;
use App\Modules\Artist\Models\ArtistAlbums;
use App\Modules\Artist\Models\ArtistPortfolio;
use App\Modules\Artist\Models\ArtistProfile;
use App\Modules\Artist\Models\ArtistSpecialty;
use App\Modules\Event\Models\ArtistEvent;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ArtistProfileRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Services\ImageUploader;

class ArtistController extends Controller {

	//ARTIST PAGE VIEW
	
	public function index()
	{
		$this->data['profile'] = ArtistProfile::with('artist_specialty')->where('user_id', '=', Auth::user()->id)->first();
		$this->data['portfolio'] = ArtistPortfolio::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->get();
        $this->data['featured'] = ArtistImages::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->where('is_featured', '=', 1)->get();
        $this->data['albums'] = ArtistAlbums::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->get();
        $this->data['events'] = ArtistEvent::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->get();
        $this->data['rating'] = ($ratings = Review::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->avg('review_rating')) ? $ratings : 0;
        $this->data['diamond_count'] = $this->data['profile']['total_earnings'] / 1000;
        $this->data['diamond_color'] = $this->_diamond_color($this->data['diamond_count']);

		return view('Artist::dashboard.index', $this->data);
	}
	
	
	public function profileSetting()
	{
		$this->data['profile'] = ArtistProfile::with('artist_specialty')->where('user_id', '=', Auth::user()->id)->first();
		$this->data['mySpecialties'] = array();
		$this->data['specialty'] = Specialty::all();
		foreach ($this->data['profile']['artist_specialty'] as $key => $value) {
			array_push($this->data['mySpecialties'], $value['specialty_id']);
		}
		return view('Artist::setting.profile.index', $this->data);
	}

	public function profileUpdate(ArtistProfileRequest $request)
	{
		$data_profile = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'middle_name' => $request->middle_name,
            'address' => $request->address,
            'about' => $request->about,
            'facebook' => $request->facebook,
            'price_from' => $request->price_from,
            'price_to' => $request->price_to
        ];

		$profileUpdate = ArtistProfile::where('user_id', Auth::user()->id)->update($data_profile);

        if ($profileUpdate) {
        	$profileID = ArtistProfile::where('user_id', Auth::user()->id)->value('artist_profile_id');
        	if(isset($request->specialty)){
        		ArtistSpecialty::where('artist_profile_id', $profileID)->delete();
	        	foreach ($request->specialty as $key => $value) {
	        		ArtistSpecialty::insert(['artist_profile_id'=>$profileID, 'specialty_id'=> $value]);
	        	}
	        }

	        if(isset($request->affiliation)){
        		ArtistAffiliations::where('artist_profile_id', $profileID)->delete();
	        	foreach ($request->affiliation as $key => $value) {
					ArtistAffiliations::insert(['artist_profile_id'=>$profileID, 'affiliation'=> $value]);
	        	}
	        } else {
	        	ArtistAffiliations::where('artist_profile_id', $profileID)->delete();
	        }

	        if ($request->hasFile('profile_image'))
	        {       
	                $imageUrl = $request->profile_image;
	                $this->_validate_image_format($imageUrl->getClientOriginalExtension());

	                $imageUploader = new ImageUploader;

	                $result = $imageUploader->upload($imageUrl, $profileID, 'profile_images');

	                $insert_Images = ArtistProfile::where('artist_profile_id', $profileID)
	                                ->update(['profile_image' => $profileID.'/'.$result]);
	        }

            return json_encode(array('result' => 'success', 'message' => 'Successfully Updated.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
	
	}

	public function passwordSetting()
	{
		return view('Artist::setting.password.index');
	}

	public function passwordUpdate(ChangePasswordRequest $request)
	{
		$id = Auth::user()->id;

    	if(Auth::validate(['id' => $id, 'password' => $request->current_pass])){
    		$newPassword = bcrypt($request->password);
    		if(User::where('id', $id)->update(['password' => $newPassword])){
    			return json_encode(array('result' => 'success', 'message' => 'You successfully changed your password'));
    		}

    		return json_decode(array('result' => 'error', 'message' => 'An error occured while updating your password'));
    	}

    	return json_encode(array('result' => 'error', 'message' => 'Current password does not match'));
	}

	public function packagelist()
	{
		$this->data['profile'] = ArtistProfile::with('artist_specialty')->where('user_id', '=', Auth::user()->id)->first();
		$this->data['package'] = ArtistPackages::where('artist_profile_id', '=', $this->data['profile']['artist_profile_id'])->first();
		
		return view('Artist::packagelist.index', $this->data);
	}
	
	public function packagelistUpdate(Request $request)
	{
		$this->data['profile'] = ArtistProfile::with('artist_specialty')->where('user_id', '=', Auth::user()->id)->first();

		$packages = ArtistPackages::firstOrNew(array('artist_profile_id' => $this->data['profile']['artist_profile_id']));
		$packages->package_description = $request->package_description;
		$packages->save();

		return json_encode(array('result' => 'success', 'message' => 'You successfully saved your packages'));

		/*$this->data['profile'] = ArtistProfile::with('artist_specialty')->where('user_id', '=', Auth::user()->id)->first();

		ArtistPackages::where('artist_profile_id', $this->data['profile']['artist_profile_id'])->delete();
		
		if(isset($request->package)) {
			$packages = $request->package;
			$description = $request->description;
			if($packages != ''){
				for($i = 0; $i < count($packages); $i++){
					ArtistPackages::create(['artist_profile_id'=> $this->data['profile']['artist_profile_id'], 'package_title'=>$packages[$i], 'package_description'=>$description[$i]]);
				}
			}
		}

		return json_encode(array('result' => 'success', 'message' => 'You successfully saved your packages'));*/
	}

	//GUEST PAGE VIEW

	public function artistList(Request $request)
	{	
		
		$this->data['specialty'] = Specialty::get();
		$this->data['myForte'] = array();
		

		if(isset($request->q)) {

			$this->data['artistList'] =  ArtistProfile::where('first_name', 'like', '%'.$request->q.'%')
								->orWhere('last_name', 'like', '%'.$request->q.'%')
								->orderBy('created_at', 'desc')
								->get();

		} else if(isset($request->forte) || isset($request->order) || isset($request->date) || isset($request->loc) || isset($request->quote)) {
			$query = ArtistProfile::join('artist_specialties', 'artist_specialties.artist_profile_id', '=', 'artist_profile.artist_profile_id');

			if(isset($request->forte)){
				$result = Specialty::whereIn('specialty_id', $request->forte)->get();
				foreach ($result as $key => $value) {
		            array_push($this->data['myForte'], $value['specialty_id']);
		        }
				$query->whereIn('artist_specialties.specialty_id', $request->forte);
			}

			if(isset($request->loc)){
				$query->where('artist_profile.address', 'like', '%'.$request->loc.'%');
			}

			if(isset($request->quote)){
				$query->where('artist_profile.price_from', '<=', $request->quote)
						->where('artist_profile.price_to', '>=', $request->quote);
			}

			if(isset($request->order)){
				$query->orderBy('artist_profile.first_name', $request->order);		
			}

			if(isset($request->date)){
				$query->orderBy('artist_profile.created_at', $request->date);		
			}

			$this->data['artistList'] = $query->groupBy('artist_specialties.artist_profile_id')->get();

		} else {

			$this->data['artistList'] = ArtistProfile::get();
			
		}


		foreach ($this->data['artistList'] as $key => $value) {
			if($value['account']['status'] != 1){
				$this->data['artistList']->forget($key);
			}
			$value['ratings'] = ( $value['artist_ratings'] ) ? $value['artist_ratings']->where('artist_profile_id', $value['artist_profile_id'])->avg('review_rating') : 0 ;
		}

		return view('Artist::public.artistlist.index', $this->data);
	}

	public function publicArtist($id)
	{
		$this->data['artist'] = ArtistProfile::where('artist_profile_id', '=', $id)->first();
        $this->data['diamond_count'] = $this->data['artist']['total_earnings'] / 1000;
        $this->data['diamond_color'] = $this->_diamond_color($this->data['diamond_count']);
		$this->data['portfolio'] = ArtistPortfolio::where('artist_profile_id', '=', $this->data['artist']['artist_profile_id'])->get();
        $this->data['featured'] = ArtistImages::where('artist_profile_id', '=', $this->data['artist']['artist_profile_id'])->where('is_featured', '=', 1)->get();
        $this->data['albums'] = ArtistAlbums::where('artist_profile_id', '=', $this->data['artist']['artist_profile_id'])->get();
        $this->data['events'] = ArtistEvent::where('artist_profile_id', '=', $this->data['artist']['artist_profile_id'])->get();
        $this->data['specialty'] = Specialty::all();
        $this->data['reviews'] = Review::where('artist_profile_id', '=', $this->data['artist']['artist_profile_id'])->get();
        $this->data['ratings'] = ($ratings = Review::where('artist_profile_id', '=', $this->data['artist']['artist_profile_id'])->avg('review_rating')) ? $ratings : 0;
        $this->data['package'] = ArtistPackages::where('artist_profile_id', '=', $this->data['artist']['artist_profile_id'])->first();
        $this->data['booking'] = Booking::where('artist_profile_id', '=', $this->data['artist']['artist_profile_id'])->count();
        $this->data['update'] = Update::where('update_expire', '>=', date('Y-m-d'))->orderBy('update_id', 'desc')->limit(3)->get();

		return view('Artist::public.artistprofile.index', $this->data);
	}

	/**
     * Diamond Color based on total earningg
     *
     * @return string
     */
	private function _diamond_color($diamond)
	{
		if($diamond >= 0 && $diamond <= 125){
			return 'diamond-white';
		} else if($diamond >= 126 && $diamond <= 375){
			return 'diamond-green';
		} else if($diamond >= 376 && $diamond <= 875){
			return 'diamond-blue';
		} else if($diamond >= 876 && $diamond <= 1625){
			return 'diamond-purple';
		} else if($diamond >= 1626 && $diamond <= 2625){
			return 'diamond-red';
		} else if($diamond >= 2626 && $diamond <= 3875){
			return 'diamond-orange';
		} else if($diamond >= 3876){
			return 'diamond-yellow';
		}
	}

	 /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
