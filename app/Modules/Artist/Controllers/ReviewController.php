<?php namespace App\Modules\Artist\Controllers;

use Auth;
use Mail;

use App\Modules\App\Models\User;
use App\Modules\App\Models\Review;
use App\Modules\Artist\Models\ArtistProfile;

use App\Http\Requests\BookingRequest;
use App\Http\Requests\ReviewRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ReviewController extends Controller {

	public function index()
	{      
        $user = ArtistProfile::where('user_id', Auth::user()->id)->first();
        $data['review'] = Review::where('artist_profile_id', $user['artist_profile_id'])->get();

		return view('Artist::review.index', $data);
	}

	public function review($id, ReviewRequest $request)
	{
		$profile = ArtistProfile::where('artist_profile_id', $id)->first();

    	$data_review = [
    		'artist_profile_id' => $profile['artist_profile_id'],
            'review_name' => $request->review_name,
            'review_email' => $request->review_email,
    		'review_title' => $request->review_title,
    		'review_description' => $request->review_description,
    		'review_rating' => $request->review_rating
    	];

    	$review = Review::create($data_review);

    	if($review){
            return json_encode(array('result' => 'success', 'message' => 'Review Successfully Saved!'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving your review'));

	}

    public function deleteReview($id)
    {  
        $reviewDelete = Review::where('review_id', $id)->delete();
        if ($reviewDelete) {
            return json_encode(array('result' => 'success', 'message' => 'Review Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

    public function reviewID($id)
    {
        $this->data['profile'] = ArtistProfile::where('artist_profile_id', $id)->first();

        return view('Artist::public.review.index', $this->data);
    }

}
