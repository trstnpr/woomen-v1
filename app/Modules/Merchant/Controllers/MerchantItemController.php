<?php namespace App\Modules\Merchant\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MerchantProfileRequest;
use App\Http\Requests\ChangePasswordRequest;

use Illuminate\Http\Request;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\App\Models\User;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;
use App\Modules\App\Models\Specialty;

use App\Services\MailSender;

use Auth;

class MerchantItemController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function item()
	{
		return view("Merchant::items");
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function public_item_view($id)
	{	
		$data['specialty'] = Specialty::all();
		$data['items'] = Item::where('item_id', $id)->first();
		$data['images'] = ItemImages::where('item_id', $id)->get();
		$data['all_items'] = ItemImages::where('item_id', '!=', $id)->limit(4)->groupBy('item_id')->get();

		return view("Merchant::public_item", $data);
	}

	public function public_item_inquiries($id, Request $request, MailSender $mailSender)
	{
		$item = Item::where('item_id', '=', $id)->first();

    	$data_inquire = [
    		'item_name' => $request->item_name,
    		'inquire_name' => $request->inquire_name,
    		'inquire_email' => $request->inquire_email,
    		'inquire_contact' => $request->inquire_contact,
    		'inquire_message' => $request->inquire_message,
    		'email' => $request->seller_email,
    		'first_name' => $request->first_name,
    		'last_name' => $request->last_name,
    		'item_image' => 'http:' . config('s3.bucket_link') . config('cdn.item') .'/'. $item->feat_image['image_path']
    	];

        $mailSender->send('email.item_inquiry', 'Item Inquiry - Woomen', $data_inquire);

        $data_inquire['email'] = 'woomen.ceo@gmail.com';
        $data_inquire['first_name'] = 'Woomen';
        $data_inquire['last_name'] = 'CEO';

        $mailSender->send('email.item_inquiry_admin', 'Item Inquiry - Woomen', $data_inquire);

        return json_encode(array('result' => 'success', 'message' => 'Inquiry Successfully Sent! You will receive a reply in your email/phone number.'));

	}


	public function merchant_item_view($id)
	{
		$data['specialty'] = Specialty::all();
		$data['email'] = User::where('id', Auth::user()->id)->first();
        $data['merchant'] = MerchantProfile::where('user_id', Auth::user()->id)->first();
        $data['items'] = Item::where('item_id', $id)->first();

        $data['images'] = ItemImages::where('item_id', $id)->get();
        
        $data['all_items'] = Item::with('feat_image')
                            ->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->get();

		return view("Merchant::view_item", $data);
	}

}
