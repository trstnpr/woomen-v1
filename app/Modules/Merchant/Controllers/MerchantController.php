<?php namespace App\Modules\Merchant\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MerchantProfileRequest;
use App\Http\Requests\ReviewRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ProfileImageRequest;

use Illuminate\Http\Request;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\Item\Models\Item;
use App\Modules\App\Models\Review;
use App\Modules\App\Models\User;
use App\Modules\App\Models\Specialty;
use Auth;
use App\Services\ImageUploader;

class MerchantController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{   
        $data['merchant'] = MerchantProfile::where('user_id', $id)->first();
        $data['item'] = Item::with('feat_image')->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])->get();
        $data['specialty'] = Specialty::all();
		return view("Merchant::index", $data);
	}

    /**
     * Display dashboard
     * @return Response
     */
    public function dashboard()
    {   
        $data['email'] = User::where('id', Auth::user()->id)->first();
        $data['merchant'] = MerchantProfile::where('user_id', Auth::user()->id)->first();

        $data['item'] = Item::where('merchant_profile_id', $data['merchant']['merchant_profile_id'])->count();
        $data['review'] = Review::where('merchant_profile_id', $data['merchant']['merchant_profile_id'])->count();

        $data['items'] = Item::with('feat_image')
                            ->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->limit(5)
                            ->get();

        return view("Merchant::dashboard", $data);
    }

    public function profile(){
        $data['email'] = User::where('id', Auth::user()->id)->first();
        $data['merchant'] = MerchantProfile::where('user_id', Auth::user()->id)->first();

        return view("Merchant::profile_settings", $data); 
    }

	/**
     * Update Profile Settings.
     *
     * @return Response
     */
    public function updateProfile(MerchantProfileRequest $request)
    {   
        $profile = MerchantProfile::where('user_id', Auth::user()->id)->first();

        $data_profile = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'middle_name' => $request->middle_name,
            'address' => $request->address,
            'facebook' => $request->facebook
        ];

        $profileUpdate = MerchantProfile::where('merchant_profile_id', $profile['merchant_profile_id'])->update($data_profile);

        if ($profileUpdate) {
            return json_encode(array('result' => 'success', 'message' => 'Successfully Updated.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
    }

    /**
     * Change Password.
     *
     * @return Response
     */
    public function changePassword(ChangePasswordRequest $request)
    {
    	$id = Auth::user()->id;

    	if(Auth::validate(['id' => $id, 'password' => $request->current_pass])){
    		$newPassword = bcrypt($request->password);
    		if(User::where('id', $id)->update(['password' => $newPassword])){
    			return json_encode(array('result' => 'success', 'message' => 'You successfully changed your password'));
    		}

    		return json_decode(array('result' => 'error', 'message' => 'An error occured while updating your password'));
    	}

    	return json_encode(array('result' => 'error', 'message' => 'Current password does not match'));
    }

    /**
     * Upload Image.
     *
     * @return Response
     */
    public function uploadImage(ProfileImageRequest $request)
    {
        $profile = MerchantProfile::where('user_id', Auth::user()->id)->first();

        if ($request->hasFile('profile_image'))
        {       
                $imageUrl = $request->profile_image;
                $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                $imageUploader = new ImageUploader;

                $result = $imageUploader->upload($imageUrl, $profile['merchant_profile_id'], 'profile_images');

                $insert_Images = MerchantProfile::where('merchant_profile_id', $profile['merchant_profile_id'])
                                ->update(['profile_image' => $profile['merchant_profile_id'].'/'.$result]);

                return json_encode(array('result' => 'success', 'message' => 'Profile Image Successfully Updated'));
        }
        return json_encode(array('result' => 'error', 'message' => 'Profile Image is required.'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

    /**
     * Submit merchant feedback
     *
     * @return Response
     */
    public function rate($id, ReviewRequest $request)
    {   
        $data = [
                'merchant_profile_id' => $id, 
                'review_name' => $request->review_name, 
                'review_email' => $request->review_email, 
                'review_title' => $request->review_title, 
                'review_description' => $request->review_description, 
                'review_rating' => $request->review_rating
                ];

        $review = Review::create($data);

        if($review){
            return json_encode(array('result' => 'success', 'message' => 'Review successfully posted!'));
        }

        return json_encode(array('result' => 'error', 'message' => 'An error occured while saving your review'));
    }


    public function merchantList(Request $request)
    {   
        if(isset($request->q)) {
            
            $this->data['merchantList'] =  MerchantProfile::where('first_name', 'like', '%'.$request->q.'%')
                                ->orWhere('last_name', 'like', '%'.$request->q.'%')
                                ->orderBy('created_at', 'desc')
                                ->get();

        } else if(isset($request->order) || isset($request->date) || isset($request->loc)) {
            $query = MerchantProfile::join('items', 'items.merchant_profile_id', '=', 'merchant_profile.merchant_profile_id');


            if(isset($request->loc)){
                $query->where('merchant_profile.address', 'like', '%'.$request->loc.'%');
            }

            if(isset($request->order)){
                $query->orderBy('merchant_profile.first_name', $request->order);      
            }

            if(isset($request->date)){
                $query->orderBy('merchant_profile.created_at', $request->date);       
            }

            $this->data['merchantList'] = $query->groupBy('merchant_profile.merchant_profile_id')->get();

        } else {

            $this->data['merchantList'] = MerchantProfile::get();
            
        }


        foreach ($this->data['merchantList'] as $key => $value) {
            if($value['account']['status'] != 1){
                $this->data['merchantList']->forget($key);
            }
            $value['ratings'] = ( $value['merchant_ratings'] ) ? $value['merchant_ratings']->where('merchant_profile_id', $value['merchant_profile_id'])->avg('review_rating') : 0 ;
        }

        $this->data['specialty'] = Specialty::all();

        return view('Merchant::merchantList.index', $this->data);
    }

}
