<?php namespace App\Modules\Merchant\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MerchantProfileRequest;
use App\Http\Requests\ChangePasswordRequest;

use Illuminate\Http\Request;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\App\Models\User;
use App\Modules\Item\Models\Item;
use App\Modules\App\Models\Review;

use Auth;

class MerchantReviewController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


    /**
     * Display dashboard
     * @return Response
     */
    public function reviews()
    {   
        $data['email'] = User::where('id', Auth::user()->id)->first();
        $data['merchant'] = MerchantProfile::where('user_id', Auth::user()->id)->first();

        $data['review'] = Review::where('merchant_profile_id', $data['merchant']['merchant_profile_id'])->get();

        return view("Merchant::reviews", $data);
    }
    
    /**
     * Delete Review
     * @return Response
     */
    public function deleteRev($id)
    {  
        $reviewDelete = Review::where('review_id', $id)->delete();
        if ($reviewDelete) {
            return json_encode(array('result' => 'success', 'message' => 'Review Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

}
