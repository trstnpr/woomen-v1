@extends('merchantMaster')

@section('title')
	<title>Woomen - dashboard</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')

	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')

	<div class="merchantDashboard-content" id="content-right">

		<section class="section section-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');">
			<div class="overlay" style="background-image:url('{{ config('s3.bucket_link') . asset('images/grid-60.png') }}');">
				<div class="container">
					<div class="dp-wrap">
						<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');"></figure>
					</div>
					<div class="info">
						<h1 class="merchant-name">{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }}</h1>
						<a class="btn woo-btn" href="{{ url('merchant/view/' . $merchant['user_id']) }}">View Public Profile</a>
					</div>
				</div>
			</div>
		</section>

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Dashboard</h2>
			</div>
		</section>

		<section class="section">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4">
						<div class="card-panel">
							<div class="row">
								<div class="col-sm-5">
									<h4><i class="fa fa-shopping-bag"></i><br/><br/>Items</h4>
								</div>
								<div class="col-sm-7">
									<h5>{{ $item }}</h5>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card-panel">
							<div class="row">
								<div class="col-sm-5">
									<h4><i class="fa fa-eye"></i><br/><br/>Reviews</h4>
								</div>
								<div class="col-sm-7">
									<h5>{{ $review }}</h5>
								</div>
							</div>	
						</div>
					</div>
					<div class="col-md-4">
						<div class="card-panel">
							<div class="row">
								<div class="col-sm-5">
									<h4><i class="fa fa-calendar"></i><br/><br/>Today</h4>
								</div>
								<div class="col-sm-7">
									<h5>
										<span class="day_" id="day_"></span>,
										<span class="year_" id="year_"></span>
									</h5>
									<h5>
										<span class="year_" id="date_"></span>
									</h5>
								</div>
							</div>	
						</div>
					</div>				
				</div>
			</div>
		</section>

		<section class="section">
			<div class="container-fluid">
				<div class="card-panel">
					<h4>Recent added items</h4>
					<hr/>
					<div class="row">
						<div class="recent-added-items">
							@foreach($items as $i)
							{{-- <a href="/merchant/items/view/{{ $i->item_id }}">
							<div class="col-md-12">
								<div class="item-wrap">
									<img src="{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->feat_image['image_path'] }}" class="img-responsive">
									<h5>{{ $i->item_name }}</h5>
									<p>{{ strip_tags(str_limit($i->item_description, 45)) }}</p>	
								</div>
							</div>
							</a> --}}

							<div class="col-md-12">
								<a href="/item/browse/{{ $i->item_id }}" target="_blank">
									<div class="merchandise-item">
										<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->feat_image['image_path'] }}');"></figure>
										<div class="item-info">
											<h4 class="item-name">{{ $i->item_name }}</h4>
											<span class="item-price">{{ 'Php '.number_format($i->item_price, 2) }}</span>
										</div>
									</div>
								</a>
							</div>

							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>	

	</div>

@stop()

@section('custom-scripts')
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick.min.js') }}"></script>
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick-init.js') }}"></script>
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/index.js') }}"></script>
@stop
