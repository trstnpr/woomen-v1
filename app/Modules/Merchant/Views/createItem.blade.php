@extends('merchantMaster')

@section('title')
	<title>Woomen - Create Item</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')
	
	<div class="merchantItems-content">

		<section class="section section-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');">
			<div class="overlay" style="background-image:url('{{ config('s3.bucket_link') . asset('images/grid-60.png') }}');">
				<div class="container">
					<div class="dp-wrap">
						<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');"></figure>
					</div>
					<div class="info">
						<h1 class="merchant-name">{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }}</h1>
						<a class="btn woo-btn" href="{{ url('merchant/view/' . $merchant['user_id']) }}">View Public Profile</a>
					</div>
				</div>
			</div>
		</section>

		<section class="section-tabmenu">
			<div class="container-fluid">
				<div class="card-panel">
					<div class="nav">
						<ul>
							<li id="tab-nav-1" class="nav-active"><a href="{{ url('merchant/item') }}">Items</a></li>
							<li>Create Item</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		@if($all_items->count() >= 50)
			<h3 align="center" style="color: red;">Only 50 Items per account are allowed to post.</h3>
		@else

		<section class="section-content item-form">
			<div class="container-fluid">

				{{-- <div class="form-group">
					<label>Upload Images</label>
					<form action="/file-upload" class="dropzone">
						<div class="fallback">
							<input name="file" type="file" multiple />
						</div>
					</form>
				</div> --}}

				<form id="frm_add_item">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="card-panel">
						<h4>Item Details</h4>
						<hr/>
						<div class="alert alert-success hide" id="result_d">
					   	 	Successfully Added!
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="dropzone" id="myAwesomeDropzone">
										<div class="fallback">
											<input type="file" name="image_path[]" id="files" multiple="multiple" required/>
										</div>
									</div>
								</div>
							</div>
							{{-- <div class="col-md-12">
								<div class="form-group">
									<label>Choose Image</label>
									<div class="well">
										<input type="file" name="image_path[]" id="files" multiple="multiple" />
									</div>
									<button type="button" class="btn btn-warning btn-xs" id="btn-clear">Clear</button>
									<div class="file-wrap">
										<div class="row" id="item-container"></div>
									</div>
								</div>
							</div> --}}
							<div class="col-md-6">
								<div class="form-group">
									<label>Item Name</label>
									<input type="text" class="form-control input-lg" name="item_name" id="item_name" placeholder="Item Name" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Item Price</label>
								   	<div class="input-group">
								     	 <div class="input-group-addon">₱</div>
								      	<input type="text" class="form-control input-lg" id="exampleInputAmount" name="item_price" placeholder="Amount">
								    </div>
								</div>  
							</div>
							<div class="col-md-3">
								<div class="fomr-group">
									<label>Item Quantity</label>
									<input type="number" class="form-control input-lg" name="item_quantity" id="item_quantity" placeholder="Quantity" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Item Description</label>
									<textarea class="form-control wysiwyg" rows="3" name="item_description" id="item_description" placeholder="Item Description" ></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" class="btn btn-lg woo-btn" id="btn_create">Create Item</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</section>

		@endif
	</div>

@stop()

@section('custom-scripts')
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/items/index.js') }}"></script>	
	<script type="text/javascript"></script>
@stop
