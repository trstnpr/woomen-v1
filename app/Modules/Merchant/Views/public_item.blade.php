@extends('publicMerchantMaster')

@section('title')
	<title>Woomen - item</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/items/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/merchant.css') }}" rel="stylesheet" />
@stop

@section('fixedHeader')
<header>
	<nav class="navbar navbar-custom navbar-fixed-top woo-app-nav">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">
					<img alt="Brand" src="{{ asset('images/brand/logo%20gold%20earth.png') }}" />
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			@if(Auth::check())
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="hidden-lg hidden-md">Home</span></a></li>
					<!-- <li><a href="#"><i class="fa fa-envelope"></i> <span class="hidden-lg hidden-md">Message</span></a></li> -->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="hidden-lg hidden-md">Others</span></a>
						<ul class="dropdown-menu">
							<li><a href="{{route('app.about')}}">About Woomen</a></li>
							<li><a href="{{route('app.faq')}}">FAQs</a></li>
							<li><a href="{{route('app.contact')}}">Contact Us</a></li>
							<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> <span class="hidden-lg hidden-md">Account</span></a>
						<ul class="dropdown-menu">
							@if(Auth::user()->role_id == 2)
							<li><a href="/artist/dashboard">Dashboard</a></li>
							@else
							<li><a href="/merchant/dashboard">Dashboard</a></li>
							@endif
							<li><a href="{{ route('app.logout') }}">Logout</a></li>
							</li>
						</ul>
					</li>
				</ul>
			@else
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#login-modal" data-toggle="modal">Login</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Signup</a>
						<ul class="dropdown-menu">
							<li><a href="#artistreg-modal" data-toggle="modal">Artist</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#merchantreg-modal" data-toggle="modal">Merchant</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="hidden-lg hidden-md">Others</span></a>
						<ul class="dropdown-menu">
							<li><a href="{{route('app.about')}}">About Woomen</a></li>
							<li><a href="{{route('app.faq')}}">FAQs</a></li>
							<li><a href="{{route('app.contact')}}">Contact Us</a></li>
							<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
						</ul>
					</li>
				</ul>
			@endif
				{{-- <form class="navbar-form navbar-right">
					<div class="form-group">
						<div class="input-group">
							<input class="form-control" name="q" id="search" placeholder="Search" value="" type="text">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-round">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</div>
				</form> --}}
			</div>
		</div>
	</nav>
</header>
@stop

@section('content')

	<div class="publicItem-content">
		<section class="section single-item-content">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="col-md-12">
							<div class="slider-for">
								@foreach($images as $i)
								<div class="col-md-12">
									<div class="item-wrap">
										<a href="{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->image_path }}" data-lity>
											<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->image_path }}');" class="item-for" data-image-src="{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->image_path }}" title="Click to View Full Image"></figure>
										</a>
									</div>
								</div>
								@endforeach
							</div>								
						</div>
						<div class="col-md-12">
							<div class="slider-nav">
								@foreach($images as $im)
								<div class="col-md-12">
									<div class="item-wrap">
										<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $im->image_path }}');" class="item-nav"></figure>
									</div>
								</div>
								@endforeach
							</div>	
						</div>
<!-- 						<div class="col-md-12">
							<div class="slider-for">
								@for($i=1;$i<10;$i++)
								<div class="col-md-12">
									<div class="item-wrap">
										<figure style="background-image:url('{{ asset('images/merchant/item2.jpg') }}');" class="item-for"></figure>
									</div>
								</div>
								@endfor
							</div>								
						</div>
						<div class="col-md-12">
							<div class="slider-nav">
								@for($i=1;$i<10;$i++)
								<div class="col-md-12">
									<div class="item-wrap">
										<figure style="background-image:url('{{ asset('images/merchant/item2.jpg') }}');" class="item-nav"></figure>
									</div>
								</div>
								@endfor
							</div>	
						</div> -->
					</div>
					<div class="col-md-5">
						<div class="item-info">
							<h3>{{ $items['item_name'] }}</h3>
							<p>{!! $items['item_description'] !!}</p>
							<hr/
>							{{-- <span><i class="fa fa-phone"></i> Contact: {{ $items['item_contact'] }}</span><br/> --}}
							<span><i class="fa fa-shopping-bag"></i> Available stock: {{ $items['item_quantity'] }}</span><br/>
							<span><i class="fa fa-tag"></i> Price: {{ 'PHP '.number_format($items['item_price'], 2) }}</span><br/>
							<span><i class="fa fa-user"></i> Seller: 
								@if($items->merchant)
								<a href="{{ route('merchant.list.show', $items->merchant->user_id) }}">{{ $items->merchant->first_name.' '.$items->merchant->last_name }}</a>
								@else
								<a href="{{ route('artist.list.show', $items->artist->artist_profile_id) }}">{{ $items->artist->first_name.' '.$items->artist->last_name }}</a>
								@endif
							</span><br/>
							<span><i class="fa fa-question-circle"></i> <button class="btn btn-xs btn-warning" data-toggle="modal" data-target="#inquire">INQUIRE</button></span>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="section other-items">
			<div class="container">
				<h3>You may also like these</h3>
				<hr/>
				<div class="row">
					<div class="more-items">
					@foreach($all_items as $all)
<!-- 						<a href="/item/browse/{{ $all->item_id }}">
							<div class="col-md-12">
								<div class="item-wrap">
									<img src="{{ config('s3.bucket_link') . config('cdn.item') .'/'. $all->image_path }}" class="img-responsive">
								</div>
							</div>
						</a> -->
						<div class="col-md-12">
							<a href="/item/browse/{{ $all->item_id }}">
								<div class="merchandise-item">
									<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $all->image_path }}');"></figure>
									<div class="item-info">
										<h4 class="item-name">{{ $all->item->item_name }}</h4>
										<span class="item-price">PHP {{ number_format($all->item->item_price, 2) }}</span>
									</div>
								</div>
							</a>
						</div>
					@endforeach				
					</div>	
				</div>
			</div>
		</section>

		<!-- Review Modal -->
		<div class="modal fade woo-modal" id="inquire" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog" role="document">
		        <form id="inquire-form" data-url="{{ route('merchant.public.item.inquire', $items->item_id) }}">
		            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
		            <input type="hidden" name="seller_email" value="{{ ($items->merchant) ? $items->merchant->account->email : $items->artist->account->email }}">
		            <input type="hidden" name="item_name" value="{{ $items->item_name }}">
		            <input type="hidden" name="first_name" value="{{ ($items->merchant) ? $items->merchant->first_name : $items->artist->first_name }}">
		            <input type="hidden" name="last_name" value="{{ ($items->merchant) ? $items->merchant->last_name : $items->artist->last_name }}">
		            <div class="modal-content">
		                <div class="modal-header gold">
		                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                    <h4 class="modal-title" id="myModalLabel">Inquiry Form</h4>
		                </div>
		                <div class="modal-body">
		                    <div class="form-group">
		                        <label>Name</label>
		                        <input type="text" class="form-control" name="inquire_name" placeholder="Your Name" required />
		                    </div>
		                    <div class="form-group">
		                        <label>Email (we do not publish this in public)</label>
		                        <input type="email" class="form-control" name="inquire_email" placeholder="Your Email Address" required />
		                    </div>
		                    <div class="form-group">
		                        <label>Contact Number</label>
		                        <input type="text" class="form-control" name="inquire_contact" placeholder="Your Contact No." required />
		                    </div>
		                    <div class="form-group">
		                        <label>Message</label>
		                        <textarea class="form-control" name="inquire_message" rows="4" placeholder="Your Inquiries" required ></textarea>
		                    </div>
		                    <div class="col-md-12">
		                        <div class="alert alert-success hide" id="inquire_result_div">
		                            <ul class="fa-ul">
		                                <li><i class="fa fa-li fa-check"></i> Success!</li>
		                                <li><i class="fa fa-li fa-exclamation"></i> Warning!</li>
		                                <li><i class="fa fa-li fa-times"></i> Error/Danger!</li>
		                            </ul>
		                        </div>
		                    </div>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn woo-btn" data-dismiss="modal">Cancel</button>
		                    <button type="submit" id="inquire-button" class="btn woo-btn">Submit</button>
		                </div>
		            </div>
		        </form>
		    </div>
		</div>


		<div class="modal fade" id="viewPhoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		          <img src="#" style="width:100%;" id="photo-src">
		      </div>
		    </div>
		  </div>
		</div>

	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick.min.js') }}"></script>
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick-init.js') }}"></script>
@stop