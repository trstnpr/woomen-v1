@extends('merchantMaster')

@section('title')
	<title>Woomen - Edit Item</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')

	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/index.css') }}" rel="stylesheet" />

@stop

@section('content')
	
	<div class="merchantItems-content">

		<section class="section section-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');">
			<div class="overlay" style="background-image:url('{{ config('s3.bucket_link') . asset('images/grid-60.png') }}');">
				<div class="container">
					<div class="dp-wrap">
						<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');"></figure>
					</div>
					<div class="info">
						<h1 class="merchant-name">{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }}</h1>
						<a class="btn woo-btn" href="{{ url('merchant/view/' . $merchant['user_id']) }}">View Public Profile</a>
					</div>
				</div>
			</div>
		</section>

		<section class="section-content item-form">
			<div class="container-fluid">

				{{-- <div class="form-group">
					<label>Upload Photos</label>
					<form action="/file-upload" class="dropzone">
						<div class="fallback">
							<input name="file" type="file" multiple />
						</div>
					</form>
				</div> --}}

				<form id="frm_update_item">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="card-panel">
						<h4>Edit Item</h4>
						<hr/>
						<div class="alert alert-success hide" id="result_d">
						    Successfully Updated!
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="dropzone" id="myAwesomeDropzone">
										<div class="fallback">
											<input type="file" name="image_path[]" id="files" multiple="multiple"/>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									{{-- <label>Choose Images</label>
									<div class="well">
										<input type="file" name="image_path[]" id="files" multiple="multiple" />
									</div>
									<label><button type="button" class="btn btn-warning btn-xs" id="btn-clear">Clear</button></label>
									<div class="file-wrap">
										<div class="row" id="item-container"></div>
									</div> --}}
									<label>Current Images</label>
									<div class="itemimg-wrap">
										<div class="row">
											@foreach($item_images as $image)
											<div class="col-md-2">
												<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $image->image_path }}');">
													<div class="overlay">
														<a href="#!" type="button" id="delete_item_button" data-id="{{ $image->item_image_id }}" class="btn btn-warning btn-xs delete_item_button"><i class="fa fa-times"></i></a>
													</div>
												</figure>
											</div>
											@endforeach
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Item Name</label>
									<input type="text" class="form-control input-lg" name="item_name" id="item_name" placeholder="Item Name" value="{{ $item['item_name'] }}" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Item Price</label>
								   	<div class="input-group">
								     	 <div class="input-group-addon">₱</div>
								      	<input type="text" class="form-control input-lg" id="InputAmount" name="item_price" placeholder="Amount" value="{{ $item['item_price'] }}">
								    </div>
								</div>  
							</div>
							<div class="col-md-3">
								<div class="fomr-group">
									<label>Item Quantity</label>
									<input type="text" class="form-control input-lg" name="item_quantity" id="item_quantity" placeholder="Item Quantity" value="{{ $item['item_quantity'] }}" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Item Description</label>
									<textarea class="form-control wysiwyg" rows="3" name="item_description" id="item_description" placeholder="Item Description" >{!! $item['item_description'] !!}</textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" class="btn btn-lg woo-btn" btn="btn_edit">Save Changes</button>
								</div>
							</div>
						</div>
					</div>
				</form>
		</section>
	</div>

	<input type="hidden" id="item_id" value="{{ $item['item_id'] }}">

@stop()

@section('custom-scripts')
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/items/index.js') }}"></script>
	<script type="text/javascript"></script>
@stop