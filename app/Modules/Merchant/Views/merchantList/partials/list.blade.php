<section class="artistlist-container">
	<div class="container-fluid" style="padding:0;">
		<div class="col-md-12" style="padding:0;">
			<div class="col-md-12">
				<h3 style="text-align:center;"><b><span>{{ $merchantList->count() }}</span> merchants found</b></h3>
			</div>
			<div class="artists-wrap">
				@foreach($merchantList as $merchant)
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<div class="artist-item">
							<div class="avatar-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant->profile_image }}');">
								<div class="overlay">
									<figure class="artist-avatar" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant->profile_image }}');"></figure>
								</div>
							</div>
							<div class="info">
								<input value="{{ $merchant->ratings }}" class="rating" data-size="xxs" data-display-only="true"/>
								<h4 class="artist-name">{{ ucwords($merchant->first_name).' '.ucwords($merchant->last_name) }}</h4>
								<h5 class="forte">{{ $merchant->address }}</h5>
								<a href="{{ route('merchant.list.show', $merchant->user_id) }}" class="btn woo-btn btn-block btn-sm">VIEW PROFILE</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>		
	</div>
</section>	

