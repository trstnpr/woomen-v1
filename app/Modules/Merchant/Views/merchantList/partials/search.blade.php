<div id="toggle-show-search">
	<i class="fa fa-search"></i>
</div>
<section class="search-header">
	<div class="col-md-5">
		<form class="search" method="GET">
			<div class="form-group">
				<div class="input-group">
					<input class="form-control" name="q" id="search" placeholder="Search for merchant" value="{{ (isset($_REQUEST['q'])) ? $_REQUEST['q'] : '' }}" type="text">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-round gold">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-7 filter-search">
		<form>
			<div class="col-md-4 no-padding">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-map-marker"></i></span>
						<input type="text" class="form-control" placeholder="Location" name="loc" value="{{ Request::get('loc') }}" />
					</div>
				</div>
			</div>
			<div class="col-md-6 no-padding">
				<div class="form-group col-md-6">
					<select class="form-control" name="order">
						<option value selected disabled>Order by:</option>
						<option value="asc">Ascending (A-Z)</option>
						<option value="desc">Descending (Z-A)</option>
					</select> 
				</div>	
				<div class="form-group col-md-6">
					<select class="form-control" name="date">
						<option value selected disabled>Order by date:</option>
						<option value="asc">New Merchant</option>
						<option value="desc">Old Merchant</option>
					</select> 
				</div>		
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<button class="btn btn-default btn-block gold">Filter</button>
				</div>
			</div>
		</form>	
		<div class="col-md-12">
			<button class="btn btn-default" id="toggle-hide-search">Close</button>
		</div>
	</div>
</section>
