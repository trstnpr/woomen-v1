@extends('appMaster')

@section('title')
	<title>Woomen - Artist</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	<style>
		* {
			font-family: Arial, Helvetica, sans-serif;
		}
		
		.navbar {
			background-color: #fff;
    		box-shadow: none !important;
		}
    
	</style>
@stop

@section('content')

	@include('App::universalHead')

	<div class="artistlist-content">

		@include('Merchant::merchantList.partials.search')

		@include('Merchant::merchantList.partials.list')

	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('.filter-search').submit(function(){
				if($('input[name="loc"]').val() == ""){
					$('input[name="loc"]').remove();
				}
				if($('input[name="quote"]').val() == ""){
					$('input[name="quote"]').remove();
				}
			});


			$("#toggle-show-search").click(function(){
				$(".search-header").slideDown();
				$("#toggle-show-search").hide();
			});		

			$("#toggle-hide-search").click(function(){
				$(".search-header").slideUp();
				$("#toggle-show-search").show();
			});	

		});
	</script>
@stop