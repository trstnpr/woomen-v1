@extends('merchantMaster')

@section('title')
	<title>Woomen - Items</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')
	<div class="merchantItems-content" id="content-right">
	
		<section class="section section-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');">
			<div class="overlay" style="background-image:url('{{ config('s3.bucket_link') . asset('images/grid-60.png') }}');">
				<div class="container">
					<div class="dp-wrap">
						<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');"></figure>
					</div>
					<div class="info">
						<h1 class="merchant-name">{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }}</h1>
						<a class="btn woo-btn" href="{{ url('merchant/view/' . $merchant['user_id']) }}">View Public Profile</a>
					</div>
				</div>
			</div>
		</section>

		<section class="section-tabmenu">
			<div class="container-fluid">
				<div class="card-panel">
					<div class="nav">
						<ul>
							<li id="tab-nav-1" class="nav-active">Items</li>
							<li><a href="{{ url('merchant/item/create') }}">Create Item</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Items</h2>
			</div>
		</section>

		<section class="section" id="tab-panel-1">
			<div class="container-fluid">
				<div class="card-panel recent-items">
					<h4>Recent Added Items</h4>
					<hr/>
					<div class="row">
						<div class="recent-added-items">
							@foreach($items as $i)
							<div class="col-md-12">
								<a href="/item/browse/{{ $i->item_id }}" target="_blank">
									<div class="merchandise-item">
										<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->feat_image['image_path'] }}');"></figure>
										<div class="item-info">
											<h4 class="item-name">{{ $i->item_name }}</h4>
											<span class="item-price">{{ 'Php '.number_format($i->item_price, 2) }}</span>
										</div>
									</div>
								</a>
							</div>
							@endforeach	
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="section" id="tab-panel-1">
			<div class="container-fluid">
				<div class="alert alert-success hide" id="result_d">
					Successfully Deleted!
				</div>

				<div class="card-panel">
					<h4>All Items</h4>
					<hr/>
					
					<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th>Item Name</th>
				                <th>Description</th>
				                <th>Quantity</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@foreach($all_items as $item)
				            <tr>
				                <td>{{ $item->item_name }}</td>
				                <td>{{ strip_tags(str_limit($i->item_description, 45)) }}</td>
				                <td>{{ $item->item_quantity }}</td>
				                <td>
				                	<a href="/merchant/items/view/{{ $item->item_id }}" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>
				                	<a href="/merchant/item/edit/{{ $item->item_id }}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o" data-id="{{ $item->item_id }}"></i></a>					
				                	<button id="btn-delete-item" type="button" class="btn btn-default btn-xs" title="Delete" data-toggle="modal" data-target="#delete-item"
				                        onclick="deleteItem(this)"
				                        data-item-id="{{ $item->item_id }}">
				                        <i class="fa fa-trash"></i>
				                    </button>
				                </td>
				            </tr>
				            @endforeach
				        </tbody>
				    </table>
				</div>
			</div>
		</section>
		
	</div>

    <div class="modal fade woo-modal" id="delete-item">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>

@stop()

@section('custom-scripts')
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick.min.js') }}"></script>
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick-init.js') }}"></script>
	
	
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/items/index.js') }}"></script>
	<script type="text/javascript">
	    function viewItem(item) {
	        $('#name').html($(item).attr('data-item-name'));
	        $('#description').html($(item).attr('data-item-description'));
	        $('#contact').html($(item).attr('data-item-contact'));
	        $('#quantity').html($(item).attr('data-item-quantity'));
	        $('#image').html($(item).attr('data-item-image'));

	        var item_id = $(item).attr('data-item-id');

	        $.get('/merchant/item/images/'+item_id, function(data){
		        if(data.result) {
		            var output = [];
	                    $.each(data.message, function(key, value){
	                        console.log(value.image_path);
	                    });
		        }
		    });
	    }

	    function deleteItem(item) {
	        $('#confirm-button').attr('data-item-id', $(item).attr('data-item-id'));
	    }

	
	
	    $('.dataTable').DataTable({
		    responsive: true
		});

	</script>
@stop