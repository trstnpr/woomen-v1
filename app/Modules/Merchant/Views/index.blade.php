@extends('publicMerchantMaster')

@section('title')
	<title>Woomen - Merchant</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/profile/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/merchant.css') }}" rel="stylesheet" />
@stop

@section('fixedHeader')
<header>
	<nav class="navbar navbar-custom navbar-fixed-top woo-app-nav">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">
					<img alt="Brand" src="{{ asset('images/brand/logo%20gold%20earth.png') }}" />
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			@if(Auth::check())
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="hidden-lg hidden-md">Home</span></a></li>
					<!-- <li><a href="#"><i class="fa fa-envelope"></i> <span class="hidden-lg hidden-md">Message</span></a></li> -->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="hidden-lg hidden-md">Others</span></a>
						<ul class="dropdown-menu">
							<li><a href="{{route('app.about')}}">About Woomen</a></li>
							<li><a href="{{route('app.faq')}}">FAQs</a></li>
							<li><a href="{{route('app.contact')}}">Contact Us</a></li>
							<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> <span class="hidden-lg hidden-md">Account</span></a>
						<ul class="dropdown-menu">
							@if(Auth::user()->role_id == 2)
							<li><a href="{{url('artist/dashboard')}}">Dashboard</a></li>
							@else
							<li><a href="{{url('merchant/dashboard')}}">Dashboard</a></li>
							@endif
							<li><a href="{{ route('app.logout') }}">Logout</a></li>
							</li>
						</ul>
					</li>
				</ul>
			@else
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#login-modal" data-toggle="modal">Login</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Signup</a>
						<ul class="dropdown-menu">
							<li><a href="#artistreg-modal" data-toggle="modal">Artist</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#merchantreg-modal" data-toggle="modal">Merchant</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="hidden-lg hidden-md">Others</span></a>
						<ul class="dropdown-menu">
							<li><a href="{{route('app.about')}}">About Woomen</a></li>
							<li><a href="{{route('app.faq')}}">FAQs</a></li>
							<li><a href="{{route('app.contact')}}">Contact Us</a></li>
							<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
						</ul>
					</li>
				</ul>
			@endif
				{{-- <form class="navbar-form navbar-right">
					<div class="form-group">
						<div class="input-group">
							<input class="form-control" name="q" id="search" placeholder="Search" value="" type="text">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-round">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</div>
				</form> --}}
			</div>
		</div>
	</nav>
</header>
@stop

@section('content')

	<div class="merchantPublic-content">
		<section class="section section-cover" style="background-image:url('{{ asset('images/merchant/merchant-cover.jpg') }}');">
			<div class="container">
				<div class="dp-container">
					<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');" class='dp'></figure>
					<div class="merchant-subscribe">
						<a href="{{ $merchant['facebook'] }}" target="_blank"><i class="fa fa-facebook"></i></a>&nbsp&nbsp
						<i class="fa fa-star" data-toggle="modal" data-target="#view-review"></i>
					</div>
				</div>
			</div>
		</section>
		<section class="section section-divider">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3>{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }} <br/></h3>
						<span>{{ ucfirst($merchant['address']) }}</span>
					</div>
				</div>
			</div>
		</section>
		<section class="section section-featured-items">
			<div class="container card-panel" style="border:none;">
				<h2 class="section-title">Merchandise</h2>
				<hr/><br/>
				<div class="merchandise-wrap">
					<div class="merchandise-carousel row">
						@foreach($item as $i) 
						<div class="col-md-12">
							<a href="/item/browse/{{ $i->item_id }}">
								<div class="merchandise-item">
									<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i['feat_image']['image_path'] }}');"></figure>
									<div class="item-info">
										<h4 class="item-name">{{ $i->item_name }}</h4>
										<span class="item-price">PHP {{ $i->item_price }}</span>
									</div>
								</div>
							</a>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</section>
	</div>


	<!--view review modal-->
	<div class="modal fade woo-modal" id="view-review" tabindex="-1" role="dialog">
		<div class="modal-dialog review-modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header review-modal-header">
		    		<h3 class="text-center">Write a Review</h3>
		    	</div>
		    	<form id="form_feedback">
		    		<input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
					<div class="modal-body review-modal-body">
						<div class="col-md-12 alert alert-success hide" id="result_div">
					  		<ul class="fa-ul">
					  			<li><i class="fa fa-li fa-check"></i> Success!</li>
					  		</ul>
						</div>


						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" name="review_name" placeholder="Your name">
						</div>
						<div class="form-group">
							<label>Email Address</label>
							<input type="email" class="form-control" name="review_email" placeholder="Your Email Address">
						</div>
						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" name="review_title" placeholder="Review subject/title">
						</div>
						<div class="form-group">
							<label>Review</label>
							<textarea placeholder="Your review/feedback" name="review_description" class="form-control review-textarea"></textarea>
						</div>
						<div class="form-group">
							<label>Rate this merchant</label>
							<input id="input-id" type="text" name="review_rating" class="rating" data-size="xs" >
						</div>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn woo-btn review-submit-btn" id="btn_feedback" value="Send">			
						<button type="button" class="btn woo-btn" data-dismiss="modal">Cancel</button>				
					</div>
				</form>
			</div>
		</div>
	</div>

	<input type="hidden" id="m_user_id" value="{{ $merchant['merchant_profile_id'] }}">
@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')

	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick-init.js') }}"></script>
	<script type="text/javascript"></script>
		<script>
	$(document).on('ready', function(){
		// initialize with defaults
		$("#input-id").rating();

		// with plugin options (do not attach the CSS class "rating" to your input if using this approach)
		$("#input-id").rating({'size':'xs'});

		//test check value
		$('#input-id').on('rating.change', function(event, value, caption) {
		    console.log(value);
		    console.log(caption);
		});

	});

		var id = $("#m_user_id").val();

		$('#form_feedback').on('submit', (function(e){
	        e.preventDefault();
	        $.ajax({
	            url: "/merchant/rate/"+id,
	            type: "POST",
	            headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            data: new FormData(this),
	            contentType: false,
	            cache: false,
	            processData: false,
	            beforeSend: function(){ $('#btn_feedback').html('Processing...');},
	            error: function(data){
	                if(data.readyState == 4){
	                    errors = JSON.parse(data.responseText);
	                    $('#result_div').empty();
	                    $('#result_div').html('<ul class="error_list fa-ul white-text"></ul>');
	                    $.each(errors,function(key,value){
	                        $('.error_list').append('<li><i class="fa fa-li fa-times"></i> '+value+'</li>');
	                    });
	                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
	                    $('#btn_feedback').html('Save Changes');

	                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
	                }
	            },

	            success: function(data){
	                var msg = JSON.parse(data);
	                if(msg.result == 'success'){
	                    $("#btn_feedback").html('Save Changes');
	                    $('#result_div').removeClass('alert-danger hide').addClass('alert-success');
	                    $('#result_div').html(msg.message);
	                    $('#result_div').removeClass('hide');
	                    setTimeout(function(){location.reload();}, 3000);
	                    
	                } else{
	                    $('#result_div').removeClass('alert-success hide').addClass('alert-danger');
	                    $('#result_div').html(msg.message);
	                    setTimeout(function(){$('#result_div').addClass('hide').html(''); }, 4000);
	                    $("#btn_feedback").html('Save Changes');
	                }
	            }
	        });
	    }));
	</script>
@stop