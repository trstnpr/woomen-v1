@extends('merchantMaster')

@section('title')
	<title>Woomen - reviews</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	
	{{-- <link href="{{ config('s3.bucket_link') . elixir('assets/merchant/merchant.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/reviews/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/index.css') }}" rel="stylesheet" /> --}}
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')
	<div class="merchantReview-content" >

		<section class="section section-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');">
			<div class="overlay" style="background-image:url('{{ config('s3.bucket_link') . asset('images/grid-60.png') }}');">
				<div class="container">
					<div class="dp-wrap">
						<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');"></figure>
					</div>
					<div class="info">
						<h1 class="merchant-name">{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }}</h1>
						<a class="btn woo-btn" href="{{ url('merchant/view/' . $merchant['user_id']) }}">View Public Profile</a>
					</div>
				</div>
			</div>
		</section>

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Reviews</h2>
			</div>
		</section>

		<section class="section">
			<div class="container-fluid">
				<div class="card-panel">
					<h4>REVIEWS</h4>
					<hr/>
					<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th>Review by</th>
				                <th>Email</th>
				                <th>Review</th>
				                <th>Rating</th>
				                <th>Date</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				        <tbody>
				   		@foreach($review as $rev)
				            <tr>
				                <td>{{ $rev->review_name }}</td>
				                <td>{{ $rev->review_email }}</td>
				                <td>{{ $rev->review_title }}</td>
				                <td>{{ $rev->review_rating }}</td>
				                <td>{{ date_format(date_create($rev->created_at), "F d, Y") }} {{ date_format(date_create($rev->event_time), "g:i A") }}</td>
				                <td>
				                	<a role="button" href="#view-review" data-toggle="modal" class="btn btn-info btn-xs" title="View" data-target="#view-review" onclick="viewReview(this)"
				                        data-review-id="{{ $rev->review_id }}"
				                        data-review-name="{{ ucfirst($rev->review_name) }}"
				                        data-review-description="{{ $rev->review_description }}"
				                        data-review-email="{{ $rev->review_email }}"
				                        data-review-rating="{{ $rev->review_rating }}"
				                        data-review-date="{{ date_format(date_create($rev->created_at), 'F d, Y') }} {{ date_format(date_create($rev->event_time), 'g:i A') }}">View</a>
				                    <a role="button" href="#delete-review" data-toggle="modal" class="btn btn-danger btn-xs" title="Delete" data-review-id="{{ $rev->review_id }}" onclick="deleteReview(this)">Delete</a>
				                </td>
				            </tr>
						@endforeach
				        </tbody>
					</table>				
				</div>
			</div>
		</section>

		<!--view modal-->
	<div class="modal fade woo-modal" id="view-review" tabindex="-1" role="dialog">
		<div class="modal-dialog review-modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header review-modal-header">
		    		<!-- <figure style="background-image:url('{{ asset('images/merchant/merchant2.jpg') }}');" class='reviewer-figure center-block'></figure> -->
		    		<h3 class="text-center" id="name"></h3>
		    	</div>
				<div class="modal-body review-modal-body">
					<p id="description"></p>
					<span id="date"></span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
				</div>
			</div>
		</div>
	</div>

	<!-- modal confirm delete merchant -->
	<div class="modal fade woo-modal" id="delete-review">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this review?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-review" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

	</div>
@stop()

@section('custom-scripts')
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/review/index.js') }}"></script>
	<script type="text/javascript">
	    function viewReview(review) {
	        $('#name').html($(review).attr('data-review-name'));
	        $('#title').html($(review).attr('data-review-title'));
	        $('#description').html($(review).attr('data-review-description'));
	        $('#date').html($(review).attr('data-review-date'));    
	    }

	    function deleteReview(review) {
	        $('#confirm-button-review').attr('data-review-id', $(review).attr('data-review-id'));
	    }

	    $('.dataTable').DataTable({
		    responsive: true
		});

		$(document).on('ready', function(){
			$('#input-id').val("1");
			// initialize with defaults
			$("#input-id").rating();

			// with plugin options (do not attach the CSS class "rating" to your input if using this approach)
			$("#input-id").rating({'size':'xs'});

			//test check value
			$('#input-id').on('rating.change', function(event, value, caption) {
			    console.log(value);
			    console.log(caption);
			});

		});


	</script>
@stop