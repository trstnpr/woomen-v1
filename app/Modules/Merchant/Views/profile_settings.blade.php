@extends('merchantMaster')

@section('title')
	<title>Woomen - profile settings</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	
	{{-- <link href="{{ config('s3.bucket_link') . elixir('assets/merchant/merchant.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/profile/index.css') }}" rel="stylesheet" /> --}}

	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/index.css') }}" rel="stylesheet" />

@stop

@section('content')
	<div class="merchantProfile-content">

		<section class="section section-cover" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');">
			<div class="overlay" style="background-image:url('{{ config('s3.bucket_link') . asset('images/grid-60.png') }}');">
				<div class="container">
					<div class="dp-wrap">
						<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');"></figure>
					</div>
					<div class="info">
						<h1 class="merchant-name">{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }}</h1>
						<a class="btn woo-btn" href="{{ url('merchant/view/' . $merchant['user_id']) }}">View Public Profile</a>
					</div>
				</div>
			</div>
		</section>

		<section class="section-tabmenu">
			<div class="container-fluid">
				<div class="card-panel">
					<div class="nav">
						<ul>
							<li class="nav-active" id="tab-nav-1">Edit Profile</li>
							<li id="tab-nav-2">Change Password</li>
							<li id="tab-nav-3">Change Profile Picture</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Account Settings</h2>
			</div>
		</section>

		<section class="section">
			<div class="container-fluid">
				<div class="card-panel">
					<div id="tab-panel-1">
						<h4>Edit Profile</h4>
						<hr/>
						<form id="frm_profile_update">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="alert alert-success hide" id="result_d">
							    Updated Successfully!
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>First Name</label>
										<input type="text" class="form-control input-lg" name="first_name" value="{{ $merchant['first_name'] }}">
									</div>	
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Middle Name</label>
										<input type="text" class="form-control input-lg" name="middle_name" value="{{ $merchant['middle_name'] }}">
									</div>	
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Last Name</label>
										<input type="text" class="form-control input-lg" name="last_name" value="{{ $merchant['last_name'] }}">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Facebook</label>
										<input type="text" class="form-control input-lg" name="facebook" value="{{ $merchant['facebook'] }}" placeholder="https://facebook.com/profile">
									</div>		
								</div>
							
								<div class="col-md-12">
									<div class="form-group">
										<label>Address</label>
										<input type="text" class="form-control input-lg" name="address" value="{{ $merchant['address'] }}">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input type="submit" class="woo-btn" value="Save changes" id="btn_profile">
									</div>	
								</div>
							</div>
						</form>
					</div>
					<div id="tab-panel-2">
						<h4>Change Password</h4>	
						<hr/>
						<form id="frm_password_update">
							<div class="alert alert-success hide" id="result_div">
							    Updated Successfully!
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Current Password</label>
										<input type="password" class="form-control input-lg" name="current_pass" id="current_pass">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>New Password</label>
										<input type="password" class="form-control input-lg" name="password" id="password">
									</div>	
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Confirm Password</label>
										<input type="password" class="form-control input-lg" name="password_confirmation" id="password_confirmation">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="submit" name="" class="woo-btn" value="Save changes" id="btn_password">
									</div>		
								</div>
							</div>
						</form>		
					</div>	
					<div id="tab-panel-3" >
						<h4>Change Profile Picture</h4>	
						<hr/>
						<div class="alert alert-success hide" id="result_divs">
						    Updated Successfully!
						</div>
						<form id="form_upload_image" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div id="application-upload-image">
								<img src="{{ asset('images/merchant/upload-photo-150x150.jpg') }}" alt="..." style="width:150px;height:150px;"  class="img-thumbnail img-responsive" id="application-image">
								<input type="file" name="profile_image" id="input-application-image" onchange="readUrl(this);">
							</div>
							<br/><p>Click upload photo to select photo</p>
							<hr/>
							<div class="form-group">
								<button type="submit" id="upload_image_btn" class="woo-btn">Save changes</button>
							</div>
						</form>
					</div>		
				</div>
			</div>
		</section>	
	</div>
@stop()

@section('custom-scripts')
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/profile/index.js') }}"></script>
@stop