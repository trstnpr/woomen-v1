@extends('merchantMaster')

@section('title')
	<title>Woomen - dashboard</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/merchant.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/merchant/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')
	<div class="merchantItems-content" id="content-right">
		{{-- <section class="section">
			<div class="card card-cover">
				<div class="row">
					<div class="col-md-12">
						<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');" class='dp'></figure>
						<ul class="info">
							<li>{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }}</li>
							<li><small>Merchant</small></li>
							<li>{{ $email['email'] }}</li>
							<li><a href="{{ url('merchant/view/' . $merchant['user_id']) }}">View public profile</a></li>
						</ul>
					</div>
				</div>
				<hr/>
				<div class="row">
					<div class="col-md-12">
						<div class="nav">
							<ul>
								<a href="{{ url('merchant/item') }}"><li id="tab-nav-1" class="nav-active">Items</li></a>
							</ul>
						</div>	
					</div>
				</div>	
			</div>
		</section> --}}

		<section class="section section-cover" style="background-image:url('{{ config('s3.bucket_link') . asset('images/merchant/merchant-cover.jpg') }}');">
			<div class="overlay" style="background-image:url('{{ config('s3.bucket_link') . asset('images/grid-60.png') }}');">
				<div class="container">
					<div class="dp-wrap">
						<figure class="dp" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant['profile_image'] }}');"></figure>
					</div>
					<div class="info">
						<h1 class="merchant-name">{{ ucwords($merchant['first_name']) }} {{ ucwords($merchant['last_name']) }}</h1>
						<a class="btn woo-btn" href="{{ url('merchant/view/' . $merchant['user_id']) }}">View Public Profile</a>
					</div>
				</div>
			</div>
		</section>


		<section class="section">
			<div class="container-fluid">
				<div class="card">
					<h3>{{ ucwords($items['item_name']) }}</h3>
					<hr/>
					<section class="section">
						<div class="row">
							<div class="col-md-6"">
								<div style="background-color:white;min-height:400px;padding-left:30px;">
									<h4>Description</h4>
									<p>{!! $items['item_description'] !!} </p>
									<hr/>
									<h4>Quantity</h4>
									<span>{{ $items['item_quantity'] }}</span>
									<hr/><br/>
									<a href="{{ url('merchant/item') }}">
										<button class="woo-btn">Go Back</button>
									</a>	
									<a href="/merchant/item/edit/{{ $items['item_id'] }}">
										<button class="woo-btn">Edit Item</button>
									</a>	
								</div>
							</div>										
							<div class="col-md-6 merchantViewSingle-item">
								<div class="col-md-12">
									<div class="slider-for">
										@foreach($images as $i)
										<div class="col-md-12">
											<div class="item-wrap">
												<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->image_path }}');" class="item-for"></figure>
											</div>
										</div>
										@endforeach
									</div>								
								</div>
								<div class="col-md-12">
									<div class="slider-nav">
										@foreach($images as $im)
										<div class="col-md-12">
											<div class="item-wrap">
												<img src="{{ config('s3.bucket_link') . config('cdn.item') .'/'. $im->image_path }}" class="img-responsive">
											</div>
										</div>
										@endforeach
									</div>	
								</div>
<!-- 								<div class="col-md-12">
									<div class="slider-for">
										@for($i=1;$i<10;$i++)
										<div class="col-md-12">
											<div class="item-wrap">
												<figure style="background-image:url('{{ asset('images/merchant/item1.jpg') }}');" class="item-for"></figure>
											</div>
										</div>
										@endfor
									</div>								
								</div>
								<div class="col-md-12">
									<br>
									<div class="slider-nav">
										@for($i=1;$i<10;$i++)
										<div class="col-md-12">
											<div class="item-wrap">
												<img src="{{ asset('images/merchant/item1.jpg') }}" class="img-responsive">
											</div>
										</div>
										@endfor
									</div>	
								</div> -->
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>	
	</div>

@stop()

@section('custom-scripts')
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick.min.js') }}"></script>
	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/dashboard/slick-init.js') }}"></script>

	<script type="text/javascript" src="{{ config('s3.bucket_link') . elixir('assets/merchant/views/items/index.js') }}"></script>
	<script type="text/javascript">
	</script>
@stop