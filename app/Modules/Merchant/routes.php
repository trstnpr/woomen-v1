<?php

Route::group(array('module' => 'Merchant', 'namespace' => 'App\Modules\Merchant\Controllers', 'middleware' => ['web'], 'prefix' => 'merchant'), function() {

    /*Route::resource('Merchant', 'MerchantController');*/
    Route::get('/view/{id}', ['uses' => 'MerchantController@index', 'as' => 'merchant.list.show']);
    Route::post('/rate/{id}', 'MerchantController@rate');

    
});

Route::group(array('module' => 'Merchant', 'namespace' => 'App\Modules\Merchant\Controllers', 'middleware' => ['web', 'auth', 'merchant'], 'prefix' => 'merchant'), function() {


    Route::get('/dashboard', ['uses' => 'MerchantController@dashboard', 'as' => 'merchant.dashboard']); 

    /*view, update Merchant Profile*/
    Route::get('/profile', ['uses' => 'MerchantController@profile', 'as' => 'merchant.profile']);
    Route::post('/updateProfile', ['uses' => 'MerchantController@updateProfile', 'as' => 'merchant.profile.update']);

    /**Change Password*/
    Route::post('/passwordSettings/update', ['uses' => 'MerchantController@changePassword', 'as' => 'merchant.password.update']);

    /**UploadImage*/
    Route::post('/uploadImage', ['uses' => 'MerchantController@uploadImage', 'as' => 'merchant.uploadImage']);
    


    /*merchant item*/
    Route::get('/items/view/{id}', ['uses' => 'MerchantItemController@merchant_item_view', 'as' => 'merchant.dashboard.item']);
    Route::get('/reviews', ['uses' => 'MerchantReviewController@reviews', 'as' => 'merchant.reviews']);
    Route::delete('/reviews/delete/{id}', ['uses' => 'MerchantReviewController@deleteRev', 'as' => 'merchant.reviews.delete']);

    /*browse Merchant*/


});
    
Route::group(array('module' => 'Merchant', 'namespace' => 'App\Modules\Merchant\Controllers', 'prefix' => 'merchant'), function() {
    Route::get('/browse', ['uses' => 'MerchantController@merchantList', 'as' => 'merchant.browse']);
});