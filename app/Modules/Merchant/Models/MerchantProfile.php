<?php namespace App\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantProfile extends Model {

	protected $table = 'merchant_profile';

    protected $fillable = ['user_id', 'profile_image', 'first_name', 'middle_name', 'last_name', 'address', 'facebook', 'is_featured'];

    protected $primaryKey = 'merchant_profile_id';

    /**
     * Get the items of the merchant.
     */
	public function items()
	{
		return $this->hasMany('App\Modules\Item\Models\Item');
	}

	/**
     * Get the merchant account.
     */
	public function account()
	{
		return $this->hasOne('App\Modules\App\Models\User', 'id', 'user_id');
	}

	/**
     * Get the ratings of the artist.
     */
	public function merchant_ratings()
	{
		return $this->hasOne('App\Modules\App\Models\Review');
	}

}
