<?php namespace App\Modules\Event\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistEvent extends Model {

	protected $table = 'artist_events';

    protected $fillable = ['artist_profile_id', 'event_image', 'event_title', 'event_description', 'event_venue', 'event_date'];

    protected $primaryKey = 'artist_event_id';

    protected $dates = ['event_date'];

    /**
     * Get the images of the item.
     */
	public function artist()
	{
		return $this->hasOne('App\Modules\Artist\Models\ArtistProfile', 'artist_profile_id', 'artist_profile_id');
	}
}
