<?php namespace App\Modules\Event\Controllers;

use Auth;
use Carbon\Carbon;

use App\Services\ImageUploader;

use App\Http\Requests\EventRequest;

use App\Modules\Event\Models\ArtistEvent;
use App\Modules\Artist\Models\ArtistProfile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UpdateEventController extends Controller {

    public function editEvent($id)
    {
        $profile = ArtistProfile::where('user_id', Auth::user()->id)->first();

        $this->data['event'] = ArtistEvent::where('artist_event_id', '=', $id)->where('artist_profile_id', '=', $profile['artist_profile_id'])->first();

        return view('Artist::event.editEvent', $this->data);
    }

	/**
     * Update Item
     *
     * @return Response
     */
    public function updateEvent($id, EventRequest $request)
    {    
    	$profile = ArtistProfile::where('user_id', Auth::user()->id)->first();
    	$data_event = [
            'artist_profile_id' => $profile['artist_profile_id'],
            'event_title' => $request->event_title,
            'event_description' => $request->event_description,
            'event_venue' => $request->event_venue,
            'event_date' => date_format(date_create($request->event_date), 'Y-m-d'),
            'event_time' => date_format(date_create($request->event_time), 'H:i:s'),
            'event_ticket' => $request->event_ticket,
            'created_at' => Carbon::now(),
        ];

    	$event = ArtistEvent::where('artist_event_id', '=', $id)->where('artist_profile_id', '=', $profile['artist_profile_id'])->update($data_event);

    	if($event){
            if ($request->hasFile('image_upload')){
                $image = $request->image_upload;

                if($image != ''){
                    $this->_validate_image_format($image->getClientOriginalExtension());

                    $imageUploader = new ImageUploader;

                    if($event_image = ArtistEvent::where('artist_event_id', '=', $id)->value('event_image') != null){
                    	$imageUploader::deleteS3(config('cdn.event') . '/' . $event_image);
                    }

                    $result = $imageUploader->upload($image, $id, 'events');

                    $data_event_image = [
                        'event_image' => $id.'/'.$result
                    ];

                	$update_image = ArtistEvent::where('artist_event_id', '=', $id)->update($data_event_image);
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Event Successfully Updated!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while updating'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
