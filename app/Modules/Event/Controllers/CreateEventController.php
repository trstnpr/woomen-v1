<?php namespace App\Modules\Event\Controllers;

use Auth;
use Carbon\Carbon;

use App\Services\ImageUploader;

use App\Http\Requests\EventRequest;

use App\Modules\Event\Models\ArtistEvent;
use App\Modules\Artist\Models\ArtistProfile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CreateEventController extends Controller {

	public function createEvent()
    {
        $profile = ArtistProfile::where('user_id', '=', Auth::user()->id)->first();

        $this->data['events'] = ArtistEvent::where('artist_profile_id', '=', $profile['artist_profile_id'])->count();

        return view('Artist::event.createEvent', $this->data);
    }

    /**
     * Store Item
     *
     * @return Response
     */
    public function storeEvent(EventRequest $request)
    {    
        //dd(Carbon::createFromFormat('Y-m-d H:i:s', $request->event_date)->toDateString());
    	$profile = ArtistProfile::where('user_id', Auth::user()->id)->first();

    	$data_event = [
    		'artist_profile_id' => $profile['artist_profile_id'],
    		'event_title' => $request->event_title,
    		'event_description' => $request->event_description,
    		'event_venue' => $request->event_venue,
    		'event_date' => date_format(date_create($request->event_date), 'Y-m-d'),
            'event_time' => date_format(date_create($request->event_time), 'H:i:s'),
            'event_ticket' => $request->event_ticket,
    		'created_at' => Carbon::now(),
    	];

    	$event = ArtistEvent::insertGetId($data_event);

    	if($event){
            if ($request->hasFile('image_upload')){
                $image = $request->image_upload;

                if($image != ''){
                    $this->_validate_image_format($image->getClientOriginalExtension());

                    $imageUploader = new ImageUploader;

                    $result = $imageUploader->upload($image, $event, 'events');

                    $data_event_image = [
                        'event_image' => $event.'/'.$result
                    ];

                	$insert_image = ArtistEvent::where('artist_event_id', '=', $event)->update($data_event_image);
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Event Successfully Added!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
