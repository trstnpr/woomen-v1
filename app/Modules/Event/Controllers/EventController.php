<?php namespace App\Modules\Event\Controllers;

use Auth;
use Carbon\Carbon;

use App\Services\ImageUploader;

use App\Http\Requests\EventRequest;

use App\Modules\Event\Models\ArtistEvent;
use App\Modules\Artist\Models\ArtistProfile;
use App\Modules\App\Models\Specialty;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class EventController extends Controller {

	public function event()
	{
		$profile = ArtistProfile::where('user_id', '=', Auth::user()->id)->first();
		$this->data['events'] = ArtistEvent::where('artist_profile_id', '=', $profile->artist_profile_id)->get();
		$this->data['upcoming'] = ArtistEvent::where('artist_profile_id', '=', $profile->artist_profile_id)->whereDate('event_date', '>=', Carbon::today()->toDateString())->where('event_time', '>=', date('H:i:s'))->orderBy('event_date', 'asc')->first();
		return view('Artist::event.index', $this->data);
	}

	public function publicEvent($id)
	{
		$data['specialty'] = Specialty::all();
		$data['event'] = ArtistEvent::where('artist_event_id', $id)->first();
		$data['events'] = ArtistEvent::where('artist_event_id', '!=', $id)->take(3)->get();
		return view('Artist::public.event.index', $data);
	}

	public function eventList(Request $request)
	{
		if(isset($request->q)){
			$data['events'] =  ArtistEvent::where('event_title', 'like', '%'.$request->q.'%')->orderBy('created_at', 'desc')->paginate(9);
			$data['specialty'] = Specialty::all();
			return view('Artist::public.eventlist.index', $data);

		} else if(isset($request->month) || isset($request->ticket)  || isset($request->order) || isset($request->date) || isset($request->venue)){

			if($request->venue != '' && $request->ticket != ''){
					$data['events'] = ArtistEvent::where('event_venue', 'like', '%'.$request->venue.'%')
						->orWhere('event_ticket', $request->ticket)
						->orderBy('event_title', $request->order)
						->orderBy('created_at', $request->date)
						->paginate(9);
			} else if ($request->venue != '') {
				$data['events'] = ArtistEvent::where('event_venue', 'like', '%'.$request->venue.'%')
						->orderBy('event_title', $request->order)
						->orderBy('created_at', $request->date)
						->paginate(9);
			} else if ($request->ticket != '') {
				$data['events'] = ArtistEvent::where('event_ticket', $request->ticket)
						->orderBy('event_title', $request->order)
						->orderBy('created_at', $request->date)
						->paginate(9);
			} else {
				$data['events'] = ArtistEvent::orderBy('event_title', $request->order)
						->orderBy('created_at', $request->date)
						->paginate(9);
			}
			$data['specialty'] = Specialty::all();
			return view('Artist::public.eventlist.index', $data);

		} else {

			$data['specialty'] = Specialty::all();
			$data['events'] = ArtistEvent::paginate(9);
			return view('Artist::public.eventlist.index', $data);

		}

	}

}
