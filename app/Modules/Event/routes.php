<?php

Route::group(array('prefix' => 'artist', 'module' => 'Event', 'namespace' => 'App\Modules\Event\Controllers', 'middleware' => ['web', 'auth', 'artist']), function() {

    Route::get('event', ['uses' => 'EventController@event', 'as' => 'artist.event.index']);
    Route::get('event/create', ['uses' => 'CreateEventController@createEvent', 'as' => 'artist.event.create']);
    Route::post('event/store', ['uses' => 'CreateEventController@storeEvent', 'as' => 'artist.event.store']);
    Route::get('event/{id}/edit', ['uses' => 'UpdateEventController@editEvent', 'as' => 'artist.event.edit']);
    Route::post('event/{id}/update', ['uses' => 'UpdateEventController@updateEvent', 'as' => 'artist.event.update']);
    Route::post('event/delete', ['uses' => 'DeleteEventController@deleteEvent', 'as' => 'artist.event.delete']);
    
});

Route::group(array('prefix' => 'event', 'module' => 'Event', 'namespace' => 'App\Modules\Event\Controllers', 'middleware' => ['web']), function() {

	Route::get('/view/{id}', ['uses' => 'EventController@publicEvent', 'as' => 'event.view']);
	Route::get('browse', 'EventController@eventList');

});