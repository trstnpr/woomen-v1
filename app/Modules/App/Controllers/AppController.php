<?php namespace App\Modules\App\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\ForgotPasswordRequest;

use App\Http\Controllers\Controller;
use App\Modules\App\Models\User;
use App\Modules\Artist\Models\ArtistProfile;
use App\Modules\Artist\Models\ArtistSpecialty;
use App\Modules\App\Models\Specialty;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\Item\Models\Item;
use App\Modules\Event\Models\ArtistEvent;

use App\Services\MailSender;
use Auth;
use Carbon\Carbon;

class AppController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {      
        $data['merchandise'] = Item::with('feat_image')->where('item_status', 0)->orderBy('created_at', 'desc')->limit(8)->get();
        $data['events'] = ArtistEvent::all();
        $data['specialty'] = Specialty::all();
        $data['featured_merchant'] = MerchantProfile::where('is_featured', '=', 1)->get();
        $data['featured_artist'] = ArtistProfile::where('is_featured', '=', 1)->get();
        foreach ($data['featured_artist'] as $key => $value) {
            if($value['account']['status'] != 1){
                $data['featured_artist']->forget($key);
            }
            $value['ratings'] = ( $value['artist_ratings'] ) ? $value['artist_ratings']->where('artist_profile_id', $value['artist_profile_id'])->avg('review_rating') : 0 ;
        }

        //$data['instagram'] = \Instagram::getFeed('self', 9);

        return view("App::app.index", $data);
	}


    //Terms and Conditions View
    public function terms(){    
        $data['merchandise'] = Item::with('feat_image')->where('item_status', 0)->orderBy('created_at', 'desc')->limit(8)->get();
        $data['featured_artist'] = ArtistProfile::where('is_featured', '=', 1)->get();
        $data['specialty'] = Specialty::all();     
        return view("App::terms.index", $data);
    }

    //About Us View
    public function about(){    
        $data['merchandise'] = Item::with('feat_image')->where('item_status', 0)->orderBy('created_at', 'desc')->limit(8)->get();
        $data['featured_artist'] = ArtistProfile::where('is_featured', '=', 1)->get();
        $data['specialty'] = Specialty::all();
        return view("App::about.index", $data);
    }

    //FAQ View
    public function faq(){    
        $data['merchandise'] = Item::with('feat_image')->where('item_status', 0)->orderBy('created_at', 'desc')->limit(8)->get();
        $data['featured_artist'] = ArtistProfile::where('is_featured', '=', 1)->get();
        $data['specialty'] = Specialty::all();
        return view("App::faq.index", $data);
    }

    //Contact Us View
    public function contact(){    
        $data['merchandise'] = Item::with('feat_image')->where('item_status', 0)->orderBy('created_at', 'desc')->limit(8)->get();
        $data['featured_artist'] = ArtistProfile::where('is_featured', '=', 1)->get();
        $data['specialty'] = Specialty::all();
        return view("App::contact.index", $data);
    }


	/**
     * Create a new user.
     *
     * @param RegisterRequest $request
     * @return view
     */
    public function register(RegisterRequest $request, MailSender $mailSender)
    {
        $activation_code = $this->_generate_verification_code();

        $data_user = [
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'activation_code' => $activation_code,
            'role_id' => $request->role_id,
            'status' => 0,
            'created_at' => Carbon::now(),
        ];

        $user = User::insertGetId($data_user);

        $data_profile = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'address' => $request->address,
            'user_id' => $user,
        ];

        if ($user)
        {
	        if($request->role_id == 2){
                $data_profile['price_from'] = $request->price_from;
                $data_profile['price_to'] = $request->price_to;
                $data_profile['user_level'] = 2;
                
	        	$profile = ArtistProfile::create($data_profile);

                if(isset($request->specialty)){
                    ArtistSpecialty::where('artist_profile_id', $user)->delete();
                    foreach ($request->specialty as $key => $value) {
                        ArtistSpecialty::insert(['artist_profile_id'=>$profile['artist_profile_id'], 'specialty_id'=> $value]);
                    }
                }
	        }

	        else if($request->role_id == 3){
	        	MerchantProfile::create($data_profile);
	        }

	        else{
	        	return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
	        }

	        $request->merge(['link' => env('APP_URL') . '/activate/' . $activation_code]);

            $mailSender->send('email.email_confirmation', 'Email Confirmation', $request->all());

            return json_encode(array('result' => 'success', 'message' => 'Registration Successful. Please check your email inbox to verify your account. <br>'));
	    }

	    return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
    }

    /**
     * Activate user account.
     *
     * @param string $verification_code
     * @return view
     */
    public function activate($activation_code)
    {
        if ($user = User::where(['activation_code' => $activation_code, 'status' => 0])->update(['status' => 1])) {

            return redirect('/')->with('account_activated', 'Your account is now activated.');
        }

        return redirect('/')->with('account_activated_already', 'Your account is already activated.');
    }

    /**
     * Password reset
     *
     * @param FogatPasswordRequest $request
     * @param MailSender $mailSender
     * @return Response
     */
    public function reset(ForgotPasswordRequest $request, MailSender $mailSender)
    {
        $user_data = User::where('email', $request->email)->first();
        $user_profile = ArtistProfile::where('user_id', $user_data['id'])->first();

        if (!$user_data) {
            return json_encode(array('result' => 'error', 'message' => 'Failed! User does not exist.'));
        }

        $data = [
            'temp_password' => $this->_generate_verification_code(),
            'email' => $request->email,
            'first_name' => $user_profile['first_name'],
            'last_name' => $user_profile['last_name']
        ];

        User::where('email', $request->email)->update(['password' => bcrypt($data['temp_password'])]);

        if ($mailSender->send('email.reset_password', 'Reset Password Request', $data)) {
            return json_encode(array('result' => 'success', 'message' => 'Success! New password has been sent to your email.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'Failed! There is an error occured while sending. Please try again.'));
    }

    /**
     * Generate new verification code.
     *
     * @return string
     */
    private function _generate_verification_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 9999) . mt_rand(10, 9999) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }

    /**
     * Submit contact us.
     *
     * @param 
     * @return view
     */
    public function submitContact(Request $request, MailSender $mailSender)
    {   
        $data = ['user_email' => $request['user_email'],
                    'name' => $request['name'],
                    'message' => $request['message']
                ];
        $mailSender->contact('email.contact_us', 'Contact Us', $data);

        return json_encode(array('result' => 'success', 'message' => 'Successfully submitted!'));

    }


    public function search(Request $request)
    {
        $q = $request->input('q');

        if(isset($q) && $q != ''){

            $this->data['artists'] = ArtistProfile::join('user', 'artist_profile.user_id', '=', 'user.id')
                                                    ->where('user.status', '=', 1)
                                                    ->where(function ($query) use ($q) {
                                                        $query->where('first_name', 'like', '%'.$q.'%')
                                                              ->orWhere('last_name', 'like', '%'.$q.'%');
                                                    })->get();
            $this->data['merchants'] = MerchantProfile::join('user', 'merchant_profile.user_id', '=', 'user.id')
                                                    ->where('user.status', '=', 1)
                                                    ->where(function ($query) use ($q) {
                                                        $query->where('first_name', 'like', '%'.$q.'%')
                                                              ->orWhere('last_name', 'like', '%'.$q.'%');
                                                    })->get();
            $this->data['events'] = ArtistEvent::where('event_title', 'like', '%'.$q.'%')->orWhere('event_description', 'like', '%'.$q.'%')->orWhere('event_venue', 'like', '%'.$q.'%')->get();
            $this->data['merchandise'] = Item::where('item_name', 'like', '%'.$q.'%')->orWhere('item_description', 'like', '%'.$q.'%')->get();

        } else {

            $this->data['artists'] = ArtistProfile::join('user', 'artist_profile.user_id', '=', 'user.id')->where('user.status', '=', 1)->get();
            $this->data['merchants'] = MerchantProfile::join('user', 'merchant_profile.user_id', '=', 'user.id')->where('user.status', '=', 1)->get();
            $this->data['events'] = ArtistEvent::all();
            $this->data['merchandise'] = Item::all();

        }

        $this->data['specialty'] = Specialty::all();

        return view("App::search.index", $this->data);
    }


}
