<?php namespace App\Modules\App\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

use App\Http\Controllers\Controller;
use App\Modules\App\Models\User;

use Auth;
use Carbon\Carbon;

class LoginController extends Controller {

    /**
     * Authenticate user credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function authenticate(LoginRequest $login_request)
    {
        if (!Auth::attempt(['email' => $login_request->email, 'password' => $login_request->password, 'status' => 1])) {
            $user = User::where('email', '=', $login_request->email)->first();
            if($user){
                if($user->status == 0){
                    return json_encode(array('result' => 'error', 'message' => 'Your account is not activated yet! Please check your inbox for the activation email.'));
                }
            }
            return json_encode(array('result' => 'error', 'message' => 'Invalid email or password.'));
        }
        return json_encode(array('result' => 'success', 'message' => env('APP_URL') .'/'. $this->getUserRole(Auth::user()->role_id) . '/dashboard'));
    }
    
    /**
     * Logout user.
     * @return Redirect
     */
    public function logout()
    {   
        Auth::logout();
        return redirect('/');
    }

}
