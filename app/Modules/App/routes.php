<?php

Route::group(array('module' => 'App', 'namespace' => 'App\Modules\App\Controllers', 'middleware' => ['web']), function() {

    /*Index*/
    //Route::get('/', 'AppController@index');
    Route::get('/', 'AppController@index');

    //Maintenance Mode
    // Route::get('/', function()
    // {
    //     return view('errors.maintenance');
    // });

    /*Register User*/
    Route::post('/signup', ['uses' => 'AppController@register', 'as' => 'app.signup']);
    Route::get('/activate/{activation_code}', ['uses' => 'AppController@activate', 'as' => 'app.activate']);

    /*Login User*/
    Route::post('/login', ['uses' => 'LoginController@authenticate', 'as' => 'app.login']);
    Route::get('/logout', ['uses' => 'LoginController@logout', 'as' => 'app.logout']);

    Route::post('/password/reset', ['uses' => 'AppController@reset', 'as' => 'app.password.reset']);
    
    /*Terms and Condition*/
    Route::get('/terms', ['uses' => 'AppController@terms', 'as' => 'app.terms']);

    /*About Us*/
    Route::get('/contact', ['uses' => 'AppController@contact', 'as' => 'app.contact']);

    /*Contact*/
    Route::get('/about', ['uses' => 'AppController@about', 'as' => 'app.about']);

    /*FAQ*/
    Route::get('/faq', ['uses' => 'AppController@faq', 'as' => 'app.faq']);

    /*FAQ*/
    Route::post('/contact_submit', ['uses' => 'AppController@submitContact', 'as' => 'app.contact.submit']);

    // SEARCH
    Route::get('/search', ['uses' => 'AppController@search', 'as' => 'app.search']);

});