<?php namespace App\Modules\App\Models;


use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

	protected $table = 'user';

    protected $fillable = ['email', 'password', 'activation_code', 'role_id', 'status', 'password_recover'];

    /**
     * Get the profile of the artist.
     */
	public function artist_profile()
	{
		return $this->hasOne('App\Modules\Artist\Models\ArtistProfile', 'user_id', 'id');
	}

	/**
     * Get the profile of the merchant.
     */
	public function merchant_profile()
	{
		return $this->hasOne('App\Modules\Merchant\Models\MerchantProfile');
	}

}
