<?php namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model {

	protected $table = 'bookings';

    protected $fillable = ['artist_profile_id', 'first_name', 'last_name', 'contact_no', 'email', 'event_title', 'no_of_heads', 'event_date', 'venue', 'call_time', 'remarks', 'status', 'final_event_title', 'final_date', 'final_time', 'final_no_of_heads', 'final_venue', 'final_price', 'final_notes', 'cancel_reason', 'diamond_count', 'seen'];

    protected $primaryKey = 'booking_id';


}
