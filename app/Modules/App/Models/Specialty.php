<?php namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model {

	protected $table = 'specialties';

    protected $fillable = ['specialty_name'];

    protected $primaryKey = 'specialty_id';


}
