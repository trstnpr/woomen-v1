<?php namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {

	protected $table = 'reviews';

    protected $fillable = ['artist_profile_id', 'merchant_profile_id', 'review_name', 'review_email', 'review_title', 'review_description', 'review_rating'];

    protected $primaryKey = 'review_id';

    /**
     * Get the profile of the artist.
     */
	public function artist_profile()
	{
		return $this->hasOne('App\Modules\Artist\Models\ArtistProfile', 'artist_profile_id', 'artist_profile_id');
	}

	/**
     * Get the profile of the artist.
     */
	public function merchant_profile()
	{
		return $this->hasOne('App\Modules\Merchant\Models\MerchantProfile', 'merchant_profile_id', 'merchant_profile_id');
	}
}
