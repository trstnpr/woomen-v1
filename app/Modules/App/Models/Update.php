<?php namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;

class Update extends Model {

	protected $table = 'updates';

    protected $fillable = ['update_title', 'update_desc', 'update_expire'];

    protected $primaryKey = 'update_id';


}
