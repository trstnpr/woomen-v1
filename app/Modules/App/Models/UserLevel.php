<?php namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model {

	protected $table = 'user_levels';

    protected $fillable = ['level_name', 'privileges', 'albums', 'pictures', 'videos', 'bookings'];

    protected $primaryKey = 'level_id';


}
