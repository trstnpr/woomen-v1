<section id="search-panel">
	<div class="container">
		<div class="col-md-12 search-wrap">
			<div class="form-group search">
	    		<h2 class="header-text">SEARCH</h2><br/>
	    		<form method="GET" action="{{ route('app.search') }}">
				 	<div class="input-group">
				      <input type="text" class="form-control input-lg search-text" placeholder="Search for artist, merchandise, events.." name="q" id="search" value="{{ Request::get('q') }}">
				      <span class="input-group-btn">
				        <button class="btn btn-default input-lg search-btn" type="submit"><i class="fa fa-search"></i> Search</button>
				      </span>
				    </div>
	    		</form>
			</div>
		</div>
	</div>
</section>		