<section id="search-result">
	<div class="container">
		<h2>Search Result</h2>
	</div>
	<div class="container">
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#artist-panel" aria-controls="artist-panel" role="tab" data-toggle="tab">Artist <span class="badge">{{ $artists->count() }}</span></a></li>
	    <li role="presentation"><a href="#event-panel" aria-controls="event-panel" role="tab" data-toggle="tab">Event <span class="badge">{{ $events->count() }}</span></a></li>
	    <li role="presentation"><a href="#merchant-panel" aria-controls="merchant-panel" role="tab" data-toggle="tab">Merchant <span class="badge">{{ $merchants->count() }}</span></a></li>
	    <li role="presentation"><a href="#merchandise-panel" aria-controls="merchandise-panel" role="tab" data-toggle="tab">Merchandise <span class="badge">{{ $merchandise->count() }}</span></a></li>
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content col-md-12">
	    <div role="tabpanel" class="tab-pane active" id="artist-panel">
	    	<h3>Artists</h3>
	    	@foreach($artists as $artist)
	    	<div class="col-md-6 col-sm-6 col-xs-12 artist-item-wrap">
	    		<div class="col-md-3 col-sm-5 col-xs-5 figure-wrap">
	    			<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $artist->profile_image }}');"></figure>
	    		</div>
    			<div class="col-md-9 col-sm-7 col-xs-7 info-wrap">
	    			<h3 class="name">{{ $artist->first_name.' '.$artist->last_name }}</h3>
	    			@if($artist->artist_specialty->count() != 0)
	    			<span class="details"><i class="fa fa-paint-brush"></i> {{ $artist->artist_specialty[0]->specialty->specialty_name . ((($result = $artist->artist_specialty->count() - 1) == 0) ? '' : ' and '.$result.' more' ) }}</span><br/>
					@else
	    			<span class="details"><i class="fa fa-paint-brush"></i> No Specialty Given</span><br/>
					@endif
	    			<span class="details"><i class="fa fa-map-marker"></i> {{ ($artist->address != null) ? str_limit($artist->address, 50) : 'No Address Given' }}</span><hr/>
	    			<a href="{{ route('artist.list.show', $artist->artist_profile_id) }}"><button class="btn btn-warning btn-xs btn-view">View profile</button></a>
	    		</div>
	    	</div>
	    	@endforeach
	    </div>
	    <div role="tabpanel" class="tab-pane" id="event-panel">
	    	<h3>Events</h3>
	    	@foreach($events as $event)
	    	<div class="col-md-6 col-sm-6 col-xs-12 event-item-wrap">
	    		<div class="col-md-3 col-sm-5 col-xs-5 figure-wrap">
	    			<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.event') . '/' . $event->event_image }}');"></figure>
	    		</div>
    			<div class="col-md-9 col-sm-7 col-xs-7 info-wrap">
	    			<h3 class="name">{{ $event->event_title }}</h3>
	    			<span class="details"><i class="fa fa-calendar"></i> {{ date_format(date_create($event->event_date), 'F d, Y') .' '. date_format(date_create($event->event_time), 'h:i A') }}</span><br/>
	    			<span class="details"><i class="fa fa-map-marker"></i> {{ $event->event_venue }}</span><br/>
	    			<span class="details"><i class="fa fa-ticket"></i> {{ $event->event_ticket }}</span><hr/>
	    			<a href="{{ route('event.view', $event->artist_event_id) }}"><button class="btn btn-warning btn-xs btn-view">View full details</button></a>
	    		</div>
	    	</div>
	    	@endforeach
	    </div>
	    <div role="tabpanel" class="tab-pane" id="merchandise-panel">
	    	<h3>Merchandise</h3>
	    	@foreach($merchandise as $item)
	    	<div class="col-md-6 col-sm-6 col-xs-12 merchandise-item-wrap">
	    		<div class="col-md-3 col-sm-5 col-xs-5 figure-wrap">
	    			<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $item->feat_image->image_path }}"></figure>
	    		</div>
    			<div class="col-md-9 col-sm-7 col-xs-7 info-wrap">
	    			<h3 class="name">{{ $item->item_name }}</h3>
	    			<span class="details"><i class="fa fa-tag"></i> {{ 'PHP '.number_format($item['item_price'], 2) }}</span><br/>
	    			<span class="details"><i class="fa fa-phone"></i> 
	    			@if($item->merchant)
						<a href="{{ route('merchant.list.show', $item->merchant->user_id) }}">{{ $item->merchant->first_name.' '.$item->merchant->last_name }}</a>
						@else
						<a href="{{ route('artist.list.show', $item->artist->artist_profile_id) }}">{{ $item->artist->first_name.' '.$item->artist->last_name }}</a>
						@endif
	    			</span><hr/>
	    			<a href="{{ route('merchant.public.item', $item->item_id) }}"><button class="btn btn-warning btn-xs btn-view">View full details</button></a>
	    		</div>
	    	</div>
	    	@endforeach
	    </div>
	    <div role="tabpanel" class="tab-pane" id="merchant-panel">
	    	<h3>Merchant</h3>
	    	@foreach($merchants as $merchant)
	    	<div class="col-md-6 col-sm-6 col-xs-12 merchant-item-wrap">
	    		<div class="col-md-3 col-sm-5 col-xs-5 figure-wrap">
	    			<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $merchant->profile_image }}');"></figure>
	    		</div>
    			<div class="col-md-9 col-sm-7 col-xs-7 info-wrap">
	    			<h3 class="name">{{ $merchant->first_name.' '.$merchant->last_name }}</h3>

	    			<span class="details"><i class="fa fa-map-marker"></i> {{ ($merchant->address != null) ? str_limit($merchant->address, 50) : 'No Address Given' }}</span><hr/>
	    			<a href="{{ route('merchant.list.show', $merchant->user_id) }}"><button class="btn btn-warning btn-xs btn-view">View profile</button></a>
	    		</div>
	    	</div>
	    	@endforeach
	    </div>
	  </div>
	</div>
</section>