@extends('appMaster')

@section('title')
	<title>Woomen</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	
@stop

@section('content')

	@include('App::landing.partials.appHeader')

	<div class="app-content">
		
		@include('App::landing.partials.section-1')

		@include('App::landing.partials.section-2')

		@include('App::landing.partials.section-3')

		@include('App::landing.partials.section-4')

		@include('App::landing.partials.section-merchandise')

		@include('App::landing.partials.section-5')

		@include('App::landing.partials.section-6')

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script type="text/javascript"></script>

	@if(session('account_activated'))
	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Hooray! Your account is now activated.');
	        });

	    </script>
	@endif

	@if(session('account_activated_already'))

	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Oops! Your account is already activated.');
	        });

	    </script>

	@endif

@stop