<section class="section-4 event">
	<div class="container">
		<h2 class="section-title">Events</h2>
		<div class="event-wrap">
			<div class="event-carousel row">
				@for($x=1;$x<=6;$x++)
				<div class="col-md-12">
					<a href="#">
						<figure class="event-item" style="background-image:url('{{ url('images/event') }}/event%20{{ $x }}.png');">
							<div class="hoverlay">
								<span class="event-name">Event {{ $x }}</span>
							</div>
						</figure>
					</a>
				</div>
				@endfor
			</div>
		</div>
	</div>
</section>