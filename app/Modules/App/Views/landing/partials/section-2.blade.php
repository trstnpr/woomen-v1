<section class="section-2 featured-artist">
	<div class="container">
		<h2 class="section-title">Featured Makeup Artist</h2>
		<div class="featArtist-wrap">
			<div class="featArtist-carousel row">
				@if($featured_artist->count() != 0)
				@foreach($featured_artist as $feat)
					<div class="col-md-12">
						<a href="{{ route('artist.list.show', $feat->artist_profile_id) }}">
							<figure class="featA-item" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $feat->profile_image }}');">
								<div class="hoverlay">
									<ul class="list-unstyled">
										<li><span class="featA-name">{{ $feat->first_name.' '.$feat->last_name }}</span></li>
										<li><input value="4" class="rating" data-size="xxs" data-display-only="true"/></li>
									</ul>
								</div>
							</figure>
						</a>
					</div>
				@endforeach
				@endif
			</div>
		</div>
	</div>
</section>