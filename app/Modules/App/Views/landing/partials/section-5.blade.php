<section class="section-5 review">
	<div class="container">
		<h2 class="section-title">Reviews</h2>
		<div class="review-wrap">
			<div class="review-carousel row">
				@for($x=1;$x<=3;$x++)
				<div class="col-md-12">
					<div class="media review-item">
						<div class="media-left">
							<a href="#">
								<img class="media-object img-circle" src="{{ url('images/review') }}/review%20{{ $x }}.png" alt="...">
							</a>
						</div>
						<div class="media-body">
							<div class="card-panel">
								<i class="fa fa-quote-left"></i> Donec sodales, lacus ut pulvinar euismod, nibh purus imperdiet diam, vel feugiat ex metus eget mauris.
							</div>
							<h3 class="media-heading">Media heading, <span class="date">10/10/16</span></h3>
						</div>
					</div>
				</div>
				@endfor
			</div>
		</div>
	</div>
</section>