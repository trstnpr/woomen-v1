<section class="section merchandise">
	<div class="container">
		<h2 class="section-title"><a href="/item/browse" style="color: #000; text-decoration: none;">Merchandise</a></h2>
		<div class="merchandise-wrap">
			<div class="merchandise-carousel row">
				@foreach($merchandise as $merch) 
				<div class="col-md-12">
					<a href="/item/browse/{{ $merch->item_id }}">
						<div class="merchandise-item">
							<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $merch['feat_image']['image_path'] }}');"></figure>
							<div class="item-info">
								<h4 class="item-name">{{ $merch->item_name }}</h4>
								<span class="item-price">PhP {{ $merch->item_price }}</span>
							</div>
						</div>
					</a>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>