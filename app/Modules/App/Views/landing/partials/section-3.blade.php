<section class="section-3 forte">
	<div class="container">
		<h2 class="section-title">Forte</h2>
		<div class="forte-wrap">
			<div class="forte-carousel row">

				<div class="col-md-12">
					<a href="#">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/make%20up.png') }}" class="img-responsive forte-icon" />
							</div>
							<h3 class="forte-label">Artistic/Special Effects</h3>
						</div>
					</a>
				</div>

				<div class="col-md-12">
					<a href="#">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/bridal.png') }}" class="img-responsive forte-icon" />
							</div>
							<h3 class="forte-label">Bridal</h3>
						</div>
					</a>
				</div>

				<div class="col-md-12">
					<a href="#">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/beauty.png') }}" class="img-responsive forte-icon" />
							</div>
							<h3 class="forte-label">Beauty/Special Occassions</h3>
						</div>
					</a>
				</div>

				<div class="col-md-12">
					<a href="#">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/fashion.png') }}" class="img-responsive forte-icon" />
							</div>
							<h3 class="forte-label">Fashion/Creative</h3>
						</div>
					</a>
				</div>

				<div class="col-md-12">
					<a href="#">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/tv.png') }}" class="img-responsive forte-icon" />
							</div>
							<h3 class="forte-label">TV/Films</h3>
						</div>
					</a>
				</div>

				<div class="col-md-12">
					<a href="#">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/theatre.png') }}" class="img-responsive forte-icon" />
							</div>
							<h3 class="forte-label">Theatr/Performance/Pageantry</h3>
						</div>
					</a>
				</div>

				<div class="col-md-12">
					<a href="#">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/hair.png') }}" class="img-responsive forte-icon" />
							</div>
							<h3 class="forte-label">Hairstylist</h3>
						</div>
					</a>
				</div>
				
			</div>
		</div>
	</div>
</section>