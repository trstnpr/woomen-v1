<section class="section" id="section">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-head">TERMS & CONDITIONS</h1>
			<hr/>
		</div>
	</div>
	<div class="article">
		<h3 class="title">In using this website you are deemed to have read and agreed to the following terms and conditions:</h3>
		<p class="text">The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: "Client", "Customer", "Service Provider", “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. "The Company", “Ourselves”, “We” and "Us", refers to Woomen or the company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves. Any Customer and/or Service Provider who access the website for the purpose of establishing, soliciting, availing of services provided by in the website shall subject themselves to all terms and conditions promulgated by the company. If you do not accept the terms of this Privacy Policy, you are directed to discontinue accessing or otherwise using the web site or any materials obtained from it. Acceptance of offers by either party for and in consideration of payment necessary to undertake the service process in the most appropriate manner, for the express purpose of meeting the Client’s needs in respect to provisions of the Service Providers stated services and products, are subject to and in accordance with prevailing Philippine Law. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>
		<hr/>
	</div>
</section>