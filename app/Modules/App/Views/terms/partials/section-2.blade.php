<div class="col-md-3 sidebar">
	<nav id="sidenav" class="scroll-spy">
		<ul>
			<li><a href="#article1">Privacy Statement</a></li>
			<li><a href="#article2">Information Sharing</a></li>
			<li><a href="#article3">Disclaimer</a></li>
			<li><a href="#article4">Types of Information We Collect</a></li>
			<li><a href="#article5">Links to this website</a></li>
			<li><a href="#article6">Links from this website</a></li>
			<li><a href="#article7">Copyright Notice</a></li>
			<li><a href="#article8">Communication</a></li>
			<li><a href="#article9">Force Majeure</a></li>
			<li><a href="#article10">Waiver</a></li>
			<li><a href="#article11">General</a></li>
			<li><a href="#article12">Notification of Changes</a></li>
			<li><a href="#top">Back to top</a></li>
		</ul>
	</nav>
</div>