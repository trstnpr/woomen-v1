<header>
	<nav class="navbar navbar-default navbar-fixed-top nav-public">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">
					<img alt="Brand" src="{{ asset('images/brand/logo%20gold%20earth.png') }}" />
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			@if(Auth::check())
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="hidden-lg hidden-md">Home</span></a></li>
					<!-- <li><a href="#"><i class="fa fa-envelope"></i> <span class="hidden-lg hidden-md">Message</span></a></li> -->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="hidden-lg hidden-md">Others</span></a>
						<ul class="dropdown-menu">
							<li><a href="{{route('app.about')}}">About Woomen</a></li>
							<li><a href="{{route('app.faq')}}">FAQs</a></li>
							<li><a href="{{route('app.contact')}}">Contact Us</a></li>
							<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> <span class="hidden-lg hidden-md">Account</span></a>
						<ul class="dropdown-menu">
							@if(Auth::user()->role_id == 2)
							<li><a href="{{ route('artist.dashboard.index') }}">Dashboard</a></li>
							@elseif(Auth::user()->role_id == 3)
							<li><a href="{{ route('merchant.dashboard') }}">Dashboard</a></li>
							@else
							<li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
							@endif
							<li><a href="{{ route('app.logout') }}">Logout</a></li>
							</li>
						</ul>
						<li><a href="{{ route('app.search') }}"><i class="fa fa-search"></i> <span class="hidden-lg hidden-md">Search</span></a></li>
					</li>
				</ul>
			@else
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#login-modal" data-toggle="modal">Login</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Signup</a>
						<ul class="dropdown-menu">
							<li><a href="#artistreg-modal" data-toggle="modal">Artist</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#merchantreg-modal" data-toggle="modal">Merchant</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="hidden-lg hidden-md">Others</span></a>
						<ul class="dropdown-menu">
							<li><a href="{{route('app.about')}}">About Woomen</a></li>
							<li><a href="{{route('app.faq')}}">FAQs</a></li>
							<li><a href="{{route('app.contact')}}">Contact Us</a></li>
							<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
						</ul>
					</li>
					<li><a href="{{ route('app.search') }}"><i class="fa fa-search"></i></a></li>
				</ul>
			@endif
				{{-- <form class="navbar-form navbar-right">
					<div class="form-group">
						<div class="input-group">
							<input class="form-control" name="q" id="search" placeholder="Search" value="" type="text">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-round">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</div>
				</form> --}}
			</div>
		</div>
	</nav>
</header>