@extends('appMaster')

@section('title')
	<title>Woomen - About Us</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	<style type="text/css">body {background-color:white;}</style>
@stop

@section('content')

	@include('App::universalHead')

	<div class="appAbout-content">
		
		@include('App::about.partials.section-1')

	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script type="text/javascript"></script>

	@if(session('account_activated'))
	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Hooray! Your account is now activated.');
	        });

	    </script>
	@endif

	@if(session('account_activated_already'))

	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Oops! Your account is already activated.');
	        });

	    </script>

	@endif

@stop