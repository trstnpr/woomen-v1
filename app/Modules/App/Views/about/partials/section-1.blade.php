<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head">ABOUT US</h1>
				<hr>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-12 articles">
                <div class="article">
                	<h3 class="title">Who are we?</h3>
                	<p class="text">
						WOOMEN is a web-based platform of Hair & Make-up Artists and other services that is related to
						beauty and fashion. Our primary goal is to raise the profile of our aspiring artisans in the beauty
						and make-up scene in the Philippines as well to provide gateway to aspiring individuals may it be a 
						professional artists or beauty enthusiasts through empowerment & opportunity.
					</p>
                </div> 
                <div class="article">
                	<h3 class="title">Meet the Team</h3>
                </div>   
                <hr/>
                <div class="article">
                	<div class="row">
	                	<div class="col-md-3 col-sm-3 img-wrap">
	                		<img src="{{asset('images/team/Guiden.jpg')}}" class="img-responsive">
	                	</div>
	                	<div class="col-md-9 col-sm-9 text-wrap">
	                		<h3 class="author">Guiden Faith Amalay</h3>
	                		<h4>CEO + Founder</h4>
	                		<p class="text">
								Guiden’s career as a professional make-up artist may have begun by accident, having initially
								pursued a calling as a physical therapist, but her inner strength, tenaciously rooted in self-discipline
								and matchless determination, has made her the dominant voice among the relative upstarts in the
								Philippine make-up scene. Trained by some of the country’s most renowned professional make-up
								artists, she continued honing her craft to perfection by enrolling at the Center for Aesthetics Studies
								Makati. An astute visionary, Guiden tireless pursued a life of action and bold ventures with people
								and industry, of a uniquely personal triumph in search for excellence and began her journey to
								forge more relevance and character in a largely underserved market of rearing aspiring
								professional make-up upstarts.
	                		</p>
	                		<p class="text">
								A true-blue altruist who finds joy in serving, building and helping others, Guiden has personified
								today’s successful Filipina professional who has used her knowledge to support, develop and retain
								talent, as well as cultivating the next generation of leaders.  
	                		</p>
	                		<p class="text">
								A graduate from the Iloilo Doctors College with a degree in Physical Therapy, Guiden embodies this
								generation’s “Jane of All Trades:” she is a businesswoman, a boxer, a tennis player, a skim and
								wake-boarder – all rolled into one petite package.
	                		</p>
	                		<p class="text">
							 	Her aspiration for Woomen.ph is simple: to be a gateway for noble dreamers who many not have
								the means to pursue them achieve their dreams through empowerment and opportunity.              			
	                		</p>			
	                	</div>
                	</div>
                </div>
                <hr>
                <div class="article">
                	<div class="row">
	                	<div class="col-md-9 col-sm-9 text-wrap">
	                		<h3 class="author">Katrina Bati Carandang</h3>
	                		<h4>COO + Co-Founder</h4>
	                		<p class="text">
								Visionaries are possessed creatures, people in the thrall of belief so powerful that they ignore all
								else – even reason – to ensure that reality catches up with their dreams. KC epitomizes this rare
								breed of young millennial visionaries whose unrelenting passion for excellence has generated a
								kind of energy that continues to inspire and astound. 
	                		</p>
	                		<p class="text">
								An avid globetrotter and pathfinder, KC actively pursues her wanderlust as a lead cabin crew and
								trainer for Philippine Airlines Express, and dabbles as an entrepreneur on the side, working as a
								freelance fashion designer and an e-commerce merchant. Employing her unmatched expertise as a
								flight coach, she consciously seeks adventures outside her comfort zone, constantly exploring new
								avenues and expanding her global network of friends from a host of different cultures and
								persuasions. 
	                		</p>
	                		<p class="text">
								Having been reared in Indonesia almost half of her life, KC embodies the kind of acceptant, broadminded
								exquisiteness that marks her leadership style as both refreshingly flexible and
								disconcertingly unpredictable; yet paradoxically possess the kind of steadiness and a keen eye for
								symmetry that makes for a formidable sartorial artisan. 
	                		</p>
	                		<p class="text">
								 A graduate of Biology from the St. Scholastica’s College Manila, KC is a born leader and a certified
								“people person,” having headed their school’s badminton and basketball varsity teams, as well as
								facilitated in the production of their school yearbook, among many other pursuits.              			
	                		</p>
	                		<p class="text">
								Her dream for Woomen.ph is to be a belvedere for aspiring fashion and beauty practitioners in
								building, not only their professional portfolios, but primarily, to raise their self-worth by creating a
								community of cooperative ventures. 
	                		</p>
	                	</div>
	                	<div class="col-md-3 col-sm-3 img-wrap">
	                		<img src="{{asset('images/team/Kc.jpg')}}" class="img-responsive">
	                	</div>
                	</div>
                </div>
                <hr>
                <div class="article">
                	<div class="row">
	                	<div class="col-md-3 col-sm-3 img-wrap">
	                		<img src="{{asset('images/team/Chris.jpg')}}" class="img-responsive">
	                	</div>
	                	<div class="col-md-9 col-sm-9 text-wrap">
	                		<h3 class="author">Chrislyn Dew Cruz</h3>
	                		<h4>CMO + Co-Founder</h4>
	                		<p class="text">
	                			Chrislyn reached for a place in the Philippine advertising industry by being one of the most dynamic
								young executives ever to hit the world of digital campaigns. Having been in the advertising business
								for six years now, she began her career as a production assistant for various television commercial
								shoots eventually catapulting her at the top of the corporate ladder. After spending several years
								working as a project-based events coordinator and a technical director for two of the country’s
								most celebrated events groups, she got her biggest career breakthrough as Regional Project
								Manager for a digital ad agency catering clients in South East Asia. This opportunity led her in
								vastly expanding her network, gaining invaluable knowledge about the digital campaign industry
								and raising her profile among her partners and peers. This experience also opened doors for highprofile
								postings, allowing her to work with some of the most renowned ad and media agencies, both
								local and international, among them, the Starcom Mediavest Group and the Omnicom Media Group.
	                		</p>
	                		<p class="text">
								A graduate of Public Relations, Advertising and Applied Communications degree from the
								Polytechnic University of the Philippines, Chrislyn later pursued her Master of Development
								Communication degree from the University of the Philippines. Whenever she is not busy with
								administrating multiple client projects, she dabbles into painting, running and travelling. 
	                		</p>
	                		<p class="text">
								Her vision for Woomen.ph extends beyond just being a platform that caters services involving
								beautification; more importantly, for it to become an icon where women empower one another by
								encouraging beauty not only from the outside but also on the inside.
	                		</p>
	                	</div>
                	</div>
                </div>
                <hr>
                <div class="article">
                	<div class="row">
	                	<div class="col-md-9 col-sm-9 text-wrap">
	                		<h3 class="author">Kristine Baltazar Estravo</h3>
	                		<h4>CSO + Co-Founder</h4>
	                		<p class="text">
								There is a tide in the affairs of select leaders which, taken at the flood from her early lifetime of
								noteworthy achievements, that led on to preeminence of Kristine. To that quality of grit and singleminded
								determination that she aspired with all the intense ambition that brought her to the zenith
								of achievement – was the most inspiring character of all. A young, single mother of two from
								Dagupan City who have had her fair share of life’s many vicissitudes, she overcame enormous
								challenges and surprised even her most dedicated colleagues with her cyclonic energy, and
								confounded her skeptics with her steely resolve to accomplish the near-impossible, often masked in
								a disarmingly alluring demeanor. 
	                		</p>
	                		<p class="text">
								In spite of her humble beginnings – from a clueless provincial lass and a single mother of two boys
								before she reached her twentieth birthday – she transformed into a formidable powerhouse of selfeffacing
								talent largely through her unremitting hope for the future and her tenacious drive for
								perfection. Her sudden extroversion was so fascinating, it is the stuff business legends are made of.
	                		</p>
	                		<p class="text">
								Determined to provide a good future for her young family, she began her career as a sales
								supervisor for a friend’s milk tea business in Manila. Sensing the volatility of the “flash-in-the-pan”
								enterprise, she adroitly moved to Etude House Philippines, a cosmetics company, as area sales
								manager. However, her biggest break came when she was offered a sales position at GeoEstate
								Development Corporation where, in just a few short months, she outperformed even the company’s
								more senior sales supervisors – a testament to her relentless pursuit for success. In less than two
								years thereafter, she was promoted to International Area Sales Manager and continues to astound
								the company management with her incredible drive. 
	                		</p>
	                		<p class="text">
								Today, Kristine is a shining example of what sheer fortitude can accomplish. A young lady in nearperpetual
								motion, Kristine now sits as member of a successful micro-lending corporation
								(Leverage.Ph), runs a thriving restaurant in Pampanga (Chefs Gone Wild), operates a fast-rising
								travel agency (En Morphe), and is currently laying the groundwork for a food park in Quezon City –
								on top of her current work at GeoEstate.              			
	                		</p>
	                		<p class="text">
								A graduate of Business Administration and Hotel and Restaurant Management of Lyceum
								Northwestern University, Kristine’s indomitable spirit and inextinguishable resolve to succeed in all
								endeavors will prove invaluable to Woomen.ph’s ascendancy, where it promises to provide a
								beacon for countless make-up and fashion practitioners who have all but given up hope in the
								traditionally “dog-eat-dog” world of Philippine fashion.               			
	                		</p>
	                	</div>
	                	<div class="col-md-3 col-sm-3 img-wrap">
	                		<img src="{{asset('images/team/Tine.jpg')}}" class="img-responsive">
	                	</div>
	                </div>	
                </div>	
{{-- 				<div class="article">
					<h3 class="title">What do we do?</h3>
					<p class="text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>--}}
			</div>
{{-- 			<div class="col-md-4 the-team">
				<h3 class="title">The Team</h3>
				<div class="figures">
					<figure class="figure1" style="background-image:url('{{asset('images/team/Chris.jpg')}}');"></figure>
					<figure class="figure2" style="background-image:url('{{asset('images/team/Guiden.jpg')}}');"></figure>
					<figure class="figure3" style="background-image:url('{{asset('images/team/Kc.jpg')}}');"></figure>
					<figure class="figure4" style="background-image:url('{{asset('images/team/Tine.jpg')}}');"></figure>
				</div>
			</div> --}}
		</div>
	</div>
</section>