<section class="section-5 review">
	<div class="container">
		<h2 class="section-title gold-text">Reviews</h2>
		<div class="review-wrap">
			<div class="review-carousel row">
				<div class="col-md-12">
					<div class="media review-item">
						<div class="media-left">
							<a href="#">
								<img class="media-object img-circle" src="{{ url('images/review') }}/jeanelle.png" alt="...">
							</a>
						</div> 
						<div class="media-body">
							<div class="card-panel">
								<i class="fa fa-quote-left"></i> <span class="trim-text">I highly recommend woomen.ph. As a client i found it easy
								to browse and check who/what i need based on where Im at.
							</div>
							<h3 class="media-heading"><span class="name">Jeanelle Gurnote</span> <span class="date">02/06/17</span></h3>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="media review-item">
						<div class="media-left">
							<a href="#">
								<img class="media-object img-circle" src="{{ url('images/review') }}/neva.png" alt="...">
							</a>
						</div> 
						<div class="media-body">
							<div class="card-panel">
								<i class="fa fa-quote-left"></i> <span class="trim-text">It's about time that we had a unified portal for make-up artists and stylists! The Philippines is probably the world's center for wedding, debuts and fiestas -- hair and make-up is a must! Woomen.ph is the answer -- it's the Linkedin/Behance for beauty and styling professionals.</span>
							</div>
							<h3 class="media-heading"><span class="name">Neva Talladen</span> <span class="date">02/07/17</span></h3>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="media review-item">
						<div class="media-left">
							<a href="#">
								<img class="media-object img-circle" src="{{ url('images/review') }}/sonjia.jpg" alt="...">
							</a>
						</div> 
						<div class="media-body">
							<div class="card-panel">
								<i class="fa fa-quote-left"></i> <span class="trim-text">Woomen is a platform I'm very excited for everyone to experience. Whether you're a 
								make up artist or someone in need of professional hair and make up services, this makes it very simple, accessible to both
								artist and client! Looking back at my own wedding, I had to go to a bridal fair to look for my HMUA team or rely on people I
								knew who had gotten married before me! If you are an artist wanting your work to easily  be showcased, or someone looking to help enhance your beauty for a special occasion, go ahead and go to woomen.ph wow!</span> 
							</div>
							<h3 class="media-heading"><span class="name">Sonjia Calit-Kakilala</span> <span class="date">02/09/17</span></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>