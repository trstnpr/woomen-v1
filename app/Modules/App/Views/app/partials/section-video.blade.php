<section class="section-1 head-banner">
	<video class="video-bg" preload="none" autoplay="true" loop="loop" muted="muted" volume="0">
        <source src="{{ env('APP_URL') }}/images/video.webm" type="video/webm">
        <source src="{{ env('APP_URL') }}/images/video.ogg" type="video/ogg">
        <source src="{{ env('APP_URL') }}/images/video.mp4" type="video/mp4">
    </video>
</section>
