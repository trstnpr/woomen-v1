{{-- Modal Login --}}
<div class="modal fade woo-modal" id="login-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content"> 
			<div class="modal-header" style="padding:0 !important;border-bottom:none !important;">
       		 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      		</div>			
			<div class="modal-body">
				<img src="{{ asset('images/brand/logo%20gold%20earth.png') }}" class="img-responsive logo-brand"/>
				<h4 class="form-label">Login</h4>
				<form class="login-form" id="login_form">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="alert alert-success hide" id="result_d">
					    Logged in Successfully!
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="email" class="form-control input-lg" name="email" id="email_login" placeholder="Your email address" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<input type="password" class="form-control input-lg" name="password" placeholder="Your password" id="login_password"/>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn woo-btn btn-block btn-lg gold" id="btn_login">Login</button>
							</div>
						</div>
					</div>
					<div class="clearfix">
						<div class="checkbox pull-left">
							<label>
								<input type="checkbox" name="remember-me" /> Remember me
							</label>
						</div>
						<p>
							<p class="pull-right forgot-pass"><a href="#" data-toggle="modal" data-target="#forgot-password-modal" data-dismiss="modal" aria-label="Close">Forgot Password?</a></p>
						</p>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

{{-- Modal Forgot Password --}}
<div class="modal fade woo-modal" id="forgot-password-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content"> 
			<div class="modal-header" style="padding:0 !important;border-bottom:none !important;">
       		 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      		</div>			
			<div class="modal-body">
				<img src="{{ asset('images/brand/logo%20gold%20earth.png') }}" class="img-responsive logo-brand"/>
				<h4 class="form-label">Forgot Password</h4>
				<form class="login-form" id="reset_password_form">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="alert alert-success hide" id="reset_result_div">
					    Logged in Successfully!
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Your email address associated with your account</label>
								<input type="email" class="form-control input-lg" name="email" id="email_for" placeholder="Email address" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn woo-btn btn-block btn-lg gold" id="reset_password_button">SUBMIT</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

{{-- Modal Artist --}}
<div class="modal fade woo-modal" id="artistreg-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" style="padding:0 !important;border-bottom:none !important;">
       		 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      		</div>	
			<div class="modal-body">
				<img src="{{ asset('images/brand/logo%20gold%20earth.png') }}" class="img-responsive logo-brand"/>
				<h4 class="form-label">SIGN UP AS ARTIST</h4>
				<form class="artistreg-form" id="artistreg_form">
					<div class="alert alert-success hide" role="alert" id="artist_alert"></div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="role_id" value="2">
					<div class="alert alert-success hide" id="result_divs">
					    Signed Up in Successfully!
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>First Name</label>
								<input type="text" class="form-control input-lg" name="first_name" id="first_name" placeholder="Your firstname" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" class="form-control input-lg" name="last_name" id="last_name" placeholder="Your lastname" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Forte</label>
								<select class="form-control selectpicker" name="specialty[]" multiple>
									@if(isset($specialty))
									@foreach($specialty as $spe)
										<option value="{{ $spe->specialty_id }}">{{ $spe->specialty_name }}</option>
									@endforeach
									@endif
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Address</label>
								<input type="text" class="form-control input-lg" name="address" id="address" placeholder="Your Address" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Price From</label>
								<input type="text" class="form-control input-lg number" name="price_from" id="price_from" placeholder="Price From" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Price To</label>
								<input type="text" class="form-control input-lg number" name="price_to" id="price_to" placeholder="Price to" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Email</label>
								<input type="email" class="form-control input-lg" name="email" id="email" placeholder="Your email address" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control input-lg" name="password" id="password" placeholder="Your password" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<input type="password" class="form-control input-lg" name="password_confirmation" id="re-password" placeholder="Retype your password" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label><input type="checkbox" name="terms" id="terms" required> I agree to the <a href="{{route('app.terms')}}" target="_blank">terms and conditions</a></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn woo-btn btn-block btn-lg gold" id="btn_artist">Signup</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

{{-- Modal Merchant --}}
<div class="modal fade woo-modal" id="merchantreg-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header" style="padding:0 !important;border-bottom:none !important;">
   		 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  		</div>	
			<div class="modal-body">
				<img src="{{ asset('images/brand/logo%20gold%20earth.png') }}" class="img-responsive logo-brand"/>
				<h4 class="form-label">SIGN UP AS MERCHANT</h4>
				<form class="merchantreg-form" id="merchantreg_form">
					<div class="alert alert-success hide" role="alert" id="merchant_alert"></div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="role_id" value="3">
					<div class="alert alert-success hide" id="result_div">
					    Signed Up in Successfully!
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>First Name</label>
								<input type="text" class="form-control input-lg" name="first_name" id="first_name" placeholder="Your firstname" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" class="form-control input-lg" name="last_name" id="last_name" placeholder="Your lastname" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Address</label>
								<input type="text" class="form-control input-lg" name="address" id="address" placeholder="Your Address" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Email</label>
								<input type="email" class="form-control input-lg" name="email" id="email" placeholder="Your email address" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control input-lg" name="password" id="password" placeholder="Your password" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<input type="password" class="form-control input-lg" name="password_confirmation" id="re-password" placeholder="Retype your password" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label><input type="checkbox" name="terms" id="terms" required> I agree to the <a href="{{route('app.terms')}}" target="_blank">terms and conditions</a></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn woo-btn btn-block btn-lg gold" id="btn_merchant">Signup</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Success Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="success_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="alert alert-success" role="alert" id="merchant_alert">Registration Successful. Please check your email inbox to verify your account.</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="ok_btn">Ok</button>
      </div>
    </div>
  </div>
</div>
