<section class="section-3 forte">
	<div class="container">
		<h2 class="section-title gold-text">Forte</h2>
		<div class="forte-wrap">
			<div class="{{-- forte-carousel --}} row">

				<div class="col-md-3 col-wrap">
					<a href="{{ url('artist/browse?forte%5B%5D=1') }}" style="text-decoration: none;">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/make%20up.png') }}" class="img-responsive forte-icon" style="display: block; margin: auto;" />
							</div> 
							<h3 class="forte-label" align="center">Artistic / Special Effects</h3>
						</div>
					</a>
				</div>

				<div class="col-md-3 col-wrap">
					<a href="{{ url('artist/browse?forte%5B%5D=2') }}" style="text-decoration: none;">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/bridal.png') }}" class="img-responsive forte-icon" style="display: block; margin: auto;" />
							</div>
							<h3 class="forte-label" align="center">Bridal</h3>
						</div>
					</a>
				</div>

				<div class="col-md-3 col-wrap">
					<a href="{{ url('artist/browse?forte%5B%5D=3') }}" style="text-decoration: none;">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/beauty.png') }}" class="img-responsive forte-icon" style="display: block; margin: auto;" />
							</div>
							<h3 class="forte-label" align="center">Beauty / Special Occassions</h3>
						</div>
					</a>
				</div>

				<div class="col-md-3 col-wrap">
					<a href="{{ url('artist/browse?forte%5B%5D=4') }}" style="text-decoration: none;">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/fashion.png') }}" class="img-responsive forte-icon" style="display: block; margin: auto;" />
							</div>
							<h3 class="forte-label" align="center">Fashion / Creative / Magazine</h3>
						</div>
					</a>
				</div>

				<div class="col-md-3 col-wrap">
					<a href="{{ url('artist/browse?forte%5B%5D=5') }}" style="text-decoration: none;">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/tv.png') }}" class="img-responsive forte-icon" style="display: block; margin: auto;" />
							</div>
							<h3 class="forte-label" align="center">TV / Films</h3>
						</div>
					</a>
				</div>

				<div class="col-md-3 col-wrap">
					<a href="{{ url('artist/browse?forte%5B%5D=6') }}" style="text-decoration: none;">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/theatre.png') }}" class="img-responsive forte-icon" style="display: block; margin: auto;" />
							</div>
							<h3 class="forte-label" align="center">Theatre / Performance / Pageantry</h3>
						</div>
					</a>
				</div>

				<div class="col-md-3 col-wrap">
					<a href="{{ url('artist/browse?forte%5B%5D=7') }}" style="text-decoration: none;">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/hair.png') }}" class="img-responsive forte-icon" style="display: block; margin: auto;" />
							</div>
							<h3 class="forte-label" align="center">Hairstylist</h3>
						</div>
					</a>
				</div>

				<div class="col-md-3 col-wrap">
					<a href="{{ url('artist/browse?forte%5B%5D=8') }}" style="text-decoration: none;">
						<div class="forte-item">
							<div class="forte-icontainer">
								<img src="{{ asset('images/forte/eyebrow.png') }}" class="img-responsive forte-icon" style="display: block; margin: auto;" />
							</div>
							<h3 class="forte-label" align="center">Eyebrow / Broidery</h3>
						</div>
					</a>
				</div>
				
			</div>
		</div>
	</div>
</section>