<section class="section-2 featured-merchant">
	<div class="container-fluid">
		<h2 class="section-title gold-text section-header">Featured Merchant</h2>
		<div class="featMerchant-wrap">
			<div class="featMerchant-carousel row">
				@if($featured_merchant->count() != 0)
				@foreach($featured_merchant as $feat)
					<div class="col-md-12">
						<a href="{{ route('merchant.list.show', $feat->user_id) }}">
							<figure class="featA-item" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $feat->profile_image }}');">
								<div class="hoverlay">
									<ul class="list-unstyled">
										<li><span class="featA-name">{{ $feat->first_name.' '.$feat->last_name }}</span></li>
										<li><input value="{{ $feat->ratings }}" class="rating" data-size="xxs" data-display-only="true"/></li>
									</ul>
								</div>
							</figure>
						</a>
					</div>
				@endforeach
				@endif
			</div>
		</div>
		
		<div class="browse-btn">
			<a class="btn woo-btn" href="{{ url('merchant/browse') }}">Browse Merchant</a>
		</div>
		
	</div>
</section>
