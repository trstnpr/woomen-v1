<section class="section merchandise">
	<div class="container-fluid">
		<h2 class="section-title"><a href="/item/browse" class="section-title" style="text-decoration: none;color:#e9a816;">Merchandise</a></h2>
		<div class="merchandise-wrap">
			<div class="merchandise-carousel row">
				@foreach($merchandise as $merch) 
				<div class="col-md-12">
					<a href="/item/browse/{{ $merch->item_id }}">
						<div class="merchandise-item">
							<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $merch['feat_image']['image_path'] }}');"></figure>
							<div class="item-info">
								<h4 class="item-name">{{ str_limit($merch->item_name, 30) }}</h4>
								<span class="item-price">PHP {{ number_format($merch->item_price, 2) }}</span>
							</div>
						</div>
					</a>
				</div>
				@endforeach
			</div>
		</div>

		<div class="browse-btn">
			<a class="btn woo-btn" href="{{ url('item/browse') }}">Browse Items</a>
		</div>

	</div>
</section>