<section class="section-4 event">
	<div class="container-fluid">
		<h2 class="section-title gold-text">Events</h2>
		<div class="event-wrap">
			<div class="event-carousel row">
				@foreach($events as $event)
					<div class="col-md-12">
						<a href="{{ url('event/view/') }}/{{ $event->artist_event_id }}">
							<figure class="event-item" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.event') .'/'. $event->event_image }}');">
								<div class="hoverlay">
									{{-- <span class="event-name">{{ $event['event_title'] }}</span> --}}
									<ul class="list-unstyled event-details">
										<li class="date">{{ $event['event_date']->format('M d, Y') }}</li>
										<li class="name">{{ $event['event_title'] }}</li>
									</ul>
								</div>
							</figure> 
						</a>
					</div>
				@endforeach
			</div>
		</div>

		<div class="browse-btn">
			<a class="btn woo-btn" href="{{ url('event/browse') }}">Browse Events</a>
		</div>

	</div>
</section>