<section class="section footer">
	<div class="layer layer-1">
		<div class="container">
			<ul class="list-unstyled sitemap">
				<li><a href="{{route('app.about')}}">About Woomen</a></li>
				<li><a href="{{route('app.contact')}}">Contact Us</a></li>
				<li><a href="{{route('app.faq')}}">FAQs</a></li>
				<li><a href="{{route('app.terms')}}">Terms & Conditions</a></li>
			</ul>
			
			<ul class="list-unstyled social">
				<li><a href="https://www.facebook.com/woomen.ph/" target="_blank"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li><a href="https://www.instagram.com/woomen.ph/" target="_blank"><i class="fa fa-instagram"></i></a></li>
			</ul>
			<br/><br/>
			<p style="color:white;font-style:italic;font-weight:normal;">
				Psalm 139:14<br/>
				"I praise you because I am fearfully and wonderfully made;
   	 			your works are wonderful,
    			I know that full well."
			</p>
		</div>
	</div>
	<div class="layer layer-2">
		<div class="container">
			<span class="footer-copy">Copyright &copy; Woomen 2017. All Rights Reserved</span>
		</div>
	</div>
</section>