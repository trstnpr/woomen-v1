@extends('appMaster')

@section('title')
	<title>Woomen</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
	<meta property="fb:app_id"        content="{{ env('FACEBOOK_APP_ID') }}">
	<meta property="og:url"           content="{{ env('APP_URL') }}" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Woomen" />
	<meta property="og:description"   content="WOOMEN is a web-based platform of Hair & Make-up Artists and other services that is related to beauty and fashion. Our primary goal is to raise the profile of our aspiring artisans in the beauty and make-up scene in the Philippines as well to provide gateway to aspiring individuals may it be a professional artists or beauty enthusiasts through empowerment & opportunity." />
	<meta property="og:image"         content="{{ env('APP_URL') }}/images/brand/logo%20gold%20earth.png" />
	<meta property="og:image:width"   content="1200">
	<meta property="og:image:height"  content="630">

	<meta name="description" content="WOOMEN is a web-based platform of Hair & Make-up Artists and other services that is related to
						beauty and fashion.">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8">
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	@include('App::app.partials.appHeader')

	<div class="app-content">
		
		@include('App::app.partials.section-video')

		@include('App::app.partials.section-featured-artist')

		@include('App::app.partials.section-forte')

		@include('App::app.partials.section-events')

		@include('App::app.partials.section-merchandise')

		@include('App::app.partials.section-featured-merchant')

		@include('App::app.partials.section-reviews')

		@include('App::app.partials.section-socialmedia')


		@include('App::app.partials.footer')

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script type="text/javascript">
		
	$(document).ready(function(){
		var read = $(".trim-text");
		$.each(read, function(){
			var text = $(this).text();
			var length = text.length;
			if(length > 135){
				var trim = text.substring(0, 135);
				$(this).html("<span class='less'>"+ trim + "... <br/><small class='read-more'>View more</small></span><span class='more'>"+text+"<br/><small class='view-less'>View less</small></span>");
				$('.more').hide();
			}
		});

		$(".read-more").click(function(){
			$(this).parent('.less').hide();
			$(this).parent('.less').next('.more').show();
		});

		$(".view-less").click(function(){
			$(this).parent('.more').hide();
			$(this).parent('.more').prev('.less').show();
		});
	});

	</script>

	@if(session('account_activated'))
	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Hooray! Your account is now activated.');
	        });

	    </script>
	@endif

	@if(session('account_activated_already'))

	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Oops! Your account is already activated.'); 
	        });

	    </script>

	@endif

@stop