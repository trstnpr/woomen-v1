<section class="section contact-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head">CONTACT US</h1>
				<hr/>
			</div>
		</div>

		<h3 class="contact-text">Do you have any questions, comments & suggestions? Feel free to contact us.</h3>

		<div class="row">
			<div class="col-md-5 contact-info">
				<ul class="fa-ul">
					<li><i class="fa fa-li fa-map-marker"></i>28th Floor Pacific Star Bldg., Makati Ave. corner Buendia Ave., Makati City,Philippines</li>
					<li><i class="fa fa-li fa-phone"></i>+632 - 822 - 2755</li>
					<li><i class="fa fa-li fa-envelope"></i>info@woomen.ph</li>
				</ul>

				<div class="map-wrap">
					<figure class="map" id="map"></figure>
				</div>

			</div>

			<div class="col-md-7 contact-form">
				<h3 class="title">Send us a Message</h3>
				<form id="contact-frm">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" name="name" placeholder="Name" class="form-control input-lg" required>
							</div>
						</div>
						<div class="col-md-12">	
							<div class="form-group">
								<input type="email" name="user_email" placeholder="Active email address" class="form-control input-lg" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<textarea name="message" class="form-control input-lg textarea" placeholder="Your message..." required></textarea>	
							</div>
						</div>
						<div class="col-md-12">
							<div class="alert alert-success hide" id="result_div">
			  					<ul class="fa-ul">
			  						<li><i class="fa fa-li fa-check"></i> Success!</li>
			  					</ul>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<input type="submit" value="Submit" class="btn woo-btn btn-lg" id="btn_contact"> 
							</div>
						<div class="col-md-12">
					</div>
				</form>
			</div>
		</div>

	</div>
</section>