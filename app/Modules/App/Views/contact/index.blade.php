@extends('appMaster')

@section('title')
	<title>Woomen - Contact Us</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	<style type="text/css">body {background-color:white;}</style>	
@stop

@section('content')

	@include('App::universalHead')

	<div class="appContact-content">

		@include('App::contact.partials.section-1')

		{{-- @include('App::contact.partials.section-2') --}}
		
	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
 	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBsRysuaoNONj3m_kWOjBvLoLh9WGY2Gs&callback=initMap"
	  async defer>
	</script>
    <script>
      function initMap() {

        var uluru = {lat: 14.561296, lng: 121.027018};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 19,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>


	@if(session('account_activated'))
	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Hooray! Your account is now activated.');
	        });

	    </script>
	@endif

	@if(session('account_activated_already'))

	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Oops! Your account is already activated.');
	        });

	    </script>

	@endif

@stop
