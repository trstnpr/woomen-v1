<div class="col-md-3 sidebar">
	<nav id="sidenav" class="scroll-spy">
		<ul>
			<li><a href="#article1">What is WOOmen?</a></li>
			<li><a href="#article2">What are the perks of being part of Woomen?</a></li>
			{{-- <li><a href="#article3">What are the levels of profile?</a></li> --}}
			<li><a href="#article4">What are the benefits of joining and maintaining the accounts with WOOmen?</a></li>
			<li><a href="#article5">How do I get my work showcased in the Woomen homepage.</a></li>
			<li><a href="#article6">What does Woomen do with the personal information it collects?</a></li>
			<li><a href="#article7">How can i get more clients?</a></li>
{{-- 			<li><a href="#article8">How to upgrade my profile?</a></li> --}}
			<li><a href="#article9">How to build a great portfolio?</a></li>
			<li><a href="#article10">What is profile strength?</a></li>
{{-- 			<li><a href="#article11">What is Virtual Credit Card?</a></li>
			<li><a href="#article12">What happens if I run out of credits?</a></li> --}}
			<li><a href="#article13">How to manage my clients?</a></li>
			<li><a href="#article14">How to become the featured artist?</a></li>
			<li><a href="#article15">How to get sponsorship or advertisement on the profile?</a></li>
			<li><a href="#article16">How to join the marketplace?</a></li>
			<li><a href="#article17">Who can join the marketplace?</a></li>
			<li><a href="#top">Back to top</a></li>
		</ul>
	</nav>
</div>