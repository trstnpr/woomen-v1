
<div class="col-md-9 content-right" data-spy="scroll" data-target=".navbar" data-offset="50">
	<h1>Woomen’s Basics</h1>
	<div class="article" id="article1">
		<h3 class="title">What is WOOmen?</h3>
		<p class="text">
			Woomen is the only credible online platform that provides professional portfolio directory of Make-up Artist (MUA), Hairstylists and Beauty Merchants. A competitive marketplace that bridges the gaps between clients and the suppliers in the fashion and beauty industry. 
		</p>
	</div>
	<div class="article" id="article2">
		<h3 class="title">What are the perks of being part of Woomen?</h3>
		<p class="text">
			Being part of Woomen allows the artisan to build their portfolio through experiences, client’s reviews and upgrade their skills in the workshops or events supported by Woomen. Providing growth and exposure on the Artists and Merchants
		</p>
	</div>
	{{-- <div class="article" id="article3">
		<h3 class="title">What are the levels of profile?</h3>
		<h4 class="subtitle">Freemium</h4>
		<p class="text">MUA get to try the privileges of joining WOOmen with a Regular Profile for 6 months for FREE.</p>
		<h4 class="subtitle">Regular Profile (Neophyte Profile)</h4>
		<p class="text">
			MUA has the chance to upload at least 2 albums with 5 pictures each of their best works and 2 video that they want to show to their clients. They MUA is also entitled to a virtual card that has a value of 20 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and a chance to be a featured artist.
		</p>
		<h4 class="subtitle">Standard Profile (Archetype Profile)</h4>
		<p class="text">
			The MUA has the chance to showcase 3 albums with 5 pictures each and 5 videos in their dashboard. They will have discounts on featured events and sponsored by Woomen. They MUA is also entitled to a virtual card that has a value of 50 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and higher chances to be a featured artist.
		</p>
		<h4 class="subtitle">Premium Profile (Transcendent Profile)</h4>
		<p class="text">
			MUA has more than 10 pictures and 10 videos that they can showcase on their portfolio. They will have discounts on featured events created or partnered by Woomen. They MUA is also entitled to a virtual card that has a value of 100 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and a chance to be a featured artist. They are also entitled to free photoshoot on 5 models or 5 Items (If they are registered in the Marketplace).
		</p>
	</div> --}}
	<div class="article" id="article4">
		<h3 class="title">What are the benefits of joining and maintaining the accounts with WOOmen?</h3>
     	<p class="text">
     		WOOmen is a platform that allows Make Up Artist to upload their portfolio and grow their clientele through social media. Instead of relying on word of mouth, all MUA has the opportunity to showcase their skills and grab the attention of the clients that inquire in Woomen. This eliminates the hassle of lack of credentials and exposure. All MUA gets to communicate directly to the customers and negotiate the price according to the demand of the client, and instantly get the booking done online. At the same time this enables the clients to give feedback on the MUA that will empower the artist to further build their experience and portfolio with Woomen.Collecting ratings and client feedbacks shows credibility, and a higher chance to be a featured artist, and get sponsorships by the brands that partner with Woomen. 
     	</p>
	</div>
	<div class="article" id="article5">
		<h3 class="title">How do I get my work showcased in the Woomen homepage.</h3>
		<p class="text">
			MUA who perform well through bookings and reviews are awarded as featured artist on the homepage of Woomen. This creates a higher chance to be recognized by the client and sponsor brands who are particular to the rankings of MUA. 
		</p>
	</div>
	<div class="article" id="article6">
		<h3 class="title">What does Woomen do with the personal information it collects?</h3>
		<p class="text">
			Woomen may retain copies of submitted information by the MUA for our personal records and may continue to use this information. Woomen reserves the right to use, modify, adapt, publish, and display content posted on the platform. 
		</p>
	</div>
	<div class="article" id="article7">
		<h3 class="title">How can i get more clients?</h3>
		<p class="text">
			Maximize your portfolio quality by updating it and uploading your best projects on Woomen. Unlock more perks by upgrading your subscription to have you profile featured on more searches which will extend your services to more clients. Building your portfolio to a maximum profile strength and continuous updating your profile will drive more clients your way. Woomen also provides support in fixing your profile and making it look seamless and professional when you avail of the top profiles. 
		</p>
	</div>
{{-- 	<div class="article" id="article8">
		<h3 class="title">How to upgrade my profile?</h3>
		<p class="text">
			To upgrade your profile and add more depth to it, all you have to do is purchase the next profile setting and it will automatically update your dashboard. Click on the upgrade profile options. This will allow you to immediately add more pictures, videos and other perks that comes with the package.
		</p>
	</div> --}}
	<div class="article" id="article9">
		<h3 class="title">How to build a great portfolio?</h3>
		<p class="text">
			To ensure the quality of profiles in Woomen, here are some guidelines that will help you build a great portfolio. 
		</p>
		<ol>
			<li>Pictures should be professionally taken. This means, the pictures and videos that you uploaded are expected to be hi-res and not blurry.</li>
			<li>Proper lighting is an important aspect for every photo. Avoid too much or too little brightness to increase picture’s quality.</li>
			<li>Upload images that you have previously done. Please refrain from using other people’s works that are not yours and upload it as your project. If the project is done together with other vendors, then make sure to give complete credits to them. Woomen has the right to take down images and/or videos that are proven to be stolen from other sources, with or without other MUA consent.</li>
			<li>You may only upload images or videos that represent your portfolio or works as part of your Projects.</li>
			<li>MUA are allowed to upload pictures or videos with promotional items as long as the put the credits on the sources.</li>
			<li>For MUA who have availed of the higher level of subscription, they get the privilege of having a professional photoshoot/videoshoot of their featured works. They could also get discounts in hiring a photographer for help in their event shots.</li>
			<li>Last but not least, Woomen does not accept nudity and partial nudity works, be it in form of image or video. Unless this is part of a specific event, Woomen must be informed so that the pictures will be filtered accordingly.</li>
		</ol>
	</div>
	<div class="article" id="article10">
		<h3 class="title">What is profile strength?</h3>
		<p class="text">
			A profile strength indicates the number of bookings, ratings and traffic one MUA is getting. This is represented by the number of Diamonds in their portfolio. This indicators will identify the level of performance of the MUA which is vital for choosing the featured artist and for the sponsorship/endorsement program.
		</p>
		<h4 class="subtitle">HOW TO SEND A MESSAGE TO A MUA?</h4>
		<ul>
			<li>Go to the profile of a vendor you intended to send a message</li>
			<li>Tap the envelope button</li>
			<li>Choose subject by tapping ‘Select subject’</li>
			<li>Write your message</li>
			<li>Tap ‘Send’</li>
			<li>All communications will be sent to your email</li>
		</ul>
		<p class="text">Note: All communications and transactions should be done online, this will be reflected in the portfolio of the MUA.</p>
	</div>
	<h1>Tips and Tricks</h1>
{{-- 	<div class="article" id="article11">
		<h3 class="title">What is Virtual Credit Card?</h3>
		<p class="text">
			Virtual credit card is a means of measuring the proficiency of your MUA portfolio. One credit is deducted every time one client books the MUA or when a product from the MUA is purchased. When a credit is deducted, you will receive an email notification and push notification via Woomen.ph Both email and push notification will then direct you to the homepage that contains the client's email information and details of the transaction.
		</p>
	</div>
	<div class="article" id="article12">
		<h3 class="title">What happens if I run out of credits?</h3>
		<p class="text">
			When you exhaust your credits:
		</p>
		<ol>
			<li>The booking mechanism will be disabled.</li>
			<li>We limit you to show maximum 1 project only.</li>
			<li>You will not be able to read messages from potential clients.</li>
			<li>You will not be able to access your business statistics.</li>
		</ol>	
	</div> --}}
	<div class="article" id="article13">
		<h3 class="title">How to manage my clients?</h3>
		<p class="text">
			Woomen has made it easier to manage your clients, as it has a built-in calendar for the MUA dashboard. Wherein it is synced to the booking system, so every time a transaction is processed,, the details will immediately appear on the MUA calendar. This will ensure that the bookings are not overlapping and the timings are well spaced out. Notifications will be sent out to the MUA if there are certain overlapping in time and reminders for upcoming bookings. 
		</p>	
	</div>
	<div class="article" id="article14">
		<h3 class="title">How to become the featured artist?</h3>
		<p class="text">
			Featured artist are based on the MUA portfolio, activities, bookings, client visits and reviews. All of these factors are measured and rated by the diamonds in their portfolio. Every month Woomen will be featuring at least 8 MUAs. 
		</p>	
	</div>
	<div class="article" id="article15">
		<h3 class="title">How to get sponsorship or advertisement on the profile?</h3>
		<p class="text">
			Sponsorship or advertisement are limited to the featured artist only. Once the MUA are in the lineup they have a higher chance of getting sponsorships and advertisement on the profile. 
		</p>	
	</div>
	<h1>WOOmen Marketplace</h1>
	<div class="article" id="article16">
		<h3 class="title">How to join the marketplace?</h3>
		<ol>
			<li>There are two ways to join the marketplace; one is through the MUA upgrade offers. The MUA can choose the upgrade wherein they can upload pictures and content on the additional feature in their homepage.</li>
			<li>The second way of joining the marketplace is by the users, where they will have to sign up and fill in the forms that are needed. This will enable the users to sell their products to both MUA and other end users.</li>
		</ol>
	</div>
	<div class="article" id="article17">
		<h3 class="title">Who can join the marketplace?</h3>
		<p class="text">
			All MUA and interested users can apply their products in the marketplace. The profile of the MUA will reflect the marketplace on the bottom showcasing the products that they are selling. While the users will have their own products showcased in the marketplace dashboard. 
		</p>	
	</div>
</div>