@extends('appMaster')

@section('title')
	<title>Woomen - FAQ</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	<style type="text/css">body {background-color:white;position:relative !important;}</style>
@stop

@section('content')

	@include('App::universalHead')

	<div class="appFaq-content">
			<div class="container">
				@include('App::faq.partials.section-1')
				@include('App::faq.partials.section-2')
				@include('App::faq.partials.section-3')
			</div>
	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()


@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/slick-init.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script type="text/javascript">
		window.addEventListener("scroll", function(event) {	  
		    var top = this.scrollY;
		  	console.log(top);
		    if(top > 130){
		    	// console.log(top);
		    	$("#sidenav").css({"position":"fixed","top":"125px"});
		    }
		    else {
		    	$("#sidenav").css({"position":"relative","top":"0px"});
		    }
		  

		    if(top > 2200){
		    	$("#sidenav").css({"position":"fixed","top":"-500px"});
		    }

		}, false);


		$(document).ready(function(){
		  // Add scrollspy to <body>
		  $('body').scrollspy({target: ".scroll-spy"});   

		  // Add smooth scrolling on all links inside the navbar
		  $("#sidenav a").on('click', function(event) {
		    // Make sure this.hash has a value before overriding default behavior
		    if (this.hash !== "") {
		      // Prevent default anchor click behavior
		      event.preventDefault();
		      // Store hash
		      var hash = this.hash;
		   	
		      if(hash == "#top"){
		      	$('html, body').animate({
			        scrollTop: 0,
			      }, 800, function(){
			          
			        window.location.hash = hash;
			    });
		      }
		      else {
			      $('html, body').animate({
			        scrollTop: $(hash).offset().top-101,
			      }, 800, function(){
			          
			        window.location.hash = hash;
			      });
		      }
		    }
		  });
		});

	</script>

	@if(session('account_activated'))
	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Hooray! Your account is now activated.');
	        });

	    </script>
	@endif

	@if(session('account_activated_already'))

	    <script type="text/javascript">

	        $(document).ready(function(){
	            alert('Oops! Your account is already activated.'); 
	        });

	    </script>

	@endif

@stop