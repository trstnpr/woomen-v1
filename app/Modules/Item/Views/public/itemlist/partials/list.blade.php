<section class="itemlist-container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h3 style="text-align:center;"><b><span>{{ $items->count() }}</span> items found</b></h3>
			</div>
			<div class="col-md-12">
				<div class="items-wrap">
					<div class="row">
						@foreach($items as $i)
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<div class="merchandise-item">
								<figure class="item-img" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.item') .'/'. $i->feat_image['image_path'] }}');"></figure>
								<div class="item-info">
									<h4 class="item-name">{{ str_limit($i->item_name, 30) }}</h4>
									<span class="item-price">Php {{ number_format($i->item_price, 2) }}</span>
									<a href="/item/browse/{{ $i->item_id }}" class="btn woo-btn btn-block btn-sm">VIEW ITEM</a>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

{{-- <div class="col-md-12">
	{{ $items->appends(\Request::except('page'))->links() }}
</div> --}}