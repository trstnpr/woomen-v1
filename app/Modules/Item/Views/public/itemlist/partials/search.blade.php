<div id="toggle-show-search">
	<i class="fa fa-search"></i>
</div>
<section class="search-header">
	<div class="col-md-5">
		<form class="search" id="search_item" method="GET">
			<div class="form-group">
				<div class="input-group">
					<input class="form-control" name="q" id="search" placeholder="Search for item" value="{{ (isset($_REQUEST['q'])) ? $_REQUEST['q'] : '' }}" type="text">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-round gold">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-7 filter-search">
		<form>
			<div class="col-md-5 price">
				<div class="form-group col-md-6">
					<input type="text" class="form-control" name="price_from" placeholder="Price from" />
				</div>
				<div class="form-group col-md-6">
					<input type="text" class="form-control" name="price_to" placeholder="Price to" />
				</div>
			</div>
			<div class="col-md-5 order">
				<div class="form-group col-md-6">
					<select class="form-control" name="order">
						<option value selected disabled>Order by:</option>
						<option value="asc">Ascending (A-Z)</option>
						<option value="desc">Descending (Z-A)</option>
					</select> 
				</div>	
				<div class="form-group col-md-6">
					<select class="form-control" name="date">
						<option value selected disabled>Order by date:</option>
						<option value="asc">New Items</option>
						<option value="desc">Old Items</option>
					</select> 
				</div>	
{{-- 					<ul class="nav nav-stacked filters" id="accordion">
						<li>
							<a data-toggle="collapse" data-parent="#accordion"  href="#sort">Sort by<i class="fa fa-sort pull-right caret_show"></i></a>
							<ul class="list-unstyled filters_dp collapse in" id="sort">
								<li class="checkbox">
									<label>
										<input type="radio" name="order" value="asc"> Ascending (A-Z)
									</label>
								</li>
								<li class="checkbox">
									<label>
										<input type="radio" name="order" value="desc"> Descending (Z-A)
									</label>
								</li>
								<li class="checkbox">
									<label>
										<input type="radio" name="date" value="desc"> New Items
									</label>
								</li>
								<li class="checkbox">
									<label>
										<input type="radio" name="date" value="asc"> Old Items
									</label>
								</li>
							</ul>
						</li>
					</ul>	 --}}		
			</div>
			<div class="col-md-2 filter-btn">
				<div class="form-group">
					<button class="btn btn-default btn-block gold">Filter</button>
				</div>
			</div>
		</form>	
		<div class="col-md-12">
			<button class="btn btn-default" id="toggle-hide-search">Close</button>
		</div>
	</div>
</section>
