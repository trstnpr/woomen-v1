@extends('appMaster')

@section('title')
	<title>Woomen - Merchandise</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />

	

	<style>
		* {
			font-family: Arial, Helvetica, sans-serif;
		}

		.navbar {
			background-color: #fff;
    		box-shadow: none !important;
		}
	</style>
@stop

@section('content')

	@include('App::universalHead')

	<div class="itemlist-content">

		@include('Item::public.itemlist.partials.search')

		@include('Item::public.itemlist.partials.list')

	</div>

@stop()

@section('footer')
	@include('App::app.partials.footer')
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.filter-search').submit(function(){
				if($('input[name="quantity"]').val() == 0){
					$('input[name="quantity"]').remove();
				}
				if($('input[name="price_from"]').val() == 0 && $('input[name="price_to"]').val() == 0){
					$('input[name="price_from"]').remove();
					$('input[name="price_to"]').remove();
				}
			});

			/*$('input[name="price_from"]').change(function(){
				if($('input[name="price_from"]').val() > $('input[name="price_to"]').val()){
					$('input[name="price_to"]').val($('input[name="price_from"]').val());
				}
			});

			$('input[name="price_to"]').change(function(){
				if($('input[name="price_from"]').val() > $('input[name="price_to"]').val()){
					$('input[name="price_from"]').val($('input[name="price_to"]').val());
				}
			});*/

			$("#toggle-show-search").click(function(){
				$(".search-header").slideDown();
				$("#toggle-show-search").hide();
			});		

			$("#toggle-hide-search").click(function(){
				$(".search-header").slideUp();
				$("#toggle-show-search").show();
			});			

		});

		$(document).ready(function() {
		    var loading_options = {
		        finishedMsg: "<div class='end-msg'>Congratulations! You've reached the end of the internet</div>",
		        msgText: "<div class='center'>Loading news items...</div>",
		        img: "/assets/img/ajax-loader.gif"
		    };

		    $('#content').infinitescroll({
		        loading: loading_options,
		        navSelector: "ul.pagination",
		        nextSelector: "ul.pagination a:first",
		        itemSelector: "#content div.item"
		    });
		}); 
	</script>
@stop