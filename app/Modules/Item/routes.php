<?php

Route::group(array('module' => 'Item', 'namespace' => 'App\Modules\Item\Controllers', 'middleware' => ['web', 'auth', 'merchant'], 'prefix' => 'merchant/item'), function() {

    /*Route::resource('Item', 'ItemController');*/

    /*----------------------------CRUD ITEMS---------------------------------*/

    Route::get('/', ['uses' => 'ItemController@index', 'as' => 'merchant.item']);

    Route::get('/images/{id}', ['uses' => 'ItemController@item_images', 'as' => 'merchant.item.images']);

    /**Create Item*/
    Route::get('/create', ['uses' => 'CreateItemController@create', 'as' => 'merchant.item.create']);
    Route::post('/store', ['uses' => 'CreateItemController@store', 'as' => 'merchant.item.store']);

    /**Edit Item*/
    Route::get('/edit/{id}', ['uses' => 'UpdateItemController@edit', 'as' => 'merchant.item.edit']);
  	Route::post('/update/{id}', ['uses' => 'UpdateItemController@update', 'as' => 'merchant.item.update']);

    /**Delete Item*/
    Route::delete('/delete/{id}', ['uses' => 'DeleteItemController@delete', 'as' => 'merchant.item.delete']);

    /**Delete Item Image*/
    Route::delete('/delete/item_image/{id}', ['uses' => 'DeleteItemController@delete_image', 'as' => 'merchant.item.delete_image']);
    
});

Route::group(array('prefix' => 'item', 'module' => 'Item', 'namespace' => 'App\Modules\Item\Controllers', 'middleware' => ['web']), function() {

    /** Test View **/
    Route::get('browse', 'ItemController@itemList');
    //Route::get('browse/search', 'ItemController@itemSearch');

});

Route::group(array('prefix' => 'item', 'module' => 'Item', 'namespace' => 'App\Modules\Merchant\Controllers', 'middleware' => ['web']), function() {

    Route::get('browse/{id}', ['uses' => 'MerchantItemController@public_item_view', 'as' => 'merchant.public.item']);
    Route::post('browse/{id}/inquire', ['uses' => 'MerchantItemController@public_item_inquiries', 'as' => 'merchant.public.item.inquire']);

});