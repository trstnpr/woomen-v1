<?php namespace App\Modules\Item\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

	protected $table = 'items';

    protected $fillable = ['merchant_profile_id', 'artist_profile_id', 'item_name', 'item_description', 'item_contact', 'item_quantitiy', 'item_price', 'clean_url'];

    protected $primaryKey = 'item_id';

    /**
     * Get the images of the item.
     */
	public function item_images()
	{
		return $this->hasMany('App\Modules\Item\Models\ItemImages');
	}

	/**
     * Get the feat_image of the item.
     */
	public function feat_image()
	{
		return $this->hasOne('App\Modules\Item\Models\ItemImages');
	}

	/**
     * Get the Merchant Profile of the item.
     */
	public function merchant()
	{
		return $this->hasOne('App\Modules\Merchant\Models\MerchantProfile', 'merchant_profile_id', 'merchant_profile_id');
	}

	public function artist()
	{
		return $this->hasOne('App\Modules\Artist\Models\ArtistProfile', 'artist_profile_id', 'artist_profile_id');
	}

}
