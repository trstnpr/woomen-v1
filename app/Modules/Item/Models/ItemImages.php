<?php namespace App\Modules\Item\Models;

use Illuminate\Database\Eloquent\Model;

class ItemImages extends Model {

	protected $table = 'item_images';

    protected $fillable = ['item_id', 'image_path'];

    protected $primaryKey = 'item_image_id';

    public function item()
    {
    	return $this->belongsTO('App\Modules\Item\Models\Item');
    }

}
