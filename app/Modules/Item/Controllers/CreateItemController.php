<?php namespace App\Modules\Item\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;
use App\Modules\App\Models\User;

use Auth;
use Carbon\Carbon;
use App\Services\ImageUploader;

class CreateItemController extends Controller {

    /**
     * Create Item Form View
     *
     * @return Response
     */
    public function create()
    {
        $data['email'] = User::where('id', Auth::user()->id)->first();
        $data['merchant'] = MerchantProfile::where('user_id', Auth::user()->id)->first();
        $data['items'] = Item::with('feat_image')
                            ->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->limit(5)
                            ->get();

        $data['all_items'] = Item::with('feat_image')
                            ->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->get();
        
        return view('Merchant::createItem', $data);
    }

	/**
     * Store Item
     *
     * @return Response
     */
    public function store(ItemRequest $request)
    {   
        if ($request->hasFile('image_path')){

            if($request->image_path != ''){

        	   $profile = MerchantProfile::where('user_id', Auth::user()->id)->first();

            	$data_item = [
            		'merchant_profile_id' => $profile['merchant_profile_id'],
            		'item_name' => $request->item_name,
            		'item_description' => $request->item_description,
            		'item_contact' => $request->item_contact,
            		'item_quantity' => $request->item_quantity,
                    'item_price' => $request->item_price,
            		'created_at' => Carbon::now(),
            	];

            	$item = Item::insertGetId($data_item);

            	if($item){
                    if ($request->hasFile('image_path')){
                        $images = $request->image_path;

                        if($images != ''){
                            foreach($images as $imageUrl){
                                    $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                                    $imageUploader = new ImageUploader;

                                    $result = $imageUploader->upload($imageUrl, $item, 'items');

                                    $data_campaign_image = [
                                        'item_id' => $item,
                                        'image_path' => $item.'/'.$result,
                                        'created_at' => Carbon::now()
                                    ];

                                $insert_Images = ItemImages::insert($data_campaign_image);
                            }
                        }
                    }

                    return json_encode(array('result' => 'success', 'message' => 'Item Successfully Added!'));
                }
                return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
            }
        }
        return json_encode(array('result' => 'error', 'message' => 'Please Insert at least 1 image for your item.'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
