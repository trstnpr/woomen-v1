<?php namespace App\Modules\Item\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;

use Auth;
use App\Services\ImageUploader;

class DeleteItemController extends Controller {

	/**
     * Delete Item
     *
     * @return Response
     */
    public function delete($id)
    {   
        $profile = MerchantProfile::where('user_id', Auth::user()->id)->first();

        $item = Item::where('item_id', $id)->where('merchant_profile_id', $profile['merchant_profile_id'])->first();

        if($item){
            if (Item::where('item_id', $id)->delete()) {
                return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
            }

            return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'You are not allowed to delete this item.'));
    	
    }

    /**
     * Delete Image of Item
     *
     * @return Response
     */

    public function delete_image($id)
    {
        $item = ItemImages::where('item_image_id', $id)->first();
        if($item){
            if (ItemImages::where('item_image_id', $id)->delete()) 
            {
                $imageUploader = new ImageUploader;
                $imageUploader::deleteS3(config('cdn.item') . '/' . $item['image_path']);

                return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
            }

            return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
        }
    }

}
