<?php namespace App\Modules\Item\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\App\Models\User;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;
use App\Modules\App\Models\Specialty;

use Illuminate\Http\Request;
use Auth;

class ItemController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['email'] = User::where('id', Auth::user()->id)->first();
        $data['merchant'] = MerchantProfile::where('user_id', Auth::user()->id)->first();
        $data['items'] = Item::with('feat_image')
                            ->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->limit(5)
                            ->get();

        $data['all_items'] = Item::with('feat_image')
                            ->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->get();

		return view("Merchant::items", $data);
	}

	/**
	 * Get Item Images
	 *
	 * @return Response
	 */
	public function item_images($id)
	{	
        $items = ItemImages::where('item_id', $id)->get();

		return ['result' => true, 'message' => $items];
	}

	/* Item Listing */
	public function itemList(Request $request) 
	{	
		if(isset($request->q))
		{
			$items =  Item::where('item_name', 'like', '%'.$request->q.'%')->orderBy('created_at', 'desc')->get();
			$specialty = Specialty::all();
			$max_price = Item::max('item_price');
			return view('Item::public.itemlist.index', compact('items', 'specialty', 'max_price'));
		}

		elseif(isset($request->quantity) || isset($request->price_from) || isset($request->price_to) || isset($request->order) || isset($request->date))
		{
			$max_price = Item::max('item_price');

			if(!isset($request->price_to)){
				$request->request->add(['price_from' => 0, 'price_to' => $max_price]);
			}

			$items =  Item::whereBetween('item_price', [$request->price_from, $request->price_to])
						->orderBy('item_name', $request->order)
						->orderBy('created_at', $request->date)
						->get();
			$specialty = Specialty::all();
			return view('Item::public.itemlist.index', compact('items', 'specialty', 'max_price'));
		}
		
		else{
			$data['specialty'] = Specialty::all();
			$data['items'] = Item::with('feat_image')->orderBy('created_at', 'desc')->get();

			$data['max_price'] = Item::max('item_price');
			return view('Item::public.itemlist.index', $data);
		}
	}

	/* Item Listing */
	public function itemSearch(Request $request) 
	{	
		if(isset($request->q))
		{
			$items =  Item::where('item_name', 'like', '%'.$request->q.'%')->orderBy('created_at', 'desc')->paginate(9);
		
		}
		
		else{
			$items= Item::with('feat_image')->orderBy('created_at', 'desc')->paginate(9);
		}

		$specialty = Specialty::all();
		
		return view('Item::public.itemlist.it', compact('items', 'specialty'));
	}

}
