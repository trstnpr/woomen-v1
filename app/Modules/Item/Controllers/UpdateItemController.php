<?php namespace App\Modules\Item\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;
use App\Modules\App\Models\User;
use Auth;
use Carbon\Carbon;
use App\Services\ImageUploader;

class UpdateItemController extends Controller {

    /**
     * Edit Item Form View
     *
     * @return Response
     */
    public function edit($id)
    {   
        $data['email'] = User::where('id', Auth::user()->id)->first();
        $data['merchant'] = MerchantProfile::where('user_id', Auth::user()->id)->first();
        $data['items'] = Item::with('feat_image')
                            ->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->limit(5)
                            ->get();

        $data['all_items'] = Item::with('feat_image')
                            ->where('merchant_profile_id', $data['merchant']['merchant_profile_id'])
                            ->orderBy('created_at', 'desc')
                            ->get();
                                    
        $data['item'] = Item::where('item_id', $id)->first();
        $data['item_images'] = ItemImages::where('item_id', $id)->get();

        return view('Merchant::editItem', $data);
    }

	/**
     * Update Item
     *
     * @return Response
     */
    public function update($id, ItemRequest $request)
    {    
    	$profile = MerchantProfile::where('user_id', Auth::user()->id)->first();

    	$data_item = [
    		'merchant_profile_id' => $profile['merchant_profile_id'],
    		'item_name' => $request->item_name,
    		'item_description' => $request->item_description,
    		'item_contact' => $request->item_contact,
            'item_price' => $request->item_price,
            'item_quantity' => $request->item_quantity,
            'updated_at' => Carbon::now(),
    	];

    	$item = Item::where('item_id', $id)->update($data_item);

    	if($item){
            if ($request->hasFile('image_path')){
                $images = $request->image_path;
                /*$del = ItemImages::where('item_id', $id)->delete();*/

                if($images != ''){
                    foreach($images as $imageUrl){
                            $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                            $imageUploader = new ImageUploader;

                            $result = $imageUploader->upload($imageUrl, $id, 'items');

                            $data_campaign_image = [
                                'item_id' => $id,
                                'image_path' => $id.'/'.$result,
                                'updated_at' => Carbon::now()
                            ];

                        $insert_Images = ItemImages::insert($data_campaign_image);
                    }
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Item Successfully Updated!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while updating'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
