<?php namespace App\Modules\Admin\Controllers\AdminItemController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\Item\Models\Item;

use Auth;

class DeleteItemController extends Controller {

	/**
     * Delete Item
     *
     * @return Response
     */
    public function delete($id)
    {   
        if (Item::where('item_id', $id)->delete()) {
            return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

}
