<?php namespace App\Modules\Admin\Controllers\AdminItemController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;

use Auth;
use Carbon\Carbon;
use App\Services\ImageUploader;

class UpdateItemController extends Controller {

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function update($id, ItemRequest $request)
    {
        $data_item = [
            'item_name' => $request->item_name,
            'item_description' => $request->item_description,
            'item_contact' => $request->item_contact,
            'item_quantity' => $request->item_quantity,
            'item_price' => $request->item_price,
            'updated_at' => Carbon::now(),
        ];

        $item = Item::where('item_id', $id)->update($data_item);

        if($item){
            if ($request->hasFile('image_path')){
                $images = $request->image_path;
                $del = ItemImages::where('item_id', $id)->delete();

                if($images != ''){
                    foreach($images as $imageUrl){
                            $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                            $imageUploader = new ImageUploader;

                            $result = $imageUploader->upload($imageUrl, $id, 'items');

                            $data_campaign_image = [
                                'item_id' => $id,
                                'image_path' => $id.'/'.$result,
                                'updated_at' => Carbon::now()
                            ];

                        $insert_Images = ItemImages::insert($data_campaign_image);
                    }
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Item Successfully Updated!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while updating'));
    }

    public function edit($id)
    {   
        $data['item'] = Item::where('item_id', $id)->first();
        return view('Admin::item.item', $data); // single item edit view
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }
}
