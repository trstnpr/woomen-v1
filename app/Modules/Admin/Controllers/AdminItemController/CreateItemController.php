<?php namespace App\Modules\Admin\Controllers\AdminItemController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;

use Auth;
use Carbon\Carbon;
use App\Services\ImageUploader;

class CreateItemController extends Controller {

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function store(ItemRequest $request)
    {
        $data_item = [
            'merchant_profile_id' => $request->merchant_profile_id,
            'item_name' => $request->item_name,
            'item_description' => $request->item_description,
            'item_contact' => $request->item_contact,
            'item_quantity' => $request->item_quantity,
            'item_price' => $request->item_price,
            'created_at' => Carbon::now(),
        ];

        $item = Item::insertGetId($data_item);

        if($item){
            if ($request->hasFile('image_path')){
                $images = $request->image_path;

                if($images != ''){
                    foreach($images as $imageUrl){
                            $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                            $imageUploader = new ImageUploader;

                            $result = $imageUploader->upload($imageUrl, $item, 'items');

                            $data_campaign_image = [
                                'item_id' => $item,
                                'image_path' => $item.'/'.$result,
                                'created_at' => Carbon::now()
                            ];

                        $insert_Images = ItemImages::insert($data_campaign_image);
                    }
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Item Successfully Added!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }
}
