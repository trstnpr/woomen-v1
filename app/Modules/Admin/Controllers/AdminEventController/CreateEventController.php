<?php namespace App\Modules\Admin\Controllers\AdminEventController;

use Auth;
use Carbon\Carbon;

use App\Services\ImageUploader;

use App\Http\Requests\EventRequest;

use App\Modules\Event\Models\ArtistEvent;
use App\Modules\Artist\Models\ArtistProfile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CreateEventController extends Controller {

	/**
     * Store Event
     *
     * @return Response
     */
    public function store(EventRequest $request)
    {    
    	$data_event = [
    		'artist_profile_id' => $request->artist_profile_id,
    		'event_title' => $request->event_title,
    		'event_description' => $request->event_description,
    		'event_venue' => $request->event_venue,
    		'event_date' => $request->event_date,
    		'created_at' => Carbon::now(),
    	];

    	$event = ArtistEvent::insertGetId($data_event);

    	if($event){
            if ($request->hasFile('event_image')){
                $image = $request->event_image;

                if($image != ''){
                    $this->_validate_image_format($image->getClientOriginalExtension());

                    $imageUploader = new ImageUploader;

                    $result = $imageUploader->upload($image, $event, 'events');

                    $data_event_image = [
                        'event_image' => $event.'/'.$result
                    ];

                	$insert_image = ArtistEvent::where('artist_event_id', '=', $event)->update($data_event_image);
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Event Successfully Added!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
