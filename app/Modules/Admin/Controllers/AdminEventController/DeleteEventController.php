<?php namespace App\Modules\Admin\Controllers\AdminEventController;

use Auth;

use App\Services\ImageUploader;

use App\Modules\Event\Models\ArtistEvent;
use App\Modules\Artist\Models\ArtistProfile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DeleteEventController extends Controller {

	/**
     * Delete Item
     *
     * @return Response
     */
    public function delete($id)
    {   
        $event = ArtistEvent::where('artist_event_id', $id)->first();

        if($event){
            if (ArtistEvent::where('artist_event_id', $id)->delete()) {
            	if($event['event_image'] != null){
            		$imageUploader = new ImageUploader;
                	$imageUploader::deleteS3(config('cdn.event') . '/' . $event['event_image']);
                }

                return json_encode(array('result' => 'success', 'message' => 'Successfully Deleted.'));
            }

            return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'You are not allowed to delete this event.'));
    	
    }

}
