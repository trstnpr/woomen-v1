<?php namespace App\Modules\Admin\Controllers\AdminEventController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\EventRequest;
use App\Modules\Event\Models\ArtistEvent;
use App\Modules\Item\Models\Item;
use App\Modules\Item\Models\ItemImages;

use Auth;
use Carbon\Carbon;
use App\Services\ImageUploader;

class UpdateEventController extends Controller {

    /**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function edit($id)
    {   
        $data['event'] = ArtistEvent::where('artist_event_id', $id)->first();

        return view("Admin::event.event", $data);
    }

	/**
     * Update Item
     *
     * @return Response
     */
    public function update($id, EventRequest $request)
    {    
        $data_event = [
            'event_title' => $request->event_title,
            'event_description' => $request->event_description,
            'event_venue' => $request->event_venue,
            'event_date' => date_format(date_create($request->event_date), 'Y-m-d'),
            'event_time' => date_format(date_create($request->event_time), 'H:i:s'),
            'event_ticket' => $request->event_ticket,
            'updated_at' => Carbon::now(),
        ];

        $event = ArtistEvent::where('artist_event_id', $id)->update($data_event);

        if($event){
            if ($request->hasFile('event_image')){
                $image = $request->event_image;

                if($image != ''){
                    $this->_validate_image_format($image->getClientOriginalExtension());

                    $imageUploader = new ImageUploader;

                    if($event_image = ArtistEvent::where('artist_event_id', '=', $id)->value('event_image') != null){
                        $imageUploader::deleteS3(config('cdn.event') . '/' . $event_image);
                    }

                    $result = $imageUploader->upload($image, $id, 'events');

                    $data_event_image = [
                        'event_image' => $event.'/'.$result
                    ];

                    $update_image = ArtistEvent::where('artist_event_id', $id)->update(['event_image' => $id.'/'.$result]);
                }
            }

            return json_encode(array('result' => 'success', 'message' => 'Event Successfully Updated!'));
        }
        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while updating'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
