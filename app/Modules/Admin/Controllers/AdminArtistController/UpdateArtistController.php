<?php namespace App\Modules\Admin\Controllers\AdminArtistController;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArtistProfileRequest;

use Illuminate\Http\Request;
use App\Modules\Artist\Models\ArtistProfile;
use App\Modules\Artist\Models\ArtistSpecialty;
use App\Modules\Artist\Models\ArtistPackages;
use App\Modules\Artist\Models\ArtistAlbums;
use App\Modules\Artist\Models\ArtistPortfolio;
use App\Modules\App\Models\Specialty;
use App\Modules\App\Models\User;
use App\Modules\App\Models\Booking;

use App\Services\ImageUploader;

class UpdateArtistController extends Controller {

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function update($id, ArtistProfileRequest $request)
    {   
        $profile = ArtistProfile::where('user_id', $id)->first();
        $userUpdate = User::where('id', $id)->update(['email' => $request->email]);

        if ($request->specialty != null) {
            $del = ArtistSpecialty::where('artist_profile_id', $profile['artist_profile_id'])->delete();
            foreach ($request->specialty as $sc) {
                $dataSpecialty = ['artist_profile_id' => $profile['artist_profile_id'], 'specialty_id' => $sc];
                $insert_specialty = ArtistSpecialty::insert($dataSpecialty);
            }
        }

        if ($request->hasFile('profile_image'))
            {       
                $imageUrl = $request->profile_image;
                $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                $imageUploader = new ImageUploader;

                $result = $imageUploader->upload($imageUrl, $profile['artist_profile_id'], 'profile_images');

                $data_profile = [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'middle_name' => $request->middle_name,
                    'address' => $request->address,
                    'about' => $request->about,
                    'achievements' => $request->achievements,
                    'facebook' => $request->facebook,
                    'profile_image' => $profile['artist_profile_id'].'/'.$result
                ];

                $profileUpdate = ArtistProfile::where('user_id', $id)->update($data_profile);
            }

        else{
            $data_profile = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'middle_name' => $request->middle_name,
                'address' => $request->address,
                'about' => $request->about,
                'achievements' => $request->achievements,
                'facebook' => $request->facebook,
            ];

            $profileUpdate = ArtistProfile::where('user_id', $id)->update($data_profile);
        }

        if ($profileUpdate) {
            return json_encode(array('result' => 'success', 'message' => 'Artist Profile Successfully Updated.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
    }

    /**
     * Upgrade Artist
     *
     * @param Request $request
     * @param $id
     * @return view
     */
    public function upgrade($id, Request $request)
    {
        $artist = ArtistProfile::where('artist_profile_id', '=', $id)->first();
        $artist->user_level = $request->level;
        $artist->save();

        return json_encode(array('result' => 'success', 'message' => 'Artist Profile Successfully Upgraded.'));
    }

    /**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function edit($id)
    {   
        $data['user'] = User::with(['artist_profile' => function($query){
                                        $query->select()
                                        ->with(['artist_specialty' => function($query){
                                            $query->select();
                                        }]);
                                    }])
                                    ->where('user.id', $id)->first();

        $artist_id = ArtistProfile::where('user_id', $id)->first();

        $data['myForte'] = array();

        $data['specialty'] = Specialty::get();
        $result = ArtistSpecialty::where('artist_profile_id', $artist_id['artist_profile_id'])->get();
        foreach ($result as $key => $value) {
            array_push($data['myForte'], $value['specialty_id']);
        }

        $data['package'] = ArtistPackages::where('artist_profile_id', $artist_id['artist_profile_id'])->orderBy('artist_package_id', 'desc')->first();
        $data['album'] = ArtistAlbums::with('artist_images')->where('artist_albums.artist_profile_id', $artist_id['artist_profile_id'])->orderBy('created_at', 'desc')->get();
        $data['video'] = ArtistPortfolio::where('artist_profile_id', $artist_id['artist_profile_id'])->orderBy('created_at', 'desc')->get();
        $data['bookings'] = Booking::where('artist_profile_id', $artist_id['artist_profile_id'])->orderBy('created_at', 'desc')->get();

        return view("Admin::artist.viewArtist", $data);
    }

    /**
     * Feature Artist
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function featured($id)
    {   
        $artist_id = ArtistProfile::where('artist_profile_id', $id)->update(['is_featured' => 1]);

        if($artist_id){
            return json_encode(array('result' => 'success', 'message' => 'Artist Profile Successfully Featured.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while featuring. Please try again later.'));
    }

    /**
     * Remove Artist
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function remove($id)
    {   
        $artist_id = ArtistProfile::where('artist_profile_id', $id)->update(['is_featured' => 0]);

        if($artist_id){
            return json_encode(array('result' => 'success', 'message' => 'Artist Profile removed from being Featured.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while featuring. Please try again later.'));
    }

    /**
     * Update Artist Package
     *
     * @param
     * @return json
     */
    public function updatePackage(Request $request)
    {   
        $package = ArtistPackages::updateOrCreate(['artist_profile_id'=>$request->artist_profile_id, 'package_description'=>$request->package_description]);

        if($package){
            return json_encode(array('result' => 'success', 'message' => 'Package Successfully Featured.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
    }

    public function activate($id, Request $request)
    {
        $result = User::where('id', $id)->update(['status'=>$request->status]);
        if($result){
            return json_encode(array('result' => 'success', 'message' => 'Success!'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }

}
