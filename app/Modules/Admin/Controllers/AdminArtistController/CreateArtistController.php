<?php namespace App\Modules\Admin\Controllers\AdminArtistController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\App\Models\User;
use App\Modules\Artist\Models\ArtistProfile;

use App\Services\MailSender;
use Auth;
use Carbon\Carbon;

class CreateArtistController extends Controller {

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function store(Request $request, MailSender $mailsender)
    {
    	$data_user = [
            'email' => $request->email,
            'password' => $request->password,
            'activation_code' => $this->_generate_verification_code(),
            'role_id' => 2,
            'status' => 1,
            'created_at' => Carbon::now()
        ];

        $user = User::insertGetId($data_user);

        $data_profile = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'address' => $request->address,
            'about' => $request->about,
            'facebook' => $request->facebook
            'user_id' => $user,
        ];

        if($data_user){
            ArtistProfile::create($data_profile);

            $mailSender->send('email.email_credentials', 'Woomen Credentials', $request->all());

            return json_encode(array('result' => 'success', 'message' => 'Successfully created'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There\'s an error encountered while saving'));
    }

    /**
     * Generate new verification code.
     *
     * @return string
     */
    private function _generate_verification_code()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZzxcvbnmasdfghjklqwertyuiop';

        $pin = mt_rand(10, 9999) . mt_rand(10, 9999) . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }
}
