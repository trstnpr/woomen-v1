<?php namespace App\Modules\Admin\Controllers\AdminArtistController;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArtistProfileRequest;

use Illuminate\Http\Request;
use App\Modules\Artist\Models\ArtistProfile;
use App\Modules\Artist\Models\ArtistAlbums;
use App\Modules\Artist\Models\ArtistPortfolio;
use App\Modules\App\Models\User;

class ViewArtistController extends Controller {

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function view_edit()
    {   
        return view("Admin::artist.viewArtist");
    }

    public function album($id)
    {   
        $data['album'] = ArtistAlbums::with('artist_images')->where('artist_albums.artist_album_id', $id)->orderBy('created_at', 'desc')->first();
        $data['video'] = ArtistPortfolio::where('artist_album_id', $id)->orderBy('created_at', 'desc')->get();

        $data['artist_id'] = ArtistAlbums::where('artist_albums.artist_album_id', $id)->first();
        $data['artist'] = ArtistProfile::where('artist_profile_id', $data['artist_id']['artist_profile_id'])->first();

        return view("Admin::artist.viewAlbum", $data);
    } 
    
    public function delete_album($id)
    {  
        $albumDelete = ArtistAlbums::where('artist_album_id', $id)->delete();
        if ($albumDelete) {
            return json_encode(array('result' => 'success', 'message' => 'Album Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

    public function delete_video($id)
    {  
        $videoDelete = ArtistPortfolio::where('artist_portfolio_id', $id)->delete();
        if ($videoDelete) {
            return json_encode(array('result' => 'success', 'message' => 'Video Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }


}
