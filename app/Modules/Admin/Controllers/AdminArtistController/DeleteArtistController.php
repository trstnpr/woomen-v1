<?php namespace App\Modules\Admin\Controllers\AdminArtistController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\App\Models\User;

class DeleteArtistController extends Controller {

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function delete($id)
    {  
    	$userDelete = User::where('id', $id)->delete();
        if ($userDelete) {
            return json_encode(array('result' => 'success', 'message' => 'Artist Profile Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

}
