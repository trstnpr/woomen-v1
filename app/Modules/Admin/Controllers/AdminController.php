<?php namespace App\Modules\Admin\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

use Auth;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("Admin::index");
	}

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function adminAuthenticate(LoginRequest $login_request)
    {
    	if (Auth::attempt(['email' => $login_request->email, 'password' => $login_request->password, 'role_id' => 1])) {
    		return json_encode(array('result' => 'success', 'message' => $this->getUserRole(Auth::user()->role_id) . '/dashboard'));
    	}
    	return json_encode(array('result' => 'error', 'message' => 'Authentication Failed!'));
    }

    /**
     * Logout admin.
     * @return Redirect
     */
    public function adminLogout()
	{
		Auth::logout();
        return redirect('/admin');
	}

}
