<?php namespace App\Modules\Admin\Controllers\AdminDashboardController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\Artist\Models\ArtistProfile;
use App\Modules\Event\Models\ArtistEvent;
use App\Modules\Item\Models\Item;
use App\Modules\App\Models\Review;
use App\Http\Requests\ChangePasswordRequest;
use App\Modules\App\Models\User;
use App\Modules\App\Models\Update;
use Illuminate\Http\Request;

use Auth;

class DashboardController extends Controller {

	/**
     * Admin Dashboard
     *
     * @param
     * @return view
     */
	public function dashboard()
    {
    	$data['merchant'] = MerchantProfile::count();
        $data['artist'] = ArtistProfile::count();
        $data['event'] = ArtistEvent::where('event_date', '>=', date('Y-m-d'))->count();
        $data['item'] = Item::count();

        $data['all_events'] = ArtistEvent::with('artist')->orderBy('event_date', 'desc')->orderBy('event_time', 'DESC')->get();
        $data['all_items'] = Item::orderBy('created_at', 'desc')->get();
        $data['all_merchant'] = MerchantProfile::with('account')->orderBy('created_at', 'desc')->get();
        $data['all_artist'] = ArtistProfile::with('account')
                                    ->with(['artist_one_specialty' => function($query){
                                        $query->select()
                                        ->with(['specialty' => function($query){
                                            $query->select();
                                        }]);
                                    }])
                                    ->orderBy('created_at', 'desc')
                                    ->get();

        return view('Admin::dashboard.index', $data);
    }

    public function artists() 
    {
        $data['all_artist'] = ArtistProfile::with('account')
                                    ->with(['artist_one_specialty' => function($query){
                                        $query->select()
                                        ->with(['specialty' => function($query){
                                            $query->select();
                                        }]);
                                    }])
                                    //->where('artist_profile.is_featured', 0)
                                    ->orderBy('created_at', 'desc')
                                    ->get();

        $data['feat_artist'] = ArtistProfile::with('account')
                                    ->with(['artist_one_specialty' => function($query){
                                        $query->select()
                                        ->with(['specialty' => function($query){
                                            $query->select();
                                        }]);
                                    }])
                                    ->where('artist_profile.is_featured', 1)
                                    ->orderBy('created_at', 'desc')
                                    ->get();

        return view('Admin::artist.index', $data);
    }
    public function events() 
    {      
        $data['upcoming'] = ArtistEvent::with('artist')->where('artist_events.event_date', '>', date('Y-m-d'))->orderBy('artist_events.event_date', 'desc')->orderBy('artist_events.event_time', 'DESC')->get();
        $data['all_event'] = ArtistEvent::with('artist')->orderBy('artist_events.event_date', 'desc')->orderBy('artist_events.event_time', 'DESC')->get();

        return view('Admin::event.index', $data); // Tab List
    }
    public function event() {
        return view('Admin::event.event'); // Single event edit
    }
    public function reviews() 
    {   
        $data['review'] = Review::with('artist_profile')->where('artist_profile_id', '!=', NULL)->get();
        return view('Admin::review.artist.index', $data); // Artist Reviews
    }
    public function merchants() 
    {
        $data['featured_merchants'] = MerchantProfile::with('account')->where('is_featured', '=', 1)->orderBy('created_at', 'desc')->get();
        $data['all_merchants'] = MerchantProfile::with('account')->orderBy('created_at', 'desc')->get();

        return view('Admin::merchant.index', $data); // merchant tab list
    }
    public function items() 
    {      
        $data['recent'] = Item::with('merchant')->orderBy('created_at', 'desc')->limit(5)->get();
        $data['items'] = Item::with('merchant')->orderBy('created_at', 'desc')->get();

        return view('Admin::item.index', $data); // item Tab List
    }
    public function item() {
        return view('Admin::item.item'); // single item edit view
    }
    public function feedbacks() 
    {   
        $data['feedback'] = Review::with('merchant_profile')->where('merchant_profile_id', '!=', NULL)->get();
        return view('Admin::review.merchant.index', $data); // Merchant Feedbacks
    }

    public function delete_review($id)
    {  
        $reviewDelete = Review::where('review_id', $id)->delete();
        if ($reviewDelete) {
            return json_encode(array('result' => 'success', 'message' => 'Review Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

    public function updates() 
    {   
        $data['updates'] = Update::orderBy('update_expire', 'desc')->get();
        $data['recent_updates'] = Update::orderBy('created_at', 'desc')->limit(3)->get();

        return view('Admin::setting.update.index', $data);
    }

    public function create_updates(Request $request) 
    {   
        $data = ['update_title' => $request->update_title, 'update_desc' => $request->update_desc, 'update_expire' => date_format(date_create($request->update_expire), 'Y-m-d')];

        $save = Update::create($data);

        if($save){
            return json_encode(array('result' => 'success', 'message' => 'Updates Successfully Created.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while saving. Please try again later.'));
    }

    public function edit_updates($id, Request $request) 
    {   
        $data = ['update_title' => $request->update_title, 'update_desc' => $request->update_desc, 'update_expire' => date_format(date_create($request->update_expire), 'Y-m-d')];

        $save = Update::where('update_id', $id)->update($data);

        if($save){
            return json_encode(array('result' => 'success', 'message' => 'Updates Successfully Updated.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
    }

    public function update($id) 
    {   
        $data['update'] = Update::where('update_id', $id)->first();
        return view('Admin::setting.update.update', $data);
    }

    public function delete_updates($id)
    {  
        $updateDelete = Update::where('update_id', $id)->delete();
        if ($updateDelete) {
            return json_encode(array('result' => 'success', 'message' => 'Update Successfully Deleted.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while deleting. Please try again later.'));
    }

    /**
     * Change Password.
     *
     * @return Response
     */
    public function changePass(ChangePasswordRequest $request)
    {
        $id = Auth::user()->id;

        if(Auth::validate(['id' => $id, 'password' => $request->current_pass])){
            $newPassword = bcrypt($request->password);
            if(User::where('id', $id)->update(['password' => $newPassword])){
                return json_encode(array('result' => 'success', 'message' => 'You successfully changed your password'));
            }

            return json_decode(array('result' => 'error', 'message' => 'An error occured while updating your password'));
        }

        return json_encode(array('result' => 'error', 'message' => 'Current password does not match'));
    }
}
