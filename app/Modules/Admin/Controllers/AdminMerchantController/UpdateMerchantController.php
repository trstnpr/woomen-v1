<?php namespace App\Modules\Admin\Controllers\AdminMerchantController;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MerchantProfileRequest;

use Illuminate\Http\Request;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\App\Models\User;

use App\Services\ImageUploader;

class UpdateMerchantController extends Controller {

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function update($id, MerchantProfileRequest $request)
    {   
        /*$userUpdate = User::where('id', $id)->update(['email' => $request->email]);*/
        $profile = MerchantProfile::where('user_id', $id)->first();

        if ($request->hasFile('profile_image'))
            {       
                $imageUrl = $request->profile_image;
                $this->_validate_image_format($imageUrl->getClientOriginalExtension());

                $imageUploader = new ImageUploader;

                $result = $imageUploader->upload($imageUrl, $profile['merchant_profile_id'], 'profile_images');

                $data_profile = [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'middle_name' => $request->middle_name,
                    'address' => $request->address,
                    'facebook' => $request->facebook,
                    'profile_image' => $profile['merchant_profile_id'].'/'.$result
                ];

                $profileUpdate = MerchantProfile::where('user_id', $id)->update($data_profile);
            }

        else{
            $data_profile = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'middle_name' => $request->middle_name,
                'address' => $request->address,
                'facebook' => $request->facebook,
            ];

            $profileUpdate = MerchantProfile::where('user_id', $id)->update($data_profile);
        }

        if ($profileUpdate) {
            return json_encode(array('result' => 'success', 'message' => 'Merchant Profile Successfully Updated.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while updating. Please try again later.'));
    }

    /**
     * Feature Merchant
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function featured($id)
    {   
        $merchant_id = MerchantProfile::where('merchant_profile_id', $id)->update(['is_featured' => 1]);

        if($merchant_id){
            return json_encode(array('result' => 'success', 'message' => 'Merchant Profile Successfully Featured.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while featuring. Please try again later.'));
    }

    /**
     * Remove Merchant
     *
     * @param LoginRequest $login_request
     * @return view
     */
    public function remove($id)
    {   
        $merchant_id = MerchantProfile::where('merchant_profile_id', $id)->update(['is_featured' => 0]);

        if($merchant_id){
            return json_encode(array('result' => 'success', 'message' => 'Merchant Profile removed from being Featured.'));
        }

        return json_encode(array('result' => 'error', 'message' => 'There is an error occured while featuring. Please try again later.'));
    }

    /**
     * Validating image format
     *
     * @return string
     */
    private function _validate_image_format($file_extension)
    {
        if (!in_array($file_extension, array('gif', 'png', 'jpg', 'jpeg', 'PNG', 'JPG'))) {
            return redirect()->back()->with('error', 'Image invalid format.')->withInput();
        }
    }
}
