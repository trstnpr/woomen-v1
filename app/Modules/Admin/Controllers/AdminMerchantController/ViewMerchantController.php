<?php namespace App\Modules\Admin\Controllers\AdminMerchantController;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArtistProfileRequest;

use Illuminate\Http\Request;
use App\Modules\Merchant\Models\MerchantProfile;
use App\Modules\App\Models\User;

class ViewMerchantController extends Controller {

	/**
     * Authenticate admin credentials.
     *
     * @param LoginRequest $login_request
     * @return view
     */
	public function view_edit($id)
    {      
        $data['user'] = User::where('id', $id)->first();
        $data['merchant'] = MerchantProfile::where('user_id', $id)->first();

        return view("Admin::merchant.viewMerchant", $data);
    }

}
