<?php

Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers', 'middleware' => ['web']), function() {

    /*Route::resource('Admin', 'AdminController');*/

    /*Login Admin*/
    Route::get('admin', ['uses' => 'AdminController@index', 'as' => 'admin']);
    Route::post('admin/authenticate', ['uses' => 'AdminController@adminAuthenticate', 'as' => 'admin.login']);
    Route::get('admin/logout', ['uses' => 'AdminController@adminLogout', 'as' => 'admin.logout']);
    
});	

/*----------------DASHBOARD----------------*/
Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers\AdminDashboardController', 'middleware' => ['web', 'auth', 'admin']), function() {

    /*Store Merchant*/
    Route::get('admin/dashboard', ['uses' => 'DashboardController@dashboard', 'as' => 'admin.dashboard']);

    Route::get('admin/artists', ['uses' => 'DashboardController@artists', 'as' => 'admin.artists']);
    Route::get('admin/artist/events', ['uses' => 'DashboardController@events', 'as' => 'admin.artist.events']);
    Route::get('admin/artist/event/id', ['uses' => 'DashboardController@event', 'as' => 'admin.artist.event.id']);
    Route::get('admin/artist/reviews', ['uses' => 'DashboardController@reviews', 'as' => 'admin.artist.reviews']);
    Route::delete('admin/review/delete/{id}', ['uses' => 'DashboardController@delete_review', 'as' => 'admin.artist.reviews.delete']);
    
    Route::get('admin/merchants', ['uses' => 'DashboardController@merchants', 'as' => 'admin.merchants']);
    Route::get('admin/merchant/items', ['uses' => 'DashboardController@items', 'as' => 'admin.merchant.items']);
    Route::get('admin/merchant/items/id', ['uses' => 'DashboardController@item', 'as' => 'admin.merchant.item.id']);
    Route::get('admin/merchant/feedbacks', ['uses' => 'DashboardController@feedbacks', 'as' => 'admin.merchant.feedbacks']);
    Route::get('admin/settings/updates', ['uses' => 'DashboardController@updates', 'as' => 'admin.setting.updates']); // Updates
    Route::post('admin/settings/updates/create', ['uses' => 'DashboardController@create_updates', 'as' => 'admin.setting.create.updates']); // store
    Route::get('admin/settings/updates/{id}', ['uses' => 'DashboardController@update', 'as' => 'admin.setting.updates.id']); // Updates CRUD
    Route::post('admin/settings/updates/edit/{id}', ['uses' => 'DashboardController@edit_updates', 'as' => 'admin.setting.edit.updates']); // Updates
    Route::delete('admin/settings/updates/delete/{id}', ['uses' => 'DashboardController@delete_updates', 'as' => 'admin.setting.delete.updates']); // Updates

    Route::post('admin/changePassword', ['uses' => 'DashboardController@changePass', 'as' => 'admin.change.password']);

    // MIGRATE ALL UNRELATED ROUTES TO THEIR CONTROLLERS AFTER 
});

/*----------------MERCHANT----------------*/
Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers\AdminMerchantController', 'middleware' => ['web', 'auth', 'admin']), function() {

    /*Store Merchant*/
    Route::post('admin/merchant/store', ['uses' => 'CreateMerchantController@store', 'as' => 'admin.merchant.create']);

    /*Update Merchant*/
    Route::post('admin/merchant/update/{id}', ['uses' => 'UpdateMerchantController@update', 'as' => 'admin.merchant.update']);

    /*Delete Merchant*/
    Route::delete('admin/merchant/delete/{id}', ['uses' => 'DeleteMerchantController@delete', 'as' => 'admin.merchant.delete']);
   
    /*View Edit Merchant*/
    Route::get('admin/merchant/view/{id}', ['uses' => 'ViewMerchantController@view_edit', 'as' => 'admin.merchant.view']);

    /*Feature Artist*/
    Route::post('admin/merchant/feat/{id}', ['uses' => 'UpdateMerchantController@featured', 'as' => 'admin.artists.feat']);

    /*Remove Artist*/
    Route::post('admin/merchant/remove/{id}', ['uses' => 'UpdateMerchantController@remove', 'as' => 'admin.artists.remove']);

});

/*----------------ITEM----------------*/
Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers\AdminItemController', 'middleware' => ['web', 'auth', 'admin']), function() {

    /*Store Merchant*/
    Route::post('admin/item/store', ['uses' => 'CreateItemController@store', 'as' => 'admin.item.create']);

    /*Update Merchant*/
    Route::post('admin/item/update/{id}', ['uses' => 'UpdateItemController@update', 'as' => 'admin.item.update']);

    /*Delete Merchant*/
    Route::delete('admin/item/delete/{id}', ['uses' => 'DeleteItemController@delete', 'as' => 'admin.item.delete']);
    
     /*View Edit Item*/
    Route::get('admin/item/view/{id}', ['uses' => 'UpdateItemController@edit', 'as' => 'admin.item.view']);
});

/*----------------ARTIST----------------*/
Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers\AdminArtistController', 'middleware' => ['web', 'auth', 'admin']), function() {

    /*Store Artist*/
    Route::post('admin/artist/store', ['uses' => 'CreateArtistController@store', 'as' => 'admin.artist.create']);

    /*Upgrade Artist*/
    Route::post('admin/artist/upgrade/{id}', ['uses' => 'UpdateArtistController@upgrade', 'as' => 'admin.artist.upgrade']);

    /*Update Artist*/
    Route::post('admin/artist/update/{id}', ['uses' => 'UpdateArtistController@update', 'as' => 'admin.artist.update']);

    /*Delete Artist*/
    Route::delete('admin/artist/delete/{id}', ['uses' => 'DeleteArtistController@delete', 'as' => 'admin.artist.delete']);

    /*Activate Artist*/
    Route::post('admin/artist/activate/{id}', ['uses' => 'UpdateArtistController@activate', 'as' => 'admin.artist.activate']);

    /*View Edit Artist*/
    Route::get('admin/artist/view/{id}', ['uses' => 'UpdateArtistController@edit', 'as' => 'admin.artist.view']);

    /*Feature Artist*/
    Route::post('admin/artists/feat/{id}', ['uses' => 'UpdateArtistController@featured', 'as' => 'admin.artists.feat']);

    /*Remove Artist*/
    Route::post('admin/artists/remove/{id}', ['uses' => 'UpdateArtistController@remove', 'as' => 'admin.artists.remove']);

    /*Add Package*/
    Route::post('admin/artists/package', ['uses' => 'UpdateArtistController@updatePackage', 'as' => 'artist.packagelist.update']);

    /*View Album*/
    Route::get('admin/artist/album/{id}', ['uses' => 'ViewArtistController@album', 'as' => 'admin.album.view']);

    /*Delete Album*/
    Route::delete('admin/album/delete/{id}', ['uses' => 'ViewArtistController@delete_album', 'as' => 'admin.album.delete']);

    /*Delete Video*/
    Route::delete('admin/video/delete/{id}', ['uses' => 'ViewArtistController@delete_video', 'as' => 'admin.video.delete']);
    
});

/*----------------Events----------------*/
Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers\AdminEventController', 'middleware' => ['web', 'auth', 'admin']), function() {

    /*Store Merchant*/
    Route::post('admin/event/store', ['uses' => 'CreateEventController@store', 'as' => 'admin.event.create']);

    /*Update Merchant*/
    Route::post('admin/event/update/{id}', ['uses' => 'UpdateEventController@update', 'as' => 'admin.event.update']);

    /*Delete Merchant*/
    Route::delete('admin/event/delete/{id}', ['uses' => 'DeleteEventController@delete', 'as' => 'admin.event.delete']);

    /*View Edit Event*/
    Route::get('admin/event/view/{id}', ['uses' => 'UpdateEventController@edit', 'as' => 'admin.event.view']);
    
});