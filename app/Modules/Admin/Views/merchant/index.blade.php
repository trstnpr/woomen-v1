@extends('adminMaster')

@section('title')
	<title>Woomen - Merchants</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="adminItem-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Merchants</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Merchant Management</li>
					<li class="active">Merchants</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				<div class="recent-item">
					<div class="section-title">
						<h3>Featured Merchants</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Merchant</th>
					                <!-- <th>Rating</th> -->
					                <th>Email</th>
					                <th>Date Joined</th>
					                <th>Status</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	<?php $ctr = 1; ?>
					         	@foreach($featured_merchants as $fm)
					            <tr>
					                <td>{{ $ctr }}</td>
					                <td><a href="/admin/merchant/view/{{ $fm->user_id }}">{{ $fm->first_name }} {{ $fm->last_name }}</a></td>
					                <!-- <td>$rating</td> -->
					                <td>{{ $fm->account['email'] }}</td>
					                <td>{{ date_format(date_create($fm->created_at), "M d, Y") }} {{ date_format(date_create($fm->created_at), "g:i A") }}</td>
					                @if($fm->account['status'] == 1)
					                <td>Active</td>
					                @else
					                <td>Not Active</td>
					                @endif
					                <td>
					                	<button type="button" class="btn btn-warning btn-xs" title="Remove" onclick="removeMerchant({{ $fm->merchant_profile_id }})">Remove</button>
					                </td>
					            </tr>
					            <?php $ctr++; ?>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

				<div class="recent-item">
					<div class="section-title">
						<h3>Merchants</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Merchant</th>
					                <!-- <th>Rating</th> -->
					                <th>Email</th>
					                <th>Date Joined</th>
					                <th>Status</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	<?php $ctr = 1; ?>
					         	@foreach($all_merchants as $am)
					            <tr>
					                <td>{{ $ctr }}</td>
					                <td><a href="/admin/merchant/view/{{ $am->user_id }}">{{ $am->first_name }} {{ $am->last_name }}</a></td>
					                <!-- <td>$rating</td> -->
					                <td>{{ $am->account['email'] }}</td>
					                <td>{{ date_format(date_create($am->created_at), "M d, Y") }} {{ date_format(date_create($am->created_at), "g:i A") }}</td>
					                @if($am->account['status'] == 1)
					                <td>Active</td>
					                @else
					                <td>Not Active</td>
					                @endif
					                <td>
					                	@if($am->is_featured == 0)
					                	<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Feature Merchant" onclick="featMerchant({{ $am->merchant_profile_id }})">Feature</button>
					                	@else
					                	<button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" title="Featured">Featured</button>
					                	@endif
					                	<a role="button" href="/admin/merchant/view/{{ $am->user_id }}" class="btn btn-primary btn-xs" title="View/Edit">View/Edit</a>
					                	<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-merchant" onclick="deleteMerchant(this)" data-merchant-id="{{$am->user_id}}">Delete</button>
					                </td>
					            </tr>
					            <?php $ctr++; ?>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

				<!-- modal confirm delete make up artist-->
				<div class="modal fade woo-modal" id="delete-merchant">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header gold">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Please Confirm</h4>
							</div>
							<div class="modal-body">
								By taking this action, you will not retrieve the data anymore.
								<strong>Are you sure you want to delete this merchant?</strong>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
								<a href="#!" type="button" id="confirm-button-merchant" data-url="#" class="btn woo-btn-button">Yes, delete</a>
							</div>
						</div>
					</div>
				</div>	

			</div>
		</section>
	</div>

	


@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});

		function deleteMerchant(merchant) {
	        $('#confirm-button-merchant').attr('data-merchant-id', $(merchant).attr('data-merchant-id'));
	    }

	    function featMerchant(merchant) {
	        $.ajax({
	            url: '/admin/merchant/feat/'+merchant,
	            headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            type: 'POST',
	            success: function(data) {
	                var msg = JSON.parse(data);
	                if(msg.result == 'success'){
	                    alert(msg.message);
	                    location.reload();
	                } else{
	                    alert(msg.message);
	                    location.reload();
	                }
	            }
	        });
	    }

	    function removeMerchant(merchant) {
	        $.ajax({
	            url: '/admin/merchant/remove/'+merchant,
	            headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            type: 'POST',
	            success: function(data) {
	                var msg = JSON.parse(data);
	                if(msg.result == 'success'){
	                    alert(msg.message);
	                    location.reload();
	                } else{
	                    alert(msg.message);
	                    location.reload();
	                }
	            }
	        });
	    }
	</script>

@stop