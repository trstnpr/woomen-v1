@extends('adminMaster')

@section('title')
	<title>Woomen - Edit Merchant</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/merchant/index.css') }}" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" />
@stop

@section('content')

<div class="viewMerchant-content">
	<section class="section-content">
		<div class="container-fluid">
			<h2>{{ ucwords($user['merchant_profile']['first_name']) .' '. ucwords($user['merchant_profile']['last_name']) }}</h2>
			<ol class="breadcrumb">
				<li class="active">Admin</li>
				<li class="active">Merchant Management</li>
				<li class="active">Merchants</li>
				<li class="active">{{ ucwords($user['merchant_profile']['first_name']) .' '. ucwords($user['merchant_profile']['last_name']) }}</li>
			</ol>
		</div>
	</section>

	<section class="section-content">
		<div class="container-fluid">
			<div class="card-panel">			
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 option-buttons">
							<a href="{{ url("admin/merchants") }}">
								<button class="btn-md btn-primary btn"><i class="fa fa-list"></i>See all Merchants</button>
							</a>	
						</div>
						<div class="col-md-6">
							<div class="time-updated alert alert-warning">
								<i class="fa fa-clock-o"></i> Updated last: {{ $user['updated_at']}}					
							</div>
						</div>
						<div class='col-md-12'>
							<hr/>
						</div>
						<form method="POST" id="merchant-form" action="">
							<input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
							<div class="col-md-2 left-content">
								<div class="form-group">
									<label>Profile Image</label>
									<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $user['merchant_profile']['profile_image'] }}');" class='user-profile-image'>
									</figure>
									<div class="well">
										<button type="button" id="trigger-browse-file"><i class="fa fa-photo"></i> Choose File</button>
										<input type="file" class="" name="profile_image" id="profile_image" onchange="readUrl(this);" />
									</div>
									<div class="file-wrap"></div>
								</div>
							</div>
							<div class="col-md-10 right-content">
								<div class="col-md-4">
									<div class="form-group">
										<label>First Name</label>
										<input type="text" class="form-control input-lg" name="first_name" id="first_name" placeholder="Merchant first name" value="{{ $merchant['first_name'] }}" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Middle Name</label>
										<input type="text" class="form-control input-lg" name="middle_name" id="middle_name" placeholder="Merchant middle name" value="{{ $merchant['middle_name'] }}" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Last Name</label>
										<input type="text" class="form-control input-lg" name="last_name" id="last_name" placeholder="Merchant last name" value="{{ $merchant['last_name'] }}" />
									</div>
								</div>
								<div class="col-md-12">
										<div class="form-group">
											<label>Facebook</label>
											<input type="text" class="form-control input-lg" name="facebook" id="facebook" placeholder="Merchant facebook profile URL" value="{{ $merchant['facebook'] }}" />
										</div>
									</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Address</label>
										<input type="text" class="form-control input-lg" name="address" id="address" placeholder="Merchant address" value="{{ $merchant['address'] }}" />
									</div>
								</div>

								<div class="col-md-12 alert alert-success hide" id="result_div">
				  					<ul class="fa-ul">
				  						<li><i class="fa fa-li fa-check"></i> Success!</li>
				  					</ul>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<button type="submit" id="merchant-button" class="btn btn-lg woo-btn-submit">Save Changes</button>
									</div>
								</div>
							</div>
						</form>				
					</div>
				</div>	
			</div>
		</div>
	</section>
</div>

<input type="hidden" value="{{ $user['id'] }}" id="merchant_id">

@stop()

@section('custom-scripts')
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
	//choose file button
		$(document).ready(function(){
		    $("#trigger-browse-file").click(function(){
		        $('#profile_image').trigger('click'); 
		    });
		});


		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function(e) {
	                $('.user-profile-image').css('background-image', 'url(' + e.target.result + ')');
	            };

	            reader.readAsDataURL(input.files[0]);
	        }
		 }       

		$(document).ready(function(){
		    $("#profile_image").change(function(){
		        readURL(this);
		    });
		});
	</script>

@stop
