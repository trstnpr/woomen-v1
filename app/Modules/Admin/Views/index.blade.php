@extends('appMaster')

@section('title')
	<title>Woomen - Admin</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/app/views/index.css') }}" rel="stylesheet" />
	<style type="text/css">
		* {
			font-family: Arial, Helvetica, sans-serif;
		}
		body {
			background-image:url('{{ asset('images/adminbg.png') }}');
		}
	</style>
@stop

@section('content')

	<div class="adminLogin-content">

		<section class="section-admin">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
						<div class="admin-login">
							<a href="{{ url('/') }}">
								<img src="{{ asset('images/brand/logo%20gold%20earth.png') }}" class="img-responsive brand-img">
							</a>
							<br/>
							<div class="panel panel-default">
								<div class="panel-heading gold">Admin</div>
								<div class="panel-body">
									<form id="frm_admin_login">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="alert alert-success hide" id="result_d">
											<i class="fa fa-li fa-check"></i> Success
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="email" class="form-control input-lg" name="email" id="email" placeholder="Email Address" />
										</div>
										<div class="form-group">
											<label>Password</label>
											<input type="password" class="form-control input-lg" name="password" id="password" placeholder="Password" />
										</div>
										<div class="form-group">
										<button type="submit" class="btn woo-btn btn-block btn-lg" id="btn_admin">Login</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/app/views/index.js') }}"></script>
	<script type="text/javascript"  src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script type="text/javascript"></script>

@stop
