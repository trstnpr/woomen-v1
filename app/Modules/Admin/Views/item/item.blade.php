@extends('adminMaster')

@section('title')
	<title>Woomen - Item</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="adminItem-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">{{ $item['item_name']}}</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Merchant Management</li>
					<li class="active">Merchant Management</li>
					<li class="active">{{ $item['item_name']}}</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="card-panel">
					<form id="item-form">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Choose Images</label>
								<div class="well">
									<input type="file" name="image_path[]" id="files" multiple="multiple" />
								</div>
								<button type="button" class="btn btn-warning btn-xs" id="btn-clear">Clear</button>
								<div class="file-wrap">
									<div class="row" id="item-container"></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Item Name</label>
								<input type="text" class="form-control input-lg" name="item_name" id="item_name" placeholder="Item Name" value="{{ $item['item_name']}}" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Item Price</label>
							   	<div class="input-group">
							     	 <div class="input-group-addon">₱</div>
							      	<input type="text" class="form-control input-lg" id="exampleInputAmount" name="item_price" placeholder="Amount" value="{{ $item['item_price']}}">
							    </div>
							</div>  
						</div>
						<div class="col-md-6">
							<div class="fomr-group">
								<label>Item Quantity</label>
								<input type="text" class="form-control input-lg" name="item_quantity" id="item_quantity" placeholder="Item Quantity" value="{{ $item['item_quantity']}}" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Item Contact</label>
								<input type="text" class="form-control input-lg" name="item_contact" id="item_contact" placeholder="Item Contact" value="{{ $item['item_contact']}}" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Item Description</label>
								<textarea class="form-control wysiwyg" rows="3" name="item_description" id="item_description" placeholder="Item Description" >{!! $item['item_description'] !!}</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="alert alert-success hide" id="result_div">
								<ul class="fa-ul">
									<li><i class="fa fa-li fa-check"></i> Success</li>
								</ul>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn btn-lg woo-btn-submit" id="btn-item">Save Changes</button>
							</div>
						</div>
					</div>
					</form>
				</div>

			</div>
		</section>
	</div>

	<input type="hidden" value="{{ $item['item_id'] }}" id="item_id">

	<!-- modal confirm delete make up artist-->
	<div class="modal fade woo-modal" id="delete-item">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this item?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		
	</script>
@stop