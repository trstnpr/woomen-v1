@extends('adminMaster')

@section('title')
	<title>Woomen - Items</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="adminItem-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Items</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Merchant Management</li>
					<li class="active">Items</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				<div class="recent-item">
					<div class="section-title">
						<h3>Recent Items</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Item</th>
					                <th>Price</th>
					                <th>Quantity</th>
					                <th>Seller</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					        <tbody>
					         	@foreach($recent as $r)
					            <tr>
					            	<td>{{ $r->item_id }}</td>
					                <td><a href="/admin/item/view/{{ $r->item_id }}">{{ $r->item_name }}</a></td>
					                <td>PhP {{ $r->item_price }}</td>
					                <td>{{ $r->item_quantity }}</td>
					                <td>
					                	@if($r->merchant)
					                	<a href="#">{{ $r->merchant['first_name'] }} {{ $r->merchant['last_name'] }}</a>
					                	@else
					                	<a href="#">{{ $r->artist['first_name'] }} {{ $r->artist['last_name'] }}</a>
					                	@endif
					                </td>
					                <td>
					                	<a role="button" href="/admin/item/view/{{ $r->item_id }}" class="btn btn-primary btn-xs" title="View/Edit">View/Edit</a>
					                	<a role="button" href="#delete-item" data-toggle="modal" class="btn btn-danger btn-xs" title="Delete" onclick="deleteItem(this)" data-item-id="{{$r->item_id}}">Delete</a>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

				<div class="item">
					<div class="section-title">
						<h3>All Items</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Item</th>
					                <th>Price</th>
					                <th>Quantity</th>
					                <th>Seller</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					            @foreach($items as $item)
					            <tr>
					            	<td>{{ $item->item_id }}</td>
					                <td><a href="/admin/item/view/{{ $item->item_id }}">{{ $item->item_name }}</a></td>
					                <td>PhP {{ $item->item_price }}</td>
					                <td>{{ $item->item_quantity }}</td>
					                <td>
					                	@if($item->merchant)
					                	<a href="#">{{ $item->merchant['first_name'] }} {{ $item->merchant['last_name'] }}</a>
					                	@else
					                	<a href="#">{{ $item->artist['first_name'] }} {{ $item->artist['last_name'] }}</a>
					                	@endif
					                </td>
					                <td>
					                	<a role="button" href="/admin/item/view/{{ $item->item_id }}" class="btn btn-primary btn-xs" title="View/Edit">View/Edit</a>
					                	<a role="button" href="#delete-item" data-toggle="modal" class="btn btn-danger btn-xs" title="Delete" onclick="deleteItem(this)" data-item-id="{{$item->item_id}}">Delete</a>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

				<!-- modal confirm delete make up artist-->
				<div class="modal fade woo-modal" id="delete-item">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header gold">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Please Confirm</h4>
							</div>
							<div class="modal-body">
								By taking this action, you will not retrieve the data anymore.
								<strong>Are you sure you want to delete this item?</strong>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
								<a href="#!" type="button" id="confirm-button-item" data-url="#" class="btn woo-btn-button">Yes, delete</a>
							</div>
						</div>
					</div>
				</div>	

			</div>
		</section>
	</div>

	


@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		function deleteItem(item) {
	        $('#confirm-button-item').attr('data-item-id', $(item).attr('data-item-id'));
	    }

	</script>

@stop