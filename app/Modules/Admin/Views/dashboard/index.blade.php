@extends('adminMaster')

@section('title')
	<title>Woomen - Admin Dashboard</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="adminDashboard-content">

		<section class="section-content">
			<div class="container-fluid">
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Welcome Admin!</strong> You can now see what's going on.
				</div>

				<div class="stat-board">
					<div class="row">
						<a href="/admin/artists">
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="stat-panel gold">
									<div class="stat-count">
										<span>{{ $artist }}</span>
									</div>
									<div class="stat-label">
										Makeup Artists
									</div>
								</div>
							</div>
						</a>

						<a href="/admin/artist/events">
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="stat-panel default">
									<div class="stat-count">
										<span>{{ $event }}</span>
									</div>
									<div class="stat-label">
										Events
									</div>
								</div>
							</div>
						</a>

						<a href="/admin/merchants">
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="stat-panel gold">
									<div class="stat-count">
										<span>{{ $merchant }}</span>
									</div>
									<div class="stat-label">
										Merchants
									</div>
								</div>
							</div>
						</a>

						<a href="/admin/merchant/items">
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="stat-panel default">
									<div class="stat-count">
										<span>{{ $item }}</span>
									</div>
									<div class="stat-label">
										Merchandise
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="makeup-artist">
					<div class="section-title">
						<h3>Makeup Artists</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Artist</th>
					                <th>Forte</th>
					                <!-- <th>Rating</th> -->
					                <th>Email</th>
					                <th>Date Joined</th>
					                <th>Status</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	@foreach($all_artist as $a)
					            <tr>
					            	<td>{{ $a->artist_profile_id }}</td>
					                <td><a href="/admin/artist/view/{{ $a->user_id }}">{{ $a->first_name }} {{ $a->last_name }}</a></td>
					                <td>{{ $a->artist_one_specialty['specialty']['specialty_name'] }}</td>
					                <!-- <td>$rating</td> -->
					                <td>{{ $a->account['email'] }}</td>
					                <td>{{ date_format(date_create($a->created_at), "M d, Y") }}</td>
						            <td>
						                @if($a->account['status'] == 1)
						                <button class="btn btn-primary activate-artist btn-xs" data-artist-id="{{ $a->user_id }}" data-status="0">Active</button>
						                @else
						                <button class="btn btn-danger activate-artist btn-xs" data-artist-id="{{ $a->user_id }}" data-status="1">Not Active</button>
						                @endif
						            </td>
					                <td>
					                	<a href="/admin/artist/view/{{ $a->user_id }}">
					                		<button type="button" class="btn btn-primary btn-xs" title="View">View / Edit</button>
					                	</a>	
					                	<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-makeup-artist"
						                        onclick="deleteArtist(this)" data-artist-id="{{ $a->user_id }}">Delete</button>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
						<p style="color: red"><button class="btn btn-primary btn-xs">Active</button> - means the Artist is <b>Active</b> and pressing the button will <b>Deactivate</b> it.</p>
						<p style="color: red"><button class="btn btn-danger btn-xs">Not Active</button> - means the Artist is <b>Not Active</b> and pressing the button will <b>Activate</b> it.</p>
					</div>
				</div>

				<div class="merchant">
					<div class="section-title">
						<h3>Merchants</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Merchant</th>
					                <!-- <th>Rating</th> -->
					                <th>Email</th>
					                <th>Date Joined</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	@foreach($all_merchant as $m)
					            <tr>
					            	<td>{{ $m->merchant_profile_id }}</td>
					                <td><a href="/admin/merchant/view/{{ $m->user_id }}">{{ $m->first_name }} {{ $m->last_name }}</a></td>
					                <!-- <td>$rating</td> -->
					                <td>{{ $m->account['email'] }}</td>
					                <td>{{ date_format(date_create($m->created_at), "M d, Y") }}</td>
					                <td>
					                	<a href="/admin/merchant/view/{{ $m->user_id }}">
					                		<button type="button" class="btn btn-primary btn-xs" title="View">View / Edit</button>
					                	</a>
					                	<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-merchant"
						                        onclick="deleteMerchant(this)" data-merchant-id="{{ $m->user_id }}">Delete</button>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

				<div class="events">
					<div class="section-title">
						<h3>Events</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Event</th>
					                <th>Venue</th>
					                <th>Date & Time</th>
					                <th>Posted by</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	@foreach($all_events as $e)
					            <tr>
					            	<td>{{ $e->artist_event_id }}</td>
					                <td><a href="/admin/event/view/{{ $e->artist_event_id }}">{{ $e->event_title }}</a></td>
					                <td>{{ $e->event_venue }}</td>
					                <td>{{ date_format(date_create($e->event_date), "M d, Y") }} - {{ date_format(date_create($e->event_time), "g:i A") }}</td>
					                <td>{{ $e->artist['first_name'] }} {{ $e->artist['last_name'] }}</td>
					                <td>
					                	<a href="/admin/event/view/{{ $e->artist_event_id }}">
					                		<button type="button" class="btn btn-primary btn-xs" title="View">View / Edit</button>
					                	</a>
					                	<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-event"
						                        onclick="deleteEvent(this)" data-event-id="{{ $e->artist_event_id }}">Delete</button>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

				<div class="merchandise">
					<div class="section-title">
						<h3>Merchandise</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Item</th>
					                <th>Price</th>
					                <th>Quantity</th>
					                <th>Contact</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	@foreach($all_items as $i)
					            <tr>
					            	<td>{{ $i->item_id }}</td>
					                <td><a href="/admin/item/view/{{ $i->item_id }}">{{ $i->item_name }}</a></td>
					                <td>P {{ $i->item_price }}</td>
					                <td>{{ $i->item_quantity }}</td>
					                <td>{{ $i->item_contact }}</td>
					                <td>
					                	<a href="/admin/item/view/{{ $i->item_id }}">
					                		<button type="button" class="btn btn-primary btn-xs" title="View">View / Edit</button>
					                	</a>
										<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-item"
						                        onclick="deleteItem(this)" data-item-id="{{ $i->item_id }}">Delete</button>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

			</div>
		</section>
	</div>

	<!-- MODALS -->

	<!-- CONFIRM DELETE MODALS-->

	<!-- modal confirm delete make up artist-->
	 <div class="modal fade woo-modal" id="delete-makeup-artist">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this artist?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-artist" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	


	<!-- modal confirm delete merchant -->
	  <div class="modal fade woo-modal" id="delete-merchant">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this merchant?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-merchant" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

	<!-- modal confirm delete events -->
	  <div class="modal fade woo-modal" id="delete-event">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this event?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-event" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

	<!-- modal confirm delete item -->
	  <div class="modal fade woo-modal" id="delete-item">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this item?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-item" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		function deleteArtist(artist) {
	        $('#confirm-button-artist').attr('data-artist-id', $(artist).attr('data-artist-id'));
	    }

	    function deleteMerchant(merchant) {
	        $('#confirm-button-merchant').attr('data-merchant-id', $(merchant).attr('data-merchant-id'));
	    }

	    function deleteEvent(event) {
	        $('#confirm-button-event').attr('data-event-id', $(event).attr('data-event-id'));
	    }

	    function deleteItem(item) {
	        $('#confirm-button-item').attr('data-item-id', $(item).attr('data-item-id'));
	    }
	</script>

@stop