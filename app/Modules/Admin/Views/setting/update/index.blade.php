@extends('adminMaster')

@section('title')
	<title>Woomen - Admin Updates</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="adminUpdates-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Admin Updates</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Settings</li>
					<li class="active">Admin Updates</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				<div class="row">
					<div class="col-md-4">
						<div class="recent-updates">
							<h3>Recent Updates</h3>
							<div class="card-panel">
								@foreach($recent_updates as $ru)
								<div class="media update-item">
									<div class="media-body">
										<h4 class="media-heading">{{ $ru->update_title}} <small class="pull-right">will expire on {{ date_format(date_create($ru->update_expire), "M d, Y") }}</small></h4>
										{{ str_limit($ru->update_desc, 130) }}
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="all-updates">
							<h3>Recent Updates</h3>
							<div class="card-panel">
								<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
									<thead>
							            <tr>
							            	<th>#</th>
							                <th>Title</th>
							                <th>Expiration</th>
							                <th>Action</th>
							            </tr>
							        </thead>
							        <tbody>
							        	@foreach($updates as $u)
							        	<tr>
							            	<td>{{ $u->update_id }}</td>
							                <td>{{ $u->update_title }}</td>
							                <td>{{ date_format(date_create($u->update_expire), "M d, Y") }}</td>
							                <td>
							                	<a href="/admin/settings/updates/{{ $u->update_id }}" role="button" class="btn btn-xs btn-primary">View/Edit</a>
							                	<a role="button" class="btn btn-danger btn-xs" data-target="#delete-update" data-toggle="modal" onclick="deleteUpdates(this)" data-update-id="{{$u->update_id}}">Delete</a>
							                </td>
							            </tr>
							            @endforeach
							        </tbody>
							    </table>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-updates">
					<h3>Post an Update</h3>
					<div class="card-panel">
						<form id="frm_update">
							<input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
							<div class="form-group">
								<input type="text" class="form-control input-lg" name="update_title" placeholder="Title" required />
							</div>
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control input-lg" rows="5" name="update_desc" placeholder="Write someting ..." required></textarea>
							</div>
							<div class="form-group">
								<label>Expiration</label>
								<div class="input-group datePicker">
				                    <input type="text" class="form-control input-lg" name="update_expire" id="expiration" placeholder="Expiration"  required/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
							</div>
							<div class="col-md-12 alert alert-success hide" id="result_div">
				  				<ul class="fa-ul">
				  					<li><i class="fa fa-li fa-check"></i> Success!</li>
				  				</ul>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-lg woo-btn-submit" id="btn_update">Submit</button>
							</div>
						</form>
					</div>
				</div>

			</div>
		</section>

	</div>

	<!-- modal confirm delete make up artist-->
	 <div class="modal fade woo-modal" id="delete-update">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this artist?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-update" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		function deleteUpdates(update) {
	        $('#confirm-button-update').attr('data-update-id', $(update).attr('data-update-id'));
	    }
	</script>
@stop