@extends('adminMaster')

@section('title')
	<title>Woomen - Admin Updates</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" />
@stop

@section('content')

	<div class="adminUpdates-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">{{ $update['update_title'] }}</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Settings</li>
					<li class="active">Admin Updates</li>
					<li class="active">{{ $update['update_title'] }}</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="form-updates">
					<h3>Edit an Update</h3>
					<div class="card-panel">
						<form id="update-update-frm">
							<input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
							<input type="hidden" name="update_id" id="update_id" value="{{ $update['update_id'] }}" />
							<div class="form-group">
								<input type="text" class="form-control input-lg" name="update_title" placeholder="Title" value="{{ $update['update_title'] }}" required />
							</div>
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control input-lg" rows="5" name="update_desc" placeholder="Write someting ..." required> {{ $update['update_desc'] }} </textarea>
							</div>
							<div class="form-group">
								<label>Expiration</label>
								<div class="input-group datePicker">
				                    <input type="text" class="form-control input-lg" name="update_expire" id="expiration" placeholder="Expiration" value="{{ date_format(date_create($update['update_expire']), 'M/d/Y') }}" required/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
							</div>

							<div class="col-md-12 alert alert-success hide" id="result_div">
				  				<ul class="fa-ul">
				  					<li><i class="fa fa-li fa-check"></i> Success!</li>
				  				</ul>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-lg woo-btn-submit" btn="update-btn">Save Changes</button>
							</div>
						</form>
					</div>
				</div>

			</div>
		</section>

	</div>

@stop()

@section('custom-scripts')
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});
	</script>
@stop