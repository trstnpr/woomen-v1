@extends('adminMaster')

@section('title')
	<title>Woomen - Artist Reviews</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="adminReview-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Reviews</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Artist Management</li>
					<li class="active">Reviews</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="event">
					<div class="section-title">
						<h3>Artist Reviews</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Artist</th>
					                <th>Review Title</th>
					                <th>Review</th>
					                <th>Rating</th>
					                <th>Reviewer</th>
					                <th>Reviewer Email</th>
					                <th>Date Evaluated</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	@foreach($review as $rev)
					            <tr>
					            	<td>{{ $rev->review_id }}</td>
					                <td>{{ $rev->artist_profile['first_name'] }} {{ $rev->artist_profile['last_name'] }}</td>
					                <td>{{ $rev->review_title }}</td>
					                <td>{{ $rev->review_description }}</td>
					                <td>{{ $rev->review_rating }}</td>
					                <td>{{ $rev->review_name }}</td>
					                <td>{{ $rev->review_email }}</td>
					                <td>{{ date_format(date_create($rev->event_date), "M d, Y") }} {{ date_format(date_create($rev->event_time), "g:i A") }}</td>
					                <td>
					                	<a role="button" href="#delete-review" data-toggle="modal" class="btn btn-danger btn-xs" title="Delete" data-target="#delete-review" onclick="deleteReview(this)" data-review-id="{{$rev->review_id}}">Delete</a>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- modal confirm delete make up artist-->
	<div class="modal fade woo-modal" id="delete-review">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this review?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-review" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		function deleteReview(review) {
	        $('#confirm-button-review').attr('data-review-id', $(review).attr('data-review-id'));
	    }
	</script>

@stop
