@extends('adminMaster')

@section('title')
	<title>Woomen - Merchant Feedbacks</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" />
@stop

@section('content')

	<div class="adminReview-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Feedbacks</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Merchant Management</li>
					<li class="active">Feedbacks</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="event">
					<div class="section-title">
						<h3>Merchant Feedbacks</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Merchant</th>
					                <th>Feedback Title</th>
					                <th>Feedback</th>
					                <th>Rating</th>
					                <th>Reviewer</th>
					                <th>Reviewer Email</th>
					                <th>Date Evaluated</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					            @foreach($feedback as $feed)
					            <tr>
					            	<td>{{ $feed->review_id }}</td>
					                <td>{{ $feed->merchant_profile['first_name'] }} {{ $feed->merchant_profile['last_name'] }}</td>
					                <td>{{ $feed->review_title }}</td>
					                <td>{{ $feed->review_description }}</td>
					                <td>{{ $feed->review_rating }}</td>
					                <td>{{ $feed->review_name }}</td>
					                <td>{{ $feed->review_email }}</td>
					                <td>{{ date_format(date_create($feed->event_date), "M d, Y") }} {{ date_format(date_create($feed->event_time), "g:i A") }}</td>
					                <td>
					                	<a role="button" href="#delete-feedback" data-toggle="modal" class="btn btn-danger btn-xs" title="Delete" data-target="#delete-feedback" onclick="deleteReview(this)" data-review-id="{{$feed->review_id}}">Delete</a>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- modal confirm delete make up artist-->
	<div class="modal fade woo-modal" id="delete-feedback">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this review?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-review" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		function deleteReview(review) {
	        $('#confirm-button-review').attr('data-review-id', $(review).attr('data-review-id'));
	    }
	</script>

@stop
