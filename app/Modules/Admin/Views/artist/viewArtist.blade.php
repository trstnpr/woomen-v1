@extends('adminMaster')

@section('title')
	<title>Woomen - Edit Artist</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/artist/index.css') }}" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" />	
@stop

@section('content')

	<div class="viewArtist-content">
		<section class="section-content">
			<div class="container-fluid">
				<h2>{{ $user['artist_profile']['first_name'] .' '. $user['artist_profile']['last_name'] }}</h2>
				<ol class="breadcrumb">
				  <li class="active">Admin</li>
				  <li class="active">Artist Management</li>
				  <li class="active">Artists</li>
				  <li class="active">{{ $user['artist_profile']['first_name'] .' '. $user['artist_profile']['last_name'] }}</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				<div class="card-panel">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12 option-buttons">
								<a href="{{ url("admin/artists") }}">
									<button class="btn-md btn-primary btn">
										<i class="fa fa-list"></i>See all Artists
									</button>
								</a>
								<button class="btn-md btn-primary btn" href="#profile-pane" aria-controls="profile-pane" role="tab" data-toggle="tab">
									<i class="fa fa-user-o"></i>Profile
								</button>	
								<button class="btn-md btn-primary btn" href="#portfolio-pane" aria-controls="porfolio-pane" role="tab" data-toggle="tab">
									<i class="fa fa-photo"></i>Portfolio
								</button>	
								<button class="btn-md btn-primary btn" href="#packagist-pane" aria-controls="packagist-pane" role="tab" data-toggle="tab">
									<i class="fa fa-cube"></i>Packages
								</button>
								<button class="btn-md btn-primary btn" href="#booking-pane" aria-controls="booking-pane" role="tab" data-toggle="tab">
									<i class="fa fa-book"></i>Bookings
								</button>
							</div>
							<div class='col-md-12'>
								<hr/>
							</div>
							<!-- tab panes -->
							<div class="tab-content">
								<!--profile pane-->
								<div class="tab-pane active" role="tabpanel" id="profile-pane">
									<form method="POST" id="profile-form" action="">
										<input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
										<div class="col-md-2 left-content">
											<div class="form-group">
												<label>Profile Image</label>
												<figure style="background-image:url('{{ config('s3.bucket_link') . config('cdn.profile_image') .'/'. $user['artist_profile']['profile_image'] }}');" class='user-profile-image'>
												</figure>
												<div class="well">
													<button type="button" id="trigger-browse-file"><i class="fa fa-photo"></i> Choose File</button>
													<input type="file" class="" name="profile_image" id="profile_image" onchange="readUrl(this);" />
												</div>
												<div class="file-wrap"></div>
											</div>
										</div>
										<div class="col-md-10 right-content">
											<div class="col-md-12">
												<div class="form-group">
													<div class="time-updated alert alert-warning">
														<i class="fa fa-clock-o"></i> Updated last: {{ $user['artist_profile']['updated_at'] }}			
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control input-lg" name="first_name" id="first_name" placeholder="Artist first name" value="{{ $user['artist_profile']['first_name'] }}" />
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Middle Name</label>
													<input type="text" class="form-control input-lg" name="middle_name" id="middle_name" placeholder="Artist middle name" value="{{ $user['artist_profile']['middle_name'] }}" />
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Last Name</label>
													<input type="text" class="form-control input-lg" name="last_name" id="last_name" placeholder="Artist last name" value="{{ $user['artist_profile']['last_name'] }}" />
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label>Address</label>
													<input type="text" class="form-control input-lg" name="address" id="address" placeholder="Artist address" value="{{ $user['artist_profile']['address'] }}" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Forte</label>
													<select class="selectpicker form-control" name="specialty[]" multiple>
														<option value="" disabled>Choose your forte</option>
														@foreach($specialty as $f)
									                    <option value="{{$f->specialty_id}}" {{ ($f->specialty_id == in_array($f->specialty_id, $myForte)) ? 'selected' : '' }}>{{$f->specialty_name}}</option>
									                    @endforeach
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Email</label>
													<input type="email" class="form-control input-lg" name="email" id="email-address" placeholder="Your email address" value="{{ $user['email'] }}" />
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label>Facebook</label>
													<input type="text" class="form-control input-lg" name="facebook" id="facebook" placeholder="Artist facebook profile URL" value="{{ $user['artist_profile']['facebook'] }}" />
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label>About</label>
													<textarea class="form-control input-lg" rows="4" name="about" id="about" placeholder="About the artist">{{ $user['artist_profile']['about'] }}</textarea>
												</div>
											</div>
											<div class="col-md-12">
												{{-- <div class="form-group">
													<label>Affiliation</label>
													<textarea class="form-control input-lg" rows="4" name="achievements" id="affiliation" placeholder="Artist Affilication/Certificate/School ">{{ $user['artist_profile']['achievements'] }}</textarea>
												</div> --}}
												<div class="form-group affil-fields">
													<label>Affiliation</label>
													<div class="input-group">
														<input type="text" class="form-control input-lg" placeholder="Your affiliations" name="affiliation[]" id="affiliation" />
														<span class="input-group-btn">
															<button class="btn btn-warning btn-lg remove-affil" type="button"><i class="fa fa-times"></i></button>
														</span>
													</div>

												</div>
												<div class="form-group">
													<button type="button" class="btn btn-default" id="add-affil"><i class="fa fa-plus"></i> ADD</button>
												</div>
											</div>

											<div class="col-md-12 alert alert-success hide" id="result_div">
							  					<ul class="fa-ul">
							  						<li><i class="fa fa-li fa-check"></i> Success!</li>
							  					</ul>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<button type="submit" id="profile-button" class="btn btn-lg woo-btn-submit">Save Changes</button>
												</div>
											</div>
										</div>	
									</form>			
								</div>
								<!--portfolio pane-->
							    <div role="tabpanel" class="tab-pane" id="portfolio-pane">
							    	<div class="col-md-12">
							    		<h4>Albums</h4>
										<!-- <button class='btn woo-btn-submit' data-toggle="modal" data-target="#create-album"><i class="fa fa-plus-circle"></i> Create Album</button><br/><br/> -->		  		
										<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
											<thead>
									            <tr>
									            	<th>#</th>
									                <th>Album Name</th>
									                <th>Number of Images</th>
									                <th>Action</th>
									            </tr>
									        </thead>
									         <tbody>
									         	@foreach($album as $album)
									            <tr>
									            	<td>{{ $album->artist_album_id }}</td>
									                <td>{{ $album->album_name }}</td>
									                <?php $image = App\Modules\Artist\Models\ArtistImages::where('artist_profile_id', $album->artist_profile_id)->where('artist_album_id', $album->artist_album_id)->count();
									                	  $vid = App\Modules\Artist\Models\ArtistPortfolio::where('artist_profile_id', $album->artist_profile_id)->where('artist_album_id', $album->artist_album_id)->count(); ?>
									                <td>{{ $image+$vid }}</td>
									                <td>
									                	<a href="/admin/artist/album/{{ $album->artist_album_id }}" type="button" class="btn btn-primary btn-xs" title="View">View<a/>
									                	<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-album"
										                 onclick="deleteAlbum(this)" data-album-id="{{ $album->artist_album_id }}">Delete</button>
									                </td>
									            </tr>
									            @endforeach
									        </tbody>
										</table>
							    	</div>
							    	<div class="col-md-12">	
							    		<hr/>
										<h4>Videos</h4>	
										<!-- <button class='btn woo-btn-submit' data-toggle="modal" data-target="#add-video"><i class="fa fa-play"></i> Add Video</button><br/><br/> -->
										<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
											<thead>
									            <tr>
									            	<th>#</th>
									                <th>Link</th>
									                <th>Action</th>
									            </tr>
									        </thead>
									         <tbody>
									         	@foreach($video as $v)
									            <tr>
									            	<td>{{ $v->artist_portfolio_id }}</td>
									                <td>{{ $v->file_path }}</td>
									                <td>
									                	<button type="button" class="btn btn-primary btn-xs" title="View" data-toggle="modal" data-target="#view-video" onclick="changeLink(this);" data-video-path="{{ $v->file_path }}">View</button>
									                	<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-video"
										                 onclick="deleteVideo(this)" data-video-id="{{ $v->artist_portfolio_id }}">Delete</button>
									                </td>
									            </tr>
									            @endforeach
									        </tbody>
										</table>				    			
							    	</div>
							    </div>
							    <!--packagist pane-->
							    <div role="tabpanel" class="tab-pane" id="packagist-pane">
							    	<div class="col-md-12">
							    		<h2 class="title">Packages</h2>
										<ul>
											{!! $package['package_description'] !!}
										</ul>
										<hr/>
							    	</div>		
									<div class="col-md-12">
										<form method="POST" id="packagelist-form">
											<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
											<input type="hidden" value="{{ $user['artist_profile']['artist_profile_id'] }}" name="artist_profile_id">

											{{-- <div class="form-group packages-fields">
												<div class="well">
													<div class="form-group">
														<label>Package</label>
														<input type="text" class="form-control" name="package[]" id="package" required/>
													</div>
													<div class="form-group">
														<label>Description</label>
														<textarea class="form-control" rows="3" name="description[]" id="description" required></textarea>
													</div>
													<div class="form-group">
														<button class="btn btn-warning remove-package" type="button">REMOVE</button>
													</div>
												</div>
											</div>

											<div class="form-group">
												<button type="button" class="btn btn-default" id="add-package">ADD</button>
											</div> --}}

											<div class="form-group">
												<textarea class="form-control wysiwyg" rows="3" name="package_description" id="package_description">{!! ($package) ? $package->package_description : '' !!}</textarea>
											</div>

											<div class="col-md-12 alert alert-success hide" id="result_div">
							  					<ul class="fa-ul">
							  						<li><i class="fa fa-li fa-check"></i> Success!</li>
							  					</ul>
											</div>
											<div class="form-group">
												<button type="submit" id="packagelist-button" class="btn btn-lg woo-btn-submit">Save Changes</button>
											</div>
										</form>							    		
							    	</div>
							    </div>
							    <!--booking pane-->
							    <div role="tabpanel" class="tab-pane" id="booking-pane">
							    	<h4>Bookings</h4>	
									<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
										<thead>
								            <tr>
								                <th>client Name</th>
								                <th>Client Email</th>
								                <th>Event Title</th>
								                <th>Event Venue</th>
								                <th>Event Date</th>
								                <th>Status</th>
								            </tr>
								        </thead>
								         <tbody>
								         	@foreach($bookings as $book)
								            <tr>
								            	<td>{{ $book->first_name.' '.$book->last_name }}</td>
								            	<td>{{ $book->email }}</td>
								            	<td>{{ $book->event_title }}</td>
								            	<td>{{ $book->venue }}</td>
								            	<td>{{ date_format(date_create($book->event_date.' '.$book->event_time), 'F d, Y h:i A') }}</td>
								            	<td>
								            		@if($book->status == 0)
								            			Pending
								            		@elseif($book->status == 1)
								            			Confirmed
								            		@elseif($book->status == 2)
								            			Denied
								            		@elseif($book->status == 3)
								            			Cancelled
								            		@endif
								            	</td>
								            </tr>
								            @endforeach
								        </tbody>
									</table>
							    </div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<input type="hidden" value="{{ $user['id'] }}" id="id">

	<!-- modal confirm delete album -->
	  <div class="modal fade woo-modal" id="delete-album">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this album?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-album" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

	<!-- modal confirm delete video -->
	  <div class="modal fade woo-modal" id="delete-video">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this video?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-video" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

	<!-- modal add album -->
	  <div class="modal fade woo-modal" id="create-album">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Create Album</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Album Name</label>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">
								<i class="fa fa-photo"></i>
							</span>
							<input type="text" class="form-control input-lg" name="album" id="album" placeholder="" aria-describedby="basic-addon1">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a href="#!" type="button" id="add-video-btn" data-url="#" class="btn woo-btn-button">Add</a>
				</div>
			</div>
		</div>
	</div>	

	<!-- modal add video -->
	  <div class="modal fade woo-modal" id="add-video">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Video</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Add video link</label>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">
								<i class="fa fa-link"></i>
							</span>
							<input type="text" class="form-control input-lg" name="video" id="video" placeholder="Video URL (e.g https://www.youtube.com/watch?v=3dSAda)" aria-describedby="basic-addon1">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a href="#!" type="button" id="add-video-btn" data-url="#" class="btn woo-btn-button">Add</a>
				</div>
			</div>
		</div>
	</div>

	<!-- View modal video -->
	  <div class="modal fade woo-modal" id="view-video">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">View Video</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Source Link: <a class="link" href="">https://www.youtube.com/watch?v=CtIR9O2mgqw</a></label>
						<iframe width="100%" class="frame" height="250px" src="https://www.youtube.com/embed/ANS9sSJA9Yc" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>						


@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>	
	<script type="text/javascript">
		//choose file button
		$(document).ready(function(){
		    $("#trigger-browse-file").click(function(){
		        $('#profile_image').trigger('click'); 
		    });
		});


		function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function(e) {
	                $('.user-profile-image').css('background-image', 'url(' + e.target.result + ')');
	            };

	            reader.readAsDataURL(input.files[0]);
	        }
		 }      

		function changeLink(path) {
			$('.link').html($(path).attr('data-video-path'));
	        $('.link').attr('href', $(path).attr('data-video-path'));
	        $('.frame').attr('src', $(path).attr('data-video-path'));
	    }

	    function deleteAlbum(album) {
	        $('#confirm-button-album').attr('data-album-id', $(album).attr('data-album-id'));
	    }

	    function deleteVideo(video) {
	        $('#confirm-button-video').attr('data-video-id', $(video).attr('data-video-id'));
	    }

		$(document).ready(function(){
		    $("#profile_image").change(function(){
		        readURL(this);
		    });
		});


		//tab script
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		$(document).ready(function(){
			// Affiliations Fields
			$('#add-affil').click(function() {
				// ADD Affiliations
				$('.affil-fields').append('<div class="input-group"><input type="text" class="form-control input-lg" placeholder="Your affiliations" name="affiliation[]" id="affiliation" /><span class="input-group-btn"><button class="btn btn-warning btn-lg remove-affil" type="button"><i class="fa fa-times"></i></button></span></div>');

				// REMOVE Affiliations
				$('.remove-affil').on('click', function() {
					$(this).parent().parent().remove();
				});
			});

			// Packages Fields
			$('#add-package').click(function() {
				// ADD Packages
				$('.packages-fields').append('<div class="well"><div class="form-group"><label>Package</label><input type="text" class="form-control" name="package[]" id="package" required/></div><div class="form-group"><label>Description</label><textarea class="form-control" rows="3" name="description[]" id="description" required></textarea></div><div class="form-group"><button class="btn btn-warning remove-package" type="button">REMOVE</button></div></div>');

				// REMOVE Packages
				$('.remove-package').on('click', function() {
					$(this).parent().parent().remove();
				});
			});
		});

	</script>


@stop
