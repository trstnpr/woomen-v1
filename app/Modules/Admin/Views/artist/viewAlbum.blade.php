@extends('adminMaster')

@section('title')
	<title>Woomen - Edit Artist</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/artist/index.css') }}" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" />	
@stop

@section('content')

	<div class="viewArtist-content">
		<section class="section-content">
			<div class="container-fluid">
				<h2>{{ $artist_id['album_name'] }}</h2>
				<ol class="breadcrumb">
				  <li class="active">Admin</li>
				  <li class="active">Artist Management</li>
				  <li class="active">Artists</li>
				  <li class="active">{{ $artist['first_name'] }} {{ $artist['last_name'] }}</a></li>
				  <li class="active">Album</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				<div class="card-panel">
					<div class="row album">
						<div class="col-md-12">
							<h3>Photos</h3>	
							@foreach($album->artist_images as $a)
							<div class="col-md-3">
								<div class="picture-placeholder">
									<figure class="picture-wrap" style="background-image:url('{{ config('s3.bucket_link') . config('cdn.portfolio') .'/'. $a->image_path }}');"></figure>
								</div>
								<!-- <span class="delete-button" data-target="#delete-photo" data-toggle="modal"><i class="fa fa-trash"></i> Delete</span> -->
							</div>
							@endforeach
						</div>			
						<div class="col-md-12">
							<hr/>
							<h3>Videos</h3>
							@foreach($video as $v)
							<div class="col-md-3">
								<iframe width="100%" class="video-wrap" src="{{ $v->file_path }}" frameborder="0" allowfullscreen></iframe>
								<!-- <span class="delete-button" data-target="#delete-video" data-toggle="modal"><i class="fa fa-trash"></i> Delete</span> -->
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>



	<!-- modal confirm delete photo -->
	  <div class="modal fade woo-modal" id="delete-photo">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this photo?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-event" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

	<!-- modal confirm delete video -->
	  <div class="modal fade woo-modal" id="delete-video">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this video?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-item" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>	

	
@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>

	<script type="text/javascript">

		$(document).ready(function(){

		});

	</script>


@stop
