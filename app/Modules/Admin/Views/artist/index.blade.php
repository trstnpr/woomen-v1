@extends('adminMaster')

@section('title')
	<title>Woomen - Admin Artist</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="adminArtist-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Artists</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Artists</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				
				<div class="feat-martist">
					<div class="section-title">
						<h3>Featured Makeup Artists</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					                <th>Artist</th>
					                <th>Forte</th>
					               <!--  <th>Rating</th> -->
					                <th>Email</th>
					                <th>Is Featured</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	@foreach($feat_artist as $f)
					            <tr>
					                <td><a href="/admin/artist/view/{{ $f->user_id }}">{{ $f->first_name }} {{ $f->last_name }}</a></td>
					                <td>{{ $f->artist_one_specialty['specialty']['specialty_name'] }}</td>
					                <!-- <td>$rating</td> -->
					                <td>{{ $f->account['email'] }}</td>
						                @if($f->is_featured == 1)
						                <td>Yes</td>
						                @else
						                <td>No</td>
						                @endif
					                <td>
					                	<button type="button" class="btn btn-warning btn-xs" title="Remove" onclick="removeArtist({{ $f->artist_profile_id }})">Remove</button>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

				<div class="martist">
					<div class="section-title">
						<h3>Makeup Artists</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Artist</th>
					                <!-- <th>Rating</th> -->
					                <th>Email</th>
					                <th>Date Joined</th>
					                <th>User Level</th>
					                <th>Status</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	<?php $ctr = 1; ?>
					         	@foreach($all_artist as $a)
					            <tr>
									<td>{{ $ctr }}</td>
					                <td><a href="/admin/artist/view/{{ $a->user_id }}">{{ $a->first_name }} {{ $a->last_name }}</a></td>
					                <!-- <td>$rating</td> -->
					                <td>{{ $a->account['email'] }}</td>
					                <td>{{ date_format(date_create($a->created_at), "M d, Y") }}</td>
					                <td>{{ $a->artist_user_level->level_name }}</td>
					                @if($a->account->status == 1)
					                	<td>Active</td>
					                @else
					                	<td>Not Active</td>
					                @endif
					                <td>
					                	<!-- Condition in upgrade button -->
					                	@if($a->artist_user_level->level_id == 1)
					                	<button type="button" onclick="upgradeArtist(this)" data-artist-id="{{ $a->artist_profile_id }}" data-tooltip="tooltip" class="btn btn-success btn-sm" title="Upgrade to Regular Profile"><i class="fa fa-upload"></i></button>
					                	@elseif($a->artist_user_level->level_id == 2)
					                	<button type="button" onclick="upgradeArtist(this)" data-artist-id="{{ $a->artist_profile_id }}" data-tooltip="tooltip" class="btn btn-success btn-sm" title="Upgrade to Standard Profile"><i class="fa fa-upload"></i></button>
					                	@elseif($a->artist_user_level->level_id == 3)
					                	<button type="button" onclick="upgradeArtist(this)" data-artist-id="{{ $a->artist_profile_id }}" data-tooltip="tooltip" class="btn btn-success btn-sm" title="Upgrade to Premium Profile"><i class="fa fa-upload"></i></button>
					                	@else
					                	<button type="button" data-artist-id="{{ $a->artist_profile_id }}" data-tooltip="tooltip" class="btn btn-success btn-sm" title="Upgraded to Premium"><i class="fa fa-upload"></i></button>
					                	@endif
					                	<!-- Condition in upgrade button -->
					                	@if($a->is_featured == 0)					                	
					                	<button type="button" class="btn btn-warning btn-sm" data-tooltip="tooltip" title="Feature Artist" onclick="featArtist({{ $a->artist_profile_id }})"><i class="fa fa-star"></i></button>
					                	@else
					                	<button type="button" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Featured"><i class="fa fa-star"></i></button>
					                	@endif
					                	<a href="/admin/artist/view/{{ $a->user_id }}">
					                		<button type="button" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="View/Edit"><i class="fa fa-edit"></i></button>
					                	</a>	
					                	<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-makeup-artist" data-tooltip="tooltip" title="Delete" onclick="deleteArtist(this)" data-artist-id="{{ $a->user_id }}"><i class="fa fa-trash"></i></button>
					                </td>
					            </tr>
					            <?php $ctr++; ?>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>

				<!-- modal confirm delete make up artist-->
				 <div class="modal fade woo-modal" id="delete-makeup-artist">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header gold">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Please Confirm</h4>
							</div>
							<div class="modal-body">
								By taking this action, you will not retrieve the data anymore.
								<strong>Are you sure you want to delete this artist?</strong>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
								<a href="#!" type="button" id="confirm-button-artist" data-url="#" class="btn woo-btn-button">Yes, delete</a>
							</div>
						</div>
					</div>
				</div>	

				<!-- modal upgrade artist-->
				 <div class="modal fade woo-modal" id="upgrade-artist">
				 	<form id="upgrade-artist-form" method="POST">
					    <input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header gold">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title"><i class="fa fa-upload"></i> Upgrade Artist</h4>
								</div>
								<div class="modal-body">
									<div class="row profile-level-wrapper">
										<div class="col-md-12">
											<h4>Hover then click to select</h4>
										</div>
										<div class="col-md-4 level">
											<div class="level-wrap" data-target="#regular">
												<h3> Regular Profile (Neophyte Profile)</h3><hr/>
												<p>
													MUA has the chance to upload at least 2 albums with 5 pictures each of their best works and 2 video that they want to show to their clients. They MUA is also entitled to a virtual card that has a value of 20 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and a chance to be a featured artist.
												</p>
												<input type="radio"  class="radio" id="regular" name="level" value="2">
											</div>
										</div>
										<div class="col-md-4 level">
											<div class="level-wrap" data-target="#standard">
												<h3> Standard Profile (Archetype Profile)</h3><hr/>
												<p>
													The MUA has the chance to showcase 3 albums with 5 pictures each and 5 videos in their dashboard. They will have discounts on featured events and sponsored by Woomen. They MUA is also entitled to a virtual card that has a value of 50 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and higher chances to be a featured artist.
												</p>
												<input type="radio"  class="radio" id="standard" name="level" value="3">
											</div>
										</div>
										<div class="col-md-4 level">
											<div class="level-wrap" data-target="#premium">
												<h3>Premium Profile (Transcendent Profile)</h3><hr/>
												<p>
													MUA has more than 10 pictures and 10 videos that they can showcase on their portfolio. They will have discounts on featured events created or partnered by Woomen. They MUA is also entitled to a virtual card that has a value of 100 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and a chance to be a featured artist. They are also entitled to free photoshoot on 5 models or 5 Items (If they are registered in the Marketplace).
												</p>
												<input type="radio" class="radio"  id="premium" name="level" value="4">
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
									<button type="submit" id="upgrade-artist-button" class="btn woo-btn-button">Upgrade</button>
								</div>
							</div>
						</div>
				 	</form>
				</div>	

			</div>
		</section>
	</div>

	


@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
		    $('[data-tooltip="tooltip"]').tooltip(); 


		    $(".level-wrap").click(function(){
		    	var id = $(this).attr("data-target");
		    	$(id).prop("checked", true);
		    	$.each($(".level-wrap"), function(){
		    		$(this).removeClass("bordered");
		    	});
		    	$(this).addClass("bordered");
		    });

		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		function deleteArtist(artist) {
	        $('#confirm-button-artist').attr('data-artist-id', $(artist).attr('data-artist-id'));
	    }

	    function upgradeArtist(artist) {
	    	$('#upgrade-artist-form').attr('data-artist-id', $(artist).attr('data-artist-id'));
	    	$('#upgrade-artist').modal('show');
	    }

	    function featArtist(artist) {
	        $.ajax({
	            url: '/admin/artists/feat/'+artist,
	            headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            type: 'POST',
	            success: function(data) {
	                var msg = JSON.parse(data);
	                if(msg.result == 'success'){
	                    alert(msg.message);
	                    location.reload();
	                } else{
	                    alert(msg.message);
	                    location.reload();
	                }
	            }
	        });
	    }

	    function removeArtist(artist) {
	        $.ajax({
	            url: '/admin/artists/remove/'+artist,
	            headers:
	            {
	                'X-CSRF-Token': $('input[name="_token"]').val()
	            },
	            type: 'POST',
	            success: function(data) {
	                var msg = JSON.parse(data);
	                if(msg.result == 'success'){
	                    alert(msg.message);
	                    location.reload();
	                } else{
	                    alert(msg.message);
	                    location.reload();
	                }
	            }
	        });
	    }
	</script>

@stop
