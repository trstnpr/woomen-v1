@extends('adminMaster')

@section('title')
	<title>Woomen - Admin Events</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" />
@stop

@section('content')

	<div class="adminEvent-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">Events</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Artist Management</li>
					<li class="active">Events</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">

				<div class="upcoming-event">
					<div class="section-title">
						<h3>Upcoming Events</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Event</th>
					                <th>Date & Time</th>
					                <th>Venue</th>
					                <th>Posted By</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					         	@foreach($upcoming as $u)
					            <tr>
					                <td>{{ $u->artist_event_id }}</td>
					                <td><a href="/admin/event/view/{{ $u->artist_event_id }}">{{ $u->event_title }}</a></td>
					                <td>{{ date_format(date_create($u->event_date), "M d, Y") }} {{ date_format(date_create($u->event_time), "g:i A") }}</td>
					                <td>{{ $u->event_venue }}</td>
					                <td>{{ $u->artist['first_name'] }} {{ $u->artist['last_name'] }}</td>
					                <td>
					                	<a role="button" href="/admin/event/view/{{ $u->artist_event_id }}" class="btn btn-primary btn-xs" title="View/Edit">View/Edit</a>
					                	<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-event" onclick="deleteEvent(this)" data-event-id="{{$u->artist_event_id}}">Delete</button>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>
				
				<div class="event">
					<div class="section-title">
						<h3>All Events</h3>
					</div>
					<div class="card-panel">
						<table class="table table-striped table-bordered dataTable dt-responsive" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Event</th>
					                <th>Date & Time</th>
					                <th>Venue</th>
					                <th>Posted By</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					         <tbody>
					            @foreach($all_event as $all)
					            <tr>
					                <td>{{ $all->artist_event_id }}</td>
					                <td><a href="/admin/event/view/{{ $all->artist_event_id }}">{{ $all->event_title }}</a></td>
					                <td>{{ date_format(date_create($all->event_date), "M d, Y") }} {{ date_format(date_create($all->event_time), "g:i A") }}</td>
					                <td>{{ $all->event_venue }}</td>
					                <td>{{ $all->artist['first_name'] }} {{ $all->artist['last_name'] }}</td>
					                <td>
					                	<a role="button" href="/admin/event/view/{{ $all->artist_event_id }}" class="btn btn-primary btn-xs" title="View/Edit">View/Edit</a>
					                	<button type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#delete-event" onclick="deleteEvent(this)" data-event-id="{{$all->artist_event_id}}">Delete</button>
					                </td>
					            </tr>
					            @endforeach
					        </tbody>
						</table>
					</div>
				</div>
				
			</div>
		</section>
	</div>

	<!-- modal confirm delete make up artist-->
	<div class="modal fade woo-modal" id="delete-event">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this event?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button-event" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dataTable').DataTable();
		});

		function deleteEvent(event) {
	        $('#confirm-button-event').attr('data-event-id', $(event).attr('data-event-id'));
	    }

	</script>

@stop
