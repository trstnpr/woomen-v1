@extends('adminMaster')

@section('title')
	<title>Woomen - Event</title>
@stop

@section('meta')
	{{-- Meta tags here --}}
@stop

@section('stylesheet')
	<link href="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.css') }}" rel="stylesheet" />
@stop

@section('content')

	<div class="adminEvent-content">

		<section class="content-title">
			<div class="container-fluid">
				<h2 class="title">{{ $event['event_title'] }}</h2>
				<ol class="breadcrumb">
					<li class="active">Admin</li>
					<li class="active">Artist Management</li>
					<li class="active">Events</li>
					<li class="active">{{ $event['event_title'] }}</li>
				</ol>
			</div>
		</section>

		<section class="section-content">
			<div class="container-fluid">
				<div class="card-panel view-edit-event">
					<form method="POST" id="event-form" action="">
						<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Choose Image</label>
									<div class="well">
										<input type="file" class="" name="event_image" id="image_upload" />
									</div>
									<div class="file-wrap" style="display: block; background-image: url('{{ config('s3.bucket_link') . config('cdn.event') .'/'. $event['event_image'] }}');"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Event Title</label>
									<input type="text" class="form-control input-lg" name="event_title" id="event_title" placeholder="Event title" value="{{ $event['event_title'] }}" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="fomr-group">
									<label>Event Date</label>
									<div class="input-group datePicker">
					                    <input type="text" class="form-control input-lg" name="event_date" id="event_date" value="{{ date_format(date_create($event['event_date']), 'mm/dd/Y') }}" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Event Time</label>
									<div class="input-group timePicker">
					                    <input type="text" class="form-control input-lg" name="event_time" id="event_time" placeholder="Event time" value="{{ $event['event_time'] }}" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-time"></span>
					                    </span>
					                </div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Event Venue</label>
									<input type="text" class="form-control input-lg" name="event_venue" id="event_venue" placeholder="Event venue" value="{{ $event['event_venue'] }}" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Event Description</label>
									<textarea class="form-control" rows="3" name="event_description" id="event_description" >{{ $event['event_description'] }} </textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Ticket</label>
									<select class="form-control input-lg" name="event_ticket" id="event_ticket">
										<option value="Free" {{ ($event['event_ticket'] == 'Free') ? 'selected' : '' }}>Free</option>
										<option value="Door Sale" {{ ($event['event_ticket'] == 'Door Sale') ? 'selected' : '' }}>Door Sale</option>
										<option value="Buy Online" {{ ($event['event_ticket'] == 'Buy Online') ? 'selected' : '' }}>Buy Online</option>
									</select>
								</div>
							</div>
							<div class="container-fluid">
								<div class="col-md-12 alert alert-success hide" id="result_div">
									<ul class="fa-ul">
										<li><i class="fa fa-li fa-check"></i> Success!</li>
									</ul>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" id="event-button" class="btn btn-lg woo-btn-submit">Save Changes</button>
									<!-- <a role="submit" href="#delete-event" data-toggle="modal" class="btn btn-lg btn-danger">Delete Event</a> -->
								</div>
							</div>
						</div>
					</form>
				</div>

			</div>
		</section>
	</div>

	<input type="hidden" value="{{ $event['artist_event_id'] }}" id="event_id">

	<!-- modal confirm delete make up artist-->
	 <div class="modal fade woo-modal" id="delete-event">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header gold">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Please Confirm</h4>
				</div>
				<div class="modal-body">
					By taking this action, you will not retrieve the data anymore.
					<strong>Are you sure you want to delete this event?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No, not now</button>
					<a href="#!" type="button" id="confirm-button" data-url="#" class="btn woo-btn-button">Yes, delete</a>
				</div>
			</div>
		</div>
	</div>

@stop()

@section('custom-scripts')
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ config('s3.bucket_link') . elixir('assets/admin/views/dashboard/index.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// $('.datePicker').datetimepicker({ format: 'MM/DD/YYYY' });
			$('.timePicker').datetimepicker({ format: 'LT' });

			// Function to preview image after validation
			$("#image_upload").change(function() {
				var file = this.files[0];
				console.log(file);
				if(typeof file == 'undefined'){
					$('.file-wrap').hide();
				} else {
					var imagefile = file.type;
					var imagesize = file.size;
					var match= ["image/jpeg","image/png","image/jpg"];
					if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
						alert('Invalid file type');
						$("#image_upload").val('');
						$('.file-wrap').hide();
					} else {
						if(imagesize>500000 || imagesize==0) {
							alert('Image should not exceed to 500kbs');
							$("#image_upload").val('');
							$('.file-wrap').hide();
						} else {
							$('.file-wrap').show();
							var reader = new FileReader();
							reader.onload = imageIsLoaded;
							reader.readAsDataURL(this.files[0]);
						}
					}
				}
			});
			function imageIsLoaded(e) {
				$('.file-wrap').css('background-image', 'url(' + e.target.result + ')');
			};
		});
	</script>
@stop