<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class InstagramServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if (file_exists(config_path('instagram.php'))) {
            $this->app->bind('instagram', function () {
                return new \App\Services\InstagramApi();
            });
        }
    }
}
