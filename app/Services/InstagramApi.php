<?php

namespace App\Services;

class InstagramApi
{
	private static $instance;

	public static function make($username = 'self')
	{
		$config = [
            'apiKey'        => config('instagram.client_id'),
            'apiSecret'     => config('instagram.client_secret'),
            'apiCallback'   => config('instagram.callback_url')
		];

		self::$instance = new \MetzWeb\Instagram\Instagram($config);

		if($username == 'self') {
        	self::$instance->setAccessToken(config('instagram.account_token'));
		} else {
			// to be implemented
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, self::$instance->getLoginUrl(['basic', 'relationships', 'likes', 'comments']));
			// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
			
			$response = curl_exec($ch);
			curl_close($ch);

			preg_match('/Location:(.*?)\n/', $response, $matches); 
            $redirect = trim(array_pop($matches));

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $redirect);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);


			$response = curl_exec($ch);
			$data = json_decode($response, true);
			curl_close($ch);
			dd($response, $redirect);
		}
	}

	public static function loginUser()
	{

	}

	public static function getFeed($username = 'self', $limit = 10)
	{
		self::make($username);

		$data = json_decode(json_encode(self::$instance->getUserMedia('self', $limit)), true)['data'];

		return $data;
	}
}