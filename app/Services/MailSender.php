<?php
namespace App\Services;

use Mail;

/**
 * A service class to send email
 *
 * @author louie
 * @package App\Services
 * @return boolean
 */
class MailSender
{
    /**
     * Send e-mail.
     *
     * @param string $view
     * @param string $subject
     * @param array $data
     * @param array $opt
     * @return void
     */
    public function send($view, $subject, $data, $opt = array())
    {
        $sendMail = Mail::send($view, ['data' => $data], function ($message) use ($data, $subject) {
            $message->from('no-reply@woomen.ph', 'Woomen')->subject($subject);
            $message->to($data['email'], $data['first_name'] . ' ' . $data['last_name'])->subject($subject);
        });

        return $sendMail;
    }

    public function sendLater($view, $subject, $data, $opt = array())
    {
        $sendMail = Mail::later(10800, $view, ['data' => $data], function ($message) use ($data, $subject) {
            $message->from('no-reply@woomen.ph', 'Woomen')->subject($subject);
            $message->to($data['email'], $data['first_name'] . ' ' . $data['last_name'])->subject($subject);
        });

        return $sendMail;
    }

    public function contact($view, $subject, $data, $opt = array())
    {
        $sendMail = Mail::send($view, ['data' => $data], function ($message) use ($data, $subject) {
            $message->from($data['user_email'], $data['name'], 'Woomen - Contact Us')->subject($subject);
            $message->to('woomen.ceo@gmail.com')->subject($subject);
        });

        return $sendMail;
    }

}