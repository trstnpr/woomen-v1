<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChangePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'current_pass' => 'required|min:8',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
        ];
                
        return $rules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'current_pass.required' => 'The Current Password field is required.',
            'current_pass.min' => 'The Current Password must be at least 8 characters.',
            'password.required' => 'The New Password field is required',
            'password.min' => 'The New Password must be at least 8 characters'
        ];
    }
}
