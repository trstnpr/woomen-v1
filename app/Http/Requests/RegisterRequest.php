<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'role_id' => 'required',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
            'email' => 'required|email|unique:user',
            'address' => 'required',
            'terms' => 'required',
        ];

        if ($this->role_id == 2) {
            $rules['specialty'] = 'required';
            $rules['price_from'] = 'required|numeric';
            $rules['price_to'] = 'required|numeric';
        }
        
        return $rules;
    }

    public function messages()
    {
        return [
            'specialty.required' => 'The forte field is required.',
            'terms.required' => 'You must agree to the terms and conditions before signing up on Woomen.',
        ];
    }
}
