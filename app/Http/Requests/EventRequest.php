<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'event_title' => 'required',
            'event_description' => 'required',
            'event_venue' => 'required',
            'event_date' => 'required|date',
            'event_time' => 'required',
            'event_ticket' => 'required',
            'image_upload' => 'mimes:jpeg,jpg,png,gif',
        ];
                
        return $rules;
    }
}
