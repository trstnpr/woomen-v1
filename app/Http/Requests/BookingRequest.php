<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'contact_no' => 'required|numeric',
            'email' => 'required',
            'event_title' => 'required',
            'no_of_heads' => 'required|numeric',
            'event_date' => 'required',
            'event_time' => 'required',
            'venue' => 'required',
            'call_time' => 'required',
            'remarks' => 'required'
        ];
                
        return $rules;
    }
}
