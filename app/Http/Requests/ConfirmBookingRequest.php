<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConfirmBookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'final_event_title' => 'required',
            'final_date' => 'required',
            'final_time' => 'required',
            'final_no_of_heads' => 'required',
            'final_venue' => 'required',
            'final_price' => 'required'
        ];
                
        return $rules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'final_event_title.required' => 'The Event Title field is required.',
            'final_date.required' => 'The Date field is required.',
            'final_time.required' => 'The Time field is required.',
            'final_no_of_heads.required' => 'The No. of Heads field is required.',
            'final_venue.required' => 'The Venue field is required.',
            'final_price.required' => 'The Price field is required.'
        ];
    }
}
