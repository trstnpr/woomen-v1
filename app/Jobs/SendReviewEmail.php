<?php

namespace App\Jobs;

use App\Modules\App\Models\Booking;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\MailSender;

class SendReviewEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $booking;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MailSender, $mailSender)
    {
        $mailSender->send('email.email_review', 'Email Review', $this->booking);
    }
}
