<?php

return [
	'client_id'		=> env('INSTAGRAM_CLIENT_ID', ''),
	'client_secret' => env('INSTAGRAM_SECRET', ''),
	'callback_url'	=> env('INSTAGRAM_CALLBACK_URL', ''),
	'account_token'	=> env('INSTAGRAM_ACCESS_TOKEN', ''),
];
