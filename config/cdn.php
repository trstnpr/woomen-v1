<?php

return [
	'item' => env('ITEM_IMAGE_PREFIX', '/uploadedImages/items'),
	'event' => env('EVENT_IMAGE_PREFIX', '/uploadedImages/events'),
	'portfolio' => env('PORTFOLIO_PREFIX', '/uploadedImages/portfolio'),
	'profile_image' => env('PROFILE_IMAGE_PREFIX', '/uploadedImages/profile_images'),
];