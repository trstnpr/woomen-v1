-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table woomen.woo_artist_affiliations
CREATE TABLE IF NOT EXISTS `woo_artist_affiliations` (
  `artist_affiliation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) DEFAULT NULL,
  `affiliation` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_affiliation_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  CONSTRAINT `FK_affiliation_artist` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_artist_affiliations: ~0 rows (approximately)
DELETE FROM `woo_artist_affiliations`;
/*!40000 ALTER TABLE `woo_artist_affiliations` DISABLE KEYS */;
/*!40000 ALTER TABLE `woo_artist_affiliations` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_artist_albums
CREATE TABLE IF NOT EXISTS `woo_artist_albums` (
  `artist_album_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `album_description` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_album_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  CONSTRAINT `FK_artist_album_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table woomen.woo_artist_albums: ~4 rows (approximately)
DELETE FROM `woo_artist_albums`;
/*!40000 ALTER TABLE `woo_artist_albums` DISABLE KEYS */;
INSERT INTO `woo_artist_albums` (`artist_album_id`, `artist_profile_id`, `album_name`, `album_description`, `created_at`, `updated_at`) VALUES
	(1, 6, 'Album 1', NULL, '2016-12-08 03:57:05', '2016-12-08 03:57:05'),
	(2, 6, 'Test Album', NULL, '2016-12-16 03:21:49', '2016-12-16 03:21:49'),
	(3, 2, 'My Album Updated 4', 'Aomine X Me', '2016-12-19 06:46:31', '2017-01-13 11:17:50'),
	(4, 2, 'HAHAHA-Album', 'asd asdasdasdasdasd', '2017-01-06 07:21:28', '2017-01-16 06:14:56'),
	(5, 2, 'Wallpapers', 'My favorite wallpapers', '2017-02-06 05:34:50', '2017-02-06 05:37:25');
/*!40000 ALTER TABLE `woo_artist_albums` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_artist_events
CREATE TABLE IF NOT EXISTS `woo_artist_events` (
  `artist_event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) NOT NULL,
  `event_image` varchar(255) DEFAULT NULL,
  `event_title` varchar(255) DEFAULT NULL,
  `event_description` varchar(255) DEFAULT NULL,
  `event_venue` varchar(255) DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  `event_time` time DEFAULT NULL,
  `event_ticket` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_event_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  CONSTRAINT `FK_artist_event_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_artist_events: ~3 rows (approximately)
DELETE FROM `woo_artist_events`;
/*!40000 ALTER TABLE `woo_artist_events` DISABLE KEYS */;
INSERT INTO `woo_artist_events` (`artist_event_id`, `artist_profile_id`, `event_image`, `event_title`, `event_description`, `event_venue`, `event_date`, `event_time`, `event_ticket`, `created_at`, `updated_at`) VALUES
	(1, 6, '1/dBUlWfoKwEJEhztCCh.png', 'Test Event', 'test ', 'Test Venue', '2016-12-06 00:00:00', '09:29:00', 'Buy Online', '2016-12-08 07:23:36', '2016-12-13 11:17:02'),
	(2, 6, '2/ybfzto3QZykVNQeX7dSt.jpg', 'Event 2', 'Event 2 ', 'Event 2', '2016-12-15 00:00:00', '16:30:00', 'Door Sale', '2016-12-14 06:22:13', '2016-12-14 06:35:59'),
	(3, 6, '3/K8mZlilVzGu.jpg', 'Test Event', 'Test Event', 'Test Event', '2016-12-17 00:00:00', '17:00:00', 'Door Sale', '2016-12-16 05:22:30', '2016-12-16 05:22:30'),
	(4, 2, '4/Tw1goarDal6miXKtihN.jpg', 'Donna B.', 'Sample Desc', 'Sample Venue', '2017-01-21 00:00:00', '17:00:00', 'Free', '2017-01-06 09:15:09', '2017-01-06 09:15:09'),
	(5, 2, '5/4R66XkNXuO65Y.png', 'Meet and Greet with Captian America', 'A Chance to Meet and Greet with Captian America', 'Mall of Asia Arena, Pasay City', '2017-01-25 00:00:00', '17:00:00', 'Free', '2017-01-24 03:40:19', '2017-01-24 03:40:20');
/*!40000 ALTER TABLE `woo_artist_events` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_artist_images
CREATE TABLE IF NOT EXISTS `woo_artist_images` (
  `artist_image_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) DEFAULT NULL,
  `artist_album_id` bigint(20) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_type` varchar(255) DEFAULT NULL,
  `is_featured` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_image_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  KEY `artist_album_id` (`artist_album_id`),
  CONSTRAINT `FK_artist_image_artist_album` FOREIGN KEY (`artist_album_id`) REFERENCES `woo_artist_albums` (`artist_album_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_artist_image_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_artist_images: ~9 rows (approximately)
DELETE FROM `woo_artist_images`;
/*!40000 ALTER TABLE `woo_artist_images` DISABLE KEYS */;
INSERT INTO `woo_artist_images` (`artist_image_id`, `artist_profile_id`, `artist_album_id`, `image_path`, `image_type`, `is_featured`, `created_at`, `updated_at`) VALUES
	(1, 6, 1, '6/Album 1/AK6wtjR5zQvzSfyjbePa.png', 'png', 0, '2016-12-08 03:57:28', '2016-12-08 03:57:28'),
	(2, 6, 1, '6/Album 1/lCsATurfy7FoVCz.png', 'png', 0, '2016-12-16 03:21:06', '2016-12-16 03:21:06'),
	(4, 2, 3, '2/My Album/JRlQ6He1Wl1XXbYPoU.jpg', 'jpg', 0, '2017-01-06 07:16:31', '2017-01-06 07:16:31'),
	(5, 2, 4, '2/HAHAHA-Album/hWgBlgI9.png', 'png', 0, '2017-01-06 07:21:54', '2017-02-02 05:59:22'),
	(7, 2, 4, '2/HAHAHA-Album/lngYiw2ITBKSA.png', 'png', 0, '2017-01-06 07:28:14', '2017-02-02 05:59:22'),
	(8, 2, 4, '2/HAHAHA-Album/XKSjRgDjtKnhx8NM.png', 'png', 0, '2017-01-06 07:31:31', '2017-02-02 05:59:22'),
	(12, 2, 4, '2/HAHAHA-Album/k6lkYWuZVr0TX8TkipgL.png', 'png', 0, '2017-01-06 07:35:18', '2017-01-06 07:35:18'),
	(13, 2, 4, '2/HAHAHA-Album/VGVnIRoji8.png', 'png', 0, '2017-01-06 07:35:18', '2017-01-06 07:35:18'),
	(14, 2, 3, '2/My Album Updated 4/PTtqlInolbX.jpg', 'jpg', 0, '2017-01-13 11:17:31', '2017-02-02 05:59:22'),
	(15, 2, 5, '2/Wallpapers/JECP3Dqn7KlOWLDHT.jpg', 'jpg', 0, '2017-02-06 05:37:26', '2017-02-06 05:37:26'),
	(16, 2, 5, '2/Wallpapers/bPzTLS1z5MdVkTirm6.jpg', 'jpg', 0, '2017-02-06 05:37:26', '2017-02-06 05:37:26');
/*!40000 ALTER TABLE `woo_artist_images` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_artist_packages
CREATE TABLE IF NOT EXISTS `woo_artist_packages` (
  `artist_package_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) NOT NULL,
  `package_title` varchar(255) NOT NULL,
  `package_description` longtext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_package_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  CONSTRAINT `FK_packages_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_artist_packages: ~4 rows (approximately)
DELETE FROM `woo_artist_packages`;
/*!40000 ALTER TABLE `woo_artist_packages` DISABLE KEYS */;
INSERT INTO `woo_artist_packages` (`artist_package_id`, `artist_profile_id`, `package_title`, `package_description`, `created_at`, `updated_at`) VALUES
	(1, 6, 'Sample Package Title', '<p>Test Package 2</p>', '2016-12-06 07:24:00', '2016-12-16 03:42:13'),
	(13, 2, '', '<p><strong>Title 1</strong></p>\r\n<p>&nbsp;</p>\r\n<p><em>&nbsp; &nbsp; &nbsp; Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</em><br /><em>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</em><br /><em>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</em><br /><em>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</em><br /><em>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</em><br /><em>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</em></p>', NULL, '2017-02-02 06:03:29'),
	(14, 2, '', '<p><strong>Title 21</strong></p>\r\n<p>&nbsp;</p>\r\n<p><em>&nbsp; &nbsp; &nbsp; Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</em><br /><em>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</em><br /><em>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</em><br /><em>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</em><br /><em>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</em><br /><em>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</em></p>', '2017-02-06 10:49:29', '2017-02-06 10:49:29'),
	(15, 4, '', '<p><strong>Sample Package</strong></p>\r\n<p>&nbsp;</p>\r\n<p>1. Sample</p>\r\n<p>&nbsp;</p>\r\n<p>1. Sample</p>\r\n<p>&nbsp;</p>\r\n<p>1. Sample</p>\r\n<p>&nbsp;</p>\r\n<p>1. Sample</p>\r\n<p>&nbsp;</p>\r\n<p>1. Sample</p>', '2017-02-06 11:28:34', '2017-02-06 11:28:34');
/*!40000 ALTER TABLE `woo_artist_packages` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_artist_portfolio
CREATE TABLE IF NOT EXISTS `woo_artist_portfolio` (
  `artist_portfolio_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) NOT NULL,
  `artist_album_id` bigint(20) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `embed_code` varchar(255) DEFAULT NULL,
  `is_featured` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_portfolio_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  KEY `artist_album_id` (`artist_album_id`),
  CONSTRAINT `FK_FK_artist_portfolio_artist_album` FOREIGN KEY (`artist_album_id`) REFERENCES `woo_artist_albums` (`artist_album_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_artist_portfolio_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_artist_portfolio: ~2 rows (approximately)
DELETE FROM `woo_artist_portfolio`;
/*!40000 ALTER TABLE `woo_artist_portfolio` DISABLE KEYS */;
INSERT INTO `woo_artist_portfolio` (`artist_portfolio_id`, `artist_profile_id`, `artist_album_id`, `file_path`, `file_type`, `embed_code`, `is_featured`, `created_at`, `updated_at`) VALUES
	(1, 2, NULL, 'https://www.youtube.com/embed/hS5CfP8n_js', 'video', 'hS5CfP8n_js', 0, '2017-02-08 09:31:16', '2017-02-08 09:31:16'),
	(2, 2, NULL, 'https://www.youtube.com/embed/e-xsg8RKv6M', 'video', 'e-xsg8RKv6M', 0, '2017-02-08 09:31:57', '2017-02-08 09:31:57'),
	(3, 2, NULL, 'https://www.youtube.com/embed/e-xsg8RKv6M', 'video', 'e-xsg8RKv6M', 0, '2017-02-08 09:32:24', '2017-02-08 09:32:24');
/*!40000 ALTER TABLE `woo_artist_portfolio` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_artist_profile
CREATE TABLE IF NOT EXISTS `woo_artist_profile` (
  `artist_profile_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `price_from` bigint(20) DEFAULT NULL,
  `price_to` bigint(20) DEFAULT NULL,
  `address` longtext,
  `about` longtext,
  `achievements` longtext,
  `facebook` varchar(255) DEFAULT NULL,
  `is_featured` int(11) DEFAULT '0',
  `user_level` int(11) DEFAULT '2' COMMENT 'look for table woo_user_levels',
  `total_earnings` bigint(20) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_profile_id`),
  KEY `user_id` (`user_id`),
  KEY `user_level` (`user_level`),
  CONSTRAINT `FK_artist_profile_user` FOREIGN KEY (`user_id`) REFERENCES `woo_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_artist_profile_user_level` FOREIGN KEY (`user_level`) REFERENCES `woo_user_levels` (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_artist_profile: ~6 rows (approximately)
DELETE FROM `woo_artist_profile`;
/*!40000 ALTER TABLE `woo_artist_profile` DISABLE KEYS */;
INSERT INTO `woo_artist_profile` (`artist_profile_id`, `user_id`, `profile_image`, `first_name`, `middle_name`, `last_name`, `price_from`, `price_to`, `address`, `about`, `achievements`, `facebook`, `is_featured`, `user_level`, `total_earnings`, `created_at`, `updated_at`) VALUES
	(2, 2, '2/8gxSvtmwEiD3.jpg', 'Kevin', '', 'Villa', 12345, 67890, 'Makati City', 'Aboutasdasdas asdasdas dasd asd asd sadasdasdasdasd', 'Affliation', 'https://www.facebook.com/louie.villena05', 0, 2, 20000, '2016-11-28 05:25:18', '2017-02-09 09:02:18'),
	(4, 4, '4/hskzNGKfSlz6.jpg', 'anne', 'curtis', 'smith', 12000, 30000, 'Pasig City', 'I am an actress, model, tv host and fashion icon.', NULL, '', 0, 3, 0, '2016-11-28 05:28:58', '2017-02-06 11:28:54'),
	(6, 7, '6/yScGbE3Qkc5oGSq.jpg', 'Jennelyn', '', 'Medianista', 20000, 80000, 'Curabitur quis velit in elit auctor dictum vel at erat. Sed lacinia odio sed felis rutrum, in iaculis lorem sagittis. Curabitur interdum dui augue, ac euismod ipsum ornare vitae. Curabitur eget dui eleifend, fermentum sapien eu, vehicula lacus. Etiam eget turpis ut sapien facilisis rhoncus non quis ipsum. Suspendisse quis est nec neque laoreet mattis. Duis efficitur dui justo, ac convallis odio tristique vel. Quisque dictum ultricies lectus vitae dictum.', 'N/A', 'N/A', NULL, 1, 1, 0, '2016-12-05 08:54:25', '2016-12-16 05:14:08'),
	(7, 9, NULL, 'Joe', '', 'Cross', 30000, 40000, 'asas', '', '', NULL, 1, 2, 0, '2016-12-05 09:35:55', '2016-12-15 01:26:12'),
	(8, 13, NULL, 'Guiden Faith', NULL, 'Amalay', 1000, 9999, NULL, NULL, NULL, NULL, 1, 2, 0, '2016-12-16 07:05:58', '2017-02-10 03:23:01'),
	(9, 14, NULL, 'Jayson', NULL, 'Cabrera', 10000, 50000, NULL, NULL, NULL, NULL, 0, 2, 0, '2017-01-16 03:46:01', '2017-01-16 03:46:01');
/*!40000 ALTER TABLE `woo_artist_profile` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_artist_specialties
CREATE TABLE IF NOT EXISTS `woo_artist_specialties` (
  `artist_profile_id` bigint(20) DEFAULT NULL,
  `specialty_id` int(11) DEFAULT NULL,
  KEY `artist_profile_id` (`artist_profile_id`),
  KEY `specialty_id` (`specialty_id`),
  CONSTRAINT `FK_artist_specialties_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_artist_specialties_specialities` FOREIGN KEY (`specialty_id`) REFERENCES `woo_specialties` (`specialty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_artist_specialties: ~17 rows (approximately)
DELETE FROM `woo_artist_specialties`;
/*!40000 ALTER TABLE `woo_artist_specialties` DISABLE KEYS */;
INSERT INTO `woo_artist_specialties` (`artist_profile_id`, `specialty_id`) VALUES
	(7, 1),
	(6, 1),
	(6, 2),
	(6, 3),
	(6, 4),
	(6, 5),
	(6, 6),
	(6, 7),
	(9, 3),
	(9, 4),
	(9, 6),
	(9, 7),
	(4, 3),
	(4, 4),
	(4, 5),
	(4, 6),
	(2, 5);
/*!40000 ALTER TABLE `woo_artist_specialties` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_bookings
CREATE TABLE IF NOT EXISTS `woo_bookings` (
  `booking_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `event_title` varchar(255) DEFAULT NULL,
  `no_of_heads` varchar(255) DEFAULT NULL,
  `event_date` varchar(255) DEFAULT NULL,
  `event_time` varchar(255) DEFAULT NULL,
  `venue` longtext,
  `call_time` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '3 - Cancelled, 2 - Denied, 1 - Confirmed, 0 - Pending',
  `final_event_title` varchar(255) DEFAULT NULL,
  `final_date` varchar(255) DEFAULT NULL,
  `final_time` varchar(255) DEFAULT NULL,
  `final_no_of_heads` varchar(255) DEFAULT NULL,
  `final_venue` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `final_notes` longtext,
  `cancel_reason` longtext,
  `diamond_count` bigint(20) DEFAULT '0',
  `seen` int(11) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`booking_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  CONSTRAINT `FK_booking_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_bookings: ~2 rows (approximately)
DELETE FROM `woo_bookings`;
/*!40000 ALTER TABLE `woo_bookings` DISABLE KEYS */;
INSERT INTO `woo_bookings` (`booking_id`, `artist_profile_id`, `first_name`, `last_name`, `contact_no`, `email`, `event_title`, `no_of_heads`, `event_date`, `event_time`, `venue`, `call_time`, `remarks`, `status`, `final_event_title`, `final_date`, `final_time`, `final_no_of_heads`, `final_venue`, `final_price`, `final_notes`, `cancel_reason`, `diamond_count`, `seen`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Test Event', 'Test Event', '451545465', 'louie@incubixtech.com', 'Test Event', '12', '12/17/2016', '6:00 PM', 'Test Event', '5:45 PM', 'Test Event', 2, 'Test Event', '12/17/2016', '6:00 PM', '12', 'Test Event', '200000', 'SAmple Notes', 'asd asd asd asd asd asdasd asdas asd asd as dsd asd asdasd as', 0, 1, '2016-12-16 09:46:42', '2017-02-09 09:00:16'),
	(2, 2, 'Test Event', 'Test Event', '451545465', 'louie@incubixtech.com', 'Test Event', '12', '12/17/2016', '6:00 PM', 'Test Event', '5:45 PM', 'Test Event', 3, 'Test Event Updated', '12/17/2016', '7:00 PM', '15', 'Test Venue Updated', '120000', 'Sample Notes', 'as dasd as dasd asd asdasdasd asd asd asd ', 0, 1, '2016-12-16 09:46:42', '2017-02-09 09:02:18');
/*!40000 ALTER TABLE `woo_bookings` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_diamonds
CREATE TABLE IF NOT EXISTS `woo_diamonds` (
  `diamond_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `color` varchar(50) DEFAULT NULL,
  `value_from` bigint(20) DEFAULT '0',
  `value_to` bigint(20) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`diamond_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_diamonds: ~7 rows (approximately)
DELETE FROM `woo_diamonds`;
/*!40000 ALTER TABLE `woo_diamonds` DISABLE KEYS */;
INSERT INTO `woo_diamonds` (`diamond_id`, `color`, `value_from`, `value_to`, `created_at`, `updated_at`) VALUES
	(1, 'WHITE', 0, 999, '2017-02-01 14:13:49', '2017-02-01 14:13:50'),
	(2, 'GREEN', 1000, 4999, '2017-02-01 14:14:13', '2017-02-01 14:14:14'),
	(3, 'BLUE', 5000, 24999, '2017-02-01 14:14:30', '2017-02-01 14:14:31'),
	(4, 'PURPLE', 25000, 99999, '2017-02-01 14:14:53', '2017-02-01 14:14:56'),
	(5, 'RED', 100000, 249999, '2017-02-01 14:18:12', '2017-02-01 14:18:13'),
	(6, 'ORANGE', 250000, 499999, '2017-02-01 14:18:38', '2017-02-01 14:18:39'),
	(7, 'YELLOW', 500000, 1000000, '2017-02-01 14:19:11', '2017-02-01 14:19:13');
/*!40000 ALTER TABLE `woo_diamonds` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_favorites
CREATE TABLE IF NOT EXISTS `woo_favorites` (
  `favorite_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) NOT NULL DEFAULT '0',
  `favorite_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`favorite_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  CONSTRAINT `FK_favorites_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table woomen.woo_favorites: ~0 rows (approximately)
DELETE FROM `woo_favorites`;
/*!40000 ALTER TABLE `woo_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `woo_favorites` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_items
CREATE TABLE IF NOT EXISTS `woo_items` (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `merchant_profile_id` bigint(20) DEFAULT NULL,
  `artist_profile_id` bigint(20) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` longtext,
  `item_contact` varchar(255) DEFAULT NULL,
  `item_quantity` int(11) DEFAULT '1',
  `item_price` bigint(20) DEFAULT NULL,
  `item_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 - Available, 1 - Sold',
  `clean_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `merchant_profile_id` (`merchant_profile_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  CONSTRAINT `FK_item_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_item_merchant_profile` FOREIGN KEY (`merchant_profile_id`) REFERENCES `woo_merchant_profile` (`merchant_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_items: ~8 rows (approximately)
DELETE FROM `woo_items`;
/*!40000 ALTER TABLE `woo_items` DISABLE KEYS */;
INSERT INTO `woo_items` (`item_id`, `merchant_profile_id`, `artist_profile_id`, `item_name`, `item_description`, `item_contact`, `item_quantity`, `item_price`, `item_status`, `clean_url`, `created_at`, `updated_at`) VALUES
	(6, NULL, 2, 'Asus Laptop', '<p><strong>2313rewrwe vrwerwer3242432</strong></p>', '2131313123', 1, 35000, 0, NULL, '2016-12-27 11:26:12', NULL),
	(7, NULL, 2, 'Aomine', '<p>sad saasdasdasdasdasdas dasd <strong>BOLD</strong></p>', NULL, 1, 121212, 0, NULL, '2017-01-03 03:43:37', '2017-01-13 10:21:40'),
	(8, NULL, 2, 'Laptop Qwertyuiop', '<p>sample sample sample</p>', '121212121212', 1, 100000, 0, NULL, '2017-01-06 08:07:42', NULL),
	(9, 2, NULL, 'Lipstick Lipstick Lipstick Lipstick Lipstick', '<p><span style="color: #222222; font-family: Consolas, \'Lucida Console\', \'Courier New\', monospace; font-size: 12px; white-space: pre-wrap;">Formulated to shade, define and showcase the lips. Hundreds of hues, high-fashion textures. The iconic product that made M&middot;A&middot;C famous.</span></p>', '09091234567', 1, 12000, 0, NULL, '2017-01-09 05:52:34', NULL),
	(10, 1, NULL, 'ilong', '<p><em>sample</em> <strong>description</strong></p>', NULL, 1, 1222, 0, NULL, '2017-01-12 06:01:14', NULL),
	(11, NULL, 2, 'Desktop', '<p>Sample <strong>description</strong></p>', NULL, 2, 10000, 0, NULL, '2017-01-12 11:34:17', NULL),
	(12, NULL, 2, 'Donna B.', '<p><strong>Donna</strong> B.</p>', NULL, 1, 100000, 0, NULL, '2017-01-13 09:53:19', NULL),
	(14, 1, NULL, 'frontend dev', '<p><em>asda asd asasdasd asd asdasd&nbsp;</em></p>', NULL, 1, 20000, 0, NULL, '2017-01-25 09:14:51', '2017-01-25 09:18:30');
/*!40000 ALTER TABLE `woo_items` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_item_images
CREATE TABLE IF NOT EXISTS `woo_item_images` (
  `item_image_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`item_image_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `FK_item_images_items` FOREIGN KEY (`item_id`) REFERENCES `woo_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_item_images: ~9 rows (approximately)
DELETE FROM `woo_item_images`;
/*!40000 ALTER TABLE `woo_item_images` DISABLE KEYS */;
INSERT INTO `woo_item_images` (`item_image_id`, `item_id`, `image_path`, `created_at`, `updated_at`) VALUES
	(8, 6, '6/7XZMWuTDCabn2oC.jpg', '2016-12-27 11:26:12', NULL),
	(10, 8, '8/qjweELFUXON4.jpg', '2017-01-06 08:07:42', NULL),
	(11, 9, '9/EzcwdMTd7sArLDyQc7.png', '2017-01-09 05:52:35', NULL),
	(12, 10, '10/cWKv1L1V1gMhGi6.png', '2017-01-12 06:01:14', NULL),
	(13, 11, '11/Eh0fOPAirH4N.png', '2017-01-12 11:34:17', NULL),
	(15, 12, '12/j2D3mPTKqOpxVPu9v.jpg', '2017-01-13 09:53:19', NULL),
	(16, 12, '12/G2YL5UsM5Wxr1bL.jpg', '2017-01-13 09:53:19', NULL),
	(17, 7, '7/CUxj91u1Po8NCWzZ.jpg', NULL, '2017-01-13 10:21:40'),
	(18, 7, '7/pm4oTqSY829M.jpg', NULL, '2017-01-13 10:21:40'),
	(20, 14, '14/r2aGE4te006f.jpg', NULL, '2017-01-25 09:18:30');
/*!40000 ALTER TABLE `woo_item_images` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_jobs
CREATE TABLE IF NOT EXISTS `woo_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table woomen.woo_jobs: ~0 rows (approximately)
DELETE FROM `woo_jobs`;
/*!40000 ALTER TABLE `woo_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `woo_jobs` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_merchant_profile
CREATE TABLE IF NOT EXISTS `woo_merchant_profile` (
  `merchant_profile_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `is_featured` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`merchant_profile_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `FK_merchant_profile_user` FOREIGN KEY (`user_id`) REFERENCES `woo_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_merchant_profile: ~2 rows (approximately)
DELETE FROM `woo_merchant_profile`;
/*!40000 ALTER TABLE `woo_merchant_profile` DISABLE KEYS */;
INSERT INTO `woo_merchant_profile` (`merchant_profile_id`, `user_id`, `profile_image`, `first_name`, `middle_name`, `last_name`, `address`, `facebook`, `is_featured`, `created_at`, `updated_at`) VALUES
	(1, 5, '1/35ercu4OhvC.jpg', 'anne', '', 'curtis', 'makati city', NULL, 0, '2016-11-28 05:31:31', '2017-02-10 03:42:55'),
	(2, 8, '2/OOYLnIWVg10.png', 'Jennelyn', 'Alborte', 'Medianista', 'adas', NULL, 1, '2016-12-05 08:57:02', '2017-02-10 03:42:58');
/*!40000 ALTER TABLE `woo_merchant_profile` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_reviews
CREATE TABLE IF NOT EXISTS `woo_reviews` (
  `review_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_profile_id` bigint(20) DEFAULT NULL,
  `merchant_profile_id` bigint(20) DEFAULT NULL,
  `review_name` varchar(255) DEFAULT NULL,
  `review_email` varchar(255) DEFAULT NULL,
  `review_title` varchar(255) DEFAULT NULL,
  `review_description` longtext,
  `review_rating` float DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `artist_profile_id` (`artist_profile_id`),
  KEY `merchant_profile_id` (`merchant_profile_id`),
  CONSTRAINT `FK_review_artist_profile` FOREIGN KEY (`artist_profile_id`) REFERENCES `woo_artist_profile` (`artist_profile_id`) ON DELETE CASCADE,
  CONSTRAINT `FK_review_merchant_profile` FOREIGN KEY (`merchant_profile_id`) REFERENCES `woo_merchant_profile` (`merchant_profile_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_reviews: ~0 rows (approximately)
DELETE FROM `woo_reviews`;
/*!40000 ALTER TABLE `woo_reviews` DISABLE KEYS */;
INSERT INTO `woo_reviews` (`review_id`, `artist_profile_id`, `merchant_profile_id`, `review_name`, `review_email`, `review_title`, `review_description`, `review_rating`, `created_at`, `updated_at`) VALUES
	(12, 6, NULL, 'Louie Pogi', 'louie.pogi@gmail.com', 'Pogi Ako', 'Mas Pogi Ako Sayo', 4, '2017-01-09 12:01:03', NULL),
	(13, 2, NULL, 'Sample Neym', 'sampleneym@gmail.com', 'Sample Taytel', 'Sample Description', 4, '2017-01-31 06:36:40', '2017-01-31 06:36:40');
/*!40000 ALTER TABLE `woo_reviews` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_roles
CREATE TABLE IF NOT EXISTS `woo_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_roles: ~3 rows (approximately)
DELETE FROM `woo_roles`;
/*!40000 ALTER TABLE `woo_roles` DISABLE KEYS */;
INSERT INTO `woo_roles` (`role_id`, `role`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'Manage All Users', '2016-11-18 11:00:55', '2016-11-18 11:00:57'),
	(2, 'Artist', 'Make-up Artist', '2016-11-18 11:01:29', '2016-11-18 11:01:30'),
	(3, 'Merchant', 'Post Items in Trade module', '2016-11-18 11:02:09', '2016-11-18 11:02:11');
/*!40000 ALTER TABLE `woo_roles` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_specialties
CREATE TABLE IF NOT EXISTS `woo_specialties` (
  `specialty_id` int(11) NOT NULL AUTO_INCREMENT,
  `specialty_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`specialty_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_specialties: ~8 rows (approximately)
DELETE FROM `woo_specialties`;
/*!40000 ALTER TABLE `woo_specialties` DISABLE KEYS */;
INSERT INTO `woo_specialties` (`specialty_id`, `specialty_name`, `created_at`, `updated_at`) VALUES
	(1, 'Artistic / Special Effects', '2016-11-18 11:38:15', '2016-11-18 11:38:17'),
	(2, 'Bridal', '2016-11-18 11:38:28', '2016-11-18 11:38:29'),
	(3, 'Beauty / Special Occasions', '2016-11-18 11:38:48', '2016-11-18 11:38:49'),
	(4, 'Fashion / Creative / Magazine', '2016-11-18 11:39:10', '2016-11-18 11:39:12'),
	(5, 'TV / Films', '2016-11-18 11:39:27', '2016-11-18 11:39:29'),
	(6, 'Theatre / Performance / Pageantry', '2016-11-18 11:39:58', '2016-11-18 11:39:59'),
	(7, 'Hairstylist', '2016-11-18 11:40:24', '2016-11-18 11:40:26'),
	(8, 'Eyebrow / Broidery', '2017-01-24 10:54:23', '2017-01-24 10:54:24');
/*!40000 ALTER TABLE `woo_specialties` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_updates
CREATE TABLE IF NOT EXISTS `woo_updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_title` varchar(255) DEFAULT NULL,
  `update_desc` varchar(255) DEFAULT NULL,
  `update_expire` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table woomen.woo_updates: ~0 rows (approximately)
DELETE FROM `woo_updates`;
/*!40000 ALTER TABLE `woo_updates` DISABLE KEYS */;
INSERT INTO `woo_updates` (`update_id`, `update_title`, `update_desc`, `update_expire`, `created_at`, `updated_at`) VALUES
	(2, 'Test Update', 'Eius possit facilisi ne eos. Eu posse urbanitas sea. Cum ea percipit pertinacia, stet copiosae ea vis. Ius cu graece placerat, ex eos movet officiis accusamus. Id duis hendrerit eam. Eu pri delicata perpetua, ea pro assum alienum sententiae, brute sanctus', '2016-12-20 00:00:00', '2016-12-16 05:36:57', '2016-12-16 05:36:57');
/*!40000 ALTER TABLE `woo_updates` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_user
CREATE TABLE IF NOT EXISTS `woo_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL COMMENT 'mapped to table woo_roles',
  `status` int(11) DEFAULT '0' COMMENT '0 - Not Active, 1 - Active, 2 - Suspended',
  `password_recover` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `FK_user_roles` FOREIGN KEY (`role_id`) REFERENCES `woo_roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_user: ~9 rows (approximately)
DELETE FROM `woo_user`;
/*!40000 ALTER TABLE `woo_user` DISABLE KEYS */;
INSERT INTO `woo_user` (`id`, `email`, `password`, `activation_code`, `role_id`, `status`, `password_recover`, `created_at`, `updated_at`, `remember_token`) VALUES
	(2, 'kevin@incubixtech.com', '$2y$10$8n9qmkuyB8ZWhfTSzVPQZ.2ZzD24WBEhsOV52hBiQn89xCFnPIj4.', '93274Q85', 2, 1, 0, '2016-11-28 05:25:18', '2017-02-10 10:36:59', '0YfYrNqCEabTtzubDVamMCtioZv5BZl6aaVAgmOTs7cxANj6BjzRv9cP1gLL'),
	(4, 'carmela@incubixtech.com', '$2y$10$whzY2koRJ7UMx14oTpfol.Ig7kIrRL.W8VNE7i.QPVm6Z8yU6qk5S', '0858j916', 2, 1, 0, '2016-11-28 05:28:58', '2017-02-06 11:28:54', NULL),
	(5, 'pat@incubixtech.com', '$2y$10$8n9qmkuyB8ZWhfTSzVPQZ.2ZzD24WBEhsOV52hBiQn89xCFnPIj4.', '113O52258', 3, 1, 0, '2016-11-28 05:31:31', '2017-01-25 10:30:57', 'hgvCc2ATIYExORhz9FxIFgh6S5J2H6XtI3pSWF2gp0OXjfamfyYEKXIMVvke'),
	(7, 'jen@incubixtech.com', '$2y$10$euWRUa6Yahr7c6iX9pWIJOweYgPfARC.jJFOQjooNXzq20O2ziRl6', '732W50617', 2, 1, 0, '2016-12-05 08:54:25', '2016-12-16 05:09:24', 'KnUfBy5icQAOlP4htnOdeeOzwUGYpAzD6iInE0qRZFOLnOW9ON44UO0sOStb'),
	(8, 'medianistajennelyn@gmail.com', '$2y$10$OO4bjhnasNbseygCEK6PL.UtGE5FKSNMsd4qHOsn4yxojE7qnJEgG', '427883z76', 3, 1, 0, '2016-12-05 08:57:02', '2016-12-15 05:07:01', '8JKYCpgJbFG7ft5TwxHK4SG5luT7TI7RFNDrTAJ2HvMVptJLdKNeA4nO1uqy'),
	(9, 'tristan@incubixtech.com', '$2y$10$hybuaktD/97cvFGDVmCbKej4p6GVsFQkf4yhftDg9s2gsNzzic00G', '7Y327046', 2, 1, 0, '2016-12-05 09:35:55', '2017-01-10 06:43:54', NULL),
	(12, 'admin@incubixtech.com', '$2y$10$Il.d9AK6Ri8aJ9Cpbzjv1uBoUwFxl5aixHYm6T9JGRKY4NaDNh3nm', NULL, 1, 1, 0, NULL, '2017-02-09 07:38:36', 'aYHDfQ2TKTbQXGu3m6hu4DQBoTIJAHSxX8HFwGsEgTDdNxVfhRLvQysLnokW'),
	(13, 'yourmakeupguide16@gmail.com', '$2y$10$2nxlsr2tDOhJOyrtBDnf4uiM8QCmCt6gbFwvI3osH8lvaP5bHNE4u', 'd19073348', 2, 1, 0, '2016-12-16 07:05:58', '2016-12-16 07:07:05', NULL),
	(14, 'stylist.jason@gmail.com', '$2y$10$4B4zzmVuVV3F/ce6wc3IVusQR/Bd18v0VrE9jFapr5Eo2XwiUN2Ri', '2r5339195', 2, 0, 0, '2017-01-16 03:46:00', NULL, NULL);
/*!40000 ALTER TABLE `woo_user` ENABLE KEYS */;

-- Dumping structure for table woomen.woo_user_levels
CREATE TABLE IF NOT EXISTS `woo_user_levels` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) DEFAULT NULL,
  `privileges` longtext,
  `albums` int(11) DEFAULT '0',
  `pictures` int(11) DEFAULT '0',
  `videos` int(11) DEFAULT '0',
  `bookings` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table woomen.woo_user_levels: ~4 rows (approximately)
DELETE FROM `woo_user_levels`;
/*!40000 ALTER TABLE `woo_user_levels` DISABLE KEYS */;
INSERT INTO `woo_user_levels` (`level_id`, `level_name`, `privileges`, `albums`, `pictures`, `videos`, `bookings`, `created_at`, `updated_at`) VALUES
	(1, 'Freemium', 'MUA get to try the privileges of joining WOOmen with a Regular Profile for 6 months for FREE.', 0, 0, 0, 0, '2017-01-27 17:42:03', '2017-01-27 17:42:05'),
	(2, 'Regular Profile (Neophyte Profile)', 'MUA has the chance to upload at least 2 albums with 5 pictures each of their best works and 2 video that they want to show to their clients. They MUA is also entitled to a virtual card that has a value of 20 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and a chance to be a featured artist.', 2, 5, 2, 20, '2017-02-06 13:53:08', '2017-02-06 13:53:10'),
	(3, 'Standard Profile (Archetype Profile)', 'The MUA has the chance to showcase 3 albums with 5 pictures each and 5 videos in their dashboard. They will have discounts on featured events and sponsored by Woomen. They MUA is also entitled to a virtual card that has a value of 50 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and higher chances to be a featured artist.', 3, 5, 5, 50, '2017-02-06 13:53:11', '2017-02-06 13:53:14'),
	(4, 'Premium Profile (Transcendent Profile)', 'MUA has more than 10 pictures and 10 videos that they can showcase on their portfolio. They will have discounts on featured events created or partnered by Woomen. They MUA is also entitled to a virtual card that has a value of 100 bookings. This will enable the client to MUA communication system, rating system, synchronized calendar for MUA and a chance to be a featured artist. They are also entitled to free photoshoot on 5 models or 5 Items (If they are registered in the Marketplace).', 10, 10, 10, 100, '2017-02-06 13:53:16', '2017-02-06 13:53:19');
/*!40000 ALTER TABLE `woo_user_levels` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
